import { V1StorageClassList, V1StorageClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1StorageClassListOptions {
    items: V1StorageClass[];
    metadata?: V1ListMeta;
}
export function createV1StorageClassList(options: V1StorageClassListOptions): V1StorageClassList {
    const resource = new V1StorageClassList();
    resource.apiVersion = "storage.k8s.io/v1";
    resource.kind = "StorageClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
