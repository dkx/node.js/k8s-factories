import { V1alpha1VolumeAttachmentSpec, V1alpha1VolumeAttachmentSource } from "@kubernetes/client-node";
export declare interface V1alpha1VolumeAttachmentSpecOptions {
    attacher: string;
    nodeName: string;
    source: V1alpha1VolumeAttachmentSource;
}
export function createV1alpha1VolumeAttachmentSpec(options: V1alpha1VolumeAttachmentSpecOptions): V1alpha1VolumeAttachmentSpec {
    const resource = new V1alpha1VolumeAttachmentSpec();
    if (typeof options.attacher !== "undefined") {
        resource.attacher = options.attacher;
    }
    if (typeof options.nodeName !== "undefined") {
        resource.nodeName = options.nodeName;
    }
    if (typeof options.source !== "undefined") {
        resource.source = options.source;
    }
    return resource;
}
