import { V1VolumeProjection, V1ConfigMapProjection, V1DownwardAPIProjection, V1SecretProjection, V1ServiceAccountTokenProjection } from "@kubernetes/client-node";
export declare interface V1VolumeProjectionOptions {
    configMap?: V1ConfigMapProjection;
    downwardAPI?: V1DownwardAPIProjection;
    secret?: V1SecretProjection;
    serviceAccountToken?: V1ServiceAccountTokenProjection;
}
export function createV1VolumeProjection(options: V1VolumeProjectionOptions = {}): V1VolumeProjection {
    const resource = new V1VolumeProjection();
    if (typeof options.configMap !== "undefined") {
        resource.configMap = options.configMap;
    }
    if (typeof options.downwardAPI !== "undefined") {
        resource.downwardAPI = options.downwardAPI;
    }
    if (typeof options.secret !== "undefined") {
        resource.secret = options.secret;
    }
    if (typeof options.serviceAccountToken !== "undefined") {
        resource.serviceAccountToken = options.serviceAccountToken;
    }
    return resource;
}
