import { ExtensionsV1beta1AllowedFlexVolume } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1AllowedFlexVolumeOptions {
    driver: string;
}
export function createExtensionsV1beta1AllowedFlexVolume(options: ExtensionsV1beta1AllowedFlexVolumeOptions): ExtensionsV1beta1AllowedFlexVolume {
    const resource = new ExtensionsV1beta1AllowedFlexVolume();
    if (typeof options.driver !== "undefined") {
        resource.driver = options.driver;
    }
    return resource;
}
