import { V1EnvVar, V1EnvVarSource } from "@kubernetes/client-node";
export declare interface V1EnvVarOptions {
    name: string;
    value?: string;
    valueFrom?: V1EnvVarSource;
}
export function createV1EnvVar(options: V1EnvVarOptions): V1EnvVar {
    const resource = new V1EnvVar();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    if (typeof options.valueFrom !== "undefined") {
        resource.valueFrom = options.valueFrom;
    }
    return resource;
}
