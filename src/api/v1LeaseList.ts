import { V1LeaseList, V1Lease, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1LeaseListOptions {
    items: V1Lease[];
    metadata?: V1ListMeta;
}
export function createV1LeaseList(options: V1LeaseListOptions): V1LeaseList {
    const resource = new V1LeaseList();
    resource.apiVersion = "coordination.k8s.io/v1";
    resource.kind = "LeaseList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
