import { V1VolumeAttachment, V1ObjectMeta, V1VolumeAttachmentSpec, V1VolumeAttachmentStatus } from "@kubernetes/client-node";
export declare interface V1VolumeAttachmentOptions {
    metadata?: V1ObjectMeta;
    spec: V1VolumeAttachmentSpec;
    status?: V1VolumeAttachmentStatus;
}
export function createV1VolumeAttachment(options: V1VolumeAttachmentOptions): V1VolumeAttachment {
    const resource = new V1VolumeAttachment();
    resource.apiVersion = "storage.k8s.io/v1";
    resource.kind = "VolumeAttachment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
