import { V1beta1ReplicaSet, V1ObjectMeta, V1beta1ReplicaSetSpec, V1beta1ReplicaSetStatus } from "@kubernetes/client-node";
export declare interface V1beta1ReplicaSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1ReplicaSetSpec;
    status?: V1beta1ReplicaSetStatus;
}
export function createV1beta1ReplicaSet(options: V1beta1ReplicaSetOptions = {}): V1beta1ReplicaSet {
    const resource = new V1beta1ReplicaSet();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "ReplicaSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
