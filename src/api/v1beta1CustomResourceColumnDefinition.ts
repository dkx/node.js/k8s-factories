import { V1beta1CustomResourceColumnDefinition } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceColumnDefinitionOptions {
    JSONPath: string;
    description?: string;
    format?: string;
    name: string;
    priority?: number;
    type: string;
}
export function createV1beta1CustomResourceColumnDefinition(options: V1beta1CustomResourceColumnDefinitionOptions): V1beta1CustomResourceColumnDefinition {
    const resource = new V1beta1CustomResourceColumnDefinition();
    if (typeof options.JSONPath !== "undefined") {
        resource.JSONPath = options.JSONPath;
    }
    if (typeof options.description !== "undefined") {
        resource.description = options.description;
    }
    if (typeof options.format !== "undefined") {
        resource.format = options.format;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.priority !== "undefined") {
        resource.priority = options.priority;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
