import { V1beta2DaemonSetStatus, V1beta2DaemonSetCondition } from "@kubernetes/client-node";
export declare interface V1beta2DaemonSetStatusOptions {
    collisionCount?: number;
    conditions?: V1beta2DaemonSetCondition[];
    currentNumberScheduled: number;
    desiredNumberScheduled: number;
    numberAvailable?: number;
    numberMisscheduled: number;
    numberReady: number;
    numberUnavailable?: number;
    observedGeneration?: number;
    updatedNumberScheduled?: number;
}
export function createV1beta2DaemonSetStatus(options: V1beta2DaemonSetStatusOptions): V1beta2DaemonSetStatus {
    const resource = new V1beta2DaemonSetStatus();
    if (typeof options.collisionCount !== "undefined") {
        resource.collisionCount = options.collisionCount;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.currentNumberScheduled !== "undefined") {
        resource.currentNumberScheduled = options.currentNumberScheduled;
    }
    if (typeof options.desiredNumberScheduled !== "undefined") {
        resource.desiredNumberScheduled = options.desiredNumberScheduled;
    }
    if (typeof options.numberAvailable !== "undefined") {
        resource.numberAvailable = options.numberAvailable;
    }
    if (typeof options.numberMisscheduled !== "undefined") {
        resource.numberMisscheduled = options.numberMisscheduled;
    }
    if (typeof options.numberReady !== "undefined") {
        resource.numberReady = options.numberReady;
    }
    if (typeof options.numberUnavailable !== "undefined") {
        resource.numberUnavailable = options.numberUnavailable;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.updatedNumberScheduled !== "undefined") {
        resource.updatedNumberScheduled = options.updatedNumberScheduled;
    }
    return resource;
}
