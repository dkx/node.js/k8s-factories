import { V1Capabilities } from "@kubernetes/client-node";
export declare interface V1CapabilitiesOptions {
    add?: string[];
    drop?: string[];
}
export function createV1Capabilities(options: V1CapabilitiesOptions = {}): V1Capabilities {
    const resource = new V1Capabilities();
    if (typeof options.add !== "undefined") {
        resource.add = options.add;
    }
    if (typeof options.drop !== "undefined") {
        resource.drop = options.drop;
    }
    return resource;
}
