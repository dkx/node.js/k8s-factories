import { V1ConfigMapKeySelector } from "@kubernetes/client-node";
export declare interface V1ConfigMapKeySelectorOptions {
    key: string;
    name?: string;
    optional?: boolean;
}
export function createV1ConfigMapKeySelector(options: V1ConfigMapKeySelectorOptions): V1ConfigMapKeySelector {
    const resource = new V1ConfigMapKeySelector();
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
