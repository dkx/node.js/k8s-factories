import { V1NodeSelectorRequirement } from "@kubernetes/client-node";
export declare interface V1NodeSelectorRequirementOptions {
    key: string;
    operator: string;
    values?: string[];
}
export function createV1NodeSelectorRequirement(options: V1NodeSelectorRequirementOptions): V1NodeSelectorRequirement {
    const resource = new V1NodeSelectorRequirement();
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.operator !== "undefined") {
        resource.operator = options.operator;
    }
    if (typeof options.values !== "undefined") {
        resource.values = options.values;
    }
    return resource;
}
