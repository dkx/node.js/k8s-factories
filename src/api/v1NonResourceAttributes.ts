import { V1NonResourceAttributes } from "@kubernetes/client-node";
export declare interface V1NonResourceAttributesOptions {
    path?: string;
    verb?: string;
}
export function createV1NonResourceAttributes(options: V1NonResourceAttributesOptions = {}): V1NonResourceAttributes {
    const resource = new V1NonResourceAttributes();
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.verb !== "undefined") {
        resource.verb = options.verb;
    }
    return resource;
}
