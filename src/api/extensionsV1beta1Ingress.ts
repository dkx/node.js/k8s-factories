import { ExtensionsV1beta1Ingress, V1ObjectMeta, ExtensionsV1beta1IngressSpec, ExtensionsV1beta1IngressStatus } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressOptions {
    metadata?: V1ObjectMeta;
    spec?: ExtensionsV1beta1IngressSpec;
    status?: ExtensionsV1beta1IngressStatus;
}
export function createExtensionsV1beta1Ingress(options: ExtensionsV1beta1IngressOptions = {}): ExtensionsV1beta1Ingress {
    const resource = new ExtensionsV1beta1Ingress();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "Ingress";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
