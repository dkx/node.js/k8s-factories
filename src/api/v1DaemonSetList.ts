import { V1DaemonSetList, V1DaemonSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1DaemonSetListOptions {
    items: V1DaemonSet[];
    metadata?: V1ListMeta;
}
export function createV1DaemonSetList(options: V1DaemonSetListOptions): V1DaemonSetList {
    const resource = new V1DaemonSetList();
    resource.apiVersion = "apps/v1";
    resource.kind = "DaemonSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
