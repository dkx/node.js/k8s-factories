import { V1beta1CronJobStatus, V1ObjectReference } from "@kubernetes/client-node";
export declare interface V1beta1CronJobStatusOptions {
    active?: V1ObjectReference[];
    lastScheduleTime?: Date;
}
export function createV1beta1CronJobStatus(options: V1beta1CronJobStatusOptions = {}): V1beta1CronJobStatus {
    const resource = new V1beta1CronJobStatus();
    if (typeof options.active !== "undefined") {
        resource.active = options.active;
    }
    if (typeof options.lastScheduleTime !== "undefined") {
        resource.lastScheduleTime = options.lastScheduleTime;
    }
    return resource;
}
