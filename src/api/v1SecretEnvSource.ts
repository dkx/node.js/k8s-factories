import { V1SecretEnvSource } from "@kubernetes/client-node";
export declare interface V1SecretEnvSourceOptions {
    name?: string;
    optional?: boolean;
}
export function createV1SecretEnvSource(options: V1SecretEnvSourceOptions = {}): V1SecretEnvSource {
    const resource = new V1SecretEnvSource();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
