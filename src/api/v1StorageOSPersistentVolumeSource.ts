import { V1StorageOSPersistentVolumeSource, V1ObjectReference } from "@kubernetes/client-node";
export declare interface V1StorageOSPersistentVolumeSourceOptions {
    fsType?: string;
    readOnly?: boolean;
    secretRef?: V1ObjectReference;
    volumeName?: string;
    volumeNamespace?: string;
}
export function createV1StorageOSPersistentVolumeSource(options: V1StorageOSPersistentVolumeSourceOptions = {}): V1StorageOSPersistentVolumeSource {
    const resource = new V1StorageOSPersistentVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.volumeName !== "undefined") {
        resource.volumeName = options.volumeName;
    }
    if (typeof options.volumeNamespace !== "undefined") {
        resource.volumeNamespace = options.volumeNamespace;
    }
    return resource;
}
