import { V1ReplicationControllerSpec, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface V1ReplicationControllerSpecOptions {
    minReadySeconds?: number;
    replicas?: number;
    selector?: {
        [key: string]: string;
    };
    template?: V1PodTemplateSpec;
}
export function createV1ReplicationControllerSpec(options: V1ReplicationControllerSpecOptions = {}): V1ReplicationControllerSpec {
    const resource = new V1ReplicationControllerSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    return resource;
}
