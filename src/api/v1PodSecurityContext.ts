import { V1PodSecurityContext, V1SELinuxOptions, V1Sysctl, V1WindowsSecurityContextOptions } from "@kubernetes/client-node";
export declare interface V1PodSecurityContextOptions {
    fsGroup?: number;
    runAsGroup?: number;
    runAsNonRoot?: boolean;
    runAsUser?: number;
    seLinuxOptions?: V1SELinuxOptions;
    supplementalGroups?: number[];
    sysctls?: V1Sysctl[];
    windowsOptions?: V1WindowsSecurityContextOptions;
}
export function createV1PodSecurityContext(options: V1PodSecurityContextOptions = {}): V1PodSecurityContext {
    const resource = new V1PodSecurityContext();
    if (typeof options.fsGroup !== "undefined") {
        resource.fsGroup = options.fsGroup;
    }
    if (typeof options.runAsGroup !== "undefined") {
        resource.runAsGroup = options.runAsGroup;
    }
    if (typeof options.runAsNonRoot !== "undefined") {
        resource.runAsNonRoot = options.runAsNonRoot;
    }
    if (typeof options.runAsUser !== "undefined") {
        resource.runAsUser = options.runAsUser;
    }
    if (typeof options.seLinuxOptions !== "undefined") {
        resource.seLinuxOptions = options.seLinuxOptions;
    }
    if (typeof options.supplementalGroups !== "undefined") {
        resource.supplementalGroups = options.supplementalGroups;
    }
    if (typeof options.sysctls !== "undefined") {
        resource.sysctls = options.sysctls;
    }
    if (typeof options.windowsOptions !== "undefined") {
        resource.windowsOptions = options.windowsOptions;
    }
    return resource;
}
