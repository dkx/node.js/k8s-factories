import { V1Subject } from "@kubernetes/client-node";
export declare interface V1SubjectOptions {
    apiGroup?: string;
    kind: string;
    name: string;
    namespace?: string;
}
export function createV1Subject(options: V1SubjectOptions): V1Subject {
    const resource = new V1Subject();
    if (typeof options.apiGroup !== "undefined") {
        resource.apiGroup = options.apiGroup;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    return resource;
}
