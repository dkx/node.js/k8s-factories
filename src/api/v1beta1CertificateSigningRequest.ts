import { V1beta1CertificateSigningRequest, V1ObjectMeta, V1beta1CertificateSigningRequestSpec, V1beta1CertificateSigningRequestStatus } from "@kubernetes/client-node";
export declare interface V1beta1CertificateSigningRequestOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1CertificateSigningRequestSpec;
    status?: V1beta1CertificateSigningRequestStatus;
}
export function createV1beta1CertificateSigningRequest(options: V1beta1CertificateSigningRequestOptions = {}): V1beta1CertificateSigningRequest {
    const resource = new V1beta1CertificateSigningRequest();
    resource.apiVersion = "certificates.k8s.io/v1beta1";
    resource.kind = "CertificateSigningRequest";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
