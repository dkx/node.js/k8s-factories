import { V1NodeCondition } from "@kubernetes/client-node";
export declare interface V1NodeConditionOptions {
    lastHeartbeatTime?: Date;
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1NodeCondition(options: V1NodeConditionOptions): V1NodeCondition {
    const resource = new V1NodeCondition();
    if (typeof options.lastHeartbeatTime !== "undefined") {
        resource.lastHeartbeatTime = options.lastHeartbeatTime;
    }
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
