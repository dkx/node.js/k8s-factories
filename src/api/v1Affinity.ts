import { V1Affinity, V1NodeAffinity, V1PodAffinity, V1PodAntiAffinity } from "@kubernetes/client-node";
export declare interface V1AffinityOptions {
    nodeAffinity?: V1NodeAffinity;
    podAffinity?: V1PodAffinity;
    podAntiAffinity?: V1PodAntiAffinity;
}
export function createV1Affinity(options: V1AffinityOptions = {}): V1Affinity {
    const resource = new V1Affinity();
    if (typeof options.nodeAffinity !== "undefined") {
        resource.nodeAffinity = options.nodeAffinity;
    }
    if (typeof options.podAffinity !== "undefined") {
        resource.podAffinity = options.podAffinity;
    }
    if (typeof options.podAntiAffinity !== "undefined") {
        resource.podAntiAffinity = options.podAntiAffinity;
    }
    return resource;
}
