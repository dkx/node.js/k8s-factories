import { V1Initializer } from "@kubernetes/client-node";
export declare interface V1InitializerOptions {
    name: string;
}
export function createV1Initializer(options: V1InitializerOptions): V1Initializer {
    const resource = new V1Initializer();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
