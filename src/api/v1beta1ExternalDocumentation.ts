import { V1beta1ExternalDocumentation } from "@kubernetes/client-node";
export declare interface V1beta1ExternalDocumentationOptions {
    description?: string;
    url?: string;
}
export function createV1beta1ExternalDocumentation(options: V1beta1ExternalDocumentationOptions = {}): V1beta1ExternalDocumentation {
    const resource = new V1beta1ExternalDocumentation();
    if (typeof options.description !== "undefined") {
        resource.description = options.description;
    }
    if (typeof options.url !== "undefined") {
        resource.url = options.url;
    }
    return resource;
}
