import { ExtensionsV1beta1SELinuxStrategyOptions, V1SELinuxOptions } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1SELinuxStrategyOptionsOptions {
    rule: string;
    seLinuxOptions?: V1SELinuxOptions;
}
export function createExtensionsV1beta1SELinuxStrategyOptions(options: ExtensionsV1beta1SELinuxStrategyOptionsOptions): ExtensionsV1beta1SELinuxStrategyOptions {
    const resource = new ExtensionsV1beta1SELinuxStrategyOptions();
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    if (typeof options.seLinuxOptions !== "undefined") {
        resource.seLinuxOptions = options.seLinuxOptions;
    }
    return resource;
}
