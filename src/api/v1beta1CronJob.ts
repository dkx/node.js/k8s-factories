import { V1beta1CronJob, V1ObjectMeta, V1beta1CronJobSpec, V1beta1CronJobStatus } from "@kubernetes/client-node";
export declare interface V1beta1CronJobOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1CronJobSpec;
    status?: V1beta1CronJobStatus;
}
export function createV1beta1CronJob(options: V1beta1CronJobOptions = {}): V1beta1CronJob {
    const resource = new V1beta1CronJob();
    resource.apiVersion = "batch/v1beta1";
    resource.kind = "CronJob";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
