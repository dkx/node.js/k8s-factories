import { NetworkingV1beta1IngressTLS } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressTLSOptions {
    hosts?: string[];
    secretName?: string;
}
export function createNetworkingV1beta1IngressTLS(options: NetworkingV1beta1IngressTLSOptions = {}): NetworkingV1beta1IngressTLS {
    const resource = new NetworkingV1beta1IngressTLS();
    if (typeof options.hosts !== "undefined") {
        resource.hosts = options.hosts;
    }
    if (typeof options.secretName !== "undefined") {
        resource.secretName = options.secretName;
    }
    return resource;
}
