import { V1alpha1PodPresetList, V1alpha1PodPreset, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1PodPresetListOptions {
    items: V1alpha1PodPreset[];
    metadata?: V1ListMeta;
}
export function createV1alpha1PodPresetList(options: V1alpha1PodPresetListOptions): V1alpha1PodPresetList {
    const resource = new V1alpha1PodPresetList();
    resource.apiVersion = "settings.k8s.io/v1alpha1";
    resource.kind = "PodPresetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
