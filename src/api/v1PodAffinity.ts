import { V1PodAffinity, V1WeightedPodAffinityTerm, V1PodAffinityTerm } from "@kubernetes/client-node";
export declare interface V1PodAffinityOptions {
    preferredDuringSchedulingIgnoredDuringExecution?: V1WeightedPodAffinityTerm[];
    requiredDuringSchedulingIgnoredDuringExecution?: V1PodAffinityTerm[];
}
export function createV1PodAffinity(options: V1PodAffinityOptions = {}): V1PodAffinity {
    const resource = new V1PodAffinity();
    if (typeof options.preferredDuringSchedulingIgnoredDuringExecution !== "undefined") {
        resource.preferredDuringSchedulingIgnoredDuringExecution = options.preferredDuringSchedulingIgnoredDuringExecution;
    }
    if (typeof options.requiredDuringSchedulingIgnoredDuringExecution !== "undefined") {
        resource.requiredDuringSchedulingIgnoredDuringExecution = options.requiredDuringSchedulingIgnoredDuringExecution;
    }
    return resource;
}
