import { V2beta2HorizontalPodAutoscaler, V1ObjectMeta, V2beta2HorizontalPodAutoscalerSpec, V2beta2HorizontalPodAutoscalerStatus } from "@kubernetes/client-node";
export declare interface V2beta2HorizontalPodAutoscalerOptions {
    metadata?: V1ObjectMeta;
    spec?: V2beta2HorizontalPodAutoscalerSpec;
    status?: V2beta2HorizontalPodAutoscalerStatus;
}
export function createV2beta2HorizontalPodAutoscaler(options: V2beta2HorizontalPodAutoscalerOptions = {}): V2beta2HorizontalPodAutoscaler {
    const resource = new V2beta2HorizontalPodAutoscaler();
    resource.apiVersion = "autoscaling/v2beta2";
    resource.kind = "HorizontalPodAutoscaler";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
