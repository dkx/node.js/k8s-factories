import { V1ReplicationControllerList, V1ReplicationController, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ReplicationControllerListOptions {
    items: V1ReplicationController[];
    metadata?: V1ListMeta;
}
export function createV1ReplicationControllerList(options: V1ReplicationControllerListOptions): V1ReplicationControllerList {
    const resource = new V1ReplicationControllerList();
    resource.apiVersion = "v1";
    resource.kind = "ReplicationControllerList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
