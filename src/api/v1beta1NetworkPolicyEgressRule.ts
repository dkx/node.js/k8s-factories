import { V1beta1NetworkPolicyEgressRule, V1beta1NetworkPolicyPort, V1beta1NetworkPolicyPeer } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicyEgressRuleOptions {
    ports?: V1beta1NetworkPolicyPort[];
    to?: V1beta1NetworkPolicyPeer[];
}
export function createV1beta1NetworkPolicyEgressRule(options: V1beta1NetworkPolicyEgressRuleOptions = {}): V1beta1NetworkPolicyEgressRule {
    const resource = new V1beta1NetworkPolicyEgressRule();
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    if (typeof options.to !== "undefined") {
        resource.to = options.to;
    }
    return resource;
}
