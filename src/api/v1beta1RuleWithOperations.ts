import { V1beta1RuleWithOperations } from "@kubernetes/client-node";
export declare interface V1beta1RuleWithOperationsOptions {
    apiGroups?: string[];
    apiVersions?: string[];
    operations?: string[];
    resources?: string[];
    scope?: string;
}
export function createV1beta1RuleWithOperations(options: V1beta1RuleWithOperationsOptions = {}): V1beta1RuleWithOperations {
    const resource = new V1beta1RuleWithOperations();
    if (typeof options.apiGroups !== "undefined") {
        resource.apiGroups = options.apiGroups;
    }
    if (typeof options.apiVersions !== "undefined") {
        resource.apiVersions = options.apiVersions;
    }
    if (typeof options.operations !== "undefined") {
        resource.operations = options.operations;
    }
    if (typeof options.resources !== "undefined") {
        resource.resources = options.resources;
    }
    if (typeof options.scope !== "undefined") {
        resource.scope = options.scope;
    }
    return resource;
}
