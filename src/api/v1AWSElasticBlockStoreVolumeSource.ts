import { V1AWSElasticBlockStoreVolumeSource } from "@kubernetes/client-node";
export declare interface V1AWSElasticBlockStoreVolumeSourceOptions {
    fsType?: string;
    partition?: number;
    readOnly?: boolean;
    volumeID: string;
}
export function createV1AWSElasticBlockStoreVolumeSource(options: V1AWSElasticBlockStoreVolumeSourceOptions): V1AWSElasticBlockStoreVolumeSource {
    const resource = new V1AWSElasticBlockStoreVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.partition !== "undefined") {
        resource.partition = options.partition;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.volumeID !== "undefined") {
        resource.volumeID = options.volumeID;
    }
    return resource;
}
