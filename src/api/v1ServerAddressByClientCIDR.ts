import { V1ServerAddressByClientCIDR } from "@kubernetes/client-node";
export declare interface V1ServerAddressByClientCIDROptions {
    clientCIDR: string;
    serverAddress: string;
}
export function createV1ServerAddressByClientCIDR(options: V1ServerAddressByClientCIDROptions): V1ServerAddressByClientCIDR {
    const resource = new V1ServerAddressByClientCIDR();
    if (typeof options.clientCIDR !== "undefined") {
        resource.clientCIDR = options.clientCIDR;
    }
    if (typeof options.serverAddress !== "undefined") {
        resource.serverAddress = options.serverAddress;
    }
    return resource;
}
