import { V1VolumeError } from "@kubernetes/client-node";
export declare interface V1VolumeErrorOptions {
    message?: string;
    time?: Date;
}
export function createV1VolumeError(options: V1VolumeErrorOptions = {}): V1VolumeError {
    const resource = new V1VolumeError();
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.time !== "undefined") {
        resource.time = options.time;
    }
    return resource;
}
