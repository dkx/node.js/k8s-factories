import { V1ContainerStateWaiting } from "@kubernetes/client-node";
export declare interface V1ContainerStateWaitingOptions {
    message?: string;
    reason?: string;
}
export function createV1ContainerStateWaiting(options: V1ContainerStateWaitingOptions = {}): V1ContainerStateWaiting {
    const resource = new V1ContainerStateWaiting();
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    return resource;
}
