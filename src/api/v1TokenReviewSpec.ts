import { V1TokenReviewSpec } from "@kubernetes/client-node";
export declare interface V1TokenReviewSpecOptions {
    audiences?: string[];
    token?: string;
}
export function createV1TokenReviewSpec(options: V1TokenReviewSpecOptions = {}): V1TokenReviewSpec {
    const resource = new V1TokenReviewSpec();
    if (typeof options.audiences !== "undefined") {
        resource.audiences = options.audiences;
    }
    if (typeof options.token !== "undefined") {
        resource.token = options.token;
    }
    return resource;
}
