import { V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ListMetaOptions {
    _continue?: string;
    remainingItemCount?: number;
    resourceVersion?: string;
    selfLink?: string;
}
export function createV1ListMeta(options: V1ListMetaOptions = {}): V1ListMeta {
    const resource = new V1ListMeta();
    if (typeof options._continue !== "undefined") {
        resource._continue = options._continue;
    }
    if (typeof options.remainingItemCount !== "undefined") {
        resource.remainingItemCount = options.remainingItemCount;
    }
    if (typeof options.resourceVersion !== "undefined") {
        resource.resourceVersion = options.resourceVersion;
    }
    if (typeof options.selfLink !== "undefined") {
        resource.selfLink = options.selfLink;
    }
    return resource;
}
