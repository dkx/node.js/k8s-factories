import { V2beta2MetricTarget } from "@kubernetes/client-node";
export declare interface V2beta2MetricTargetOptions {
    averageUtilization?: number;
    averageValue?: string;
    type: string;
    value?: string;
}
export function createV2beta2MetricTarget(options: V2beta2MetricTargetOptions): V2beta2MetricTarget {
    const resource = new V2beta2MetricTarget();
    if (typeof options.averageUtilization !== "undefined") {
        resource.averageUtilization = options.averageUtilization;
    }
    if (typeof options.averageValue !== "undefined") {
        resource.averageValue = options.averageValue;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
