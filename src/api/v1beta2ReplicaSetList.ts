import { V1beta2ReplicaSetList, V1beta2ReplicaSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta2ReplicaSetListOptions {
    items: V1beta2ReplicaSet[];
    metadata?: V1ListMeta;
}
export function createV1beta2ReplicaSetList(options: V1beta2ReplicaSetListOptions): V1beta2ReplicaSetList {
    const resource = new V1beta2ReplicaSetList();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "ReplicaSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
