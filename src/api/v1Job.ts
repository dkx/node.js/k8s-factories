import { V1Job, V1ObjectMeta, V1JobSpec, V1JobStatus } from "@kubernetes/client-node";
export declare interface V1JobOptions {
    metadata?: V1ObjectMeta;
    spec?: V1JobSpec;
    status?: V1JobStatus;
}
export function createV1Job(options: V1JobOptions = {}): V1Job {
    const resource = new V1Job();
    resource.apiVersion = "batch/v1";
    resource.kind = "Job";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
