import { V1EndpointSubset, V1EndpointAddress, V1EndpointPort } from "@kubernetes/client-node";
export declare interface V1EndpointSubsetOptions {
    addresses?: V1EndpointAddress[];
    notReadyAddresses?: V1EndpointAddress[];
    ports?: V1EndpointPort[];
}
export function createV1EndpointSubset(options: V1EndpointSubsetOptions = {}): V1EndpointSubset {
    const resource = new V1EndpointSubset();
    if (typeof options.addresses !== "undefined") {
        resource.addresses = options.addresses;
    }
    if (typeof options.notReadyAddresses !== "undefined") {
        resource.notReadyAddresses = options.notReadyAddresses;
    }
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    return resource;
}
