import { V1PhotonPersistentDiskVolumeSource } from "@kubernetes/client-node";
export declare interface V1PhotonPersistentDiskVolumeSourceOptions {
    fsType?: string;
    pdID: string;
}
export function createV1PhotonPersistentDiskVolumeSource(options: V1PhotonPersistentDiskVolumeSourceOptions): V1PhotonPersistentDiskVolumeSource {
    const resource = new V1PhotonPersistentDiskVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.pdID !== "undefined") {
        resource.pdID = options.pdID;
    }
    return resource;
}
