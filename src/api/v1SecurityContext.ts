import { V1SecurityContext, V1Capabilities, V1SELinuxOptions, V1WindowsSecurityContextOptions } from "@kubernetes/client-node";
export declare interface V1SecurityContextOptions {
    allowPrivilegeEscalation?: boolean;
    capabilities?: V1Capabilities;
    privileged?: boolean;
    procMount?: string;
    readOnlyRootFilesystem?: boolean;
    runAsGroup?: number;
    runAsNonRoot?: boolean;
    runAsUser?: number;
    seLinuxOptions?: V1SELinuxOptions;
    windowsOptions?: V1WindowsSecurityContextOptions;
}
export function createV1SecurityContext(options: V1SecurityContextOptions = {}): V1SecurityContext {
    const resource = new V1SecurityContext();
    if (typeof options.allowPrivilegeEscalation !== "undefined") {
        resource.allowPrivilegeEscalation = options.allowPrivilegeEscalation;
    }
    if (typeof options.capabilities !== "undefined") {
        resource.capabilities = options.capabilities;
    }
    if (typeof options.privileged !== "undefined") {
        resource.privileged = options.privileged;
    }
    if (typeof options.procMount !== "undefined") {
        resource.procMount = options.procMount;
    }
    if (typeof options.readOnlyRootFilesystem !== "undefined") {
        resource.readOnlyRootFilesystem = options.readOnlyRootFilesystem;
    }
    if (typeof options.runAsGroup !== "undefined") {
        resource.runAsGroup = options.runAsGroup;
    }
    if (typeof options.runAsNonRoot !== "undefined") {
        resource.runAsNonRoot = options.runAsNonRoot;
    }
    if (typeof options.runAsUser !== "undefined") {
        resource.runAsUser = options.runAsUser;
    }
    if (typeof options.seLinuxOptions !== "undefined") {
        resource.seLinuxOptions = options.seLinuxOptions;
    }
    if (typeof options.windowsOptions !== "undefined") {
        resource.windowsOptions = options.windowsOptions;
    }
    return resource;
}
