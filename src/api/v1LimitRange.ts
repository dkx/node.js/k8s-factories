import { V1LimitRange, V1ObjectMeta, V1LimitRangeSpec } from "@kubernetes/client-node";
export declare interface V1LimitRangeOptions {
    metadata?: V1ObjectMeta;
    spec?: V1LimitRangeSpec;
}
export function createV1LimitRange(options: V1LimitRangeOptions = {}): V1LimitRange {
    const resource = new V1LimitRange();
    resource.apiVersion = "v1";
    resource.kind = "LimitRange";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
