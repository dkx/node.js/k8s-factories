import { V1StatusDetails, V1StatusCause } from "@kubernetes/client-node";
export declare interface V1StatusDetailsOptions {
    causes?: V1StatusCause[];
    group?: string;
    kind?: string;
    name?: string;
    retryAfterSeconds?: number;
    uid?: string;
}
export function createV1StatusDetails(options: V1StatusDetailsOptions = {}): V1StatusDetails {
    const resource = new V1StatusDetails();
    if (typeof options.causes !== "undefined") {
        resource.causes = options.causes;
    }
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.retryAfterSeconds !== "undefined") {
        resource.retryAfterSeconds = options.retryAfterSeconds;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    return resource;
}
