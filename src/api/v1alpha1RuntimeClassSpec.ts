import { V1alpha1RuntimeClassSpec } from "@kubernetes/client-node";
export declare interface V1alpha1RuntimeClassSpecOptions {
    runtimeHandler: string;
}
export function createV1alpha1RuntimeClassSpec(options: V1alpha1RuntimeClassSpecOptions): V1alpha1RuntimeClassSpec {
    const resource = new V1alpha1RuntimeClassSpec();
    if (typeof options.runtimeHandler !== "undefined") {
        resource.runtimeHandler = options.runtimeHandler;
    }
    return resource;
}
