import { V1ContainerImage } from "@kubernetes/client-node";
export declare interface V1ContainerImageOptions {
    names: string[];
    sizeBytes?: number;
}
export function createV1ContainerImage(options: V1ContainerImageOptions): V1ContainerImage {
    const resource = new V1ContainerImage();
    if (typeof options.names !== "undefined") {
        resource.names = options.names;
    }
    if (typeof options.sizeBytes !== "undefined") {
        resource.sizeBytes = options.sizeBytes;
    }
    return resource;
}
