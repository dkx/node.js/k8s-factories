import { ApiextensionsV1beta1WebhookClientConfig, ApiextensionsV1beta1ServiceReference } from "@kubernetes/client-node";
export declare interface ApiextensionsV1beta1WebhookClientConfigOptions {
    caBundle?: string;
    service?: ApiextensionsV1beta1ServiceReference;
    url?: string;
}
export function createApiextensionsV1beta1WebhookClientConfig(options: ApiextensionsV1beta1WebhookClientConfigOptions = {}): ApiextensionsV1beta1WebhookClientConfig {
    const resource = new ApiextensionsV1beta1WebhookClientConfig();
    if (typeof options.caBundle !== "undefined") {
        resource.caBundle = options.caBundle;
    }
    if (typeof options.service !== "undefined") {
        resource.service = options.service;
    }
    if (typeof options.url !== "undefined") {
        resource.url = options.url;
    }
    return resource;
}
