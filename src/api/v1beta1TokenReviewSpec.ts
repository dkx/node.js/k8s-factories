import { V1beta1TokenReviewSpec } from "@kubernetes/client-node";
export declare interface V1beta1TokenReviewSpecOptions {
    audiences?: string[];
    token?: string;
}
export function createV1beta1TokenReviewSpec(options: V1beta1TokenReviewSpecOptions = {}): V1beta1TokenReviewSpec {
    const resource = new V1beta1TokenReviewSpec();
    if (typeof options.audiences !== "undefined") {
        resource.audiences = options.audiences;
    }
    if (typeof options.token !== "undefined") {
        resource.token = options.token;
    }
    return resource;
}
