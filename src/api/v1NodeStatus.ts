import { V1NodeStatus, V1NodeAddress, V1NodeCondition, V1NodeConfigStatus, V1NodeDaemonEndpoints, V1ContainerImage, V1NodeSystemInfo, V1AttachedVolume } from "@kubernetes/client-node";
export declare interface V1NodeStatusOptions {
    addresses?: V1NodeAddress[];
    allocatable?: {
        [key: string]: string;
    };
    capacity?: {
        [key: string]: string;
    };
    conditions?: V1NodeCondition[];
    config?: V1NodeConfigStatus;
    daemonEndpoints?: V1NodeDaemonEndpoints;
    images?: V1ContainerImage[];
    nodeInfo?: V1NodeSystemInfo;
    phase?: string;
    volumesAttached?: V1AttachedVolume[];
    volumesInUse?: string[];
}
export function createV1NodeStatus(options: V1NodeStatusOptions = {}): V1NodeStatus {
    const resource = new V1NodeStatus();
    if (typeof options.addresses !== "undefined") {
        resource.addresses = options.addresses;
    }
    if (typeof options.allocatable !== "undefined") {
        resource.allocatable = options.allocatable;
    }
    if (typeof options.capacity !== "undefined") {
        resource.capacity = options.capacity;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.config !== "undefined") {
        resource.config = options.config;
    }
    if (typeof options.daemonEndpoints !== "undefined") {
        resource.daemonEndpoints = options.daemonEndpoints;
    }
    if (typeof options.images !== "undefined") {
        resource.images = options.images;
    }
    if (typeof options.nodeInfo !== "undefined") {
        resource.nodeInfo = options.nodeInfo;
    }
    if (typeof options.phase !== "undefined") {
        resource.phase = options.phase;
    }
    if (typeof options.volumesAttached !== "undefined") {
        resource.volumesAttached = options.volumesAttached;
    }
    if (typeof options.volumesInUse !== "undefined") {
        resource.volumesInUse = options.volumesInUse;
    }
    return resource;
}
