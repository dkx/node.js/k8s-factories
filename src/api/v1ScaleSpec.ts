import { V1ScaleSpec } from "@kubernetes/client-node";
export declare interface V1ScaleSpecOptions {
    replicas?: number;
}
export function createV1ScaleSpec(options: V1ScaleSpecOptions = {}): V1ScaleSpec {
    const resource = new V1ScaleSpec();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    return resource;
}
