import { ExtensionsV1beta1IngressList, ExtensionsV1beta1Ingress, V1ListMeta } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressListOptions {
    items: ExtensionsV1beta1Ingress[];
    metadata?: V1ListMeta;
}
export function createExtensionsV1beta1IngressList(options: ExtensionsV1beta1IngressListOptions): ExtensionsV1beta1IngressList {
    const resource = new ExtensionsV1beta1IngressList();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "IngressList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
