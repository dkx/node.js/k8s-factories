import { ExtensionsV1beta1IDRange } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IDRangeOptions {
    max: number;
    min: number;
}
export function createExtensionsV1beta1IDRange(options: ExtensionsV1beta1IDRangeOptions): ExtensionsV1beta1IDRange {
    const resource = new ExtensionsV1beta1IDRange();
    if (typeof options.max !== "undefined") {
        resource.max = options.max;
    }
    if (typeof options.min !== "undefined") {
        resource.min = options.min;
    }
    return resource;
}
