import { V1beta2StatefulSetSpec, V1LabelSelector, V1PodTemplateSpec, V1beta2StatefulSetUpdateStrategy, V1PersistentVolumeClaim } from "@kubernetes/client-node";
export declare interface V1beta2StatefulSetSpecOptions {
    podManagementPolicy?: string;
    replicas?: number;
    revisionHistoryLimit?: number;
    selector: V1LabelSelector;
    serviceName: string;
    template: V1PodTemplateSpec;
    updateStrategy?: V1beta2StatefulSetUpdateStrategy;
    volumeClaimTemplates?: V1PersistentVolumeClaim[];
}
export function createV1beta2StatefulSetSpec(options: V1beta2StatefulSetSpecOptions): V1beta2StatefulSetSpec {
    const resource = new V1beta2StatefulSetSpec();
    if (typeof options.podManagementPolicy !== "undefined") {
        resource.podManagementPolicy = options.podManagementPolicy;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.revisionHistoryLimit !== "undefined") {
        resource.revisionHistoryLimit = options.revisionHistoryLimit;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.serviceName !== "undefined") {
        resource.serviceName = options.serviceName;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    if (typeof options.updateStrategy !== "undefined") {
        resource.updateStrategy = options.updateStrategy;
    }
    if (typeof options.volumeClaimTemplates !== "undefined") {
        resource.volumeClaimTemplates = options.volumeClaimTemplates;
    }
    return resource;
}
