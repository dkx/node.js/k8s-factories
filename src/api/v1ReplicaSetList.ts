import { V1ReplicaSetList, V1ReplicaSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ReplicaSetListOptions {
    items: V1ReplicaSet[];
    metadata?: V1ListMeta;
}
export function createV1ReplicaSetList(options: V1ReplicaSetListOptions): V1ReplicaSetList {
    const resource = new V1ReplicaSetList();
    resource.apiVersion = "apps/v1";
    resource.kind = "ReplicaSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
