import { V1NetworkPolicyPort } from "@kubernetes/client-node";
export declare interface V1NetworkPolicyPortOptions {
    port?: object;
    protocol?: string;
}
export function createV1NetworkPolicyPort(options: V1NetworkPolicyPortOptions = {}): V1NetworkPolicyPort {
    const resource = new V1NetworkPolicyPort();
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    if (typeof options.protocol !== "undefined") {
        resource.protocol = options.protocol;
    }
    return resource;
}
