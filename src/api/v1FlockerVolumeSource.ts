import { V1FlockerVolumeSource } from "@kubernetes/client-node";
export declare interface V1FlockerVolumeSourceOptions {
    datasetName?: string;
    datasetUUID?: string;
}
export function createV1FlockerVolumeSource(options: V1FlockerVolumeSourceOptions = {}): V1FlockerVolumeSource {
    const resource = new V1FlockerVolumeSource();
    if (typeof options.datasetName !== "undefined") {
        resource.datasetName = options.datasetName;
    }
    if (typeof options.datasetUUID !== "undefined") {
        resource.datasetUUID = options.datasetUUID;
    }
    return resource;
}
