import { V2alpha1CronJobStatus, V1ObjectReference } from "@kubernetes/client-node";
export declare interface V2alpha1CronJobStatusOptions {
    active?: V1ObjectReference[];
    lastScheduleTime?: Date;
}
export function createV2alpha1CronJobStatus(options: V2alpha1CronJobStatusOptions = {}): V2alpha1CronJobStatus {
    const resource = new V2alpha1CronJobStatus();
    if (typeof options.active !== "undefined") {
        resource.active = options.active;
    }
    if (typeof options.lastScheduleTime !== "undefined") {
        resource.lastScheduleTime = options.lastScheduleTime;
    }
    return resource;
}
