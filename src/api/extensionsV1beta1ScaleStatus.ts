import { ExtensionsV1beta1ScaleStatus } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1ScaleStatusOptions {
    replicas: number;
    selector?: {
        [key: string]: string;
    };
    targetSelector?: string;
}
export function createExtensionsV1beta1ScaleStatus(options: ExtensionsV1beta1ScaleStatusOptions): ExtensionsV1beta1ScaleStatus {
    const resource = new ExtensionsV1beta1ScaleStatus();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.targetSelector !== "undefined") {
        resource.targetSelector = options.targetSelector;
    }
    return resource;
}
