import { AppsV1beta1DeploymentList, AppsV1beta1Deployment, V1ListMeta } from "@kubernetes/client-node";
export declare interface AppsV1beta1DeploymentListOptions {
    items: AppsV1beta1Deployment[];
    metadata?: V1ListMeta;
}
export function createAppsV1beta1DeploymentList(options: AppsV1beta1DeploymentListOptions): AppsV1beta1DeploymentList {
    const resource = new AppsV1beta1DeploymentList();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "DeploymentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
