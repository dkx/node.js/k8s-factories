import { V1ClusterRoleBindingList, V1ClusterRoleBinding, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ClusterRoleBindingListOptions {
    items: V1ClusterRoleBinding[];
    metadata?: V1ListMeta;
}
export function createV1ClusterRoleBindingList(options: V1ClusterRoleBindingListOptions): V1ClusterRoleBindingList {
    const resource = new V1ClusterRoleBindingList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "ClusterRoleBindingList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
