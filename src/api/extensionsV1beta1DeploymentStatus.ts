import { ExtensionsV1beta1DeploymentStatus, ExtensionsV1beta1DeploymentCondition } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1DeploymentStatusOptions {
    availableReplicas?: number;
    collisionCount?: number;
    conditions?: ExtensionsV1beta1DeploymentCondition[];
    observedGeneration?: number;
    readyReplicas?: number;
    replicas?: number;
    unavailableReplicas?: number;
    updatedReplicas?: number;
}
export function createExtensionsV1beta1DeploymentStatus(options: ExtensionsV1beta1DeploymentStatusOptions = {}): ExtensionsV1beta1DeploymentStatus {
    const resource = new ExtensionsV1beta1DeploymentStatus();
    if (typeof options.availableReplicas !== "undefined") {
        resource.availableReplicas = options.availableReplicas;
    }
    if (typeof options.collisionCount !== "undefined") {
        resource.collisionCount = options.collisionCount;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.readyReplicas !== "undefined") {
        resource.readyReplicas = options.readyReplicas;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.unavailableReplicas !== "undefined") {
        resource.unavailableReplicas = options.unavailableReplicas;
    }
    if (typeof options.updatedReplicas !== "undefined") {
        resource.updatedReplicas = options.updatedReplicas;
    }
    return resource;
}
