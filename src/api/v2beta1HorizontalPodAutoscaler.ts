import { V2beta1HorizontalPodAutoscaler, V1ObjectMeta, V2beta1HorizontalPodAutoscalerSpec, V2beta1HorizontalPodAutoscalerStatus } from "@kubernetes/client-node";
export declare interface V2beta1HorizontalPodAutoscalerOptions {
    metadata?: V1ObjectMeta;
    spec?: V2beta1HorizontalPodAutoscalerSpec;
    status?: V2beta1HorizontalPodAutoscalerStatus;
}
export function createV2beta1HorizontalPodAutoscaler(options: V2beta1HorizontalPodAutoscalerOptions = {}): V2beta1HorizontalPodAutoscaler {
    const resource = new V2beta1HorizontalPodAutoscaler();
    resource.apiVersion = "autoscaling/v2beta1";
    resource.kind = "HorizontalPodAutoscaler";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
