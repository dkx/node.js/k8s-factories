import { V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1LocalObjectReferenceOptions {
    name?: string;
}
export function createV1LocalObjectReference(options: V1LocalObjectReferenceOptions = {}): V1LocalObjectReference {
    const resource = new V1LocalObjectReference();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
