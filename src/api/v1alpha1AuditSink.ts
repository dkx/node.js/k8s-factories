import { V1alpha1AuditSink, V1ObjectMeta, V1alpha1AuditSinkSpec } from "@kubernetes/client-node";
export declare interface V1alpha1AuditSinkOptions {
    metadata?: V1ObjectMeta;
    spec?: V1alpha1AuditSinkSpec;
}
export function createV1alpha1AuditSink(options: V1alpha1AuditSinkOptions = {}): V1alpha1AuditSink {
    const resource = new V1alpha1AuditSink();
    resource.apiVersion = "auditregistration.k8s.io/v1alpha1";
    resource.kind = "AuditSink";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
