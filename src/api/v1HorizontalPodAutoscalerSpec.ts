import { V1HorizontalPodAutoscalerSpec, V1CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V1HorizontalPodAutoscalerSpecOptions {
    maxReplicas: number;
    minReplicas?: number;
    scaleTargetRef: V1CrossVersionObjectReference;
    targetCPUUtilizationPercentage?: number;
}
export function createV1HorizontalPodAutoscalerSpec(options: V1HorizontalPodAutoscalerSpecOptions): V1HorizontalPodAutoscalerSpec {
    const resource = new V1HorizontalPodAutoscalerSpec();
    if (typeof options.maxReplicas !== "undefined") {
        resource.maxReplicas = options.maxReplicas;
    }
    if (typeof options.minReplicas !== "undefined") {
        resource.minReplicas = options.minReplicas;
    }
    if (typeof options.scaleTargetRef !== "undefined") {
        resource.scaleTargetRef = options.scaleTargetRef;
    }
    if (typeof options.targetCPUUtilizationPercentage !== "undefined") {
        resource.targetCPUUtilizationPercentage = options.targetCPUUtilizationPercentage;
    }
    return resource;
}
