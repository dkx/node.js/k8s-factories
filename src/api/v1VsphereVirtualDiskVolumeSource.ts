import { V1VsphereVirtualDiskVolumeSource } from "@kubernetes/client-node";
export declare interface V1VsphereVirtualDiskVolumeSourceOptions {
    fsType?: string;
    storagePolicyID?: string;
    storagePolicyName?: string;
    volumePath: string;
}
export function createV1VsphereVirtualDiskVolumeSource(options: V1VsphereVirtualDiskVolumeSourceOptions): V1VsphereVirtualDiskVolumeSource {
    const resource = new V1VsphereVirtualDiskVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.storagePolicyID !== "undefined") {
        resource.storagePolicyID = options.storagePolicyID;
    }
    if (typeof options.storagePolicyName !== "undefined") {
        resource.storagePolicyName = options.storagePolicyName;
    }
    if (typeof options.volumePath !== "undefined") {
        resource.volumePath = options.volumePath;
    }
    return resource;
}
