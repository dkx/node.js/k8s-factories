import { V1beta1ClusterRoleList, V1beta1ClusterRole, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1ClusterRoleListOptions {
    items: V1beta1ClusterRole[];
    metadata?: V1ListMeta;
}
export function createV1beta1ClusterRoleList(options: V1beta1ClusterRoleListOptions): V1beta1ClusterRoleList {
    const resource = new V1beta1ClusterRoleList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "ClusterRoleList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
