import { PolicyV1beta1AllowedFlexVolume } from "@kubernetes/client-node";
export declare interface PolicyV1beta1AllowedFlexVolumeOptions {
    driver: string;
}
export function createPolicyV1beta1AllowedFlexVolume(options: PolicyV1beta1AllowedFlexVolumeOptions): PolicyV1beta1AllowedFlexVolume {
    const resource = new PolicyV1beta1AllowedFlexVolume();
    if (typeof options.driver !== "undefined") {
        resource.driver = options.driver;
    }
    return resource;
}
