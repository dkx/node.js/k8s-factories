import { V1GCEPersistentDiskVolumeSource } from "@kubernetes/client-node";
export declare interface V1GCEPersistentDiskVolumeSourceOptions {
    fsType?: string;
    partition?: number;
    pdName: string;
    readOnly?: boolean;
}
export function createV1GCEPersistentDiskVolumeSource(options: V1GCEPersistentDiskVolumeSourceOptions): V1GCEPersistentDiskVolumeSource {
    const resource = new V1GCEPersistentDiskVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.partition !== "undefined") {
        resource.partition = options.partition;
    }
    if (typeof options.pdName !== "undefined") {
        resource.pdName = options.pdName;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
