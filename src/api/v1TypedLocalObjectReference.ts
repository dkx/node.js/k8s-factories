import { V1TypedLocalObjectReference } from "@kubernetes/client-node";
export declare interface V1TypedLocalObjectReferenceOptions {
    apiGroup?: string;
    kind: string;
    name: string;
}
export function createV1TypedLocalObjectReference(options: V1TypedLocalObjectReferenceOptions): V1TypedLocalObjectReference {
    const resource = new V1TypedLocalObjectReference();
    if (typeof options.apiGroup !== "undefined") {
        resource.apiGroup = options.apiGroup;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
