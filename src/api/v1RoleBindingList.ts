import { V1RoleBindingList, V1RoleBinding, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1RoleBindingListOptions {
    items: V1RoleBinding[];
    metadata?: V1ListMeta;
}
export function createV1RoleBindingList(options: V1RoleBindingListOptions): V1RoleBindingList {
    const resource = new V1RoleBindingList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "RoleBindingList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
