import { V1alpha1VolumeError } from "@kubernetes/client-node";
export declare interface V1alpha1VolumeErrorOptions {
    message?: string;
    time?: Date;
}
export function createV1alpha1VolumeError(options: V1alpha1VolumeErrorOptions = {}): V1alpha1VolumeError {
    const resource = new V1alpha1VolumeError();
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.time !== "undefined") {
        resource.time = options.time;
    }
    return resource;
}
