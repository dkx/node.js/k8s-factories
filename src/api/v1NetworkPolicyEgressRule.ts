import { V1NetworkPolicyEgressRule, V1NetworkPolicyPort, V1NetworkPolicyPeer } from "@kubernetes/client-node";
export declare interface V1NetworkPolicyEgressRuleOptions {
    ports?: V1NetworkPolicyPort[];
    to?: V1NetworkPolicyPeer[];
}
export function createV1NetworkPolicyEgressRule(options: V1NetworkPolicyEgressRuleOptions = {}): V1NetworkPolicyEgressRule {
    const resource = new V1NetworkPolicyEgressRule();
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    if (typeof options.to !== "undefined") {
        resource.to = options.to;
    }
    return resource;
}
