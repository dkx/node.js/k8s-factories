import { V1beta1ClusterRoleBindingList, V1beta1ClusterRoleBinding, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1ClusterRoleBindingListOptions {
    items: V1beta1ClusterRoleBinding[];
    metadata?: V1ListMeta;
}
export function createV1beta1ClusterRoleBindingList(options: V1beta1ClusterRoleBindingListOptions): V1beta1ClusterRoleBindingList {
    const resource = new V1beta1ClusterRoleBindingList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "ClusterRoleBindingList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
