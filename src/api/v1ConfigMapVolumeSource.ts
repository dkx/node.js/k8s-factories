import { V1ConfigMapVolumeSource, V1KeyToPath } from "@kubernetes/client-node";
export declare interface V1ConfigMapVolumeSourceOptions {
    defaultMode?: number;
    items?: V1KeyToPath[];
    name?: string;
    optional?: boolean;
}
export function createV1ConfigMapVolumeSource(options: V1ConfigMapVolumeSourceOptions = {}): V1ConfigMapVolumeSource {
    const resource = new V1ConfigMapVolumeSource();
    if (typeof options.defaultMode !== "undefined") {
        resource.defaultMode = options.defaultMode;
    }
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
