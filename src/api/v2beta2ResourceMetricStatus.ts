import { V2beta2ResourceMetricStatus, V2beta2MetricValueStatus } from "@kubernetes/client-node";
export declare interface V2beta2ResourceMetricStatusOptions {
    current: V2beta2MetricValueStatus;
    name: string;
}
export function createV2beta2ResourceMetricStatus(options: V2beta2ResourceMetricStatusOptions): V2beta2ResourceMetricStatus {
    const resource = new V2beta2ResourceMetricStatus();
    if (typeof options.current !== "undefined") {
        resource.current = options.current;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
