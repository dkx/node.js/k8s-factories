import { V1beta1NetworkPolicyIngressRule, V1beta1NetworkPolicyPeer, V1beta1NetworkPolicyPort } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicyIngressRuleOptions {
    from?: V1beta1NetworkPolicyPeer[];
    ports?: V1beta1NetworkPolicyPort[];
}
export function createV1beta1NetworkPolicyIngressRule(options: V1beta1NetworkPolicyIngressRuleOptions = {}): V1beta1NetworkPolicyIngressRule {
    const resource = new V1beta1NetworkPolicyIngressRule();
    if (typeof options.from !== "undefined") {
        resource.from = options.from;
    }
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    return resource;
}
