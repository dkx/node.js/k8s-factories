import { V1NetworkPolicy, V1ObjectMeta, V1NetworkPolicySpec } from "@kubernetes/client-node";
export declare interface V1NetworkPolicyOptions {
    metadata?: V1ObjectMeta;
    spec?: V1NetworkPolicySpec;
}
export function createV1NetworkPolicy(options: V1NetworkPolicyOptions = {}): V1NetworkPolicy {
    const resource = new V1NetworkPolicy();
    resource.apiVersion = "networking.k8s.io/v1";
    resource.kind = "NetworkPolicy";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
