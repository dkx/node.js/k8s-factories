import { V1beta1ResourceRule } from "@kubernetes/client-node";
export declare interface V1beta1ResourceRuleOptions {
    apiGroups?: string[];
    resourceNames?: string[];
    resources?: string[];
    verbs: string[];
}
export function createV1beta1ResourceRule(options: V1beta1ResourceRuleOptions): V1beta1ResourceRule {
    const resource = new V1beta1ResourceRule();
    if (typeof options.apiGroups !== "undefined") {
        resource.apiGroups = options.apiGroups;
    }
    if (typeof options.resourceNames !== "undefined") {
        resource.resourceNames = options.resourceNames;
    }
    if (typeof options.resources !== "undefined") {
        resource.resources = options.resources;
    }
    if (typeof options.verbs !== "undefined") {
        resource.verbs = options.verbs;
    }
    return resource;
}
