import { V1LocalVolumeSource } from "@kubernetes/client-node";
export declare interface V1LocalVolumeSourceOptions {
    fsType?: string;
    path: string;
}
export function createV1LocalVolumeSource(options: V1LocalVolumeSourceOptions): V1LocalVolumeSource {
    const resource = new V1LocalVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    return resource;
}
