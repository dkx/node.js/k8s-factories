import { PolicyV1beta1RunAsUserStrategyOptions, PolicyV1beta1IDRange } from "@kubernetes/client-node";
export declare interface PolicyV1beta1RunAsUserStrategyOptionsOptions {
    ranges?: PolicyV1beta1IDRange[];
    rule: string;
}
export function createPolicyV1beta1RunAsUserStrategyOptions(options: PolicyV1beta1RunAsUserStrategyOptionsOptions): PolicyV1beta1RunAsUserStrategyOptions {
    const resource = new PolicyV1beta1RunAsUserStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
