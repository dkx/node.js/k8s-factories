import { V1SessionAffinityConfig, V1ClientIPConfig } from "@kubernetes/client-node";
export declare interface V1SessionAffinityConfigOptions {
    clientIP?: V1ClientIPConfig;
}
export function createV1SessionAffinityConfig(options: V1SessionAffinityConfigOptions = {}): V1SessionAffinityConfig {
    const resource = new V1SessionAffinityConfig();
    if (typeof options.clientIP !== "undefined") {
        resource.clientIP = options.clientIP;
    }
    return resource;
}
