import { V1alpha1AuditSinkList, V1alpha1AuditSink, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1AuditSinkListOptions {
    items: V1alpha1AuditSink[];
    metadata?: V1ListMeta;
}
export function createV1alpha1AuditSinkList(options: V1alpha1AuditSinkListOptions): V1alpha1AuditSinkList {
    const resource = new V1alpha1AuditSinkList();
    resource.apiVersion = "auditregistration.k8s.io/v1alpha1";
    resource.kind = "AuditSinkList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
