import { V2beta2HorizontalPodAutoscalerList, V2beta2HorizontalPodAutoscaler, V1ListMeta } from "@kubernetes/client-node";
export declare interface V2beta2HorizontalPodAutoscalerListOptions {
    items: V2beta2HorizontalPodAutoscaler[];
    metadata?: V1ListMeta;
}
export function createV2beta2HorizontalPodAutoscalerList(options: V2beta2HorizontalPodAutoscalerListOptions): V2beta2HorizontalPodAutoscalerList {
    const resource = new V2beta2HorizontalPodAutoscalerList();
    resource.apiVersion = "autoscaling/v2beta2";
    resource.kind = "HorizontalPodAutoscalerList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
