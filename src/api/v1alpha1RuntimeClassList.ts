import { V1alpha1RuntimeClassList, V1alpha1RuntimeClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1RuntimeClassListOptions {
    items: V1alpha1RuntimeClass[];
    metadata?: V1ListMeta;
}
export function createV1alpha1RuntimeClassList(options: V1alpha1RuntimeClassListOptions): V1alpha1RuntimeClassList {
    const resource = new V1alpha1RuntimeClassList();
    resource.apiVersion = "node.k8s.io/v1alpha1";
    resource.kind = "RuntimeClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
