import { V2beta2ResourceMetricSource, V2beta2MetricTarget } from "@kubernetes/client-node";
export declare interface V2beta2ResourceMetricSourceOptions {
    name: string;
    target: V2beta2MetricTarget;
}
export function createV2beta2ResourceMetricSource(options: V2beta2ResourceMetricSourceOptions): V2beta2ResourceMetricSource {
    const resource = new V2beta2ResourceMetricSource();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    return resource;
}
