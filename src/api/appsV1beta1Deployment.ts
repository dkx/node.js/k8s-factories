import { AppsV1beta1Deployment, V1ObjectMeta, AppsV1beta1DeploymentSpec, AppsV1beta1DeploymentStatus } from "@kubernetes/client-node";
export declare interface AppsV1beta1DeploymentOptions {
    metadata?: V1ObjectMeta;
    spec?: AppsV1beta1DeploymentSpec;
    status?: AppsV1beta1DeploymentStatus;
}
export function createAppsV1beta1Deployment(options: AppsV1beta1DeploymentOptions = {}): AppsV1beta1Deployment {
    const resource = new AppsV1beta1Deployment();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "Deployment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
