import { V1beta1SelfSubjectAccessReview, V1ObjectMeta, V1beta1SelfSubjectAccessReviewSpec, V1beta1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1beta1SelfSubjectAccessReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1SelfSubjectAccessReviewSpec;
    status?: V1beta1SubjectAccessReviewStatus;
}
export function createV1beta1SelfSubjectAccessReview(options: V1beta1SelfSubjectAccessReviewOptions): V1beta1SelfSubjectAccessReview {
    const resource = new V1beta1SelfSubjectAccessReview();
    resource.apiVersion = "authorization.k8s.io/v1beta1";
    resource.kind = "SelfSubjectAccessReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
