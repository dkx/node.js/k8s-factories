import { V1HTTPHeader } from "@kubernetes/client-node";
export declare interface V1HTTPHeaderOptions {
    name: string;
    value: string;
}
export function createV1HTTPHeader(options: V1HTTPHeaderOptions): V1HTTPHeader {
    const resource = new V1HTTPHeader();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
