import { V1beta2DaemonSetUpdateStrategy, V1beta2RollingUpdateDaemonSet } from "@kubernetes/client-node";
export declare interface V1beta2DaemonSetUpdateStrategyOptions {
    rollingUpdate?: V1beta2RollingUpdateDaemonSet;
    type?: string;
}
export function createV1beta2DaemonSetUpdateStrategy(options: V1beta2DaemonSetUpdateStrategyOptions = {}): V1beta2DaemonSetUpdateStrategy {
    const resource = new V1beta2DaemonSetUpdateStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
