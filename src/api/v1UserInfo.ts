import { V1UserInfo } from "@kubernetes/client-node";
export declare interface V1UserInfoOptions {
    extra?: {
        [key: string]: string[];
    };
    groups?: string[];
    uid?: string;
    username?: string;
}
export function createV1UserInfo(options: V1UserInfoOptions = {}): V1UserInfo {
    const resource = new V1UserInfo();
    if (typeof options.extra !== "undefined") {
        resource.extra = options.extra;
    }
    if (typeof options.groups !== "undefined") {
        resource.groups = options.groups;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    if (typeof options.username !== "undefined") {
        resource.username = options.username;
    }
    return resource;
}
