import { ExtensionsV1beta1IngressRule, ExtensionsV1beta1HTTPIngressRuleValue } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressRuleOptions {
    host?: string;
    http?: ExtensionsV1beta1HTTPIngressRuleValue;
}
export function createExtensionsV1beta1IngressRule(options: ExtensionsV1beta1IngressRuleOptions = {}): ExtensionsV1beta1IngressRule {
    const resource = new ExtensionsV1beta1IngressRule();
    if (typeof options.host !== "undefined") {
        resource.host = options.host;
    }
    if (typeof options.http !== "undefined") {
        resource.http = options.http;
    }
    return resource;
}
