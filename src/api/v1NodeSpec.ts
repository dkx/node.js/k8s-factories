import { V1NodeSpec, V1NodeConfigSource, V1Taint } from "@kubernetes/client-node";
export declare interface V1NodeSpecOptions {
    configSource?: V1NodeConfigSource;
    externalID?: string;
    podCIDR?: string;
    providerID?: string;
    taints?: V1Taint[];
    unschedulable?: boolean;
}
export function createV1NodeSpec(options: V1NodeSpecOptions = {}): V1NodeSpec {
    const resource = new V1NodeSpec();
    if (typeof options.configSource !== "undefined") {
        resource.configSource = options.configSource;
    }
    if (typeof options.externalID !== "undefined") {
        resource.externalID = options.externalID;
    }
    if (typeof options.podCIDR !== "undefined") {
        resource.podCIDR = options.podCIDR;
    }
    if (typeof options.providerID !== "undefined") {
        resource.providerID = options.providerID;
    }
    if (typeof options.taints !== "undefined") {
        resource.taints = options.taints;
    }
    if (typeof options.unschedulable !== "undefined") {
        resource.unschedulable = options.unschedulable;
    }
    return resource;
}
