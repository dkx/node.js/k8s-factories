import { V1StatefulSet, V1ObjectMeta, V1StatefulSetSpec, V1StatefulSetStatus } from "@kubernetes/client-node";
export declare interface V1StatefulSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1StatefulSetSpec;
    status?: V1StatefulSetStatus;
}
export function createV1StatefulSet(options: V1StatefulSetOptions = {}): V1StatefulSet {
    const resource = new V1StatefulSet();
    resource.apiVersion = "apps/v1";
    resource.kind = "StatefulSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
