import { V1RBDVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1RBDVolumeSourceOptions {
    fsType?: string;
    image: string;
    keyring?: string;
    monitors: string[];
    pool?: string;
    readOnly?: boolean;
    secretRef?: V1LocalObjectReference;
    user?: string;
}
export function createV1RBDVolumeSource(options: V1RBDVolumeSourceOptions): V1RBDVolumeSource {
    const resource = new V1RBDVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.image !== "undefined") {
        resource.image = options.image;
    }
    if (typeof options.keyring !== "undefined") {
        resource.keyring = options.keyring;
    }
    if (typeof options.monitors !== "undefined") {
        resource.monitors = options.monitors;
    }
    if (typeof options.pool !== "undefined") {
        resource.pool = options.pool;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
