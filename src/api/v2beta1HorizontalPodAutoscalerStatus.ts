import { V2beta1HorizontalPodAutoscalerStatus, V2beta1HorizontalPodAutoscalerCondition, V2beta1MetricStatus } from "@kubernetes/client-node";
export declare interface V2beta1HorizontalPodAutoscalerStatusOptions {
    conditions: V2beta1HorizontalPodAutoscalerCondition[];
    currentMetrics?: V2beta1MetricStatus[];
    currentReplicas: number;
    desiredReplicas: number;
    lastScaleTime?: Date;
    observedGeneration?: number;
}
export function createV2beta1HorizontalPodAutoscalerStatus(options: V2beta1HorizontalPodAutoscalerStatusOptions): V2beta1HorizontalPodAutoscalerStatus {
    const resource = new V2beta1HorizontalPodAutoscalerStatus();
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.currentMetrics !== "undefined") {
        resource.currentMetrics = options.currentMetrics;
    }
    if (typeof options.currentReplicas !== "undefined") {
        resource.currentReplicas = options.currentReplicas;
    }
    if (typeof options.desiredReplicas !== "undefined") {
        resource.desiredReplicas = options.desiredReplicas;
    }
    if (typeof options.lastScaleTime !== "undefined") {
        resource.lastScaleTime = options.lastScaleTime;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    return resource;
}
