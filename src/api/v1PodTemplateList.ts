import { V1PodTemplateList, V1PodTemplate, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1PodTemplateListOptions {
    items: V1PodTemplate[];
    metadata?: V1ListMeta;
}
export function createV1PodTemplateList(options: V1PodTemplateListOptions): V1PodTemplateList {
    const resource = new V1PodTemplateList();
    resource.apiVersion = "v1";
    resource.kind = "PodTemplateList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
