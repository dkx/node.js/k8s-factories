import { V1alpha1Webhook, V1alpha1WebhookClientConfig, V1alpha1WebhookThrottleConfig } from "@kubernetes/client-node";
export declare interface V1alpha1WebhookOptions {
    clientConfig: V1alpha1WebhookClientConfig;
    throttle?: V1alpha1WebhookThrottleConfig;
}
export function createV1alpha1Webhook(options: V1alpha1WebhookOptions): V1alpha1Webhook {
    const resource = new V1alpha1Webhook();
    if (typeof options.clientConfig !== "undefined") {
        resource.clientConfig = options.clientConfig;
    }
    if (typeof options.throttle !== "undefined") {
        resource.throttle = options.throttle;
    }
    return resource;
}
