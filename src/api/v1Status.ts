import { V1Status, V1StatusDetails, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1StatusOptions {
    code?: number;
    details?: V1StatusDetails;
    message?: string;
    metadata?: V1ListMeta;
    reason?: string;
    status?: string;
}
export function createV1Status(options: V1StatusOptions = {}): V1Status {
    const resource = new V1Status();
    resource.apiVersion = "v1";
    resource.kind = "Status";
    if (typeof options.code !== "undefined") {
        resource.code = options.code;
    }
    if (typeof options.details !== "undefined") {
        resource.details = options.details;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
