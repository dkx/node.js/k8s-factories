import { V1ScaleIOVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1ScaleIOVolumeSourceOptions {
    fsType?: string;
    gateway: string;
    protectionDomain?: string;
    readOnly?: boolean;
    secretRef: V1LocalObjectReference;
    sslEnabled?: boolean;
    storageMode?: string;
    storagePool?: string;
    system: string;
    volumeName?: string;
}
export function createV1ScaleIOVolumeSource(options: V1ScaleIOVolumeSourceOptions): V1ScaleIOVolumeSource {
    const resource = new V1ScaleIOVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.gateway !== "undefined") {
        resource.gateway = options.gateway;
    }
    if (typeof options.protectionDomain !== "undefined") {
        resource.protectionDomain = options.protectionDomain;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.sslEnabled !== "undefined") {
        resource.sslEnabled = options.sslEnabled;
    }
    if (typeof options.storageMode !== "undefined") {
        resource.storageMode = options.storageMode;
    }
    if (typeof options.storagePool !== "undefined") {
        resource.storagePool = options.storagePool;
    }
    if (typeof options.system !== "undefined") {
        resource.system = options.system;
    }
    if (typeof options.volumeName !== "undefined") {
        resource.volumeName = options.volumeName;
    }
    return resource;
}
