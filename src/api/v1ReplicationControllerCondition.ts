import { V1ReplicationControllerCondition } from "@kubernetes/client-node";
export declare interface V1ReplicationControllerConditionOptions {
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1ReplicationControllerCondition(options: V1ReplicationControllerConditionOptions): V1ReplicationControllerCondition {
    const resource = new V1ReplicationControllerCondition();
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
