import { PolicyV1beta1SELinuxStrategyOptions, V1SELinuxOptions } from "@kubernetes/client-node";
export declare interface PolicyV1beta1SELinuxStrategyOptionsOptions {
    rule: string;
    seLinuxOptions?: V1SELinuxOptions;
}
export function createPolicyV1beta1SELinuxStrategyOptions(options: PolicyV1beta1SELinuxStrategyOptionsOptions): PolicyV1beta1SELinuxStrategyOptions {
    const resource = new PolicyV1beta1SELinuxStrategyOptions();
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    if (typeof options.seLinuxOptions !== "undefined") {
        resource.seLinuxOptions = options.seLinuxOptions;
    }
    return resource;
}
