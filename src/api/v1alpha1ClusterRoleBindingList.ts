import { V1alpha1ClusterRoleBindingList, V1alpha1ClusterRoleBinding, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1ClusterRoleBindingListOptions {
    items: V1alpha1ClusterRoleBinding[];
    metadata?: V1ListMeta;
}
export function createV1alpha1ClusterRoleBindingList(options: V1alpha1ClusterRoleBindingListOptions): V1alpha1ClusterRoleBindingList {
    const resource = new V1alpha1ClusterRoleBindingList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "ClusterRoleBindingList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
