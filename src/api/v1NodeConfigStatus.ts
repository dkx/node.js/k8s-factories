import { V1NodeConfigStatus, V1NodeConfigSource } from "@kubernetes/client-node";
export declare interface V1NodeConfigStatusOptions {
    active?: V1NodeConfigSource;
    assigned?: V1NodeConfigSource;
    error?: string;
    lastKnownGood?: V1NodeConfigSource;
}
export function createV1NodeConfigStatus(options: V1NodeConfigStatusOptions = {}): V1NodeConfigStatus {
    const resource = new V1NodeConfigStatus();
    if (typeof options.active !== "undefined") {
        resource.active = options.active;
    }
    if (typeof options.assigned !== "undefined") {
        resource.assigned = options.assigned;
    }
    if (typeof options.error !== "undefined") {
        resource.error = options.error;
    }
    if (typeof options.lastKnownGood !== "undefined") {
        resource.lastKnownGood = options.lastKnownGood;
    }
    return resource;
}
