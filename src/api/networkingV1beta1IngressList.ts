import { NetworkingV1beta1IngressList, NetworkingV1beta1Ingress, V1ListMeta } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressListOptions {
    items: NetworkingV1beta1Ingress[];
    metadata?: V1ListMeta;
}
export function createNetworkingV1beta1IngressList(options: NetworkingV1beta1IngressListOptions): NetworkingV1beta1IngressList {
    const resource = new NetworkingV1beta1IngressList();
    resource.apiVersion = "networking.k8s.io/v1beta1";
    resource.kind = "IngressList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
