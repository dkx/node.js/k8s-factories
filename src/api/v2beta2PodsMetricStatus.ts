import { V2beta2PodsMetricStatus, V2beta2MetricValueStatus, V2beta2MetricIdentifier } from "@kubernetes/client-node";
export declare interface V2beta2PodsMetricStatusOptions {
    current: V2beta2MetricValueStatus;
    metric: V2beta2MetricIdentifier;
}
export function createV2beta2PodsMetricStatus(options: V2beta2PodsMetricStatusOptions): V2beta2PodsMetricStatus {
    const resource = new V2beta2PodsMetricStatus();
    if (typeof options.current !== "undefined") {
        resource.current = options.current;
    }
    if (typeof options.metric !== "undefined") {
        resource.metric = options.metric;
    }
    return resource;
}
