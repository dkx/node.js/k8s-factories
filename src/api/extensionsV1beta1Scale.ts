import { ExtensionsV1beta1Scale, V1ObjectMeta, ExtensionsV1beta1ScaleSpec, ExtensionsV1beta1ScaleStatus } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1ScaleOptions {
    metadata?: V1ObjectMeta;
    spec?: ExtensionsV1beta1ScaleSpec;
    status?: ExtensionsV1beta1ScaleStatus;
}
export function createExtensionsV1beta1Scale(options: ExtensionsV1beta1ScaleOptions = {}): ExtensionsV1beta1Scale {
    const resource = new ExtensionsV1beta1Scale();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "Scale";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
