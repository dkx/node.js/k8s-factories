import { AppsV1beta1DeploymentRollback, AppsV1beta1RollbackConfig } from "@kubernetes/client-node";
export declare interface AppsV1beta1DeploymentRollbackOptions {
    name: string;
    rollbackTo: AppsV1beta1RollbackConfig;
    updatedAnnotations?: {
        [key: string]: string;
    };
}
export function createAppsV1beta1DeploymentRollback(options: AppsV1beta1DeploymentRollbackOptions): AppsV1beta1DeploymentRollback {
    const resource = new AppsV1beta1DeploymentRollback();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "DeploymentRollback";
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.rollbackTo !== "undefined") {
        resource.rollbackTo = options.rollbackTo;
    }
    if (typeof options.updatedAnnotations !== "undefined") {
        resource.updatedAnnotations = options.updatedAnnotations;
    }
    return resource;
}
