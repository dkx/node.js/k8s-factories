import { V1beta1NonResourceAttributes } from "@kubernetes/client-node";
export declare interface V1beta1NonResourceAttributesOptions {
    path?: string;
    verb?: string;
}
export function createV1beta1NonResourceAttributes(options: V1beta1NonResourceAttributesOptions = {}): V1beta1NonResourceAttributes {
    const resource = new V1beta1NonResourceAttributes();
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.verb !== "undefined") {
        resource.verb = options.verb;
    }
    return resource;
}
