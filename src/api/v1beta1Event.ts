import { V1beta1Event, V1EventSource, V1ObjectMeta, V1ObjectReference, V1beta1EventSeries } from "@kubernetes/client-node";
export declare interface V1beta1EventOptions {
    action?: string;
    deprecatedCount?: number;
    deprecatedFirstTimestamp?: Date;
    deprecatedLastTimestamp?: Date;
    deprecatedSource?: V1EventSource;
    eventTime: Date;
    metadata?: V1ObjectMeta;
    note?: string;
    reason?: string;
    regarding?: V1ObjectReference;
    related?: V1ObjectReference;
    reportingController?: string;
    reportingInstance?: string;
    series?: V1beta1EventSeries;
    type?: string;
}
export function createV1beta1Event(options: V1beta1EventOptions): V1beta1Event {
    const resource = new V1beta1Event();
    resource.apiVersion = "events.k8s.io/v1beta1";
    resource.kind = "Event";
    if (typeof options.action !== "undefined") {
        resource.action = options.action;
    }
    if (typeof options.deprecatedCount !== "undefined") {
        resource.deprecatedCount = options.deprecatedCount;
    }
    if (typeof options.deprecatedFirstTimestamp !== "undefined") {
        resource.deprecatedFirstTimestamp = options.deprecatedFirstTimestamp;
    }
    if (typeof options.deprecatedLastTimestamp !== "undefined") {
        resource.deprecatedLastTimestamp = options.deprecatedLastTimestamp;
    }
    if (typeof options.deprecatedSource !== "undefined") {
        resource.deprecatedSource = options.deprecatedSource;
    }
    if (typeof options.eventTime !== "undefined") {
        resource.eventTime = options.eventTime;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.note !== "undefined") {
        resource.note = options.note;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.regarding !== "undefined") {
        resource.regarding = options.regarding;
    }
    if (typeof options.related !== "undefined") {
        resource.related = options.related;
    }
    if (typeof options.reportingController !== "undefined") {
        resource.reportingController = options.reportingController;
    }
    if (typeof options.reportingInstance !== "undefined") {
        resource.reportingInstance = options.reportingInstance;
    }
    if (typeof options.series !== "undefined") {
        resource.series = options.series;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
