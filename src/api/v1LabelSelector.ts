import { V1LabelSelector, V1LabelSelectorRequirement } from "@kubernetes/client-node";
export declare interface V1LabelSelectorOptions {
    matchExpressions?: V1LabelSelectorRequirement[];
    matchLabels?: {
        [key: string]: string;
    };
}
export function createV1LabelSelector(options: V1LabelSelectorOptions = {}): V1LabelSelector {
    const resource = new V1LabelSelector();
    if (typeof options.matchExpressions !== "undefined") {
        resource.matchExpressions = options.matchExpressions;
    }
    if (typeof options.matchLabels !== "undefined") {
        resource.matchLabels = options.matchLabels;
    }
    return resource;
}
