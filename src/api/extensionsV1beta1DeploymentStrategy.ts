import { ExtensionsV1beta1DeploymentStrategy, ExtensionsV1beta1RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1DeploymentStrategyOptions {
    rollingUpdate?: ExtensionsV1beta1RollingUpdateDeployment;
    type?: string;
}
export function createExtensionsV1beta1DeploymentStrategy(options: ExtensionsV1beta1DeploymentStrategyOptions = {}): ExtensionsV1beta1DeploymentStrategy {
    const resource = new ExtensionsV1beta1DeploymentStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
