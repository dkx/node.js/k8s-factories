import { V1APIServiceList, V1APIService, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1APIServiceListOptions {
    items: V1APIService[];
    metadata?: V1ListMeta;
}
export function createV1APIServiceList(options: V1APIServiceListOptions): V1APIServiceList {
    const resource = new V1APIServiceList();
    resource.apiVersion = "apiregistration.k8s.io/v1";
    resource.kind = "APIServiceList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
