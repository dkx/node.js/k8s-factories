import { V1StatusCause } from "@kubernetes/client-node";
export declare interface V1StatusCauseOptions {
    field?: string;
    message?: string;
    reason?: string;
}
export function createV1StatusCause(options: V1StatusCauseOptions = {}): V1StatusCause {
    const resource = new V1StatusCause();
    if (typeof options.field !== "undefined") {
        resource.field = options.field;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    return resource;
}
