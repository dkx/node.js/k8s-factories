import { V2beta1PodsMetricStatus, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V2beta1PodsMetricStatusOptions {
    currentAverageValue: string;
    metricName: string;
    selector?: V1LabelSelector;
}
export function createV2beta1PodsMetricStatus(options: V2beta1PodsMetricStatusOptions): V2beta1PodsMetricStatus {
    const resource = new V2beta1PodsMetricStatus();
    if (typeof options.currentAverageValue !== "undefined") {
        resource.currentAverageValue = options.currentAverageValue;
    }
    if (typeof options.metricName !== "undefined") {
        resource.metricName = options.metricName;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    return resource;
}
