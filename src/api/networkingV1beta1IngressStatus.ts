import { NetworkingV1beta1IngressStatus, V1LoadBalancerStatus } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressStatusOptions {
    loadBalancer?: V1LoadBalancerStatus;
}
export function createNetworkingV1beta1IngressStatus(options: NetworkingV1beta1IngressStatusOptions = {}): NetworkingV1beta1IngressStatus {
    const resource = new NetworkingV1beta1IngressStatus();
    if (typeof options.loadBalancer !== "undefined") {
        resource.loadBalancer = options.loadBalancer;
    }
    return resource;
}
