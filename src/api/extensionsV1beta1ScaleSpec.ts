import { ExtensionsV1beta1ScaleSpec } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1ScaleSpecOptions {
    replicas?: number;
}
export function createExtensionsV1beta1ScaleSpec(options: ExtensionsV1beta1ScaleSpecOptions = {}): ExtensionsV1beta1ScaleSpec {
    const resource = new ExtensionsV1beta1ScaleSpec();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    return resource;
}
