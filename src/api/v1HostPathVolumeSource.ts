import { V1HostPathVolumeSource } from "@kubernetes/client-node";
export declare interface V1HostPathVolumeSourceOptions {
    path: string;
    type?: string;
}
export function createV1HostPathVolumeSource(options: V1HostPathVolumeSourceOptions): V1HostPathVolumeSource {
    const resource = new V1HostPathVolumeSource();
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
