import { V1PodList, V1Pod, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1PodListOptions {
    items: V1Pod[];
    metadata?: V1ListMeta;
}
export function createV1PodList(options: V1PodListOptions): V1PodList {
    const resource = new V1PodList();
    resource.apiVersion = "v1";
    resource.kind = "PodList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
