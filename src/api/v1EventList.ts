import { V1EventList, V1Event, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1EventListOptions {
    items: V1Event[];
    metadata?: V1ListMeta;
}
export function createV1EventList(options: V1EventListOptions): V1EventList {
    const resource = new V1EventList();
    resource.apiVersion = "v1";
    resource.kind = "EventList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
