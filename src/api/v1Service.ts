import { V1Service, V1ObjectMeta, V1ServiceSpec, V1ServiceStatus } from "@kubernetes/client-node";
export declare interface V1ServiceOptions {
    metadata?: V1ObjectMeta;
    spec?: V1ServiceSpec;
    status?: V1ServiceStatus;
}
export function createV1Service(options: V1ServiceOptions = {}): V1Service {
    const resource = new V1Service();
    resource.apiVersion = "v1";
    resource.kind = "Service";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
