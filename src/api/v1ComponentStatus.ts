import { V1ComponentStatus, V1ComponentCondition, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1ComponentStatusOptions {
    conditions?: V1ComponentCondition[];
    metadata?: V1ObjectMeta;
}
export function createV1ComponentStatus(options: V1ComponentStatusOptions = {}): V1ComponentStatus {
    const resource = new V1ComponentStatus();
    resource.apiVersion = "v1";
    resource.kind = "ComponentStatus";
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
