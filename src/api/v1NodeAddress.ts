import { V1NodeAddress } from "@kubernetes/client-node";
export declare interface V1NodeAddressOptions {
    address: string;
    type: string;
}
export function createV1NodeAddress(options: V1NodeAddressOptions): V1NodeAddress {
    const resource = new V1NodeAddress();
    if (typeof options.address !== "undefined") {
        resource.address = options.address;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
