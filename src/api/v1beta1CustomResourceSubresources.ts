import { V1beta1CustomResourceSubresources, V1beta1CustomResourceSubresourceScale } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceSubresourcesOptions {
    scale?: V1beta1CustomResourceSubresourceScale;
    status?: object;
}
export function createV1beta1CustomResourceSubresources(options: V1beta1CustomResourceSubresourcesOptions = {}): V1beta1CustomResourceSubresources {
    const resource = new V1beta1CustomResourceSubresources();
    if (typeof options.scale !== "undefined") {
        resource.scale = options.scale;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
