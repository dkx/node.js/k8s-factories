import { ExtensionsV1beta1FSGroupStrategyOptions, ExtensionsV1beta1IDRange } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1FSGroupStrategyOptionsOptions {
    ranges?: ExtensionsV1beta1IDRange[];
    rule?: string;
}
export function createExtensionsV1beta1FSGroupStrategyOptions(options: ExtensionsV1beta1FSGroupStrategyOptionsOptions = {}): ExtensionsV1beta1FSGroupStrategyOptions {
    const resource = new ExtensionsV1beta1FSGroupStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
