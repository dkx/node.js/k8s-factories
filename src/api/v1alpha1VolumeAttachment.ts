import { V1alpha1VolumeAttachment, V1ObjectMeta, V1alpha1VolumeAttachmentSpec, V1alpha1VolumeAttachmentStatus } from "@kubernetes/client-node";
export declare interface V1alpha1VolumeAttachmentOptions {
    metadata?: V1ObjectMeta;
    spec: V1alpha1VolumeAttachmentSpec;
    status?: V1alpha1VolumeAttachmentStatus;
}
export function createV1alpha1VolumeAttachment(options: V1alpha1VolumeAttachmentOptions): V1alpha1VolumeAttachment {
    const resource = new V1alpha1VolumeAttachment();
    resource.apiVersion = "storage.k8s.io/v1alpha1";
    resource.kind = "VolumeAttachment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
