import { V1APIResource } from "@kubernetes/client-node";
export declare interface V1APIResourceOptions {
    categories?: string[];
    group?: string;
    kind: string;
    name: string;
    namespaced: boolean;
    shortNames?: string[];
    singularName: string;
    storageVersionHash?: string;
    verbs: string[];
    version?: string;
}
export function createV1APIResource(options: V1APIResourceOptions): V1APIResource {
    const resource = new V1APIResource();
    if (typeof options.categories !== "undefined") {
        resource.categories = options.categories;
    }
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespaced !== "undefined") {
        resource.namespaced = options.namespaced;
    }
    if (typeof options.shortNames !== "undefined") {
        resource.shortNames = options.shortNames;
    }
    if (typeof options.singularName !== "undefined") {
        resource.singularName = options.singularName;
    }
    if (typeof options.storageVersionHash !== "undefined") {
        resource.storageVersionHash = options.storageVersionHash;
    }
    if (typeof options.verbs !== "undefined") {
        resource.verbs = options.verbs;
    }
    if (typeof options.version !== "undefined") {
        resource.version = options.version;
    }
    return resource;
}
