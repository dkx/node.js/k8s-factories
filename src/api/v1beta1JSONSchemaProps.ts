import { V1beta1JSONSchemaProps, V1beta1ExternalDocumentation } from "@kubernetes/client-node";
export declare interface V1beta1JSONSchemaPropsOptions {
    $ref?: string;
    $schema?: string;
    additionalItems?: object;
    additionalProperties?: object;
    allOf?: V1beta1JSONSchemaProps[];
    anyOf?: V1beta1JSONSchemaProps[];
    _default?: object;
    definitions?: {
        [key: string]: V1beta1JSONSchemaProps;
    };
    dependencies?: {
        [key: string]: object;
    };
    description?: string;
    _enum?: object[];
    example?: object;
    exclusiveMaximum?: boolean;
    exclusiveMinimum?: boolean;
    externalDocs?: V1beta1ExternalDocumentation;
    format?: string;
    id?: string;
    items?: object;
    maxItems?: number;
    maxLength?: number;
    maxProperties?: number;
    maximum?: number;
    minItems?: number;
    minLength?: number;
    minProperties?: number;
    minimum?: number;
    multipleOf?: number;
    not?: V1beta1JSONSchemaProps;
    nullable?: boolean;
    oneOf?: V1beta1JSONSchemaProps[];
    pattern?: string;
    patternProperties?: {
        [key: string]: V1beta1JSONSchemaProps;
    };
    properties?: {
        [key: string]: V1beta1JSONSchemaProps;
    };
    required?: string[];
    title?: string;
    type?: string;
    uniqueItems?: boolean;
    x_kubernetes_embedded_resource?: boolean;
    x_kubernetes_int_or_string?: boolean;
    x_kubernetes_preserve_unknown_fields?: boolean;
}
export function createV1beta1JSONSchemaProps(options: V1beta1JSONSchemaPropsOptions = {}): V1beta1JSONSchemaProps {
    const resource = new V1beta1JSONSchemaProps();
    if (typeof options.$ref !== "undefined") {
        resource.$ref = options.$ref;
    }
    if (typeof options.$schema !== "undefined") {
        resource.$schema = options.$schema;
    }
    if (typeof options.additionalItems !== "undefined") {
        resource.additionalItems = options.additionalItems;
    }
    if (typeof options.additionalProperties !== "undefined") {
        resource.additionalProperties = options.additionalProperties;
    }
    if (typeof options.allOf !== "undefined") {
        resource.allOf = options.allOf;
    }
    if (typeof options.anyOf !== "undefined") {
        resource.anyOf = options.anyOf;
    }
    if (typeof options._default !== "undefined") {
        resource._default = options._default;
    }
    if (typeof options.definitions !== "undefined") {
        resource.definitions = options.definitions;
    }
    if (typeof options.dependencies !== "undefined") {
        resource.dependencies = options.dependencies;
    }
    if (typeof options.description !== "undefined") {
        resource.description = options.description;
    }
    if (typeof options._enum !== "undefined") {
        resource._enum = options._enum;
    }
    if (typeof options.example !== "undefined") {
        resource.example = options.example;
    }
    if (typeof options.exclusiveMaximum !== "undefined") {
        resource.exclusiveMaximum = options.exclusiveMaximum;
    }
    if (typeof options.exclusiveMinimum !== "undefined") {
        resource.exclusiveMinimum = options.exclusiveMinimum;
    }
    if (typeof options.externalDocs !== "undefined") {
        resource.externalDocs = options.externalDocs;
    }
    if (typeof options.format !== "undefined") {
        resource.format = options.format;
    }
    if (typeof options.id !== "undefined") {
        resource.id = options.id;
    }
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.maxItems !== "undefined") {
        resource.maxItems = options.maxItems;
    }
    if (typeof options.maxLength !== "undefined") {
        resource.maxLength = options.maxLength;
    }
    if (typeof options.maxProperties !== "undefined") {
        resource.maxProperties = options.maxProperties;
    }
    if (typeof options.maximum !== "undefined") {
        resource.maximum = options.maximum;
    }
    if (typeof options.minItems !== "undefined") {
        resource.minItems = options.minItems;
    }
    if (typeof options.minLength !== "undefined") {
        resource.minLength = options.minLength;
    }
    if (typeof options.minProperties !== "undefined") {
        resource.minProperties = options.minProperties;
    }
    if (typeof options.minimum !== "undefined") {
        resource.minimum = options.minimum;
    }
    if (typeof options.multipleOf !== "undefined") {
        resource.multipleOf = options.multipleOf;
    }
    if (typeof options.not !== "undefined") {
        resource.not = options.not;
    }
    if (typeof options.nullable !== "undefined") {
        resource.nullable = options.nullable;
    }
    if (typeof options.oneOf !== "undefined") {
        resource.oneOf = options.oneOf;
    }
    if (typeof options.pattern !== "undefined") {
        resource.pattern = options.pattern;
    }
    if (typeof options.patternProperties !== "undefined") {
        resource.patternProperties = options.patternProperties;
    }
    if (typeof options.properties !== "undefined") {
        resource.properties = options.properties;
    }
    if (typeof options.required !== "undefined") {
        resource.required = options.required;
    }
    if (typeof options.title !== "undefined") {
        resource.title = options.title;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    if (typeof options.uniqueItems !== "undefined") {
        resource.uniqueItems = options.uniqueItems;
    }
    if (typeof options.x_kubernetes_embedded_resource !== "undefined") {
        resource.x_kubernetes_embedded_resource = options.x_kubernetes_embedded_resource;
    }
    if (typeof options.x_kubernetes_int_or_string !== "undefined") {
        resource.x_kubernetes_int_or_string = options.x_kubernetes_int_or_string;
    }
    if (typeof options.x_kubernetes_preserve_unknown_fields !== "undefined") {
        resource.x_kubernetes_preserve_unknown_fields = options.x_kubernetes_preserve_unknown_fields;
    }
    return resource;
}
