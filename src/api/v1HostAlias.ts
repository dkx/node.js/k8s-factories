import { V1HostAlias } from "@kubernetes/client-node";
export declare interface V1HostAliasOptions {
    hostnames?: string[];
    ip?: string;
}
export function createV1HostAlias(options: V1HostAliasOptions = {}): V1HostAlias {
    const resource = new V1HostAlias();
    if (typeof options.hostnames !== "undefined") {
        resource.hostnames = options.hostnames;
    }
    if (typeof options.ip !== "undefined") {
        resource.ip = options.ip;
    }
    return resource;
}
