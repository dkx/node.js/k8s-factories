import { V1VolumeAttachmentSpec, V1VolumeAttachmentSource } from "@kubernetes/client-node";
export declare interface V1VolumeAttachmentSpecOptions {
    attacher: string;
    nodeName: string;
    source: V1VolumeAttachmentSource;
}
export function createV1VolumeAttachmentSpec(options: V1VolumeAttachmentSpecOptions): V1VolumeAttachmentSpec {
    const resource = new V1VolumeAttachmentSpec();
    if (typeof options.attacher !== "undefined") {
        resource.attacher = options.attacher;
    }
    if (typeof options.nodeName !== "undefined") {
        resource.nodeName = options.nodeName;
    }
    if (typeof options.source !== "undefined") {
        resource.source = options.source;
    }
    return resource;
}
