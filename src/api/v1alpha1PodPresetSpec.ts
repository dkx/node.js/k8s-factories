import { V1alpha1PodPresetSpec, V1EnvVar, V1EnvFromSource, V1LabelSelector, V1VolumeMount, V1Volume } from "@kubernetes/client-node";
export declare interface V1alpha1PodPresetSpecOptions {
    env?: V1EnvVar[];
    envFrom?: V1EnvFromSource[];
    selector?: V1LabelSelector;
    volumeMounts?: V1VolumeMount[];
    volumes?: V1Volume[];
}
export function createV1alpha1PodPresetSpec(options: V1alpha1PodPresetSpecOptions = {}): V1alpha1PodPresetSpec {
    const resource = new V1alpha1PodPresetSpec();
    if (typeof options.env !== "undefined") {
        resource.env = options.env;
    }
    if (typeof options.envFrom !== "undefined") {
        resource.envFrom = options.envFrom;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.volumeMounts !== "undefined") {
        resource.volumeMounts = options.volumeMounts;
    }
    if (typeof options.volumes !== "undefined") {
        resource.volumes = options.volumes;
    }
    return resource;
}
