import { V1Deployment, V1ObjectMeta, V1DeploymentSpec, V1DeploymentStatus } from "@kubernetes/client-node";
export declare interface V1DeploymentOptions {
    metadata?: V1ObjectMeta;
    spec?: V1DeploymentSpec;
    status?: V1DeploymentStatus;
}
export function createV1Deployment(options: V1DeploymentOptions = {}): V1Deployment {
    const resource = new V1Deployment();
    resource.apiVersion = "apps/v1";
    resource.kind = "Deployment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
