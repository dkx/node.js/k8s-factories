import { ExtensionsV1beta1PodSecurityPolicy, V1ObjectMeta, ExtensionsV1beta1PodSecurityPolicySpec } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1PodSecurityPolicyOptions {
    metadata?: V1ObjectMeta;
    spec?: ExtensionsV1beta1PodSecurityPolicySpec;
}
export function createExtensionsV1beta1PodSecurityPolicy(options: ExtensionsV1beta1PodSecurityPolicyOptions = {}): ExtensionsV1beta1PodSecurityPolicy {
    const resource = new ExtensionsV1beta1PodSecurityPolicy();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "PodSecurityPolicy";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
