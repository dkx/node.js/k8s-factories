import { V1beta1VolumeAttachmentSource, V1PersistentVolumeSpec } from "@kubernetes/client-node";
export declare interface V1beta1VolumeAttachmentSourceOptions {
    inlineVolumeSpec?: V1PersistentVolumeSpec;
    persistentVolumeName?: string;
}
export function createV1beta1VolumeAttachmentSource(options: V1beta1VolumeAttachmentSourceOptions = {}): V1beta1VolumeAttachmentSource {
    const resource = new V1beta1VolumeAttachmentSource();
    if (typeof options.inlineVolumeSpec !== "undefined") {
        resource.inlineVolumeSpec = options.inlineVolumeSpec;
    }
    if (typeof options.persistentVolumeName !== "undefined") {
        resource.persistentVolumeName = options.persistentVolumeName;
    }
    return resource;
}
