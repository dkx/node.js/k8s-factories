import { V1Probe, V1ExecAction, V1HTTPGetAction, V1TCPSocketAction } from "@kubernetes/client-node";
export declare interface V1ProbeOptions {
    exec?: V1ExecAction;
    failureThreshold?: number;
    httpGet?: V1HTTPGetAction;
    initialDelaySeconds?: number;
    periodSeconds?: number;
    successThreshold?: number;
    tcpSocket?: V1TCPSocketAction;
    timeoutSeconds?: number;
}
export function createV1Probe(options: V1ProbeOptions = {}): V1Probe {
    const resource = new V1Probe();
    if (typeof options.exec !== "undefined") {
        resource.exec = options.exec;
    }
    if (typeof options.failureThreshold !== "undefined") {
        resource.failureThreshold = options.failureThreshold;
    }
    if (typeof options.httpGet !== "undefined") {
        resource.httpGet = options.httpGet;
    }
    if (typeof options.initialDelaySeconds !== "undefined") {
        resource.initialDelaySeconds = options.initialDelaySeconds;
    }
    if (typeof options.periodSeconds !== "undefined") {
        resource.periodSeconds = options.periodSeconds;
    }
    if (typeof options.successThreshold !== "undefined") {
        resource.successThreshold = options.successThreshold;
    }
    if (typeof options.tcpSocket !== "undefined") {
        resource.tcpSocket = options.tcpSocket;
    }
    if (typeof options.timeoutSeconds !== "undefined") {
        resource.timeoutSeconds = options.timeoutSeconds;
    }
    return resource;
}
