import { V1beta1APIServiceList, V1beta1APIService, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1APIServiceListOptions {
    items: V1beta1APIService[];
    metadata?: V1ListMeta;
}
export function createV1beta1APIServiceList(options: V1beta1APIServiceListOptions): V1beta1APIServiceList {
    const resource = new V1beta1APIServiceList();
    resource.apiVersion = "apiregistration.k8s.io/v1beta1";
    resource.kind = "APIServiceList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
