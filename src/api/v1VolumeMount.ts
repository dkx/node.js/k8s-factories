import { V1VolumeMount } from "@kubernetes/client-node";
export declare interface V1VolumeMountOptions {
    mountPath: string;
    mountPropagation?: string;
    name: string;
    readOnly?: boolean;
    subPath?: string;
    subPathExpr?: string;
}
export function createV1VolumeMount(options: V1VolumeMountOptions): V1VolumeMount {
    const resource = new V1VolumeMount();
    if (typeof options.mountPath !== "undefined") {
        resource.mountPath = options.mountPath;
    }
    if (typeof options.mountPropagation !== "undefined") {
        resource.mountPropagation = options.mountPropagation;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.subPath !== "undefined") {
        resource.subPath = options.subPath;
    }
    if (typeof options.subPathExpr !== "undefined") {
        resource.subPathExpr = options.subPathExpr;
    }
    return resource;
}
