import { V1beta1CertificateSigningRequestStatus, V1beta1CertificateSigningRequestCondition } from "@kubernetes/client-node";
export declare interface V1beta1CertificateSigningRequestStatusOptions {
    certificate?: string;
    conditions?: V1beta1CertificateSigningRequestCondition[];
}
export function createV1beta1CertificateSigningRequestStatus(options: V1beta1CertificateSigningRequestStatusOptions = {}): V1beta1CertificateSigningRequestStatus {
    const resource = new V1beta1CertificateSigningRequestStatus();
    if (typeof options.certificate !== "undefined") {
        resource.certificate = options.certificate;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    return resource;
}
