import { V1beta1SelfSubjectRulesReview, V1ObjectMeta, V1beta1SelfSubjectRulesReviewSpec, V1beta1SubjectRulesReviewStatus } from "@kubernetes/client-node";
export declare interface V1beta1SelfSubjectRulesReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1SelfSubjectRulesReviewSpec;
    status?: V1beta1SubjectRulesReviewStatus;
}
export function createV1beta1SelfSubjectRulesReview(options: V1beta1SelfSubjectRulesReviewOptions): V1beta1SelfSubjectRulesReview {
    const resource = new V1beta1SelfSubjectRulesReview();
    resource.apiVersion = "authorization.k8s.io/v1beta1";
    resource.kind = "SelfSubjectRulesReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
