import { V1Scale, V1ObjectMeta, V1ScaleSpec, V1ScaleStatus } from "@kubernetes/client-node";
export declare interface V1ScaleOptions {
    metadata?: V1ObjectMeta;
    spec?: V1ScaleSpec;
    status?: V1ScaleStatus;
}
export function createV1Scale(options: V1ScaleOptions = {}): V1Scale {
    const resource = new V1Scale();
    resource.apiVersion = "autoscaling/v1";
    resource.kind = "Scale";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
