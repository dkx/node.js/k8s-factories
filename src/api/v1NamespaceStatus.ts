import { V1NamespaceStatus } from "@kubernetes/client-node";
export declare interface V1NamespaceStatusOptions {
    phase?: string;
}
export function createV1NamespaceStatus(options: V1NamespaceStatusOptions = {}): V1NamespaceStatus {
    const resource = new V1NamespaceStatus();
    if (typeof options.phase !== "undefined") {
        resource.phase = options.phase;
    }
    return resource;
}
