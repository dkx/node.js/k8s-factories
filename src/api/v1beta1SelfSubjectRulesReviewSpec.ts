import { V1beta1SelfSubjectRulesReviewSpec } from "@kubernetes/client-node";
export declare interface V1beta1SelfSubjectRulesReviewSpecOptions {
    namespace?: string;
}
export function createV1beta1SelfSubjectRulesReviewSpec(options: V1beta1SelfSubjectRulesReviewSpecOptions = {}): V1beta1SelfSubjectRulesReviewSpec {
    const resource = new V1beta1SelfSubjectRulesReviewSpec();
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    return resource;
}
