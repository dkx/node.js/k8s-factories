import { V1beta1SubjectAccessReviewSpec, V1beta1NonResourceAttributes, V1beta1ResourceAttributes } from "@kubernetes/client-node";
export declare interface V1beta1SubjectAccessReviewSpecOptions {
    extra?: {
        [key: string]: string[];
    };
    group?: string[];
    nonResourceAttributes?: V1beta1NonResourceAttributes;
    resourceAttributes?: V1beta1ResourceAttributes;
    uid?: string;
    user?: string;
}
export function createV1beta1SubjectAccessReviewSpec(options: V1beta1SubjectAccessReviewSpecOptions = {}): V1beta1SubjectAccessReviewSpec {
    const resource = new V1beta1SubjectAccessReviewSpec();
    if (typeof options.extra !== "undefined") {
        resource.extra = options.extra;
    }
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.nonResourceAttributes !== "undefined") {
        resource.nonResourceAttributes = options.nonResourceAttributes;
    }
    if (typeof options.resourceAttributes !== "undefined") {
        resource.resourceAttributes = options.resourceAttributes;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
