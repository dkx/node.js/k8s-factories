import { V1PreferredSchedulingTerm, V1NodeSelectorTerm } from "@kubernetes/client-node";
export declare interface V1PreferredSchedulingTermOptions {
    preference: V1NodeSelectorTerm;
    weight: number;
}
export function createV1PreferredSchedulingTerm(options: V1PreferredSchedulingTermOptions): V1PreferredSchedulingTerm {
    const resource = new V1PreferredSchedulingTerm();
    if (typeof options.preference !== "undefined") {
        resource.preference = options.preference;
    }
    if (typeof options.weight !== "undefined") {
        resource.weight = options.weight;
    }
    return resource;
}
