import { V2beta1ResourceMetricStatus } from "@kubernetes/client-node";
export declare interface V2beta1ResourceMetricStatusOptions {
    currentAverageUtilization?: number;
    currentAverageValue: string;
    name: string;
}
export function createV2beta1ResourceMetricStatus(options: V2beta1ResourceMetricStatusOptions): V2beta1ResourceMetricStatus {
    const resource = new V2beta1ResourceMetricStatus();
    if (typeof options.currentAverageUtilization !== "undefined") {
        resource.currentAverageUtilization = options.currentAverageUtilization;
    }
    if (typeof options.currentAverageValue !== "undefined") {
        resource.currentAverageValue = options.currentAverageValue;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
