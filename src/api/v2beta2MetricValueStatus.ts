import { V2beta2MetricValueStatus } from "@kubernetes/client-node";
export declare interface V2beta2MetricValueStatusOptions {
    averageUtilization?: number;
    averageValue?: string;
    value?: string;
}
export function createV2beta2MetricValueStatus(options: V2beta2MetricValueStatusOptions = {}): V2beta2MetricValueStatus {
    const resource = new V2beta2MetricValueStatus();
    if (typeof options.averageUtilization !== "undefined") {
        resource.averageUtilization = options.averageUtilization;
    }
    if (typeof options.averageValue !== "undefined") {
        resource.averageValue = options.averageValue;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
