import { V1alpha1VolumeAttachmentList, V1alpha1VolumeAttachment, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1VolumeAttachmentListOptions {
    items: V1alpha1VolumeAttachment[];
    metadata?: V1ListMeta;
}
export function createV1alpha1VolumeAttachmentList(options: V1alpha1VolumeAttachmentListOptions): V1alpha1VolumeAttachmentList {
    const resource = new V1alpha1VolumeAttachmentList();
    resource.apiVersion = "storage.k8s.io/v1alpha1";
    resource.kind = "VolumeAttachmentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
