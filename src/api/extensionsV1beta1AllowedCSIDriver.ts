import { ExtensionsV1beta1AllowedCSIDriver } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1AllowedCSIDriverOptions {
    name: string;
}
export function createExtensionsV1beta1AllowedCSIDriver(options: ExtensionsV1beta1AllowedCSIDriverOptions): ExtensionsV1beta1AllowedCSIDriver {
    const resource = new ExtensionsV1beta1AllowedCSIDriver();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
