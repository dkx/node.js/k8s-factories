import { V2beta1ExternalMetricSource, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V2beta1ExternalMetricSourceOptions {
    metricName: string;
    metricSelector?: V1LabelSelector;
    targetAverageValue?: string;
    targetValue?: string;
}
export function createV2beta1ExternalMetricSource(options: V2beta1ExternalMetricSourceOptions): V2beta1ExternalMetricSource {
    const resource = new V2beta1ExternalMetricSource();
    if (typeof options.metricName !== "undefined") {
        resource.metricName = options.metricName;
    }
    if (typeof options.metricSelector !== "undefined") {
        resource.metricSelector = options.metricSelector;
    }
    if (typeof options.targetAverageValue !== "undefined") {
        resource.targetAverageValue = options.targetAverageValue;
    }
    if (typeof options.targetValue !== "undefined") {
        resource.targetValue = options.targetValue;
    }
    return resource;
}
