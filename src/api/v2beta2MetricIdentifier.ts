import { V2beta2MetricIdentifier, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V2beta2MetricIdentifierOptions {
    name: string;
    selector?: V1LabelSelector;
}
export function createV2beta2MetricIdentifier(options: V2beta2MetricIdentifierOptions): V2beta2MetricIdentifier {
    const resource = new V2beta2MetricIdentifier();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    return resource;
}
