import { V1beta1ResourceAttributes } from "@kubernetes/client-node";
export declare interface V1beta1ResourceAttributesOptions {
    group?: string;
    name?: string;
    namespace?: string;
    resource?: string;
    subresource?: string;
    verb?: string;
    version?: string;
}
export function createV1beta1ResourceAttributes(options: V1beta1ResourceAttributesOptions = {}): V1beta1ResourceAttributes {
    const resource = new V1beta1ResourceAttributes();
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.resource !== "undefined") {
        resource.resource = options.resource;
    }
    if (typeof options.subresource !== "undefined") {
        resource.subresource = options.subresource;
    }
    if (typeof options.verb !== "undefined") {
        resource.verb = options.verb;
    }
    if (typeof options.version !== "undefined") {
        resource.version = options.version;
    }
    return resource;
}
