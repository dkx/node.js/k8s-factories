import { V1beta1SubjectRulesReviewStatus, V1beta1NonResourceRule, V1beta1ResourceRule } from "@kubernetes/client-node";
export declare interface V1beta1SubjectRulesReviewStatusOptions {
    evaluationError?: string;
    incomplete: boolean;
    nonResourceRules: V1beta1NonResourceRule[];
    resourceRules: V1beta1ResourceRule[];
}
export function createV1beta1SubjectRulesReviewStatus(options: V1beta1SubjectRulesReviewStatusOptions): V1beta1SubjectRulesReviewStatus {
    const resource = new V1beta1SubjectRulesReviewStatus();
    if (typeof options.evaluationError !== "undefined") {
        resource.evaluationError = options.evaluationError;
    }
    if (typeof options.incomplete !== "undefined") {
        resource.incomplete = options.incomplete;
    }
    if (typeof options.nonResourceRules !== "undefined") {
        resource.nonResourceRules = options.nonResourceRules;
    }
    if (typeof options.resourceRules !== "undefined") {
        resource.resourceRules = options.resourceRules;
    }
    return resource;
}
