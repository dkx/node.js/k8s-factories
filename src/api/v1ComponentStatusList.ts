import { V1ComponentStatusList, V1ComponentStatus, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ComponentStatusListOptions {
    items: V1ComponentStatus[];
    metadata?: V1ListMeta;
}
export function createV1ComponentStatusList(options: V1ComponentStatusListOptions): V1ComponentStatusList {
    const resource = new V1ComponentStatusList();
    resource.apiVersion = "v1";
    resource.kind = "ComponentStatusList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
