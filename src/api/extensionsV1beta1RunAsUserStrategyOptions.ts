import { ExtensionsV1beta1RunAsUserStrategyOptions, ExtensionsV1beta1IDRange } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1RunAsUserStrategyOptionsOptions {
    ranges?: ExtensionsV1beta1IDRange[];
    rule: string;
}
export function createExtensionsV1beta1RunAsUserStrategyOptions(options: ExtensionsV1beta1RunAsUserStrategyOptionsOptions): ExtensionsV1beta1RunAsUserStrategyOptions {
    const resource = new ExtensionsV1beta1RunAsUserStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
