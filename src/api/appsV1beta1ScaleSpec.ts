import { AppsV1beta1ScaleSpec } from "@kubernetes/client-node";
export declare interface AppsV1beta1ScaleSpecOptions {
    replicas?: number;
}
export function createAppsV1beta1ScaleSpec(options: AppsV1beta1ScaleSpecOptions = {}): AppsV1beta1ScaleSpec {
    const resource = new AppsV1beta1ScaleSpec();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    return resource;
}
