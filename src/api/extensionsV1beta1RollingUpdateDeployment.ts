import { ExtensionsV1beta1RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1RollingUpdateDeploymentOptions {
    maxSurge?: object;
    maxUnavailable?: object;
}
export function createExtensionsV1beta1RollingUpdateDeployment(options: ExtensionsV1beta1RollingUpdateDeploymentOptions = {}): ExtensionsV1beta1RollingUpdateDeployment {
    const resource = new ExtensionsV1beta1RollingUpdateDeployment();
    if (typeof options.maxSurge !== "undefined") {
        resource.maxSurge = options.maxSurge;
    }
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
