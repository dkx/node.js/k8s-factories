import { V1beta1TokenReview, V1ObjectMeta, V1beta1TokenReviewSpec, V1beta1TokenReviewStatus } from "@kubernetes/client-node";
export declare interface V1beta1TokenReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1TokenReviewSpec;
    status?: V1beta1TokenReviewStatus;
}
export function createV1beta1TokenReview(options: V1beta1TokenReviewOptions): V1beta1TokenReview {
    const resource = new V1beta1TokenReview();
    resource.apiVersion = "authentication.k8s.io/v1beta1";
    resource.kind = "TokenReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
