import { V1beta2ScaleSpec } from "@kubernetes/client-node";
export declare interface V1beta2ScaleSpecOptions {
    replicas?: number;
}
export function createV1beta2ScaleSpec(options: V1beta2ScaleSpecOptions = {}): V1beta2ScaleSpec {
    const resource = new V1beta2ScaleSpec();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    return resource;
}
