import { V1ResourceRequirements } from "@kubernetes/client-node";
export declare interface V1ResourceRequirementsOptions {
    limits?: {
        [key: string]: string;
    };
    requests?: {
        [key: string]: string;
    };
}
export function createV1ResourceRequirements(options: V1ResourceRequirementsOptions = {}): V1ResourceRequirements {
    const resource = new V1ResourceRequirements();
    if (typeof options.limits !== "undefined") {
        resource.limits = options.limits;
    }
    if (typeof options.requests !== "undefined") {
        resource.requests = options.requests;
    }
    return resource;
}
