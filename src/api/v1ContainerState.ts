import { V1ContainerState, V1ContainerStateRunning, V1ContainerStateTerminated, V1ContainerStateWaiting } from "@kubernetes/client-node";
export declare interface V1ContainerStateOptions {
    running?: V1ContainerStateRunning;
    terminated?: V1ContainerStateTerminated;
    waiting?: V1ContainerStateWaiting;
}
export function createV1ContainerState(options: V1ContainerStateOptions = {}): V1ContainerState {
    const resource = new V1ContainerState();
    if (typeof options.running !== "undefined") {
        resource.running = options.running;
    }
    if (typeof options.terminated !== "undefined") {
        resource.terminated = options.terminated;
    }
    if (typeof options.waiting !== "undefined") {
        resource.waiting = options.waiting;
    }
    return resource;
}
