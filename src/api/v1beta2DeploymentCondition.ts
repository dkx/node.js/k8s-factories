import { V1beta2DeploymentCondition } from "@kubernetes/client-node";
export declare interface V1beta2DeploymentConditionOptions {
    lastTransitionTime?: Date;
    lastUpdateTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1beta2DeploymentCondition(options: V1beta2DeploymentConditionOptions): V1beta2DeploymentCondition {
    const resource = new V1beta2DeploymentCondition();
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.lastUpdateTime !== "undefined") {
        resource.lastUpdateTime = options.lastUpdateTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
