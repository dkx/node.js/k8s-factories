import { PolicyV1beta1PodSecurityPolicyList, PolicyV1beta1PodSecurityPolicy, V1ListMeta } from "@kubernetes/client-node";
export declare interface PolicyV1beta1PodSecurityPolicyListOptions {
    items: PolicyV1beta1PodSecurityPolicy[];
    metadata?: V1ListMeta;
}
export function createPolicyV1beta1PodSecurityPolicyList(options: PolicyV1beta1PodSecurityPolicyListOptions): PolicyV1beta1PodSecurityPolicyList {
    const resource = new PolicyV1beta1PodSecurityPolicyList();
    resource.apiVersion = "policy/v1beta1";
    resource.kind = "PodSecurityPolicyList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
