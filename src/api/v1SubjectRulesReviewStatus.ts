import { V1SubjectRulesReviewStatus, V1NonResourceRule, V1ResourceRule } from "@kubernetes/client-node";
export declare interface V1SubjectRulesReviewStatusOptions {
    evaluationError?: string;
    incomplete: boolean;
    nonResourceRules: V1NonResourceRule[];
    resourceRules: V1ResourceRule[];
}
export function createV1SubjectRulesReviewStatus(options: V1SubjectRulesReviewStatusOptions): V1SubjectRulesReviewStatus {
    const resource = new V1SubjectRulesReviewStatus();
    if (typeof options.evaluationError !== "undefined") {
        resource.evaluationError = options.evaluationError;
    }
    if (typeof options.incomplete !== "undefined") {
        resource.incomplete = options.incomplete;
    }
    if (typeof options.nonResourceRules !== "undefined") {
        resource.nonResourceRules = options.nonResourceRules;
    }
    if (typeof options.resourceRules !== "undefined") {
        resource.resourceRules = options.resourceRules;
    }
    return resource;
}
