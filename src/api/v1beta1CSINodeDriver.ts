import { V1beta1CSINodeDriver } from "@kubernetes/client-node";
export declare interface V1beta1CSINodeDriverOptions {
    name: string;
    nodeID: string;
    topologyKeys?: string[];
}
export function createV1beta1CSINodeDriver(options: V1beta1CSINodeDriverOptions): V1beta1CSINodeDriver {
    const resource = new V1beta1CSINodeDriver();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.nodeID !== "undefined") {
        resource.nodeID = options.nodeID;
    }
    if (typeof options.topologyKeys !== "undefined") {
        resource.topologyKeys = options.topologyKeys;
    }
    return resource;
}
