import { V1APIResourceList, V1APIResource } from "@kubernetes/client-node";
export declare interface V1APIResourceListOptions {
    groupVersion: string;
    resources: V1APIResource[];
}
export function createV1APIResourceList(options: V1APIResourceListOptions): V1APIResourceList {
    const resource = new V1APIResourceList();
    resource.apiVersion = "v1";
    resource.kind = "APIResourceList";
    if (typeof options.groupVersion !== "undefined") {
        resource.groupVersion = options.groupVersion;
    }
    if (typeof options.resources !== "undefined") {
        resource.resources = options.resources;
    }
    return resource;
}
