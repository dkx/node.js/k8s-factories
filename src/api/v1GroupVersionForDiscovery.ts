import { V1GroupVersionForDiscovery } from "@kubernetes/client-node";
export declare interface V1GroupVersionForDiscoveryOptions {
    groupVersion: string;
    version: string;
}
export function createV1GroupVersionForDiscovery(options: V1GroupVersionForDiscoveryOptions): V1GroupVersionForDiscovery {
    const resource = new V1GroupVersionForDiscovery();
    if (typeof options.groupVersion !== "undefined") {
        resource.groupVersion = options.groupVersion;
    }
    if (typeof options.version !== "undefined") {
        resource.version = options.version;
    }
    return resource;
}
