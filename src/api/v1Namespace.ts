import { V1Namespace, V1ObjectMeta, V1NamespaceSpec, V1NamespaceStatus } from "@kubernetes/client-node";
export declare interface V1NamespaceOptions {
    metadata?: V1ObjectMeta;
    spec?: V1NamespaceSpec;
    status?: V1NamespaceStatus;
}
export function createV1Namespace(options: V1NamespaceOptions = {}): V1Namespace {
    const resource = new V1Namespace();
    resource.apiVersion = "v1";
    resource.kind = "Namespace";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
