import { V1beta1NetworkPolicyList, V1beta1NetworkPolicy, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicyListOptions {
    items: V1beta1NetworkPolicy[];
    metadata?: V1ListMeta;
}
export function createV1beta1NetworkPolicyList(options: V1beta1NetworkPolicyListOptions): V1beta1NetworkPolicyList {
    const resource = new V1beta1NetworkPolicyList();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "NetworkPolicyList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
