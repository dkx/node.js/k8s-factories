import { NetworkingV1beta1IngressSpec, NetworkingV1beta1IngressBackend, NetworkingV1beta1IngressRule, NetworkingV1beta1IngressTLS } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressSpecOptions {
    backend?: NetworkingV1beta1IngressBackend;
    rules?: NetworkingV1beta1IngressRule[];
    tls?: NetworkingV1beta1IngressTLS[];
}
export function createNetworkingV1beta1IngressSpec(options: NetworkingV1beta1IngressSpecOptions = {}): NetworkingV1beta1IngressSpec {
    const resource = new NetworkingV1beta1IngressSpec();
    if (typeof options.backend !== "undefined") {
        resource.backend = options.backend;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    if (typeof options.tls !== "undefined") {
        resource.tls = options.tls;
    }
    return resource;
}
