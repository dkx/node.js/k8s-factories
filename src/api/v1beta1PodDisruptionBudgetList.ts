import { V1beta1PodDisruptionBudgetList, V1beta1PodDisruptionBudget, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1PodDisruptionBudgetListOptions {
    items: V1beta1PodDisruptionBudget[];
    metadata?: V1ListMeta;
}
export function createV1beta1PodDisruptionBudgetList(options: V1beta1PodDisruptionBudgetListOptions): V1beta1PodDisruptionBudgetList {
    const resource = new V1beta1PodDisruptionBudgetList();
    resource.apiVersion = "policy/v1beta1";
    resource.kind = "PodDisruptionBudgetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
