import { V1HTTPGetAction, V1HTTPHeader } from "@kubernetes/client-node";
export declare interface V1HTTPGetActionOptions {
    host?: string;
    httpHeaders?: V1HTTPHeader[];
    path?: string;
    port: object;
    scheme?: string;
}
export function createV1HTTPGetAction(options: V1HTTPGetActionOptions): V1HTTPGetAction {
    const resource = new V1HTTPGetAction();
    if (typeof options.host !== "undefined") {
        resource.host = options.host;
    }
    if (typeof options.httpHeaders !== "undefined") {
        resource.httpHeaders = options.httpHeaders;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    if (typeof options.scheme !== "undefined") {
        resource.scheme = options.scheme;
    }
    return resource;
}
