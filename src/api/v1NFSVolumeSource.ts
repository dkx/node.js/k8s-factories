import { V1NFSVolumeSource } from "@kubernetes/client-node";
export declare interface V1NFSVolumeSourceOptions {
    path: string;
    readOnly?: boolean;
    server: string;
}
export function createV1NFSVolumeSource(options: V1NFSVolumeSourceOptions): V1NFSVolumeSource {
    const resource = new V1NFSVolumeSource();
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.server !== "undefined") {
        resource.server = options.server;
    }
    return resource;
}
