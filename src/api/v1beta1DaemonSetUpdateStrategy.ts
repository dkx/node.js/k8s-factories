import { V1beta1DaemonSetUpdateStrategy, V1beta1RollingUpdateDaemonSet } from "@kubernetes/client-node";
export declare interface V1beta1DaemonSetUpdateStrategyOptions {
    rollingUpdate?: V1beta1RollingUpdateDaemonSet;
    type?: string;
}
export function createV1beta1DaemonSetUpdateStrategy(options: V1beta1DaemonSetUpdateStrategyOptions = {}): V1beta1DaemonSetUpdateStrategy {
    const resource = new V1beta1DaemonSetUpdateStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
