import { V1RoleBinding, V1ObjectMeta, V1RoleRef, V1Subject } from "@kubernetes/client-node";
export declare interface V1RoleBindingOptions {
    metadata?: V1ObjectMeta;
    roleRef: V1RoleRef;
    subjects?: V1Subject[];
}
export function createV1RoleBinding(options: V1RoleBindingOptions): V1RoleBinding {
    const resource = new V1RoleBinding();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "RoleBinding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.roleRef !== "undefined") {
        resource.roleRef = options.roleRef;
    }
    if (typeof options.subjects !== "undefined") {
        resource.subjects = options.subjects;
    }
    return resource;
}
