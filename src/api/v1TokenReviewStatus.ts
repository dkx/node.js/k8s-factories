import { V1TokenReviewStatus, V1UserInfo } from "@kubernetes/client-node";
export declare interface V1TokenReviewStatusOptions {
    audiences?: string[];
    authenticated?: boolean;
    error?: string;
    user?: V1UserInfo;
}
export function createV1TokenReviewStatus(options: V1TokenReviewStatusOptions = {}): V1TokenReviewStatus {
    const resource = new V1TokenReviewStatus();
    if (typeof options.audiences !== "undefined") {
        resource.audiences = options.audiences;
    }
    if (typeof options.authenticated !== "undefined") {
        resource.authenticated = options.authenticated;
    }
    if (typeof options.error !== "undefined") {
        resource.error = options.error;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
