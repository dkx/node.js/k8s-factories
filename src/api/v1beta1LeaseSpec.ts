import { V1beta1LeaseSpec } from "@kubernetes/client-node";
export declare interface V1beta1LeaseSpecOptions {
    acquireTime?: Date;
    holderIdentity?: string;
    leaseDurationSeconds?: number;
    leaseTransitions?: number;
    renewTime?: Date;
}
export function createV1beta1LeaseSpec(options: V1beta1LeaseSpecOptions = {}): V1beta1LeaseSpec {
    const resource = new V1beta1LeaseSpec();
    if (typeof options.acquireTime !== "undefined") {
        resource.acquireTime = options.acquireTime;
    }
    if (typeof options.holderIdentity !== "undefined") {
        resource.holderIdentity = options.holderIdentity;
    }
    if (typeof options.leaseDurationSeconds !== "undefined") {
        resource.leaseDurationSeconds = options.leaseDurationSeconds;
    }
    if (typeof options.leaseTransitions !== "undefined") {
        resource.leaseTransitions = options.leaseTransitions;
    }
    if (typeof options.renewTime !== "undefined") {
        resource.renewTime = options.renewTime;
    }
    return resource;
}
