import { V1AzureFileVolumeSource } from "@kubernetes/client-node";
export declare interface V1AzureFileVolumeSourceOptions {
    readOnly?: boolean;
    secretName: string;
    shareName: string;
}
export function createV1AzureFileVolumeSource(options: V1AzureFileVolumeSourceOptions): V1AzureFileVolumeSource {
    const resource = new V1AzureFileVolumeSource();
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretName !== "undefined") {
        resource.secretName = options.secretName;
    }
    if (typeof options.shareName !== "undefined") {
        resource.shareName = options.shareName;
    }
    return resource;
}
