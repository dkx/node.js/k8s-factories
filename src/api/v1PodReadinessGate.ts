import { V1PodReadinessGate } from "@kubernetes/client-node";
export declare interface V1PodReadinessGateOptions {
    conditionType: string;
}
export function createV1PodReadinessGate(options: V1PodReadinessGateOptions): V1PodReadinessGate {
    const resource = new V1PodReadinessGate();
    if (typeof options.conditionType !== "undefined") {
        resource.conditionType = options.conditionType;
    }
    return resource;
}
