import { V1SelfSubjectRulesReview, V1ObjectMeta, V1SelfSubjectRulesReviewSpec, V1SubjectRulesReviewStatus } from "@kubernetes/client-node";
export declare interface V1SelfSubjectRulesReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1SelfSubjectRulesReviewSpec;
    status?: V1SubjectRulesReviewStatus;
}
export function createV1SelfSubjectRulesReview(options: V1SelfSubjectRulesReviewOptions): V1SelfSubjectRulesReview {
    const resource = new V1SelfSubjectRulesReview();
    resource.apiVersion = "authorization.k8s.io/v1";
    resource.kind = "SelfSubjectRulesReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
