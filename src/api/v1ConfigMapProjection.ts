import { V1ConfigMapProjection, V1KeyToPath } from "@kubernetes/client-node";
export declare interface V1ConfigMapProjectionOptions {
    items?: V1KeyToPath[];
    name?: string;
    optional?: boolean;
}
export function createV1ConfigMapProjection(options: V1ConfigMapProjectionOptions = {}): V1ConfigMapProjection {
    const resource = new V1ConfigMapProjection();
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
