import { V1ClusterRoleBinding, V1ObjectMeta, V1RoleRef, V1Subject } from "@kubernetes/client-node";
export declare interface V1ClusterRoleBindingOptions {
    metadata?: V1ObjectMeta;
    roleRef: V1RoleRef;
    subjects?: V1Subject[];
}
export function createV1ClusterRoleBinding(options: V1ClusterRoleBindingOptions): V1ClusterRoleBinding {
    const resource = new V1ClusterRoleBinding();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "ClusterRoleBinding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.roleRef !== "undefined") {
        resource.roleRef = options.roleRef;
    }
    if (typeof options.subjects !== "undefined") {
        resource.subjects = options.subjects;
    }
    return resource;
}
