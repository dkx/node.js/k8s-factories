import { V1beta2ReplicaSet, V1ObjectMeta, V1beta2ReplicaSetSpec, V1beta2ReplicaSetStatus } from "@kubernetes/client-node";
export declare interface V1beta2ReplicaSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta2ReplicaSetSpec;
    status?: V1beta2ReplicaSetStatus;
}
export function createV1beta2ReplicaSet(options: V1beta2ReplicaSetOptions = {}): V1beta2ReplicaSet {
    const resource = new V1beta2ReplicaSet();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "ReplicaSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
