import { V1Endpoints, V1ObjectMeta, V1EndpointSubset } from "@kubernetes/client-node";
export declare interface V1EndpointsOptions {
    metadata?: V1ObjectMeta;
    subsets?: V1EndpointSubset[];
}
export function createV1Endpoints(options: V1EndpointsOptions = {}): V1Endpoints {
    const resource = new V1Endpoints();
    resource.apiVersion = "v1";
    resource.kind = "Endpoints";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.subsets !== "undefined") {
        resource.subsets = options.subsets;
    }
    return resource;
}
