import { V1alpha1RoleList, V1alpha1Role, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1RoleListOptions {
    items: V1alpha1Role[];
    metadata?: V1ListMeta;
}
export function createV1alpha1RoleList(options: V1alpha1RoleListOptions): V1alpha1RoleList {
    const resource = new V1alpha1RoleList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "RoleList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
