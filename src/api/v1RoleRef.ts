import { V1RoleRef } from "@kubernetes/client-node";
export declare interface V1RoleRefOptions {
    apiGroup: string;
    kind: string;
    name: string;
}
export function createV1RoleRef(options: V1RoleRefOptions): V1RoleRef {
    const resource = new V1RoleRef();
    if (typeof options.apiGroup !== "undefined") {
        resource.apiGroup = options.apiGroup;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
