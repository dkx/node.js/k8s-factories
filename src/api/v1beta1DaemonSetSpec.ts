import { V1beta1DaemonSetSpec, V1LabelSelector, V1PodTemplateSpec, V1beta1DaemonSetUpdateStrategy } from "@kubernetes/client-node";
export declare interface V1beta1DaemonSetSpecOptions {
    minReadySeconds?: number;
    revisionHistoryLimit?: number;
    selector?: V1LabelSelector;
    template: V1PodTemplateSpec;
    templateGeneration?: number;
    updateStrategy?: V1beta1DaemonSetUpdateStrategy;
}
export function createV1beta1DaemonSetSpec(options: V1beta1DaemonSetSpecOptions): V1beta1DaemonSetSpec {
    const resource = new V1beta1DaemonSetSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.revisionHistoryLimit !== "undefined") {
        resource.revisionHistoryLimit = options.revisionHistoryLimit;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    if (typeof options.templateGeneration !== "undefined") {
        resource.templateGeneration = options.templateGeneration;
    }
    if (typeof options.updateStrategy !== "undefined") {
        resource.updateStrategy = options.updateStrategy;
    }
    return resource;
}
