import { V1ServiceSpec, V1ServicePort, V1SessionAffinityConfig } from "@kubernetes/client-node";
export declare interface V1ServiceSpecOptions {
    clusterIP?: string;
    externalIPs?: string[];
    externalName?: string;
    externalTrafficPolicy?: string;
    healthCheckNodePort?: number;
    loadBalancerIP?: string;
    loadBalancerSourceRanges?: string[];
    ports?: V1ServicePort[];
    publishNotReadyAddresses?: boolean;
    selector?: {
        [key: string]: string;
    };
    sessionAffinity?: string;
    sessionAffinityConfig?: V1SessionAffinityConfig;
    type?: string;
}
export function createV1ServiceSpec(options: V1ServiceSpecOptions = {}): V1ServiceSpec {
    const resource = new V1ServiceSpec();
    if (typeof options.clusterIP !== "undefined") {
        resource.clusterIP = options.clusterIP;
    }
    if (typeof options.externalIPs !== "undefined") {
        resource.externalIPs = options.externalIPs;
    }
    if (typeof options.externalName !== "undefined") {
        resource.externalName = options.externalName;
    }
    if (typeof options.externalTrafficPolicy !== "undefined") {
        resource.externalTrafficPolicy = options.externalTrafficPolicy;
    }
    if (typeof options.healthCheckNodePort !== "undefined") {
        resource.healthCheckNodePort = options.healthCheckNodePort;
    }
    if (typeof options.loadBalancerIP !== "undefined") {
        resource.loadBalancerIP = options.loadBalancerIP;
    }
    if (typeof options.loadBalancerSourceRanges !== "undefined") {
        resource.loadBalancerSourceRanges = options.loadBalancerSourceRanges;
    }
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    if (typeof options.publishNotReadyAddresses !== "undefined") {
        resource.publishNotReadyAddresses = options.publishNotReadyAddresses;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.sessionAffinity !== "undefined") {
        resource.sessionAffinity = options.sessionAffinity;
    }
    if (typeof options.sessionAffinityConfig !== "undefined") {
        resource.sessionAffinityConfig = options.sessionAffinityConfig;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
