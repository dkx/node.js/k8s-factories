import { V2beta1ExternalMetricStatus, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V2beta1ExternalMetricStatusOptions {
    currentAverageValue?: string;
    currentValue: string;
    metricName: string;
    metricSelector?: V1LabelSelector;
}
export function createV2beta1ExternalMetricStatus(options: V2beta1ExternalMetricStatusOptions): V2beta1ExternalMetricStatus {
    const resource = new V2beta1ExternalMetricStatus();
    if (typeof options.currentAverageValue !== "undefined") {
        resource.currentAverageValue = options.currentAverageValue;
    }
    if (typeof options.currentValue !== "undefined") {
        resource.currentValue = options.currentValue;
    }
    if (typeof options.metricName !== "undefined") {
        resource.metricName = options.metricName;
    }
    if (typeof options.metricSelector !== "undefined") {
        resource.metricSelector = options.metricSelector;
    }
    return resource;
}
