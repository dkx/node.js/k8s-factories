import { V1DeploymentSpec, V1LabelSelector, V1DeploymentStrategy, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface V1DeploymentSpecOptions {
    minReadySeconds?: number;
    paused?: boolean;
    progressDeadlineSeconds?: number;
    replicas?: number;
    revisionHistoryLimit?: number;
    selector: V1LabelSelector;
    strategy?: V1DeploymentStrategy;
    template: V1PodTemplateSpec;
}
export function createV1DeploymentSpec(options: V1DeploymentSpecOptions): V1DeploymentSpec {
    const resource = new V1DeploymentSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.paused !== "undefined") {
        resource.paused = options.paused;
    }
    if (typeof options.progressDeadlineSeconds !== "undefined") {
        resource.progressDeadlineSeconds = options.progressDeadlineSeconds;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.revisionHistoryLimit !== "undefined") {
        resource.revisionHistoryLimit = options.revisionHistoryLimit;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.strategy !== "undefined") {
        resource.strategy = options.strategy;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    return resource;
}
