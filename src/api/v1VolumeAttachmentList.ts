import { V1VolumeAttachmentList, V1VolumeAttachment, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1VolumeAttachmentListOptions {
    items: V1VolumeAttachment[];
    metadata?: V1ListMeta;
}
export function createV1VolumeAttachmentList(options: V1VolumeAttachmentListOptions): V1VolumeAttachmentList {
    const resource = new V1VolumeAttachmentList();
    resource.apiVersion = "storage.k8s.io/v1";
    resource.kind = "VolumeAttachmentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
