import { V1beta1ControllerRevisionList, V1beta1ControllerRevision, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1ControllerRevisionListOptions {
    items: V1beta1ControllerRevision[];
    metadata?: V1ListMeta;
}
export function createV1beta1ControllerRevisionList(options: V1beta1ControllerRevisionListOptions): V1beta1ControllerRevisionList {
    const resource = new V1beta1ControllerRevisionList();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "ControllerRevisionList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
