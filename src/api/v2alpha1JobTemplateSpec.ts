import { V2alpha1JobTemplateSpec, V1ObjectMeta, V1JobSpec } from "@kubernetes/client-node";
export declare interface V2alpha1JobTemplateSpecOptions {
    metadata?: V1ObjectMeta;
    spec?: V1JobSpec;
}
export function createV2alpha1JobTemplateSpec(options: V2alpha1JobTemplateSpecOptions = {}): V2alpha1JobTemplateSpec {
    const resource = new V2alpha1JobTemplateSpec();
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
