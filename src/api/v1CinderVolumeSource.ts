import { V1CinderVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1CinderVolumeSourceOptions {
    fsType?: string;
    readOnly?: boolean;
    secretRef?: V1LocalObjectReference;
    volumeID: string;
}
export function createV1CinderVolumeSource(options: V1CinderVolumeSourceOptions): V1CinderVolumeSource {
    const resource = new V1CinderVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.volumeID !== "undefined") {
        resource.volumeID = options.volumeID;
    }
    return resource;
}
