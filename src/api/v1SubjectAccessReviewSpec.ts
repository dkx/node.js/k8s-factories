import { V1SubjectAccessReviewSpec, V1NonResourceAttributes, V1ResourceAttributes } from "@kubernetes/client-node";
export declare interface V1SubjectAccessReviewSpecOptions {
    extra?: {
        [key: string]: string[];
    };
    groups?: string[];
    nonResourceAttributes?: V1NonResourceAttributes;
    resourceAttributes?: V1ResourceAttributes;
    uid?: string;
    user?: string;
}
export function createV1SubjectAccessReviewSpec(options: V1SubjectAccessReviewSpecOptions = {}): V1SubjectAccessReviewSpec {
    const resource = new V1SubjectAccessReviewSpec();
    if (typeof options.extra !== "undefined") {
        resource.extra = options.extra;
    }
    if (typeof options.groups !== "undefined") {
        resource.groups = options.groups;
    }
    if (typeof options.nonResourceAttributes !== "undefined") {
        resource.nonResourceAttributes = options.nonResourceAttributes;
    }
    if (typeof options.resourceAttributes !== "undefined") {
        resource.resourceAttributes = options.resourceAttributes;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
