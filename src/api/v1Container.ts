import { V1Container, V1EnvVar, V1EnvFromSource, V1Lifecycle, V1Probe, V1ContainerPort, V1ResourceRequirements, V1SecurityContext, V1VolumeDevice, V1VolumeMount } from "@kubernetes/client-node";
export declare interface V1ContainerOptions {
    args?: string[];
    command?: string[];
    env?: V1EnvVar[];
    envFrom?: V1EnvFromSource[];
    image?: string;
    imagePullPolicy?: string;
    lifecycle?: V1Lifecycle;
    livenessProbe?: V1Probe;
    name: string;
    ports?: V1ContainerPort[];
    readinessProbe?: V1Probe;
    resources?: V1ResourceRequirements;
    securityContext?: V1SecurityContext;
    stdin?: boolean;
    stdinOnce?: boolean;
    terminationMessagePath?: string;
    terminationMessagePolicy?: string;
    tty?: boolean;
    volumeDevices?: V1VolumeDevice[];
    volumeMounts?: V1VolumeMount[];
    workingDir?: string;
}
export function createV1Container(options: V1ContainerOptions): V1Container {
    const resource = new V1Container();
    if (typeof options.args !== "undefined") {
        resource.args = options.args;
    }
    if (typeof options.command !== "undefined") {
        resource.command = options.command;
    }
    if (typeof options.env !== "undefined") {
        resource.env = options.env;
    }
    if (typeof options.envFrom !== "undefined") {
        resource.envFrom = options.envFrom;
    }
    if (typeof options.image !== "undefined") {
        resource.image = options.image;
    }
    if (typeof options.imagePullPolicy !== "undefined") {
        resource.imagePullPolicy = options.imagePullPolicy;
    }
    if (typeof options.lifecycle !== "undefined") {
        resource.lifecycle = options.lifecycle;
    }
    if (typeof options.livenessProbe !== "undefined") {
        resource.livenessProbe = options.livenessProbe;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    if (typeof options.readinessProbe !== "undefined") {
        resource.readinessProbe = options.readinessProbe;
    }
    if (typeof options.resources !== "undefined") {
        resource.resources = options.resources;
    }
    if (typeof options.securityContext !== "undefined") {
        resource.securityContext = options.securityContext;
    }
    if (typeof options.stdin !== "undefined") {
        resource.stdin = options.stdin;
    }
    if (typeof options.stdinOnce !== "undefined") {
        resource.stdinOnce = options.stdinOnce;
    }
    if (typeof options.terminationMessagePath !== "undefined") {
        resource.terminationMessagePath = options.terminationMessagePath;
    }
    if (typeof options.terminationMessagePolicy !== "undefined") {
        resource.terminationMessagePolicy = options.terminationMessagePolicy;
    }
    if (typeof options.tty !== "undefined") {
        resource.tty = options.tty;
    }
    if (typeof options.volumeDevices !== "undefined") {
        resource.volumeDevices = options.volumeDevices;
    }
    if (typeof options.volumeMounts !== "undefined") {
        resource.volumeMounts = options.volumeMounts;
    }
    if (typeof options.workingDir !== "undefined") {
        resource.workingDir = options.workingDir;
    }
    return resource;
}
