import { V1KeyToPath } from "@kubernetes/client-node";
export declare interface V1KeyToPathOptions {
    key: string;
    mode?: number;
    path: string;
}
export function createV1KeyToPath(options: V1KeyToPathOptions): V1KeyToPath {
    const resource = new V1KeyToPath();
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.mode !== "undefined") {
        resource.mode = options.mode;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    return resource;
}
