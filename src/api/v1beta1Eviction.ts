import { V1beta1Eviction, V1DeleteOptions, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1beta1EvictionOptions {
    deleteOptions?: V1DeleteOptions;
    metadata?: V1ObjectMeta;
}
export function createV1beta1Eviction(options: V1beta1EvictionOptions = {}): V1beta1Eviction {
    const resource = new V1beta1Eviction();
    resource.apiVersion = "policy/v1beta1";
    resource.kind = "Eviction";
    if (typeof options.deleteOptions !== "undefined") {
        resource.deleteOptions = options.deleteOptions;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
