import { V1LoadBalancerStatus, V1LoadBalancerIngress } from "@kubernetes/client-node";
export declare interface V1LoadBalancerStatusOptions {
    ingress?: V1LoadBalancerIngress[];
}
export function createV1LoadBalancerStatus(options: V1LoadBalancerStatusOptions = {}): V1LoadBalancerStatus {
    const resource = new V1LoadBalancerStatus();
    if (typeof options.ingress !== "undefined") {
        resource.ingress = options.ingress;
    }
    return resource;
}
