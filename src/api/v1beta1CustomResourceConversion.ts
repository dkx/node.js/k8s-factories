import { V1beta1CustomResourceConversion, ApiextensionsV1beta1WebhookClientConfig } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceConversionOptions {
    conversionReviewVersions?: string[];
    strategy: string;
    webhookClientConfig?: ApiextensionsV1beta1WebhookClientConfig;
}
export function createV1beta1CustomResourceConversion(options: V1beta1CustomResourceConversionOptions): V1beta1CustomResourceConversion {
    const resource = new V1beta1CustomResourceConversion();
    if (typeof options.conversionReviewVersions !== "undefined") {
        resource.conversionReviewVersions = options.conversionReviewVersions;
    }
    if (typeof options.strategy !== "undefined") {
        resource.strategy = options.strategy;
    }
    if (typeof options.webhookClientConfig !== "undefined") {
        resource.webhookClientConfig = options.webhookClientConfig;
    }
    return resource;
}
