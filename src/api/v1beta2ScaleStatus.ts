import { V1beta2ScaleStatus } from "@kubernetes/client-node";
export declare interface V1beta2ScaleStatusOptions {
    replicas: number;
    selector?: {
        [key: string]: string;
    };
    targetSelector?: string;
}
export function createV1beta2ScaleStatus(options: V1beta2ScaleStatusOptions): V1beta2ScaleStatus {
    const resource = new V1beta2ScaleStatus();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.targetSelector !== "undefined") {
        resource.targetSelector = options.targetSelector;
    }
    return resource;
}
