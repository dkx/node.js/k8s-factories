import { V1DaemonSetSpec, V1LabelSelector, V1PodTemplateSpec, V1DaemonSetUpdateStrategy } from "@kubernetes/client-node";
export declare interface V1DaemonSetSpecOptions {
    minReadySeconds?: number;
    revisionHistoryLimit?: number;
    selector: V1LabelSelector;
    template: V1PodTemplateSpec;
    updateStrategy?: V1DaemonSetUpdateStrategy;
}
export function createV1DaemonSetSpec(options: V1DaemonSetSpecOptions): V1DaemonSetSpec {
    const resource = new V1DaemonSetSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.revisionHistoryLimit !== "undefined") {
        resource.revisionHistoryLimit = options.revisionHistoryLimit;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    if (typeof options.updateStrategy !== "undefined") {
        resource.updateStrategy = options.updateStrategy;
    }
    return resource;
}
