import { V1ControllerRevisionList, V1ControllerRevision, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ControllerRevisionListOptions {
    items: V1ControllerRevision[];
    metadata?: V1ListMeta;
}
export function createV1ControllerRevisionList(options: V1ControllerRevisionListOptions): V1ControllerRevisionList {
    const resource = new V1ControllerRevisionList();
    resource.apiVersion = "apps/v1";
    resource.kind = "ControllerRevisionList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
