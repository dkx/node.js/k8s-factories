import { V1DaemonSet, V1ObjectMeta, V1DaemonSetSpec, V1DaemonSetStatus } from "@kubernetes/client-node";
export declare interface V1DaemonSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1DaemonSetSpec;
    status?: V1DaemonSetStatus;
}
export function createV1DaemonSet(options: V1DaemonSetOptions = {}): V1DaemonSet {
    const resource = new V1DaemonSet();
    resource.apiVersion = "apps/v1";
    resource.kind = "DaemonSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
