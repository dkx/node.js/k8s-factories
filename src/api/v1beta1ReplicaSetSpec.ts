import { V1beta1ReplicaSetSpec, V1LabelSelector, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface V1beta1ReplicaSetSpecOptions {
    minReadySeconds?: number;
    replicas?: number;
    selector?: V1LabelSelector;
    template?: V1PodTemplateSpec;
}
export function createV1beta1ReplicaSetSpec(options: V1beta1ReplicaSetSpecOptions = {}): V1beta1ReplicaSetSpec {
    const resource = new V1beta1ReplicaSetSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    return resource;
}
