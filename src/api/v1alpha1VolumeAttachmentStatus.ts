import { V1alpha1VolumeAttachmentStatus, V1alpha1VolumeError } from "@kubernetes/client-node";
export declare interface V1alpha1VolumeAttachmentStatusOptions {
    attachError?: V1alpha1VolumeError;
    attached: boolean;
    attachmentMetadata?: {
        [key: string]: string;
    };
    detachError?: V1alpha1VolumeError;
}
export function createV1alpha1VolumeAttachmentStatus(options: V1alpha1VolumeAttachmentStatusOptions): V1alpha1VolumeAttachmentStatus {
    const resource = new V1alpha1VolumeAttachmentStatus();
    if (typeof options.attachError !== "undefined") {
        resource.attachError = options.attachError;
    }
    if (typeof options.attached !== "undefined") {
        resource.attached = options.attached;
    }
    if (typeof options.attachmentMetadata !== "undefined") {
        resource.attachmentMetadata = options.attachmentMetadata;
    }
    if (typeof options.detachError !== "undefined") {
        resource.detachError = options.detachError;
    }
    return resource;
}
