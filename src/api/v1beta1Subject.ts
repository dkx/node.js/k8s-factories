import { V1beta1Subject } from "@kubernetes/client-node";
export declare interface V1beta1SubjectOptions {
    apiGroup?: string;
    kind: string;
    name: string;
    namespace?: string;
}
export function createV1beta1Subject(options: V1beta1SubjectOptions): V1beta1Subject {
    const resource = new V1beta1Subject();
    if (typeof options.apiGroup !== "undefined") {
        resource.apiGroup = options.apiGroup;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    return resource;
}
