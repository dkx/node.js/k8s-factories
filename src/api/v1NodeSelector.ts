import { V1NodeSelector, V1NodeSelectorTerm } from "@kubernetes/client-node";
export declare interface V1NodeSelectorOptions {
    nodeSelectorTerms: V1NodeSelectorTerm[];
}
export function createV1NodeSelector(options: V1NodeSelectorOptions): V1NodeSelector {
    const resource = new V1NodeSelector();
    if (typeof options.nodeSelectorTerms !== "undefined") {
        resource.nodeSelectorTerms = options.nodeSelectorTerms;
    }
    return resource;
}
