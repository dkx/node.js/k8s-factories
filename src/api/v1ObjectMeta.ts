import { V1ObjectMeta, V1Initializers, V1ManagedFieldsEntry, V1OwnerReference } from "@kubernetes/client-node";
export declare interface V1ObjectMetaOptions {
    annotations?: {
        [key: string]: string;
    };
    clusterName?: string;
    creationTimestamp?: Date;
    deletionGracePeriodSeconds?: number;
    deletionTimestamp?: Date;
    finalizers?: string[];
    generateName?: string;
    generation?: number;
    initializers?: V1Initializers;
    labels?: {
        [key: string]: string;
    };
    managedFields?: V1ManagedFieldsEntry[];
    name?: string;
    namespace?: string;
    ownerReferences?: V1OwnerReference[];
    resourceVersion?: string;
    selfLink?: string;
    uid?: string;
}
export function createV1ObjectMeta(options: V1ObjectMetaOptions = {}): V1ObjectMeta {
    const resource = new V1ObjectMeta();
    if (typeof options.annotations !== "undefined") {
        resource.annotations = options.annotations;
    }
    if (typeof options.clusterName !== "undefined") {
        resource.clusterName = options.clusterName;
    }
    if (typeof options.creationTimestamp !== "undefined") {
        resource.creationTimestamp = options.creationTimestamp;
    }
    if (typeof options.deletionGracePeriodSeconds !== "undefined") {
        resource.deletionGracePeriodSeconds = options.deletionGracePeriodSeconds;
    }
    if (typeof options.deletionTimestamp !== "undefined") {
        resource.deletionTimestamp = options.deletionTimestamp;
    }
    if (typeof options.finalizers !== "undefined") {
        resource.finalizers = options.finalizers;
    }
    if (typeof options.generateName !== "undefined") {
        resource.generateName = options.generateName;
    }
    if (typeof options.generation !== "undefined") {
        resource.generation = options.generation;
    }
    if (typeof options.initializers !== "undefined") {
        resource.initializers = options.initializers;
    }
    if (typeof options.labels !== "undefined") {
        resource.labels = options.labels;
    }
    if (typeof options.managedFields !== "undefined") {
        resource.managedFields = options.managedFields;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.ownerReferences !== "undefined") {
        resource.ownerReferences = options.ownerReferences;
    }
    if (typeof options.resourceVersion !== "undefined") {
        resource.resourceVersion = options.resourceVersion;
    }
    if (typeof options.selfLink !== "undefined") {
        resource.selfLink = options.selfLink;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    return resource;
}
