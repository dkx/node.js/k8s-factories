import { V1beta1IPBlock } from "@kubernetes/client-node";
export declare interface V1beta1IPBlockOptions {
    cidr: string;
    except?: string[];
}
export function createV1beta1IPBlock(options: V1beta1IPBlockOptions): V1beta1IPBlock {
    const resource = new V1beta1IPBlock();
    if (typeof options.cidr !== "undefined") {
        resource.cidr = options.cidr;
    }
    if (typeof options.except !== "undefined") {
        resource.except = options.except;
    }
    return resource;
}
