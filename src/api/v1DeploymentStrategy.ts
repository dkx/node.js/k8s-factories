import { V1DeploymentStrategy, V1RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface V1DeploymentStrategyOptions {
    rollingUpdate?: V1RollingUpdateDeployment;
    type?: string;
}
export function createV1DeploymentStrategy(options: V1DeploymentStrategyOptions = {}): V1DeploymentStrategy {
    const resource = new V1DeploymentStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
