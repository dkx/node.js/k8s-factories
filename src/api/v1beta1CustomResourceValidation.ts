import { V1beta1CustomResourceValidation, V1beta1JSONSchemaProps } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceValidationOptions {
    openAPIV3Schema?: V1beta1JSONSchemaProps;
}
export function createV1beta1CustomResourceValidation(options: V1beta1CustomResourceValidationOptions = {}): V1beta1CustomResourceValidation {
    const resource = new V1beta1CustomResourceValidation();
    if (typeof options.openAPIV3Schema !== "undefined") {
        resource.openAPIV3Schema = options.openAPIV3Schema;
    }
    return resource;
}
