import { V1ConfigMapNodeConfigSource } from "@kubernetes/client-node";
export declare interface V1ConfigMapNodeConfigSourceOptions {
    kubeletConfigKey: string;
    name: string;
    namespace: string;
    resourceVersion?: string;
    uid?: string;
}
export function createV1ConfigMapNodeConfigSource(options: V1ConfigMapNodeConfigSourceOptions): V1ConfigMapNodeConfigSource {
    const resource = new V1ConfigMapNodeConfigSource();
    if (typeof options.kubeletConfigKey !== "undefined") {
        resource.kubeletConfigKey = options.kubeletConfigKey;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.resourceVersion !== "undefined") {
        resource.resourceVersion = options.resourceVersion;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    return resource;
}
