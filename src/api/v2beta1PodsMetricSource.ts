import { V2beta1PodsMetricSource, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V2beta1PodsMetricSourceOptions {
    metricName: string;
    selector?: V1LabelSelector;
    targetAverageValue: string;
}
export function createV2beta1PodsMetricSource(options: V2beta1PodsMetricSourceOptions): V2beta1PodsMetricSource {
    const resource = new V2beta1PodsMetricSource();
    if (typeof options.metricName !== "undefined") {
        resource.metricName = options.metricName;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.targetAverageValue !== "undefined") {
        resource.targetAverageValue = options.targetAverageValue;
    }
    return resource;
}
