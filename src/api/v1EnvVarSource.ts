import { V1EnvVarSource, V1ConfigMapKeySelector, V1ObjectFieldSelector, V1ResourceFieldSelector, V1SecretKeySelector } from "@kubernetes/client-node";
export declare interface V1EnvVarSourceOptions {
    configMapKeyRef?: V1ConfigMapKeySelector;
    fieldRef?: V1ObjectFieldSelector;
    resourceFieldRef?: V1ResourceFieldSelector;
    secretKeyRef?: V1SecretKeySelector;
}
export function createV1EnvVarSource(options: V1EnvVarSourceOptions = {}): V1EnvVarSource {
    const resource = new V1EnvVarSource();
    if (typeof options.configMapKeyRef !== "undefined") {
        resource.configMapKeyRef = options.configMapKeyRef;
    }
    if (typeof options.fieldRef !== "undefined") {
        resource.fieldRef = options.fieldRef;
    }
    if (typeof options.resourceFieldRef !== "undefined") {
        resource.resourceFieldRef = options.resourceFieldRef;
    }
    if (typeof options.secretKeyRef !== "undefined") {
        resource.secretKeyRef = options.secretKeyRef;
    }
    return resource;
}
