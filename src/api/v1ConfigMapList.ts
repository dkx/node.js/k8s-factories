import { V1ConfigMapList, V1ConfigMap, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ConfigMapListOptions {
    items: V1ConfigMap[];
    metadata?: V1ListMeta;
}
export function createV1ConfigMapList(options: V1ConfigMapListOptions): V1ConfigMapList {
    const resource = new V1ConfigMapList();
    resource.apiVersion = "v1";
    resource.kind = "ConfigMapList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
