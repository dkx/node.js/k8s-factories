import { V1beta1JobTemplateSpec, V1ObjectMeta, V1JobSpec } from "@kubernetes/client-node";
export declare interface V1beta1JobTemplateSpecOptions {
    metadata?: V1ObjectMeta;
    spec?: V1JobSpec;
}
export function createV1beta1JobTemplateSpec(options: V1beta1JobTemplateSpecOptions = {}): V1beta1JobTemplateSpec {
    const resource = new V1beta1JobTemplateSpec();
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
