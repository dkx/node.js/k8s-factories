import { V1alpha1WebhookThrottleConfig } from "@kubernetes/client-node";
export declare interface V1alpha1WebhookThrottleConfigOptions {
    burst?: number;
    qps?: number;
}
export function createV1alpha1WebhookThrottleConfig(options: V1alpha1WebhookThrottleConfigOptions = {}): V1alpha1WebhookThrottleConfig {
    const resource = new V1alpha1WebhookThrottleConfig();
    if (typeof options.burst !== "undefined") {
        resource.burst = options.burst;
    }
    if (typeof options.qps !== "undefined") {
        resource.qps = options.qps;
    }
    return resource;
}
