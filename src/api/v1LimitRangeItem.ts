import { V1LimitRangeItem } from "@kubernetes/client-node";
export declare interface V1LimitRangeItemOptions {
    _default?: {
        [key: string]: string;
    };
    defaultRequest?: {
        [key: string]: string;
    };
    max?: {
        [key: string]: string;
    };
    maxLimitRequestRatio?: {
        [key: string]: string;
    };
    min?: {
        [key: string]: string;
    };
    type?: string;
}
export function createV1LimitRangeItem(options: V1LimitRangeItemOptions = {}): V1LimitRangeItem {
    const resource = new V1LimitRangeItem();
    if (typeof options._default !== "undefined") {
        resource._default = options._default;
    }
    if (typeof options.defaultRequest !== "undefined") {
        resource.defaultRequest = options.defaultRequest;
    }
    if (typeof options.max !== "undefined") {
        resource.max = options.max;
    }
    if (typeof options.maxLimitRequestRatio !== "undefined") {
        resource.maxLimitRequestRatio = options.maxLimitRequestRatio;
    }
    if (typeof options.min !== "undefined") {
        resource.min = options.min;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
