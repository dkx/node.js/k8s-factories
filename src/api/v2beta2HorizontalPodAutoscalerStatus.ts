import { V2beta2HorizontalPodAutoscalerStatus, V2beta2HorizontalPodAutoscalerCondition, V2beta2MetricStatus } from "@kubernetes/client-node";
export declare interface V2beta2HorizontalPodAutoscalerStatusOptions {
    conditions: V2beta2HorizontalPodAutoscalerCondition[];
    currentMetrics?: V2beta2MetricStatus[];
    currentReplicas: number;
    desiredReplicas: number;
    lastScaleTime?: Date;
    observedGeneration?: number;
}
export function createV2beta2HorizontalPodAutoscalerStatus(options: V2beta2HorizontalPodAutoscalerStatusOptions): V2beta2HorizontalPodAutoscalerStatus {
    const resource = new V2beta2HorizontalPodAutoscalerStatus();
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.currentMetrics !== "undefined") {
        resource.currentMetrics = options.currentMetrics;
    }
    if (typeof options.currentReplicas !== "undefined") {
        resource.currentReplicas = options.currentReplicas;
    }
    if (typeof options.desiredReplicas !== "undefined") {
        resource.desiredReplicas = options.desiredReplicas;
    }
    if (typeof options.lastScaleTime !== "undefined") {
        resource.lastScaleTime = options.lastScaleTime;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    return resource;
}
