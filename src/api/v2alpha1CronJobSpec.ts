import { V2alpha1CronJobSpec, V2alpha1JobTemplateSpec } from "@kubernetes/client-node";
export declare interface V2alpha1CronJobSpecOptions {
    concurrencyPolicy?: string;
    failedJobsHistoryLimit?: number;
    jobTemplate: V2alpha1JobTemplateSpec;
    schedule: string;
    startingDeadlineSeconds?: number;
    successfulJobsHistoryLimit?: number;
    suspend?: boolean;
}
export function createV2alpha1CronJobSpec(options: V2alpha1CronJobSpecOptions): V2alpha1CronJobSpec {
    const resource = new V2alpha1CronJobSpec();
    if (typeof options.concurrencyPolicy !== "undefined") {
        resource.concurrencyPolicy = options.concurrencyPolicy;
    }
    if (typeof options.failedJobsHistoryLimit !== "undefined") {
        resource.failedJobsHistoryLimit = options.failedJobsHistoryLimit;
    }
    if (typeof options.jobTemplate !== "undefined") {
        resource.jobTemplate = options.jobTemplate;
    }
    if (typeof options.schedule !== "undefined") {
        resource.schedule = options.schedule;
    }
    if (typeof options.startingDeadlineSeconds !== "undefined") {
        resource.startingDeadlineSeconds = options.startingDeadlineSeconds;
    }
    if (typeof options.successfulJobsHistoryLimit !== "undefined") {
        resource.successfulJobsHistoryLimit = options.successfulJobsHistoryLimit;
    }
    if (typeof options.suspend !== "undefined") {
        resource.suspend = options.suspend;
    }
    return resource;
}
