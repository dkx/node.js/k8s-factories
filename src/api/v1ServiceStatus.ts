import { V1ServiceStatus, V1LoadBalancerStatus } from "@kubernetes/client-node";
export declare interface V1ServiceStatusOptions {
    loadBalancer?: V1LoadBalancerStatus;
}
export function createV1ServiceStatus(options: V1ServiceStatusOptions = {}): V1ServiceStatus {
    const resource = new V1ServiceStatus();
    if (typeof options.loadBalancer !== "undefined") {
        resource.loadBalancer = options.loadBalancer;
    }
    return resource;
}
