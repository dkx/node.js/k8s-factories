import { ExtensionsV1beta1IngressSpec, ExtensionsV1beta1IngressBackend, ExtensionsV1beta1IngressRule, ExtensionsV1beta1IngressTLS } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressSpecOptions {
    backend?: ExtensionsV1beta1IngressBackend;
    rules?: ExtensionsV1beta1IngressRule[];
    tls?: ExtensionsV1beta1IngressTLS[];
}
export function createExtensionsV1beta1IngressSpec(options: ExtensionsV1beta1IngressSpecOptions = {}): ExtensionsV1beta1IngressSpec {
    const resource = new ExtensionsV1beta1IngressSpec();
    if (typeof options.backend !== "undefined") {
        resource.backend = options.backend;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    if (typeof options.tls !== "undefined") {
        resource.tls = options.tls;
    }
    return resource;
}
