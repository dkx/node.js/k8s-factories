import { V1beta2StatefulSet, V1ObjectMeta, V1beta2StatefulSetSpec, V1beta2StatefulSetStatus } from "@kubernetes/client-node";
export declare interface V1beta2StatefulSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta2StatefulSetSpec;
    status?: V1beta2StatefulSetStatus;
}
export function createV1beta2StatefulSet(options: V1beta2StatefulSetOptions = {}): V1beta2StatefulSet {
    const resource = new V1beta2StatefulSet();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "StatefulSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
