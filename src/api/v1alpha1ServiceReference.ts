import { V1alpha1ServiceReference } from "@kubernetes/client-node";
export declare interface V1alpha1ServiceReferenceOptions {
    name: string;
    namespace: string;
    path?: string;
    port?: number;
}
export function createV1alpha1ServiceReference(options: V1alpha1ServiceReferenceOptions): V1alpha1ServiceReference {
    const resource = new V1alpha1ServiceReference();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    return resource;
}
