import { V1PersistentVolumeStatus } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeStatusOptions {
    message?: string;
    phase?: string;
    reason?: string;
}
export function createV1PersistentVolumeStatus(options: V1PersistentVolumeStatusOptions = {}): V1PersistentVolumeStatus {
    const resource = new V1PersistentVolumeStatus();
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.phase !== "undefined") {
        resource.phase = options.phase;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    return resource;
}
