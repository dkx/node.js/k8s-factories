import { V1LabelSelectorRequirement } from "@kubernetes/client-node";
export declare interface V1LabelSelectorRequirementOptions {
    key: string;
    operator: string;
    values?: string[];
}
export function createV1LabelSelectorRequirement(options: V1LabelSelectorRequirementOptions): V1LabelSelectorRequirement {
    const resource = new V1LabelSelectorRequirement();
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.operator !== "undefined") {
        resource.operator = options.operator;
    }
    if (typeof options.values !== "undefined") {
        resource.values = options.values;
    }
    return resource;
}
