import { V1beta1NetworkPolicy, V1ObjectMeta, V1beta1NetworkPolicySpec } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicyOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1NetworkPolicySpec;
}
export function createV1beta1NetworkPolicy(options: V1beta1NetworkPolicyOptions = {}): V1beta1NetworkPolicy {
    const resource = new V1beta1NetworkPolicy();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "NetworkPolicy";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
