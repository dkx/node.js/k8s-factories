import { V1beta1ValidatingWebhookConfigurationList, V1beta1ValidatingWebhookConfiguration, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1ValidatingWebhookConfigurationListOptions {
    items: V1beta1ValidatingWebhookConfiguration[];
    metadata?: V1ListMeta;
}
export function createV1beta1ValidatingWebhookConfigurationList(options: V1beta1ValidatingWebhookConfigurationListOptions): V1beta1ValidatingWebhookConfigurationList {
    const resource = new V1beta1ValidatingWebhookConfigurationList();
    resource.apiVersion = "admissionregistration.k8s.io/v1beta1";
    resource.kind = "ValidatingWebhookConfigurationList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
