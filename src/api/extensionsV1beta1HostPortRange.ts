import { ExtensionsV1beta1HostPortRange } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1HostPortRangeOptions {
    max: number;
    min: number;
}
export function createExtensionsV1beta1HostPortRange(options: ExtensionsV1beta1HostPortRangeOptions): ExtensionsV1beta1HostPortRange {
    const resource = new ExtensionsV1beta1HostPortRange();
    if (typeof options.max !== "undefined") {
        resource.max = options.max;
    }
    if (typeof options.min !== "undefined") {
        resource.min = options.min;
    }
    return resource;
}
