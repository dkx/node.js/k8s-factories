import { V2beta2PodsMetricSource, V2beta2MetricIdentifier, V2beta2MetricTarget } from "@kubernetes/client-node";
export declare interface V2beta2PodsMetricSourceOptions {
    metric: V2beta2MetricIdentifier;
    target: V2beta2MetricTarget;
}
export function createV2beta2PodsMetricSource(options: V2beta2PodsMetricSourceOptions): V2beta2PodsMetricSource {
    const resource = new V2beta2PodsMetricSource();
    if (typeof options.metric !== "undefined") {
        resource.metric = options.metric;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    return resource;
}
