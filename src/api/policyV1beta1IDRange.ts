import { PolicyV1beta1IDRange } from "@kubernetes/client-node";
export declare interface PolicyV1beta1IDRangeOptions {
    max: number;
    min: number;
}
export function createPolicyV1beta1IDRange(options: PolicyV1beta1IDRangeOptions): PolicyV1beta1IDRange {
    const resource = new PolicyV1beta1IDRange();
    if (typeof options.max !== "undefined") {
        resource.max = options.max;
    }
    if (typeof options.min !== "undefined") {
        resource.min = options.min;
    }
    return resource;
}
