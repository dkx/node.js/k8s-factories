import { V1Secret, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1SecretOptions {
    data?: {
        [key: string]: string;
    };
    metadata?: V1ObjectMeta;
    stringData?: {
        [key: string]: string;
    };
    type?: string;
}
export function createV1Secret(options: V1SecretOptions = {}): V1Secret {
    const resource = new V1Secret();
    resource.apiVersion = "v1";
    resource.kind = "Secret";
    if (typeof options.data !== "undefined") {
        resource.data = options.data;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.stringData !== "undefined") {
        resource.stringData = options.stringData;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
