import { V1PodAffinityTerm, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1PodAffinityTermOptions {
    labelSelector?: V1LabelSelector;
    namespaces?: string[];
    topologyKey: string;
}
export function createV1PodAffinityTerm(options: V1PodAffinityTermOptions): V1PodAffinityTerm {
    const resource = new V1PodAffinityTerm();
    if (typeof options.labelSelector !== "undefined") {
        resource.labelSelector = options.labelSelector;
    }
    if (typeof options.namespaces !== "undefined") {
        resource.namespaces = options.namespaces;
    }
    if (typeof options.topologyKey !== "undefined") {
        resource.topologyKey = options.topologyKey;
    }
    return resource;
}
