import { AppsV1beta1RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface AppsV1beta1RollingUpdateDeploymentOptions {
    maxSurge?: object;
    maxUnavailable?: object;
}
export function createAppsV1beta1RollingUpdateDeployment(options: AppsV1beta1RollingUpdateDeploymentOptions = {}): AppsV1beta1RollingUpdateDeployment {
    const resource = new AppsV1beta1RollingUpdateDeployment();
    if (typeof options.maxSurge !== "undefined") {
        resource.maxSurge = options.maxSurge;
    }
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
