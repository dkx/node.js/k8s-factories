import { V1WatchEvent, RuntimeRawExtension } from "@kubernetes/client-node";
export declare interface V1WatchEventOptions {
    object: RuntimeRawExtension;
    type: string;
}
export function createV1WatchEvent(options: V1WatchEventOptions): V1WatchEvent {
    const resource = new V1WatchEvent();
    resource.kind = "WatchEvent";
    if (typeof options.object !== "undefined") {
        resource.object = options.object;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
