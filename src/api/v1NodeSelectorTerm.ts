import { V1NodeSelectorTerm, V1NodeSelectorRequirement } from "@kubernetes/client-node";
export declare interface V1NodeSelectorTermOptions {
    matchExpressions?: V1NodeSelectorRequirement[];
    matchFields?: V1NodeSelectorRequirement[];
}
export function createV1NodeSelectorTerm(options: V1NodeSelectorTermOptions = {}): V1NodeSelectorTerm {
    const resource = new V1NodeSelectorTerm();
    if (typeof options.matchExpressions !== "undefined") {
        resource.matchExpressions = options.matchExpressions;
    }
    if (typeof options.matchFields !== "undefined") {
        resource.matchFields = options.matchFields;
    }
    return resource;
}
