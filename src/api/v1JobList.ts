import { V1JobList, V1Job, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1JobListOptions {
    items: V1Job[];
    metadata?: V1ListMeta;
}
export function createV1JobList(options: V1JobListOptions): V1JobList {
    const resource = new V1JobList();
    resource.apiVersion = "batch/v1";
    resource.kind = "JobList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
