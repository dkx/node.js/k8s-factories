import { V1beta1CustomResourceDefinitionVersion, V1beta1CustomResourceColumnDefinition, V1beta1CustomResourceValidation, V1beta1CustomResourceSubresources } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceDefinitionVersionOptions {
    additionalPrinterColumns?: V1beta1CustomResourceColumnDefinition[];
    name: string;
    schema?: V1beta1CustomResourceValidation;
    served: boolean;
    storage: boolean;
    subresources?: V1beta1CustomResourceSubresources;
}
export function createV1beta1CustomResourceDefinitionVersion(options: V1beta1CustomResourceDefinitionVersionOptions): V1beta1CustomResourceDefinitionVersion {
    const resource = new V1beta1CustomResourceDefinitionVersion();
    if (typeof options.additionalPrinterColumns !== "undefined") {
        resource.additionalPrinterColumns = options.additionalPrinterColumns;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.schema !== "undefined") {
        resource.schema = options.schema;
    }
    if (typeof options.served !== "undefined") {
        resource.served = options.served;
    }
    if (typeof options.storage !== "undefined") {
        resource.storage = options.storage;
    }
    if (typeof options.subresources !== "undefined") {
        resource.subresources = options.subresources;
    }
    return resource;
}
