import { V1alpha1WebhookClientConfig, V1alpha1ServiceReference } from "@kubernetes/client-node";
export declare interface V1alpha1WebhookClientConfigOptions {
    caBundle?: string;
    service?: V1alpha1ServiceReference;
    url?: string;
}
export function createV1alpha1WebhookClientConfig(options: V1alpha1WebhookClientConfigOptions = {}): V1alpha1WebhookClientConfig {
    const resource = new V1alpha1WebhookClientConfig();
    if (typeof options.caBundle !== "undefined") {
        resource.caBundle = options.caBundle;
    }
    if (typeof options.service !== "undefined") {
        resource.service = options.service;
    }
    if (typeof options.url !== "undefined") {
        resource.url = options.url;
    }
    return resource;
}
