import { V1CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V1CrossVersionObjectReferenceOptions {
    apiVersion?: string;
    kind: string;
    name: string;
}
export function createV1CrossVersionObjectReference(options: V1CrossVersionObjectReferenceOptions): V1CrossVersionObjectReference {
    const resource = new V1CrossVersionObjectReference();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
