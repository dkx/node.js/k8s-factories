import { V1SecretKeySelector } from "@kubernetes/client-node";
export declare interface V1SecretKeySelectorOptions {
    key: string;
    name?: string;
    optional?: boolean;
}
export function createV1SecretKeySelector(options: V1SecretKeySelectorOptions): V1SecretKeySelector {
    const resource = new V1SecretKeySelector();
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
