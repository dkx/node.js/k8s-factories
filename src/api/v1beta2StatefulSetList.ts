import { V1beta2StatefulSetList, V1beta2StatefulSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta2StatefulSetListOptions {
    items: V1beta2StatefulSet[];
    metadata?: V1ListMeta;
}
export function createV1beta2StatefulSetList(options: V1beta2StatefulSetListOptions): V1beta2StatefulSetList {
    const resource = new V1beta2StatefulSetList();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "StatefulSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
