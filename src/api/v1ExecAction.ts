import { V1ExecAction } from "@kubernetes/client-node";
export declare interface V1ExecActionOptions {
    command?: string[];
}
export function createV1ExecAction(options: V1ExecActionOptions = {}): V1ExecAction {
    const resource = new V1ExecAction();
    if (typeof options.command !== "undefined") {
        resource.command = options.command;
    }
    return resource;
}
