import { V1GlusterfsPersistentVolumeSource } from "@kubernetes/client-node";
export declare interface V1GlusterfsPersistentVolumeSourceOptions {
    endpoints: string;
    endpointsNamespace?: string;
    path: string;
    readOnly?: boolean;
}
export function createV1GlusterfsPersistentVolumeSource(options: V1GlusterfsPersistentVolumeSourceOptions): V1GlusterfsPersistentVolumeSource {
    const resource = new V1GlusterfsPersistentVolumeSource();
    if (typeof options.endpoints !== "undefined") {
        resource.endpoints = options.endpoints;
    }
    if (typeof options.endpointsNamespace !== "undefined") {
        resource.endpointsNamespace = options.endpointsNamespace;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
