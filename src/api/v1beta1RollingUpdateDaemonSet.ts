import { V1beta1RollingUpdateDaemonSet } from "@kubernetes/client-node";
export declare interface V1beta1RollingUpdateDaemonSetOptions {
    maxUnavailable?: object;
}
export function createV1beta1RollingUpdateDaemonSet(options: V1beta1RollingUpdateDaemonSetOptions = {}): V1beta1RollingUpdateDaemonSet {
    const resource = new V1beta1RollingUpdateDaemonSet();
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
