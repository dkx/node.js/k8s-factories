import { V1PersistentVolumeList, V1PersistentVolume, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeListOptions {
    items: V1PersistentVolume[];
    metadata?: V1ListMeta;
}
export function createV1PersistentVolumeList(options: V1PersistentVolumeListOptions): V1PersistentVolumeList {
    const resource = new V1PersistentVolumeList();
    resource.apiVersion = "v1";
    resource.kind = "PersistentVolumeList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
