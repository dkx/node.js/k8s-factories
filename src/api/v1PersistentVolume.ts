import { V1PersistentVolume, V1ObjectMeta, V1PersistentVolumeSpec, V1PersistentVolumeStatus } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeOptions {
    metadata?: V1ObjectMeta;
    spec?: V1PersistentVolumeSpec;
    status?: V1PersistentVolumeStatus;
}
export function createV1PersistentVolume(options: V1PersistentVolumeOptions = {}): V1PersistentVolume {
    const resource = new V1PersistentVolume();
    resource.apiVersion = "v1";
    resource.kind = "PersistentVolume";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
