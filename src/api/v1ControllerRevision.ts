import { V1ControllerRevision, RuntimeRawExtension, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1ControllerRevisionOptions {
    data?: RuntimeRawExtension;
    metadata?: V1ObjectMeta;
    revision: number;
}
export function createV1ControllerRevision(options: V1ControllerRevisionOptions): V1ControllerRevision {
    const resource = new V1ControllerRevision();
    resource.apiVersion = "apps/v1";
    resource.kind = "ControllerRevision";
    if (typeof options.data !== "undefined") {
        resource.data = options.data;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.revision !== "undefined") {
        resource.revision = options.revision;
    }
    return resource;
}
