import { V1ConfigMap, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1ConfigMapOptions {
    binaryData?: {
        [key: string]: string;
    };
    data?: {
        [key: string]: string;
    };
    metadata?: V1ObjectMeta;
}
export function createV1ConfigMap(options: V1ConfigMapOptions = {}): V1ConfigMap {
    const resource = new V1ConfigMap();
    resource.apiVersion = "v1";
    resource.kind = "ConfigMap";
    if (typeof options.binaryData !== "undefined") {
        resource.binaryData = options.binaryData;
    }
    if (typeof options.data !== "undefined") {
        resource.data = options.data;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
