import { V1PersistentVolumeClaimSpec, V1TypedLocalObjectReference, V1ResourceRequirements, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeClaimSpecOptions {
    accessModes?: string[];
    dataSource?: V1TypedLocalObjectReference;
    resources?: V1ResourceRequirements;
    selector?: V1LabelSelector;
    storageClassName?: string;
    volumeMode?: string;
    volumeName?: string;
}
export function createV1PersistentVolumeClaimSpec(options: V1PersistentVolumeClaimSpecOptions = {}): V1PersistentVolumeClaimSpec {
    const resource = new V1PersistentVolumeClaimSpec();
    if (typeof options.accessModes !== "undefined") {
        resource.accessModes = options.accessModes;
    }
    if (typeof options.dataSource !== "undefined") {
        resource.dataSource = options.dataSource;
    }
    if (typeof options.resources !== "undefined") {
        resource.resources = options.resources;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.storageClassName !== "undefined") {
        resource.storageClassName = options.storageClassName;
    }
    if (typeof options.volumeMode !== "undefined") {
        resource.volumeMode = options.volumeMode;
    }
    if (typeof options.volumeName !== "undefined") {
        resource.volumeName = options.volumeName;
    }
    return resource;
}
