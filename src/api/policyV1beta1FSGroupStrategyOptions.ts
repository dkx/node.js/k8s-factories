import { PolicyV1beta1FSGroupStrategyOptions, PolicyV1beta1IDRange } from "@kubernetes/client-node";
export declare interface PolicyV1beta1FSGroupStrategyOptionsOptions {
    ranges?: PolicyV1beta1IDRange[];
    rule?: string;
}
export function createPolicyV1beta1FSGroupStrategyOptions(options: PolicyV1beta1FSGroupStrategyOptionsOptions = {}): PolicyV1beta1FSGroupStrategyOptions {
    const resource = new PolicyV1beta1FSGroupStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
