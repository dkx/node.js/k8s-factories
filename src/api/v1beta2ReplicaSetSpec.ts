import { V1beta2ReplicaSetSpec, V1LabelSelector, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface V1beta2ReplicaSetSpecOptions {
    minReadySeconds?: number;
    replicas?: number;
    selector: V1LabelSelector;
    template?: V1PodTemplateSpec;
}
export function createV1beta2ReplicaSetSpec(options: V1beta2ReplicaSetSpecOptions): V1beta2ReplicaSetSpec {
    const resource = new V1beta2ReplicaSetSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    return resource;
}
