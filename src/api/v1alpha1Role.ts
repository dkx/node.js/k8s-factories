import { V1alpha1Role, V1ObjectMeta, V1alpha1PolicyRule } from "@kubernetes/client-node";
export declare interface V1alpha1RoleOptions {
    metadata?: V1ObjectMeta;
    rules?: V1alpha1PolicyRule[];
}
export function createV1alpha1Role(options: V1alpha1RoleOptions = {}): V1alpha1Role {
    const resource = new V1alpha1Role();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "Role";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    return resource;
}
