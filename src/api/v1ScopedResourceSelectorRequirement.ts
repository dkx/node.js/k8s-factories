import { V1ScopedResourceSelectorRequirement } from "@kubernetes/client-node";
export declare interface V1ScopedResourceSelectorRequirementOptions {
    operator: string;
    scopeName: string;
    values?: string[];
}
export function createV1ScopedResourceSelectorRequirement(options: V1ScopedResourceSelectorRequirementOptions): V1ScopedResourceSelectorRequirement {
    const resource = new V1ScopedResourceSelectorRequirement();
    if (typeof options.operator !== "undefined") {
        resource.operator = options.operator;
    }
    if (typeof options.scopeName !== "undefined") {
        resource.scopeName = options.scopeName;
    }
    if (typeof options.values !== "undefined") {
        resource.values = options.values;
    }
    return resource;
}
