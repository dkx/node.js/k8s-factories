import { V1PolicyRule } from "@kubernetes/client-node";
export declare interface V1PolicyRuleOptions {
    apiGroups?: string[];
    nonResourceURLs?: string[];
    resourceNames?: string[];
    resources?: string[];
    verbs: string[];
}
export function createV1PolicyRule(options: V1PolicyRuleOptions): V1PolicyRule {
    const resource = new V1PolicyRule();
    if (typeof options.apiGroups !== "undefined") {
        resource.apiGroups = options.apiGroups;
    }
    if (typeof options.nonResourceURLs !== "undefined") {
        resource.nonResourceURLs = options.nonResourceURLs;
    }
    if (typeof options.resourceNames !== "undefined") {
        resource.resourceNames = options.resourceNames;
    }
    if (typeof options.resources !== "undefined") {
        resource.resources = options.resources;
    }
    if (typeof options.verbs !== "undefined") {
        resource.verbs = options.verbs;
    }
    return resource;
}
