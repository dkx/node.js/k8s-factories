import { V1ClusterRole, V1AggregationRule, V1ObjectMeta, V1PolicyRule } from "@kubernetes/client-node";
export declare interface V1ClusterRoleOptions {
    aggregationRule?: V1AggregationRule;
    metadata?: V1ObjectMeta;
    rules?: V1PolicyRule[];
}
export function createV1ClusterRole(options: V1ClusterRoleOptions = {}): V1ClusterRole {
    const resource = new V1ClusterRole();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "ClusterRole";
    if (typeof options.aggregationRule !== "undefined") {
        resource.aggregationRule = options.aggregationRule;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    return resource;
}
