import { V1beta1ReplicaSetList, V1beta1ReplicaSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1ReplicaSetListOptions {
    items: V1beta1ReplicaSet[];
    metadata?: V1ListMeta;
}
export function createV1beta1ReplicaSetList(options: V1beta1ReplicaSetListOptions): V1beta1ReplicaSetList {
    const resource = new V1beta1ReplicaSetList();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "ReplicaSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
