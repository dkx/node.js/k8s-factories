import { V1Toleration } from "@kubernetes/client-node";
export declare interface V1TolerationOptions {
    effect?: string;
    key?: string;
    operator?: string;
    tolerationSeconds?: number;
    value?: string;
}
export function createV1Toleration(options: V1TolerationOptions = {}): V1Toleration {
    const resource = new V1Toleration();
    if (typeof options.effect !== "undefined") {
        resource.effect = options.effect;
    }
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.operator !== "undefined") {
        resource.operator = options.operator;
    }
    if (typeof options.tolerationSeconds !== "undefined") {
        resource.tolerationSeconds = options.tolerationSeconds;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
