import { V1alpha1PriorityClass, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1alpha1PriorityClassOptions {
    description?: string;
    globalDefault?: boolean;
    metadata?: V1ObjectMeta;
    preemptionPolicy?: string;
    value: number;
}
export function createV1alpha1PriorityClass(options: V1alpha1PriorityClassOptions): V1alpha1PriorityClass {
    const resource = new V1alpha1PriorityClass();
    resource.apiVersion = "scheduling.k8s.io/v1alpha1";
    resource.kind = "PriorityClass";
    if (typeof options.description !== "undefined") {
        resource.description = options.description;
    }
    if (typeof options.globalDefault !== "undefined") {
        resource.globalDefault = options.globalDefault;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.preemptionPolicy !== "undefined") {
        resource.preemptionPolicy = options.preemptionPolicy;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
