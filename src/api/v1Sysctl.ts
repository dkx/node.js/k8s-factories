import { V1Sysctl } from "@kubernetes/client-node";
export declare interface V1SysctlOptions {
    name: string;
    value: string;
}
export function createV1Sysctl(options: V1SysctlOptions): V1Sysctl {
    const resource = new V1Sysctl();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
