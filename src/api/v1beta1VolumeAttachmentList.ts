import { V1beta1VolumeAttachmentList, V1beta1VolumeAttachment, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1VolumeAttachmentListOptions {
    items: V1beta1VolumeAttachment[];
    metadata?: V1ListMeta;
}
export function createV1beta1VolumeAttachmentList(options: V1beta1VolumeAttachmentListOptions): V1beta1VolumeAttachmentList {
    const resource = new V1beta1VolumeAttachmentList();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "VolumeAttachmentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
