import { V1beta2RollingUpdateDaemonSet } from "@kubernetes/client-node";
export declare interface V1beta2RollingUpdateDaemonSetOptions {
    maxUnavailable?: object;
}
export function createV1beta2RollingUpdateDaemonSet(options: V1beta2RollingUpdateDaemonSetOptions = {}): V1beta2RollingUpdateDaemonSet {
    const resource = new V1beta2RollingUpdateDaemonSet();
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
