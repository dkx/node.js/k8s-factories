import { V1CephFSPersistentVolumeSource, V1SecretReference } from "@kubernetes/client-node";
export declare interface V1CephFSPersistentVolumeSourceOptions {
    monitors: string[];
    path?: string;
    readOnly?: boolean;
    secretFile?: string;
    secretRef?: V1SecretReference;
    user?: string;
}
export function createV1CephFSPersistentVolumeSource(options: V1CephFSPersistentVolumeSourceOptions): V1CephFSPersistentVolumeSource {
    const resource = new V1CephFSPersistentVolumeSource();
    if (typeof options.monitors !== "undefined") {
        resource.monitors = options.monitors;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretFile !== "undefined") {
        resource.secretFile = options.secretFile;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
