import { V1APIService, V1ObjectMeta, V1APIServiceSpec, V1APIServiceStatus } from "@kubernetes/client-node";
export declare interface V1APIServiceOptions {
    metadata?: V1ObjectMeta;
    spec?: V1APIServiceSpec;
    status?: V1APIServiceStatus;
}
export function createV1APIService(options: V1APIServiceOptions = {}): V1APIService {
    const resource = new V1APIService();
    resource.apiVersion = "apiregistration.k8s.io/v1";
    resource.kind = "APIService";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
