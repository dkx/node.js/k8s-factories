import { V1TopologySelectorLabelRequirement } from "@kubernetes/client-node";
export declare interface V1TopologySelectorLabelRequirementOptions {
    key: string;
    values: string[];
}
export function createV1TopologySelectorLabelRequirement(options: V1TopologySelectorLabelRequirementOptions): V1TopologySelectorLabelRequirement {
    const resource = new V1TopologySelectorLabelRequirement();
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.values !== "undefined") {
        resource.values = options.values;
    }
    return resource;
}
