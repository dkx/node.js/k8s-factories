import { V1alpha1ClusterRoleList, V1alpha1ClusterRole, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1ClusterRoleListOptions {
    items: V1alpha1ClusterRole[];
    metadata?: V1ListMeta;
}
export function createV1alpha1ClusterRoleList(options: V1alpha1ClusterRoleListOptions): V1alpha1ClusterRoleList {
    const resource = new V1alpha1ClusterRoleList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "ClusterRoleList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
