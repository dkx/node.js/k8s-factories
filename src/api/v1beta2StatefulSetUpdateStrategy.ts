import { V1beta2StatefulSetUpdateStrategy, V1beta2RollingUpdateStatefulSetStrategy } from "@kubernetes/client-node";
export declare interface V1beta2StatefulSetUpdateStrategyOptions {
    rollingUpdate?: V1beta2RollingUpdateStatefulSetStrategy;
    type?: string;
}
export function createV1beta2StatefulSetUpdateStrategy(options: V1beta2StatefulSetUpdateStrategyOptions = {}): V1beta2StatefulSetUpdateStrategy {
    const resource = new V1beta2StatefulSetUpdateStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
