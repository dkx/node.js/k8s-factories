import { V1AggregationRule, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1AggregationRuleOptions {
    clusterRoleSelectors?: V1LabelSelector[];
}
export function createV1AggregationRule(options: V1AggregationRuleOptions = {}): V1AggregationRule {
    const resource = new V1AggregationRule();
    if (typeof options.clusterRoleSelectors !== "undefined") {
        resource.clusterRoleSelectors = options.clusterRoleSelectors;
    }
    return resource;
}
