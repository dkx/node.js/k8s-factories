import { V1beta1MutatingWebhook, AdmissionregistrationV1beta1WebhookClientConfig, V1LabelSelector, V1beta1RuleWithOperations } from "@kubernetes/client-node";
export declare interface V1beta1MutatingWebhookOptions {
    admissionReviewVersions?: string[];
    clientConfig: AdmissionregistrationV1beta1WebhookClientConfig;
    failurePolicy?: string;
    matchPolicy?: string;
    name: string;
    namespaceSelector?: V1LabelSelector;
    objectSelector?: V1LabelSelector;
    reinvocationPolicy?: string;
    rules?: V1beta1RuleWithOperations[];
    sideEffects?: string;
    timeoutSeconds?: number;
}
export function createV1beta1MutatingWebhook(options: V1beta1MutatingWebhookOptions): V1beta1MutatingWebhook {
    const resource = new V1beta1MutatingWebhook();
    if (typeof options.admissionReviewVersions !== "undefined") {
        resource.admissionReviewVersions = options.admissionReviewVersions;
    }
    if (typeof options.clientConfig !== "undefined") {
        resource.clientConfig = options.clientConfig;
    }
    if (typeof options.failurePolicy !== "undefined") {
        resource.failurePolicy = options.failurePolicy;
    }
    if (typeof options.matchPolicy !== "undefined") {
        resource.matchPolicy = options.matchPolicy;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespaceSelector !== "undefined") {
        resource.namespaceSelector = options.namespaceSelector;
    }
    if (typeof options.objectSelector !== "undefined") {
        resource.objectSelector = options.objectSelector;
    }
    if (typeof options.reinvocationPolicy !== "undefined") {
        resource.reinvocationPolicy = options.reinvocationPolicy;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    if (typeof options.sideEffects !== "undefined") {
        resource.sideEffects = options.sideEffects;
    }
    if (typeof options.timeoutSeconds !== "undefined") {
        resource.timeoutSeconds = options.timeoutSeconds;
    }
    return resource;
}
