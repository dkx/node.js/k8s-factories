import { V1beta1StatefulSet, V1ObjectMeta, V1beta1StatefulSetSpec, V1beta1StatefulSetStatus } from "@kubernetes/client-node";
export declare interface V1beta1StatefulSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1StatefulSetSpec;
    status?: V1beta1StatefulSetStatus;
}
export function createV1beta1StatefulSet(options: V1beta1StatefulSetOptions = {}): V1beta1StatefulSet {
    const resource = new V1beta1StatefulSet();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "StatefulSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
