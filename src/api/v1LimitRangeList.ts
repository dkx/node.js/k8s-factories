import { V1LimitRangeList, V1LimitRange, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1LimitRangeListOptions {
    items: V1LimitRange[];
    metadata?: V1ListMeta;
}
export function createV1LimitRangeList(options: V1LimitRangeListOptions): V1LimitRangeList {
    const resource = new V1LimitRangeList();
    resource.apiVersion = "v1";
    resource.kind = "LimitRangeList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
