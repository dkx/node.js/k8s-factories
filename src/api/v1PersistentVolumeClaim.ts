import { V1PersistentVolumeClaim, V1ObjectMeta, V1PersistentVolumeClaimSpec, V1PersistentVolumeClaimStatus } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeClaimOptions {
    metadata?: V1ObjectMeta;
    spec?: V1PersistentVolumeClaimSpec;
    status?: V1PersistentVolumeClaimStatus;
}
export function createV1PersistentVolumeClaim(options: V1PersistentVolumeClaimOptions = {}): V1PersistentVolumeClaim {
    const resource = new V1PersistentVolumeClaim();
    resource.apiVersion = "v1";
    resource.kind = "PersistentVolumeClaim";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
