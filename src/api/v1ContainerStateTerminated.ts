import { V1ContainerStateTerminated } from "@kubernetes/client-node";
export declare interface V1ContainerStateTerminatedOptions {
    containerID?: string;
    exitCode: number;
    finishedAt?: Date;
    message?: string;
    reason?: string;
    signal?: number;
    startedAt?: Date;
}
export function createV1ContainerStateTerminated(options: V1ContainerStateTerminatedOptions): V1ContainerStateTerminated {
    const resource = new V1ContainerStateTerminated();
    if (typeof options.containerID !== "undefined") {
        resource.containerID = options.containerID;
    }
    if (typeof options.exitCode !== "undefined") {
        resource.exitCode = options.exitCode;
    }
    if (typeof options.finishedAt !== "undefined") {
        resource.finishedAt = options.finishedAt;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.signal !== "undefined") {
        resource.signal = options.signal;
    }
    if (typeof options.startedAt !== "undefined") {
        resource.startedAt = options.startedAt;
    }
    return resource;
}
