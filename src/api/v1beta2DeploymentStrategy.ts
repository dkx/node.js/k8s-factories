import { V1beta2DeploymentStrategy, V1beta2RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface V1beta2DeploymentStrategyOptions {
    rollingUpdate?: V1beta2RollingUpdateDeployment;
    type?: string;
}
export function createV1beta2DeploymentStrategy(options: V1beta2DeploymentStrategyOptions = {}): V1beta2DeploymentStrategy {
    const resource = new V1beta2DeploymentStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
