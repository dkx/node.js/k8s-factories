import { V1alpha1ClusterRole, V1alpha1AggregationRule, V1ObjectMeta, V1alpha1PolicyRule } from "@kubernetes/client-node";
export declare interface V1alpha1ClusterRoleOptions {
    aggregationRule?: V1alpha1AggregationRule;
    metadata?: V1ObjectMeta;
    rules?: V1alpha1PolicyRule[];
}
export function createV1alpha1ClusterRole(options: V1alpha1ClusterRoleOptions = {}): V1alpha1ClusterRole {
    const resource = new V1alpha1ClusterRole();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "ClusterRole";
    if (typeof options.aggregationRule !== "undefined") {
        resource.aggregationRule = options.aggregationRule;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    return resource;
}
