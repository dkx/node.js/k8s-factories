import { V1NodeAffinity, V1PreferredSchedulingTerm, V1NodeSelector } from "@kubernetes/client-node";
export declare interface V1NodeAffinityOptions {
    preferredDuringSchedulingIgnoredDuringExecution?: V1PreferredSchedulingTerm[];
    requiredDuringSchedulingIgnoredDuringExecution?: V1NodeSelector;
}
export function createV1NodeAffinity(options: V1NodeAffinityOptions = {}): V1NodeAffinity {
    const resource = new V1NodeAffinity();
    if (typeof options.preferredDuringSchedulingIgnoredDuringExecution !== "undefined") {
        resource.preferredDuringSchedulingIgnoredDuringExecution = options.preferredDuringSchedulingIgnoredDuringExecution;
    }
    if (typeof options.requiredDuringSchedulingIgnoredDuringExecution !== "undefined") {
        resource.requiredDuringSchedulingIgnoredDuringExecution = options.requiredDuringSchedulingIgnoredDuringExecution;
    }
    return resource;
}
