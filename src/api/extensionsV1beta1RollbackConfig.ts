import { ExtensionsV1beta1RollbackConfig } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1RollbackConfigOptions {
    revision?: number;
}
export function createExtensionsV1beta1RollbackConfig(options: ExtensionsV1beta1RollbackConfigOptions = {}): ExtensionsV1beta1RollbackConfig {
    const resource = new ExtensionsV1beta1RollbackConfig();
    if (typeof options.revision !== "undefined") {
        resource.revision = options.revision;
    }
    return resource;
}
