import { V1beta1PodDisruptionBudget, V1ObjectMeta, V1beta1PodDisruptionBudgetSpec, V1beta1PodDisruptionBudgetStatus } from "@kubernetes/client-node";
export declare interface V1beta1PodDisruptionBudgetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1PodDisruptionBudgetSpec;
    status?: V1beta1PodDisruptionBudgetStatus;
}
export function createV1beta1PodDisruptionBudget(options: V1beta1PodDisruptionBudgetOptions = {}): V1beta1PodDisruptionBudget {
    const resource = new V1beta1PodDisruptionBudget();
    resource.apiVersion = "policy/v1beta1";
    resource.kind = "PodDisruptionBudget";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
