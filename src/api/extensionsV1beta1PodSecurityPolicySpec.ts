import { ExtensionsV1beta1PodSecurityPolicySpec, ExtensionsV1beta1AllowedCSIDriver, ExtensionsV1beta1AllowedFlexVolume, ExtensionsV1beta1AllowedHostPath, ExtensionsV1beta1FSGroupStrategyOptions, ExtensionsV1beta1HostPortRange, ExtensionsV1beta1RunAsGroupStrategyOptions, ExtensionsV1beta1RunAsUserStrategyOptions, ExtensionsV1beta1RuntimeClassStrategyOptions, ExtensionsV1beta1SELinuxStrategyOptions, ExtensionsV1beta1SupplementalGroupsStrategyOptions } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1PodSecurityPolicySpecOptions {
    allowPrivilegeEscalation?: boolean;
    allowedCSIDrivers?: ExtensionsV1beta1AllowedCSIDriver[];
    allowedCapabilities?: string[];
    allowedFlexVolumes?: ExtensionsV1beta1AllowedFlexVolume[];
    allowedHostPaths?: ExtensionsV1beta1AllowedHostPath[];
    allowedProcMountTypes?: string[];
    allowedUnsafeSysctls?: string[];
    defaultAddCapabilities?: string[];
    defaultAllowPrivilegeEscalation?: boolean;
    forbiddenSysctls?: string[];
    fsGroup: ExtensionsV1beta1FSGroupStrategyOptions;
    hostIPC?: boolean;
    hostNetwork?: boolean;
    hostPID?: boolean;
    hostPorts?: ExtensionsV1beta1HostPortRange[];
    privileged?: boolean;
    readOnlyRootFilesystem?: boolean;
    requiredDropCapabilities?: string[];
    runAsGroup?: ExtensionsV1beta1RunAsGroupStrategyOptions;
    runAsUser: ExtensionsV1beta1RunAsUserStrategyOptions;
    runtimeClass?: ExtensionsV1beta1RuntimeClassStrategyOptions;
    seLinux: ExtensionsV1beta1SELinuxStrategyOptions;
    supplementalGroups: ExtensionsV1beta1SupplementalGroupsStrategyOptions;
    volumes?: string[];
}
export function createExtensionsV1beta1PodSecurityPolicySpec(options: ExtensionsV1beta1PodSecurityPolicySpecOptions): ExtensionsV1beta1PodSecurityPolicySpec {
    const resource = new ExtensionsV1beta1PodSecurityPolicySpec();
    if (typeof options.allowPrivilegeEscalation !== "undefined") {
        resource.allowPrivilegeEscalation = options.allowPrivilegeEscalation;
    }
    if (typeof options.allowedCSIDrivers !== "undefined") {
        resource.allowedCSIDrivers = options.allowedCSIDrivers;
    }
    if (typeof options.allowedCapabilities !== "undefined") {
        resource.allowedCapabilities = options.allowedCapabilities;
    }
    if (typeof options.allowedFlexVolumes !== "undefined") {
        resource.allowedFlexVolumes = options.allowedFlexVolumes;
    }
    if (typeof options.allowedHostPaths !== "undefined") {
        resource.allowedHostPaths = options.allowedHostPaths;
    }
    if (typeof options.allowedProcMountTypes !== "undefined") {
        resource.allowedProcMountTypes = options.allowedProcMountTypes;
    }
    if (typeof options.allowedUnsafeSysctls !== "undefined") {
        resource.allowedUnsafeSysctls = options.allowedUnsafeSysctls;
    }
    if (typeof options.defaultAddCapabilities !== "undefined") {
        resource.defaultAddCapabilities = options.defaultAddCapabilities;
    }
    if (typeof options.defaultAllowPrivilegeEscalation !== "undefined") {
        resource.defaultAllowPrivilegeEscalation = options.defaultAllowPrivilegeEscalation;
    }
    if (typeof options.forbiddenSysctls !== "undefined") {
        resource.forbiddenSysctls = options.forbiddenSysctls;
    }
    if (typeof options.fsGroup !== "undefined") {
        resource.fsGroup = options.fsGroup;
    }
    if (typeof options.hostIPC !== "undefined") {
        resource.hostIPC = options.hostIPC;
    }
    if (typeof options.hostNetwork !== "undefined") {
        resource.hostNetwork = options.hostNetwork;
    }
    if (typeof options.hostPID !== "undefined") {
        resource.hostPID = options.hostPID;
    }
    if (typeof options.hostPorts !== "undefined") {
        resource.hostPorts = options.hostPorts;
    }
    if (typeof options.privileged !== "undefined") {
        resource.privileged = options.privileged;
    }
    if (typeof options.readOnlyRootFilesystem !== "undefined") {
        resource.readOnlyRootFilesystem = options.readOnlyRootFilesystem;
    }
    if (typeof options.requiredDropCapabilities !== "undefined") {
        resource.requiredDropCapabilities = options.requiredDropCapabilities;
    }
    if (typeof options.runAsGroup !== "undefined") {
        resource.runAsGroup = options.runAsGroup;
    }
    if (typeof options.runAsUser !== "undefined") {
        resource.runAsUser = options.runAsUser;
    }
    if (typeof options.runtimeClass !== "undefined") {
        resource.runtimeClass = options.runtimeClass;
    }
    if (typeof options.seLinux !== "undefined") {
        resource.seLinux = options.seLinux;
    }
    if (typeof options.supplementalGroups !== "undefined") {
        resource.supplementalGroups = options.supplementalGroups;
    }
    if (typeof options.volumes !== "undefined") {
        resource.volumes = options.volumes;
    }
    return resource;
}
