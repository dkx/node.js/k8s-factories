import { V1QuobyteVolumeSource } from "@kubernetes/client-node";
export declare interface V1QuobyteVolumeSourceOptions {
    group?: string;
    readOnly?: boolean;
    registry: string;
    tenant?: string;
    user?: string;
    volume: string;
}
export function createV1QuobyteVolumeSource(options: V1QuobyteVolumeSourceOptions): V1QuobyteVolumeSource {
    const resource = new V1QuobyteVolumeSource();
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.registry !== "undefined") {
        resource.registry = options.registry;
    }
    if (typeof options.tenant !== "undefined") {
        resource.tenant = options.tenant;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    if (typeof options.volume !== "undefined") {
        resource.volume = options.volume;
    }
    return resource;
}
