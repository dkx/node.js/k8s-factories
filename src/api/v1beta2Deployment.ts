import { V1beta2Deployment, V1ObjectMeta, V1beta2DeploymentSpec, V1beta2DeploymentStatus } from "@kubernetes/client-node";
export declare interface V1beta2DeploymentOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta2DeploymentSpec;
    status?: V1beta2DeploymentStatus;
}
export function createV1beta2Deployment(options: V1beta2DeploymentOptions = {}): V1beta2Deployment {
    const resource = new V1beta2Deployment();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "Deployment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
