import { V1beta1StatefulSetUpdateStrategy, V1beta1RollingUpdateStatefulSetStrategy } from "@kubernetes/client-node";
export declare interface V1beta1StatefulSetUpdateStrategyOptions {
    rollingUpdate?: V1beta1RollingUpdateStatefulSetStrategy;
    type?: string;
}
export function createV1beta1StatefulSetUpdateStrategy(options: V1beta1StatefulSetUpdateStrategyOptions = {}): V1beta1StatefulSetUpdateStrategy {
    const resource = new V1beta1StatefulSetUpdateStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
