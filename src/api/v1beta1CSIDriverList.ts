import { V1beta1CSIDriverList, V1beta1CSIDriver, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1CSIDriverListOptions {
    items: V1beta1CSIDriver[];
    metadata?: V1ListMeta;
}
export function createV1beta1CSIDriverList(options: V1beta1CSIDriverListOptions): V1beta1CSIDriverList {
    const resource = new V1beta1CSIDriverList();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "CSIDriverList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
