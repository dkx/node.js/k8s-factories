import { ApiregistrationV1beta1ServiceReference } from "@kubernetes/client-node";
export declare interface ApiregistrationV1beta1ServiceReferenceOptions {
    name?: string;
    namespace?: string;
    port?: number;
}
export function createApiregistrationV1beta1ServiceReference(options: ApiregistrationV1beta1ServiceReferenceOptions = {}): ApiregistrationV1beta1ServiceReference {
    const resource = new ApiregistrationV1beta1ServiceReference();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    return resource;
}
