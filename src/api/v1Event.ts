import { V1Event, V1ObjectReference, V1ObjectMeta, V1EventSeries, V1EventSource } from "@kubernetes/client-node";
export declare interface V1EventOptions {
    action?: string;
    count?: number;
    eventTime?: Date;
    firstTimestamp?: Date;
    involvedObject: V1ObjectReference;
    lastTimestamp?: Date;
    message?: string;
    metadata: V1ObjectMeta;
    reason?: string;
    related?: V1ObjectReference;
    reportingComponent?: string;
    reportingInstance?: string;
    series?: V1EventSeries;
    source?: V1EventSource;
    type?: string;
}
export function createV1Event(options: V1EventOptions): V1Event {
    const resource = new V1Event();
    resource.apiVersion = "v1";
    resource.kind = "Event";
    if (typeof options.action !== "undefined") {
        resource.action = options.action;
    }
    if (typeof options.count !== "undefined") {
        resource.count = options.count;
    }
    if (typeof options.eventTime !== "undefined") {
        resource.eventTime = options.eventTime;
    }
    if (typeof options.firstTimestamp !== "undefined") {
        resource.firstTimestamp = options.firstTimestamp;
    }
    if (typeof options.involvedObject !== "undefined") {
        resource.involvedObject = options.involvedObject;
    }
    if (typeof options.lastTimestamp !== "undefined") {
        resource.lastTimestamp = options.lastTimestamp;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.related !== "undefined") {
        resource.related = options.related;
    }
    if (typeof options.reportingComponent !== "undefined") {
        resource.reportingComponent = options.reportingComponent;
    }
    if (typeof options.reportingInstance !== "undefined") {
        resource.reportingInstance = options.reportingInstance;
    }
    if (typeof options.series !== "undefined") {
        resource.series = options.series;
    }
    if (typeof options.source !== "undefined") {
        resource.source = options.source;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
