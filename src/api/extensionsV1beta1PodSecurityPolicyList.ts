import { ExtensionsV1beta1PodSecurityPolicyList, ExtensionsV1beta1PodSecurityPolicy, V1ListMeta } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1PodSecurityPolicyListOptions {
    items: ExtensionsV1beta1PodSecurityPolicy[];
    metadata?: V1ListMeta;
}
export function createExtensionsV1beta1PodSecurityPolicyList(options: ExtensionsV1beta1PodSecurityPolicyListOptions): ExtensionsV1beta1PodSecurityPolicyList {
    const resource = new ExtensionsV1beta1PodSecurityPolicyList();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "PodSecurityPolicyList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
