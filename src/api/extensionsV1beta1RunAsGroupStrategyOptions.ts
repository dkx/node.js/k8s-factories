import { ExtensionsV1beta1RunAsGroupStrategyOptions, ExtensionsV1beta1IDRange } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1RunAsGroupStrategyOptionsOptions {
    ranges?: ExtensionsV1beta1IDRange[];
    rule: string;
}
export function createExtensionsV1beta1RunAsGroupStrategyOptions(options: ExtensionsV1beta1RunAsGroupStrategyOptionsOptions): ExtensionsV1beta1RunAsGroupStrategyOptions {
    const resource = new ExtensionsV1beta1RunAsGroupStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
