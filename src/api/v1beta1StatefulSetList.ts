import { V1beta1StatefulSetList, V1beta1StatefulSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1StatefulSetListOptions {
    items: V1beta1StatefulSet[];
    metadata?: V1ListMeta;
}
export function createV1beta1StatefulSetList(options: V1beta1StatefulSetListOptions): V1beta1StatefulSetList {
    const resource = new V1beta1StatefulSetList();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "StatefulSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
