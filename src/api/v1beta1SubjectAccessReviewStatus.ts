import { V1beta1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1beta1SubjectAccessReviewStatusOptions {
    allowed: boolean;
    denied?: boolean;
    evaluationError?: string;
    reason?: string;
}
export function createV1beta1SubjectAccessReviewStatus(options: V1beta1SubjectAccessReviewStatusOptions): V1beta1SubjectAccessReviewStatus {
    const resource = new V1beta1SubjectAccessReviewStatus();
    if (typeof options.allowed !== "undefined") {
        resource.allowed = options.allowed;
    }
    if (typeof options.denied !== "undefined") {
        resource.denied = options.denied;
    }
    if (typeof options.evaluationError !== "undefined") {
        resource.evaluationError = options.evaluationError;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    return resource;
}
