import { V1beta1SubjectAccessReview, V1ObjectMeta, V1beta1SubjectAccessReviewSpec, V1beta1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1beta1SubjectAccessReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1SubjectAccessReviewSpec;
    status?: V1beta1SubjectAccessReviewStatus;
}
export function createV1beta1SubjectAccessReview(options: V1beta1SubjectAccessReviewOptions): V1beta1SubjectAccessReview {
    const resource = new V1beta1SubjectAccessReview();
    resource.apiVersion = "authorization.k8s.io/v1beta1";
    resource.kind = "SubjectAccessReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
