import { V1RollingUpdateDaemonSet } from "@kubernetes/client-node";
export declare interface V1RollingUpdateDaemonSetOptions {
    maxUnavailable?: object;
}
export function createV1RollingUpdateDaemonSet(options: V1RollingUpdateDaemonSetOptions = {}): V1RollingUpdateDaemonSet {
    const resource = new V1RollingUpdateDaemonSet();
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
