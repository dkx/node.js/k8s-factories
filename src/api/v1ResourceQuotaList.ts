import { V1ResourceQuotaList, V1ResourceQuota, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ResourceQuotaListOptions {
    items: V1ResourceQuota[];
    metadata?: V1ListMeta;
}
export function createV1ResourceQuotaList(options: V1ResourceQuotaListOptions): V1ResourceQuotaList {
    const resource = new V1ResourceQuotaList();
    resource.apiVersion = "v1";
    resource.kind = "ResourceQuotaList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
