import { V1beta1NetworkPolicySpec, V1beta1NetworkPolicyEgressRule, V1beta1NetworkPolicyIngressRule, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicySpecOptions {
    egress?: V1beta1NetworkPolicyEgressRule[];
    ingress?: V1beta1NetworkPolicyIngressRule[];
    podSelector: V1LabelSelector;
    policyTypes?: string[];
}
export function createV1beta1NetworkPolicySpec(options: V1beta1NetworkPolicySpecOptions): V1beta1NetworkPolicySpec {
    const resource = new V1beta1NetworkPolicySpec();
    if (typeof options.egress !== "undefined") {
        resource.egress = options.egress;
    }
    if (typeof options.ingress !== "undefined") {
        resource.ingress = options.ingress;
    }
    if (typeof options.podSelector !== "undefined") {
        resource.podSelector = options.podSelector;
    }
    if (typeof options.policyTypes !== "undefined") {
        resource.policyTypes = options.policyTypes;
    }
    return resource;
}
