import { V1beta2ControllerRevisionList, V1beta2ControllerRevision, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta2ControllerRevisionListOptions {
    items: V1beta2ControllerRevision[];
    metadata?: V1ListMeta;
}
export function createV1beta2ControllerRevisionList(options: V1beta2ControllerRevisionListOptions): V1beta2ControllerRevisionList {
    const resource = new V1beta2ControllerRevisionList();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "ControllerRevisionList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
