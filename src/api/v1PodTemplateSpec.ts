import { V1PodTemplateSpec, V1ObjectMeta, V1PodSpec } from "@kubernetes/client-node";
export declare interface V1PodTemplateSpecOptions {
    metadata?: V1ObjectMeta;
    spec?: V1PodSpec;
}
export function createV1PodTemplateSpec(options: V1PodTemplateSpecOptions = {}): V1PodTemplateSpec {
    const resource = new V1PodTemplateSpec();
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
