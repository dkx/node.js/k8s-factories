import { ExtensionsV1beta1SupplementalGroupsStrategyOptions, ExtensionsV1beta1IDRange } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1SupplementalGroupsStrategyOptionsOptions {
    ranges?: ExtensionsV1beta1IDRange[];
    rule?: string;
}
export function createExtensionsV1beta1SupplementalGroupsStrategyOptions(options: ExtensionsV1beta1SupplementalGroupsStrategyOptionsOptions = {}): ExtensionsV1beta1SupplementalGroupsStrategyOptions {
    const resource = new ExtensionsV1beta1SupplementalGroupsStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
