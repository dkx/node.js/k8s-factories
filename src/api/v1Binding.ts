import { V1Binding, V1ObjectMeta, V1ObjectReference } from "@kubernetes/client-node";
export declare interface V1BindingOptions {
    metadata?: V1ObjectMeta;
    target: V1ObjectReference;
}
export function createV1Binding(options: V1BindingOptions): V1Binding {
    const resource = new V1Binding();
    resource.apiVersion = "v1";
    resource.kind = "Binding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    return resource;
}
