import { V2beta2ObjectMetricSource, V2beta2CrossVersionObjectReference, V2beta2MetricIdentifier, V2beta2MetricTarget } from "@kubernetes/client-node";
export declare interface V2beta2ObjectMetricSourceOptions {
    describedObject: V2beta2CrossVersionObjectReference;
    metric: V2beta2MetricIdentifier;
    target: V2beta2MetricTarget;
}
export function createV2beta2ObjectMetricSource(options: V2beta2ObjectMetricSourceOptions): V2beta2ObjectMetricSource {
    const resource = new V2beta2ObjectMetricSource();
    if (typeof options.describedObject !== "undefined") {
        resource.describedObject = options.describedObject;
    }
    if (typeof options.metric !== "undefined") {
        resource.metric = options.metric;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    return resource;
}
