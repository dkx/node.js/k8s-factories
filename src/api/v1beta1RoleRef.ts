import { V1beta1RoleRef } from "@kubernetes/client-node";
export declare interface V1beta1RoleRefOptions {
    apiGroup: string;
    kind: string;
    name: string;
}
export function createV1beta1RoleRef(options: V1beta1RoleRefOptions): V1beta1RoleRef {
    const resource = new V1beta1RoleRef();
    if (typeof options.apiGroup !== "undefined") {
        resource.apiGroup = options.apiGroup;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
