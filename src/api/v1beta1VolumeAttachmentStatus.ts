import { V1beta1VolumeAttachmentStatus, V1beta1VolumeError } from "@kubernetes/client-node";
export declare interface V1beta1VolumeAttachmentStatusOptions {
    attachError?: V1beta1VolumeError;
    attached: boolean;
    attachmentMetadata?: {
        [key: string]: string;
    };
    detachError?: V1beta1VolumeError;
}
export function createV1beta1VolumeAttachmentStatus(options: V1beta1VolumeAttachmentStatusOptions): V1beta1VolumeAttachmentStatus {
    const resource = new V1beta1VolumeAttachmentStatus();
    if (typeof options.attachError !== "undefined") {
        resource.attachError = options.attachError;
    }
    if (typeof options.attached !== "undefined") {
        resource.attached = options.attached;
    }
    if (typeof options.attachmentMetadata !== "undefined") {
        resource.attachmentMetadata = options.attachmentMetadata;
    }
    if (typeof options.detachError !== "undefined") {
        resource.detachError = options.detachError;
    }
    return resource;
}
