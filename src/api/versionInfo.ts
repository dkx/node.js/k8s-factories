import { VersionInfo } from "@kubernetes/client-node";
export declare interface VersionInfoOptions {
    buildDate: string;
    compiler: string;
    gitCommit: string;
    gitTreeState: string;
    gitVersion: string;
    goVersion: string;
    major: string;
    minor: string;
    platform: string;
}
export function createVersionInfo(options: VersionInfoOptions): VersionInfo {
    const resource = new VersionInfo();
    if (typeof options.buildDate !== "undefined") {
        resource.buildDate = options.buildDate;
    }
    if (typeof options.compiler !== "undefined") {
        resource.compiler = options.compiler;
    }
    if (typeof options.gitCommit !== "undefined") {
        resource.gitCommit = options.gitCommit;
    }
    if (typeof options.gitTreeState !== "undefined") {
        resource.gitTreeState = options.gitTreeState;
    }
    if (typeof options.gitVersion !== "undefined") {
        resource.gitVersion = options.gitVersion;
    }
    if (typeof options.goVersion !== "undefined") {
        resource.goVersion = options.goVersion;
    }
    if (typeof options.major !== "undefined") {
        resource.major = options.major;
    }
    if (typeof options.minor !== "undefined") {
        resource.minor = options.minor;
    }
    if (typeof options.platform !== "undefined") {
        resource.platform = options.platform;
    }
    return resource;
}
