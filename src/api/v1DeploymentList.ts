import { V1DeploymentList, V1Deployment, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1DeploymentListOptions {
    items: V1Deployment[];
    metadata?: V1ListMeta;
}
export function createV1DeploymentList(options: V1DeploymentListOptions): V1DeploymentList {
    const resource = new V1DeploymentList();
    resource.apiVersion = "apps/v1";
    resource.kind = "DeploymentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
