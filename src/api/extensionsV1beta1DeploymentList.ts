import { ExtensionsV1beta1DeploymentList, ExtensionsV1beta1Deployment, V1ListMeta } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1DeploymentListOptions {
    items: ExtensionsV1beta1Deployment[];
    metadata?: V1ListMeta;
}
export function createExtensionsV1beta1DeploymentList(options: ExtensionsV1beta1DeploymentListOptions): ExtensionsV1beta1DeploymentList {
    const resource = new ExtensionsV1beta1DeploymentList();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "DeploymentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
