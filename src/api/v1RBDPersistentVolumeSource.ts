import { V1RBDPersistentVolumeSource, V1SecretReference } from "@kubernetes/client-node";
export declare interface V1RBDPersistentVolumeSourceOptions {
    fsType?: string;
    image: string;
    keyring?: string;
    monitors: string[];
    pool?: string;
    readOnly?: boolean;
    secretRef?: V1SecretReference;
    user?: string;
}
export function createV1RBDPersistentVolumeSource(options: V1RBDPersistentVolumeSourceOptions): V1RBDPersistentVolumeSource {
    const resource = new V1RBDPersistentVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.image !== "undefined") {
        resource.image = options.image;
    }
    if (typeof options.keyring !== "undefined") {
        resource.keyring = options.keyring;
    }
    if (typeof options.monitors !== "undefined") {
        resource.monitors = options.monitors;
    }
    if (typeof options.pool !== "undefined") {
        resource.pool = options.pool;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
