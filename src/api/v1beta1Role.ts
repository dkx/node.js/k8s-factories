import { V1beta1Role, V1ObjectMeta, V1beta1PolicyRule } from "@kubernetes/client-node";
export declare interface V1beta1RoleOptions {
    metadata?: V1ObjectMeta;
    rules?: V1beta1PolicyRule[];
}
export function createV1beta1Role(options: V1beta1RoleOptions = {}): V1beta1Role {
    const resource = new V1beta1Role();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "Role";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    return resource;
}
