import { V1beta1RoleBindingList, V1beta1RoleBinding, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1RoleBindingListOptions {
    items: V1beta1RoleBinding[];
    metadata?: V1ListMeta;
}
export function createV1beta1RoleBindingList(options: V1beta1RoleBindingListOptions): V1beta1RoleBindingList {
    const resource = new V1beta1RoleBindingList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "RoleBindingList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
