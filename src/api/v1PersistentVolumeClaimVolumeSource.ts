import { V1PersistentVolumeClaimVolumeSource } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeClaimVolumeSourceOptions {
    claimName: string;
    readOnly?: boolean;
}
export function createV1PersistentVolumeClaimVolumeSource(options: V1PersistentVolumeClaimVolumeSourceOptions): V1PersistentVolumeClaimVolumeSource {
    const resource = new V1PersistentVolumeClaimVolumeSource();
    if (typeof options.claimName !== "undefined") {
        resource.claimName = options.claimName;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
