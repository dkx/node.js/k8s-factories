import { V1beta1Lease, V1ObjectMeta, V1beta1LeaseSpec } from "@kubernetes/client-node";
export declare interface V1beta1LeaseOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1LeaseSpec;
}
export function createV1beta1Lease(options: V1beta1LeaseOptions = {}): V1beta1Lease {
    const resource = new V1beta1Lease();
    resource.apiVersion = "coordination.k8s.io/v1beta1";
    resource.kind = "Lease";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
