import { V1NodeSystemInfo } from "@kubernetes/client-node";
export declare interface V1NodeSystemInfoOptions {
    architecture: string;
    bootID: string;
    containerRuntimeVersion: string;
    kernelVersion: string;
    kubeProxyVersion: string;
    kubeletVersion: string;
    machineID: string;
    operatingSystem: string;
    osImage: string;
    systemUUID: string;
}
export function createV1NodeSystemInfo(options: V1NodeSystemInfoOptions): V1NodeSystemInfo {
    const resource = new V1NodeSystemInfo();
    if (typeof options.architecture !== "undefined") {
        resource.architecture = options.architecture;
    }
    if (typeof options.bootID !== "undefined") {
        resource.bootID = options.bootID;
    }
    if (typeof options.containerRuntimeVersion !== "undefined") {
        resource.containerRuntimeVersion = options.containerRuntimeVersion;
    }
    if (typeof options.kernelVersion !== "undefined") {
        resource.kernelVersion = options.kernelVersion;
    }
    if (typeof options.kubeProxyVersion !== "undefined") {
        resource.kubeProxyVersion = options.kubeProxyVersion;
    }
    if (typeof options.kubeletVersion !== "undefined") {
        resource.kubeletVersion = options.kubeletVersion;
    }
    if (typeof options.machineID !== "undefined") {
        resource.machineID = options.machineID;
    }
    if (typeof options.operatingSystem !== "undefined") {
        resource.operatingSystem = options.operatingSystem;
    }
    if (typeof options.osImage !== "undefined") {
        resource.osImage = options.osImage;
    }
    if (typeof options.systemUUID !== "undefined") {
        resource.systemUUID = options.systemUUID;
    }
    return resource;
}
