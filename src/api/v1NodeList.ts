import { V1NodeList, V1Node, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1NodeListOptions {
    items: V1Node[];
    metadata?: V1ListMeta;
}
export function createV1NodeList(options: V1NodeListOptions): V1NodeList {
    const resource = new V1NodeList();
    resource.apiVersion = "v1";
    resource.kind = "NodeList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
