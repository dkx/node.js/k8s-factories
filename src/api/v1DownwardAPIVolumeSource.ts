import { V1DownwardAPIVolumeSource, V1DownwardAPIVolumeFile } from "@kubernetes/client-node";
export declare interface V1DownwardAPIVolumeSourceOptions {
    defaultMode?: number;
    items?: V1DownwardAPIVolumeFile[];
}
export function createV1DownwardAPIVolumeSource(options: V1DownwardAPIVolumeSourceOptions = {}): V1DownwardAPIVolumeSource {
    const resource = new V1DownwardAPIVolumeSource();
    if (typeof options.defaultMode !== "undefined") {
        resource.defaultMode = options.defaultMode;
    }
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    return resource;
}
