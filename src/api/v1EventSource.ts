import { V1EventSource } from "@kubernetes/client-node";
export declare interface V1EventSourceOptions {
    component?: string;
    host?: string;
}
export function createV1EventSource(options: V1EventSourceOptions = {}): V1EventSource {
    const resource = new V1EventSource();
    if (typeof options.component !== "undefined") {
        resource.component = options.component;
    }
    if (typeof options.host !== "undefined") {
        resource.host = options.host;
    }
    return resource;
}
