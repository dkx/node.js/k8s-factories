import { V1TCPSocketAction } from "@kubernetes/client-node";
export declare interface V1TCPSocketActionOptions {
    host?: string;
    port: object;
}
export function createV1TCPSocketAction(options: V1TCPSocketActionOptions): V1TCPSocketAction {
    const resource = new V1TCPSocketAction();
    if (typeof options.host !== "undefined") {
        resource.host = options.host;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    return resource;
}
