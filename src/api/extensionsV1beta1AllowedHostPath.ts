import { ExtensionsV1beta1AllowedHostPath } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1AllowedHostPathOptions {
    pathPrefix?: string;
    readOnly?: boolean;
}
export function createExtensionsV1beta1AllowedHostPath(options: ExtensionsV1beta1AllowedHostPathOptions = {}): ExtensionsV1beta1AllowedHostPath {
    const resource = new ExtensionsV1beta1AllowedHostPath();
    if (typeof options.pathPrefix !== "undefined") {
        resource.pathPrefix = options.pathPrefix;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
