import { V1alpha1ClusterRoleBinding, V1ObjectMeta, V1alpha1RoleRef, V1alpha1Subject } from "@kubernetes/client-node";
export declare interface V1alpha1ClusterRoleBindingOptions {
    metadata?: V1ObjectMeta;
    roleRef: V1alpha1RoleRef;
    subjects?: V1alpha1Subject[];
}
export function createV1alpha1ClusterRoleBinding(options: V1alpha1ClusterRoleBindingOptions): V1alpha1ClusterRoleBinding {
    const resource = new V1alpha1ClusterRoleBinding();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "ClusterRoleBinding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.roleRef !== "undefined") {
        resource.roleRef = options.roleRef;
    }
    if (typeof options.subjects !== "undefined") {
        resource.subjects = options.subjects;
    }
    return resource;
}
