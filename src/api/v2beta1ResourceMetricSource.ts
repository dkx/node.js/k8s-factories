import { V2beta1ResourceMetricSource } from "@kubernetes/client-node";
export declare interface V2beta1ResourceMetricSourceOptions {
    name: string;
    targetAverageUtilization?: number;
    targetAverageValue?: string;
}
export function createV2beta1ResourceMetricSource(options: V2beta1ResourceMetricSourceOptions): V2beta1ResourceMetricSource {
    const resource = new V2beta1ResourceMetricSource();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.targetAverageUtilization !== "undefined") {
        resource.targetAverageUtilization = options.targetAverageUtilization;
    }
    if (typeof options.targetAverageValue !== "undefined") {
        resource.targetAverageValue = options.targetAverageValue;
    }
    return resource;
}
