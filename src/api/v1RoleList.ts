import { V1RoleList, V1Role, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1RoleListOptions {
    items: V1Role[];
    metadata?: V1ListMeta;
}
export function createV1RoleList(options: V1RoleListOptions): V1RoleList {
    const resource = new V1RoleList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "RoleList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
