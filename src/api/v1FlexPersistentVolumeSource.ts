import { V1FlexPersistentVolumeSource, V1SecretReference } from "@kubernetes/client-node";
export declare interface V1FlexPersistentVolumeSourceOptions {
    driver: string;
    fsType?: string;
    options?: {
        [key: string]: string;
    };
    readOnly?: boolean;
    secretRef?: V1SecretReference;
}
export function createV1FlexPersistentVolumeSource(options: V1FlexPersistentVolumeSourceOptions): V1FlexPersistentVolumeSource {
    const resource = new V1FlexPersistentVolumeSource();
    if (typeof options.driver !== "undefined") {
        resource.driver = options.driver;
    }
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.options !== "undefined") {
        resource.options = options.options;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    return resource;
}
