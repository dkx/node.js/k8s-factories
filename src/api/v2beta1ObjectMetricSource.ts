import { V2beta1ObjectMetricSource, V1LabelSelector, V2beta1CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V2beta1ObjectMetricSourceOptions {
    averageValue?: string;
    metricName: string;
    selector?: V1LabelSelector;
    target: V2beta1CrossVersionObjectReference;
    targetValue: string;
}
export function createV2beta1ObjectMetricSource(options: V2beta1ObjectMetricSourceOptions): V2beta1ObjectMetricSource {
    const resource = new V2beta1ObjectMetricSource();
    if (typeof options.averageValue !== "undefined") {
        resource.averageValue = options.averageValue;
    }
    if (typeof options.metricName !== "undefined") {
        resource.metricName = options.metricName;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    if (typeof options.targetValue !== "undefined") {
        resource.targetValue = options.targetValue;
    }
    return resource;
}
