import { V1GitRepoVolumeSource } from "@kubernetes/client-node";
export declare interface V1GitRepoVolumeSourceOptions {
    directory?: string;
    repository: string;
    revision?: string;
}
export function createV1GitRepoVolumeSource(options: V1GitRepoVolumeSourceOptions): V1GitRepoVolumeSource {
    const resource = new V1GitRepoVolumeSource();
    if (typeof options.directory !== "undefined") {
        resource.directory = options.directory;
    }
    if (typeof options.repository !== "undefined") {
        resource.repository = options.repository;
    }
    if (typeof options.revision !== "undefined") {
        resource.revision = options.revision;
    }
    return resource;
}
