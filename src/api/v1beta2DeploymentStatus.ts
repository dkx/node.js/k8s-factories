import { V1beta2DeploymentStatus, V1beta2DeploymentCondition } from "@kubernetes/client-node";
export declare interface V1beta2DeploymentStatusOptions {
    availableReplicas?: number;
    collisionCount?: number;
    conditions?: V1beta2DeploymentCondition[];
    observedGeneration?: number;
    readyReplicas?: number;
    replicas?: number;
    unavailableReplicas?: number;
    updatedReplicas?: number;
}
export function createV1beta2DeploymentStatus(options: V1beta2DeploymentStatusOptions = {}): V1beta2DeploymentStatus {
    const resource = new V1beta2DeploymentStatus();
    if (typeof options.availableReplicas !== "undefined") {
        resource.availableReplicas = options.availableReplicas;
    }
    if (typeof options.collisionCount !== "undefined") {
        resource.collisionCount = options.collisionCount;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.readyReplicas !== "undefined") {
        resource.readyReplicas = options.readyReplicas;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.unavailableReplicas !== "undefined") {
        resource.unavailableReplicas = options.unavailableReplicas;
    }
    if (typeof options.updatedReplicas !== "undefined") {
        resource.updatedReplicas = options.updatedReplicas;
    }
    return resource;
}
