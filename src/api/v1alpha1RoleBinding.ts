import { V1alpha1RoleBinding, V1ObjectMeta, V1alpha1RoleRef, V1alpha1Subject } from "@kubernetes/client-node";
export declare interface V1alpha1RoleBindingOptions {
    metadata?: V1ObjectMeta;
    roleRef: V1alpha1RoleRef;
    subjects?: V1alpha1Subject[];
}
export function createV1alpha1RoleBinding(options: V1alpha1RoleBindingOptions): V1alpha1RoleBinding {
    const resource = new V1alpha1RoleBinding();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "RoleBinding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.roleRef !== "undefined") {
        resource.roleRef = options.roleRef;
    }
    if (typeof options.subjects !== "undefined") {
        resource.subjects = options.subjects;
    }
    return resource;
}
