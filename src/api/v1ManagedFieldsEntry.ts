import { V1ManagedFieldsEntry } from "@kubernetes/client-node";
export declare interface V1ManagedFieldsEntryOptions {
    apiVersion?: string;
    fields?: object;
    manager?: string;
    operation?: string;
    time?: Date;
}
export function createV1ManagedFieldsEntry(options: V1ManagedFieldsEntryOptions = {}): V1ManagedFieldsEntry {
    const resource = new V1ManagedFieldsEntry();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.fields !== "undefined") {
        resource.fields = options.fields;
    }
    if (typeof options.manager !== "undefined") {
        resource.manager = options.manager;
    }
    if (typeof options.operation !== "undefined") {
        resource.operation = options.operation;
    }
    if (typeof options.time !== "undefined") {
        resource.time = options.time;
    }
    return resource;
}
