import { V1beta1VolumeError } from "@kubernetes/client-node";
export declare interface V1beta1VolumeErrorOptions {
    message?: string;
    time?: Date;
}
export function createV1beta1VolumeError(options: V1beta1VolumeErrorOptions = {}): V1beta1VolumeError {
    const resource = new V1beta1VolumeError();
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.time !== "undefined") {
        resource.time = options.time;
    }
    return resource;
}
