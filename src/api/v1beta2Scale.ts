import { V1beta2Scale, V1ObjectMeta, V1beta2ScaleSpec, V1beta2ScaleStatus } from "@kubernetes/client-node";
export declare interface V1beta2ScaleOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta2ScaleSpec;
    status?: V1beta2ScaleStatus;
}
export function createV1beta2Scale(options: V1beta2ScaleOptions = {}): V1beta2Scale {
    const resource = new V1beta2Scale();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "Scale";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
