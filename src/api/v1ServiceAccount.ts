import { V1ServiceAccount, V1LocalObjectReference, V1ObjectMeta, V1ObjectReference } from "@kubernetes/client-node";
export declare interface V1ServiceAccountOptions {
    automountServiceAccountToken?: boolean;
    imagePullSecrets?: V1LocalObjectReference[];
    metadata?: V1ObjectMeta;
    secrets?: V1ObjectReference[];
}
export function createV1ServiceAccount(options: V1ServiceAccountOptions = {}): V1ServiceAccount {
    const resource = new V1ServiceAccount();
    resource.apiVersion = "v1";
    resource.kind = "ServiceAccount";
    if (typeof options.automountServiceAccountToken !== "undefined") {
        resource.automountServiceAccountToken = options.automountServiceAccountToken;
    }
    if (typeof options.imagePullSecrets !== "undefined") {
        resource.imagePullSecrets = options.imagePullSecrets;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.secrets !== "undefined") {
        resource.secrets = options.secrets;
    }
    return resource;
}
