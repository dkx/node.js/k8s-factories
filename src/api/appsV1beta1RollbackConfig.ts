import { AppsV1beta1RollbackConfig } from "@kubernetes/client-node";
export declare interface AppsV1beta1RollbackConfigOptions {
    revision?: number;
}
export function createAppsV1beta1RollbackConfig(options: AppsV1beta1RollbackConfigOptions = {}): AppsV1beta1RollbackConfig {
    const resource = new AppsV1beta1RollbackConfig();
    if (typeof options.revision !== "undefined") {
        resource.revision = options.revision;
    }
    return resource;
}
