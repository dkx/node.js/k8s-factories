import { V1SecretProjection, V1KeyToPath } from "@kubernetes/client-node";
export declare interface V1SecretProjectionOptions {
    items?: V1KeyToPath[];
    name?: string;
    optional?: boolean;
}
export function createV1SecretProjection(options: V1SecretProjectionOptions = {}): V1SecretProjection {
    const resource = new V1SecretProjection();
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
