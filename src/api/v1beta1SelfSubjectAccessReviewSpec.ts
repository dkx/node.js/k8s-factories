import { V1beta1SelfSubjectAccessReviewSpec, V1beta1NonResourceAttributes, V1beta1ResourceAttributes } from "@kubernetes/client-node";
export declare interface V1beta1SelfSubjectAccessReviewSpecOptions {
    nonResourceAttributes?: V1beta1NonResourceAttributes;
    resourceAttributes?: V1beta1ResourceAttributes;
}
export function createV1beta1SelfSubjectAccessReviewSpec(options: V1beta1SelfSubjectAccessReviewSpecOptions = {}): V1beta1SelfSubjectAccessReviewSpec {
    const resource = new V1beta1SelfSubjectAccessReviewSpec();
    if (typeof options.nonResourceAttributes !== "undefined") {
        resource.nonResourceAttributes = options.nonResourceAttributes;
    }
    if (typeof options.resourceAttributes !== "undefined") {
        resource.resourceAttributes = options.resourceAttributes;
    }
    return resource;
}
