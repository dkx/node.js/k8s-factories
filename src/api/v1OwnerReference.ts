import { V1OwnerReference } from "@kubernetes/client-node";
export declare interface V1OwnerReferenceOptions {
    apiVersion: string;
    blockOwnerDeletion?: boolean;
    controller?: boolean;
    kind: string;
    name: string;
    uid: string;
}
export function createV1OwnerReference(options: V1OwnerReferenceOptions): V1OwnerReference {
    const resource = new V1OwnerReference();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.blockOwnerDeletion !== "undefined") {
        resource.blockOwnerDeletion = options.blockOwnerDeletion;
    }
    if (typeof options.controller !== "undefined") {
        resource.controller = options.controller;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    return resource;
}
