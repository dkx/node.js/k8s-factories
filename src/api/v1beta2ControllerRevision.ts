import { V1beta2ControllerRevision, RuntimeRawExtension, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1beta2ControllerRevisionOptions {
    data?: RuntimeRawExtension;
    metadata?: V1ObjectMeta;
    revision: number;
}
export function createV1beta2ControllerRevision(options: V1beta2ControllerRevisionOptions): V1beta2ControllerRevision {
    const resource = new V1beta2ControllerRevision();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "ControllerRevision";
    if (typeof options.data !== "undefined") {
        resource.data = options.data;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.revision !== "undefined") {
        resource.revision = options.revision;
    }
    return resource;
}
