import { V1Preconditions } from "@kubernetes/client-node";
export declare interface V1PreconditionsOptions {
    resourceVersion?: string;
    uid?: string;
}
export function createV1Preconditions(options: V1PreconditionsOptions = {}): V1Preconditions {
    const resource = new V1Preconditions();
    if (typeof options.resourceVersion !== "undefined") {
        resource.resourceVersion = options.resourceVersion;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    return resource;
}
