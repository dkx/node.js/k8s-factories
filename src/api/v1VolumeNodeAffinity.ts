import { V1VolumeNodeAffinity, V1NodeSelector } from "@kubernetes/client-node";
export declare interface V1VolumeNodeAffinityOptions {
    required?: V1NodeSelector;
}
export function createV1VolumeNodeAffinity(options: V1VolumeNodeAffinityOptions = {}): V1VolumeNodeAffinity {
    const resource = new V1VolumeNodeAffinity();
    if (typeof options.required !== "undefined") {
        resource.required = options.required;
    }
    return resource;
}
