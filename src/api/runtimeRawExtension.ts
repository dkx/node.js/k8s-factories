import { RuntimeRawExtension } from "@kubernetes/client-node";
export declare interface RuntimeRawExtensionOptions {
    Raw: string;
}
export function createRuntimeRawExtension(options: RuntimeRawExtensionOptions): RuntimeRawExtension {
    const resource = new RuntimeRawExtension();
    if (typeof options.Raw !== "undefined") {
        resource.Raw = options.Raw;
    }
    return resource;
}
