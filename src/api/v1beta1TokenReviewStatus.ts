import { V1beta1TokenReviewStatus, V1beta1UserInfo } from "@kubernetes/client-node";
export declare interface V1beta1TokenReviewStatusOptions {
    audiences?: string[];
    authenticated?: boolean;
    error?: string;
    user?: V1beta1UserInfo;
}
export function createV1beta1TokenReviewStatus(options: V1beta1TokenReviewStatusOptions = {}): V1beta1TokenReviewStatus {
    const resource = new V1beta1TokenReviewStatus();
    if (typeof options.audiences !== "undefined") {
        resource.audiences = options.audiences;
    }
    if (typeof options.authenticated !== "undefined") {
        resource.authenticated = options.authenticated;
    }
    if (typeof options.error !== "undefined") {
        resource.error = options.error;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
