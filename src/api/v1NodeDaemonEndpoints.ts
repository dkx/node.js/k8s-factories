import { V1NodeDaemonEndpoints, V1DaemonEndpoint } from "@kubernetes/client-node";
export declare interface V1NodeDaemonEndpointsOptions {
    kubeletEndpoint?: V1DaemonEndpoint;
}
export function createV1NodeDaemonEndpoints(options: V1NodeDaemonEndpointsOptions = {}): V1NodeDaemonEndpoints {
    const resource = new V1NodeDaemonEndpoints();
    if (typeof options.kubeletEndpoint !== "undefined") {
        resource.kubeletEndpoint = options.kubeletEndpoint;
    }
    return resource;
}
