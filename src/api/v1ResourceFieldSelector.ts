import { V1ResourceFieldSelector } from "@kubernetes/client-node";
export declare interface V1ResourceFieldSelectorOptions {
    containerName?: string;
    divisor?: string;
    resource: string;
}
export function createV1ResourceFieldSelector(options: V1ResourceFieldSelectorOptions): V1ResourceFieldSelector {
    const resource = new V1ResourceFieldSelector();
    if (typeof options.containerName !== "undefined") {
        resource.containerName = options.containerName;
    }
    if (typeof options.divisor !== "undefined") {
        resource.divisor = options.divisor;
    }
    if (typeof options.resource !== "undefined") {
        resource.resource = options.resource;
    }
    return resource;
}
