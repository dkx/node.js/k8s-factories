import { V1beta2ReplicaSetCondition } from "@kubernetes/client-node";
export declare interface V1beta2ReplicaSetConditionOptions {
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1beta2ReplicaSetCondition(options: V1beta2ReplicaSetConditionOptions): V1beta2ReplicaSetCondition {
    const resource = new V1beta2ReplicaSetCondition();
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
