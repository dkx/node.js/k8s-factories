import { V1beta1CronJobSpec, V1beta1JobTemplateSpec } from "@kubernetes/client-node";
export declare interface V1beta1CronJobSpecOptions {
    concurrencyPolicy?: string;
    failedJobsHistoryLimit?: number;
    jobTemplate: V1beta1JobTemplateSpec;
    schedule: string;
    startingDeadlineSeconds?: number;
    successfulJobsHistoryLimit?: number;
    suspend?: boolean;
}
export function createV1beta1CronJobSpec(options: V1beta1CronJobSpecOptions): V1beta1CronJobSpec {
    const resource = new V1beta1CronJobSpec();
    if (typeof options.concurrencyPolicy !== "undefined") {
        resource.concurrencyPolicy = options.concurrencyPolicy;
    }
    if (typeof options.failedJobsHistoryLimit !== "undefined") {
        resource.failedJobsHistoryLimit = options.failedJobsHistoryLimit;
    }
    if (typeof options.jobTemplate !== "undefined") {
        resource.jobTemplate = options.jobTemplate;
    }
    if (typeof options.schedule !== "undefined") {
        resource.schedule = options.schedule;
    }
    if (typeof options.startingDeadlineSeconds !== "undefined") {
        resource.startingDeadlineSeconds = options.startingDeadlineSeconds;
    }
    if (typeof options.successfulJobsHistoryLimit !== "undefined") {
        resource.successfulJobsHistoryLimit = options.successfulJobsHistoryLimit;
    }
    if (typeof options.suspend !== "undefined") {
        resource.suspend = options.suspend;
    }
    return resource;
}
