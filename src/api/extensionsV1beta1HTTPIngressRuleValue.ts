import { ExtensionsV1beta1HTTPIngressRuleValue, ExtensionsV1beta1HTTPIngressPath } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1HTTPIngressRuleValueOptions {
    paths: ExtensionsV1beta1HTTPIngressPath[];
}
export function createExtensionsV1beta1HTTPIngressRuleValue(options: ExtensionsV1beta1HTTPIngressRuleValueOptions): ExtensionsV1beta1HTTPIngressRuleValue {
    const resource = new ExtensionsV1beta1HTTPIngressRuleValue();
    if (typeof options.paths !== "undefined") {
        resource.paths = options.paths;
    }
    return resource;
}
