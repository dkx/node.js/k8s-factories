import { V1NonResourceRule } from "@kubernetes/client-node";
export declare interface V1NonResourceRuleOptions {
    nonResourceURLs?: string[];
    verbs: string[];
}
export function createV1NonResourceRule(options: V1NonResourceRuleOptions): V1NonResourceRule {
    const resource = new V1NonResourceRule();
    if (typeof options.nonResourceURLs !== "undefined") {
        resource.nonResourceURLs = options.nonResourceURLs;
    }
    if (typeof options.verbs !== "undefined") {
        resource.verbs = options.verbs;
    }
    return resource;
}
