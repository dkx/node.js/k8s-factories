import { AppsV1beta1Scale, V1ObjectMeta, AppsV1beta1ScaleSpec, AppsV1beta1ScaleStatus } from "@kubernetes/client-node";
export declare interface AppsV1beta1ScaleOptions {
    metadata?: V1ObjectMeta;
    spec?: AppsV1beta1ScaleSpec;
    status?: AppsV1beta1ScaleStatus;
}
export function createAppsV1beta1Scale(options: AppsV1beta1ScaleOptions = {}): AppsV1beta1Scale {
    const resource = new AppsV1beta1Scale();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "Scale";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
