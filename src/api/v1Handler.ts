import { V1Handler, V1ExecAction, V1HTTPGetAction, V1TCPSocketAction } from "@kubernetes/client-node";
export declare interface V1HandlerOptions {
    exec?: V1ExecAction;
    httpGet?: V1HTTPGetAction;
    tcpSocket?: V1TCPSocketAction;
}
export function createV1Handler(options: V1HandlerOptions = {}): V1Handler {
    const resource = new V1Handler();
    if (typeof options.exec !== "undefined") {
        resource.exec = options.exec;
    }
    if (typeof options.httpGet !== "undefined") {
        resource.httpGet = options.httpGet;
    }
    if (typeof options.tcpSocket !== "undefined") {
        resource.tcpSocket = options.tcpSocket;
    }
    return resource;
}
