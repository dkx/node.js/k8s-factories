import { V1ClusterRoleList, V1ClusterRole, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ClusterRoleListOptions {
    items: V1ClusterRole[];
    metadata?: V1ListMeta;
}
export function createV1ClusterRoleList(options: V1ClusterRoleListOptions): V1ClusterRoleList {
    const resource = new V1ClusterRoleList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "ClusterRoleList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
