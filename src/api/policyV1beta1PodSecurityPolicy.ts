import { PolicyV1beta1PodSecurityPolicy, V1ObjectMeta, PolicyV1beta1PodSecurityPolicySpec } from "@kubernetes/client-node";
export declare interface PolicyV1beta1PodSecurityPolicyOptions {
    metadata?: V1ObjectMeta;
    spec?: PolicyV1beta1PodSecurityPolicySpec;
}
export function createPolicyV1beta1PodSecurityPolicy(options: PolicyV1beta1PodSecurityPolicyOptions = {}): PolicyV1beta1PodSecurityPolicy {
    const resource = new PolicyV1beta1PodSecurityPolicy();
    resource.apiVersion = "policy/v1beta1";
    resource.kind = "PodSecurityPolicy";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
