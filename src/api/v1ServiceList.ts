import { V1ServiceList, V1Service, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ServiceListOptions {
    items: V1Service[];
    metadata?: V1ListMeta;
}
export function createV1ServiceList(options: V1ServiceListOptions): V1ServiceList {
    const resource = new V1ServiceList();
    resource.apiVersion = "v1";
    resource.kind = "ServiceList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
