import { V2beta2HorizontalPodAutoscalerSpec, V2beta2MetricSpec, V2beta2CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V2beta2HorizontalPodAutoscalerSpecOptions {
    maxReplicas: number;
    metrics?: V2beta2MetricSpec[];
    minReplicas?: number;
    scaleTargetRef: V2beta2CrossVersionObjectReference;
}
export function createV2beta2HorizontalPodAutoscalerSpec(options: V2beta2HorizontalPodAutoscalerSpecOptions): V2beta2HorizontalPodAutoscalerSpec {
    const resource = new V2beta2HorizontalPodAutoscalerSpec();
    if (typeof options.maxReplicas !== "undefined") {
        resource.maxReplicas = options.maxReplicas;
    }
    if (typeof options.metrics !== "undefined") {
        resource.metrics = options.metrics;
    }
    if (typeof options.minReplicas !== "undefined") {
        resource.minReplicas = options.minReplicas;
    }
    if (typeof options.scaleTargetRef !== "undefined") {
        resource.scaleTargetRef = options.scaleTargetRef;
    }
    return resource;
}
