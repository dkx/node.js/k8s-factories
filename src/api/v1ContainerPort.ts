import { V1ContainerPort } from "@kubernetes/client-node";
export declare interface V1ContainerPortOptions {
    containerPort: number;
    hostIP?: string;
    hostPort?: number;
    name?: string;
    protocol?: string;
}
export function createV1ContainerPort(options: V1ContainerPortOptions): V1ContainerPort {
    const resource = new V1ContainerPort();
    if (typeof options.containerPort !== "undefined") {
        resource.containerPort = options.containerPort;
    }
    if (typeof options.hostIP !== "undefined") {
        resource.hostIP = options.hostIP;
    }
    if (typeof options.hostPort !== "undefined") {
        resource.hostPort = options.hostPort;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.protocol !== "undefined") {
        resource.protocol = options.protocol;
    }
    return resource;
}
