import { NetworkingV1beta1HTTPIngressRuleValue, NetworkingV1beta1HTTPIngressPath } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1HTTPIngressRuleValueOptions {
    paths: NetworkingV1beta1HTTPIngressPath[];
}
export function createNetworkingV1beta1HTTPIngressRuleValue(options: NetworkingV1beta1HTTPIngressRuleValueOptions): NetworkingV1beta1HTTPIngressRuleValue {
    const resource = new NetworkingV1beta1HTTPIngressRuleValue();
    if (typeof options.paths !== "undefined") {
        resource.paths = options.paths;
    }
    return resource;
}
