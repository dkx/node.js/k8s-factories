import { V1AttachedVolume } from "@kubernetes/client-node";
export declare interface V1AttachedVolumeOptions {
    devicePath: string;
    name: string;
}
export function createV1AttachedVolume(options: V1AttachedVolumeOptions): V1AttachedVolume {
    const resource = new V1AttachedVolume();
    if (typeof options.devicePath !== "undefined") {
        resource.devicePath = options.devicePath;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
