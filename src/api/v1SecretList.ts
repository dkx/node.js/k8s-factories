import { V1SecretList, V1Secret, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1SecretListOptions {
    items: V1Secret[];
    metadata?: V1ListMeta;
}
export function createV1SecretList(options: V1SecretListOptions): V1SecretList {
    const resource = new V1SecretList();
    resource.apiVersion = "v1";
    resource.kind = "SecretList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
