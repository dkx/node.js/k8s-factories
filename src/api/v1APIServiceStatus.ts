import { V1APIServiceStatus, V1APIServiceCondition } from "@kubernetes/client-node";
export declare interface V1APIServiceStatusOptions {
    conditions?: V1APIServiceCondition[];
}
export function createV1APIServiceStatus(options: V1APIServiceStatusOptions = {}): V1APIServiceStatus {
    const resource = new V1APIServiceStatus();
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    return resource;
}
