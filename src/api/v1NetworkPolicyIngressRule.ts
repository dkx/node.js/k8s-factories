import { V1NetworkPolicyIngressRule, V1NetworkPolicyPeer, V1NetworkPolicyPort } from "@kubernetes/client-node";
export declare interface V1NetworkPolicyIngressRuleOptions {
    from?: V1NetworkPolicyPeer[];
    ports?: V1NetworkPolicyPort[];
}
export function createV1NetworkPolicyIngressRule(options: V1NetworkPolicyIngressRuleOptions = {}): V1NetworkPolicyIngressRule {
    const resource = new V1NetworkPolicyIngressRule();
    if (typeof options.from !== "undefined") {
        resource.from = options.from;
    }
    if (typeof options.ports !== "undefined") {
        resource.ports = options.ports;
    }
    return resource;
}
