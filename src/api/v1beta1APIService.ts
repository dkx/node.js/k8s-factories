import { V1beta1APIService, V1ObjectMeta, V1beta1APIServiceSpec, V1beta1APIServiceStatus } from "@kubernetes/client-node";
export declare interface V1beta1APIServiceOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1APIServiceSpec;
    status?: V1beta1APIServiceStatus;
}
export function createV1beta1APIService(options: V1beta1APIServiceOptions = {}): V1beta1APIService {
    const resource = new V1beta1APIService();
    resource.apiVersion = "apiregistration.k8s.io/v1beta1";
    resource.kind = "APIService";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
