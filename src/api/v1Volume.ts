import { V1Volume, V1AWSElasticBlockStoreVolumeSource, V1AzureDiskVolumeSource, V1AzureFileVolumeSource, V1CephFSVolumeSource, V1CinderVolumeSource, V1ConfigMapVolumeSource, V1CSIVolumeSource, V1DownwardAPIVolumeSource, V1EmptyDirVolumeSource, V1FCVolumeSource, V1FlexVolumeSource, V1FlockerVolumeSource, V1GCEPersistentDiskVolumeSource, V1GitRepoVolumeSource, V1GlusterfsVolumeSource, V1HostPathVolumeSource, V1ISCSIVolumeSource, V1NFSVolumeSource, V1PersistentVolumeClaimVolumeSource, V1PhotonPersistentDiskVolumeSource, V1PortworxVolumeSource, V1ProjectedVolumeSource, V1QuobyteVolumeSource, V1RBDVolumeSource, V1ScaleIOVolumeSource, V1SecretVolumeSource, V1StorageOSVolumeSource, V1VsphereVirtualDiskVolumeSource } from "@kubernetes/client-node";
export declare interface V1VolumeOptions {
    awsElasticBlockStore?: V1AWSElasticBlockStoreVolumeSource;
    azureDisk?: V1AzureDiskVolumeSource;
    azureFile?: V1AzureFileVolumeSource;
    cephfs?: V1CephFSVolumeSource;
    cinder?: V1CinderVolumeSource;
    configMap?: V1ConfigMapVolumeSource;
    csi?: V1CSIVolumeSource;
    downwardAPI?: V1DownwardAPIVolumeSource;
    emptyDir?: V1EmptyDirVolumeSource;
    fc?: V1FCVolumeSource;
    flexVolume?: V1FlexVolumeSource;
    flocker?: V1FlockerVolumeSource;
    gcePersistentDisk?: V1GCEPersistentDiskVolumeSource;
    gitRepo?: V1GitRepoVolumeSource;
    glusterfs?: V1GlusterfsVolumeSource;
    hostPath?: V1HostPathVolumeSource;
    iscsi?: V1ISCSIVolumeSource;
    name: string;
    nfs?: V1NFSVolumeSource;
    persistentVolumeClaim?: V1PersistentVolumeClaimVolumeSource;
    photonPersistentDisk?: V1PhotonPersistentDiskVolumeSource;
    portworxVolume?: V1PortworxVolumeSource;
    projected?: V1ProjectedVolumeSource;
    quobyte?: V1QuobyteVolumeSource;
    rbd?: V1RBDVolumeSource;
    scaleIO?: V1ScaleIOVolumeSource;
    secret?: V1SecretVolumeSource;
    storageos?: V1StorageOSVolumeSource;
    vsphereVolume?: V1VsphereVirtualDiskVolumeSource;
}
export function createV1Volume(options: V1VolumeOptions): V1Volume {
    const resource = new V1Volume();
    if (typeof options.awsElasticBlockStore !== "undefined") {
        resource.awsElasticBlockStore = options.awsElasticBlockStore;
    }
    if (typeof options.azureDisk !== "undefined") {
        resource.azureDisk = options.azureDisk;
    }
    if (typeof options.azureFile !== "undefined") {
        resource.azureFile = options.azureFile;
    }
    if (typeof options.cephfs !== "undefined") {
        resource.cephfs = options.cephfs;
    }
    if (typeof options.cinder !== "undefined") {
        resource.cinder = options.cinder;
    }
    if (typeof options.configMap !== "undefined") {
        resource.configMap = options.configMap;
    }
    if (typeof options.csi !== "undefined") {
        resource.csi = options.csi;
    }
    if (typeof options.downwardAPI !== "undefined") {
        resource.downwardAPI = options.downwardAPI;
    }
    if (typeof options.emptyDir !== "undefined") {
        resource.emptyDir = options.emptyDir;
    }
    if (typeof options.fc !== "undefined") {
        resource.fc = options.fc;
    }
    if (typeof options.flexVolume !== "undefined") {
        resource.flexVolume = options.flexVolume;
    }
    if (typeof options.flocker !== "undefined") {
        resource.flocker = options.flocker;
    }
    if (typeof options.gcePersistentDisk !== "undefined") {
        resource.gcePersistentDisk = options.gcePersistentDisk;
    }
    if (typeof options.gitRepo !== "undefined") {
        resource.gitRepo = options.gitRepo;
    }
    if (typeof options.glusterfs !== "undefined") {
        resource.glusterfs = options.glusterfs;
    }
    if (typeof options.hostPath !== "undefined") {
        resource.hostPath = options.hostPath;
    }
    if (typeof options.iscsi !== "undefined") {
        resource.iscsi = options.iscsi;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.nfs !== "undefined") {
        resource.nfs = options.nfs;
    }
    if (typeof options.persistentVolumeClaim !== "undefined") {
        resource.persistentVolumeClaim = options.persistentVolumeClaim;
    }
    if (typeof options.photonPersistentDisk !== "undefined") {
        resource.photonPersistentDisk = options.photonPersistentDisk;
    }
    if (typeof options.portworxVolume !== "undefined") {
        resource.portworxVolume = options.portworxVolume;
    }
    if (typeof options.projected !== "undefined") {
        resource.projected = options.projected;
    }
    if (typeof options.quobyte !== "undefined") {
        resource.quobyte = options.quobyte;
    }
    if (typeof options.rbd !== "undefined") {
        resource.rbd = options.rbd;
    }
    if (typeof options.scaleIO !== "undefined") {
        resource.scaleIO = options.scaleIO;
    }
    if (typeof options.secret !== "undefined") {
        resource.secret = options.secret;
    }
    if (typeof options.storageos !== "undefined") {
        resource.storageos = options.storageos;
    }
    if (typeof options.vsphereVolume !== "undefined") {
        resource.vsphereVolume = options.vsphereVolume;
    }
    return resource;
}
