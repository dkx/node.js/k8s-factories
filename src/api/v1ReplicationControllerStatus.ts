import { V1ReplicationControllerStatus, V1ReplicationControllerCondition } from "@kubernetes/client-node";
export declare interface V1ReplicationControllerStatusOptions {
    availableReplicas?: number;
    conditions?: V1ReplicationControllerCondition[];
    fullyLabeledReplicas?: number;
    observedGeneration?: number;
    readyReplicas?: number;
    replicas: number;
}
export function createV1ReplicationControllerStatus(options: V1ReplicationControllerStatusOptions): V1ReplicationControllerStatus {
    const resource = new V1ReplicationControllerStatus();
    if (typeof options.availableReplicas !== "undefined") {
        resource.availableReplicas = options.availableReplicas;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.fullyLabeledReplicas !== "undefined") {
        resource.fullyLabeledReplicas = options.fullyLabeledReplicas;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.readyReplicas !== "undefined") {
        resource.readyReplicas = options.readyReplicas;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    return resource;
}
