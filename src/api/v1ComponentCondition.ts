import { V1ComponentCondition } from "@kubernetes/client-node";
export declare interface V1ComponentConditionOptions {
    error?: string;
    message?: string;
    status: string;
    type: string;
}
export function createV1ComponentCondition(options: V1ComponentConditionOptions): V1ComponentCondition {
    const resource = new V1ComponentCondition();
    if (typeof options.error !== "undefined") {
        resource.error = options.error;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
