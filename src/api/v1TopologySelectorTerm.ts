import { V1TopologySelectorTerm, V1TopologySelectorLabelRequirement } from "@kubernetes/client-node";
export declare interface V1TopologySelectorTermOptions {
    matchLabelExpressions?: V1TopologySelectorLabelRequirement[];
}
export function createV1TopologySelectorTerm(options: V1TopologySelectorTermOptions = {}): V1TopologySelectorTerm {
    const resource = new V1TopologySelectorTerm();
    if (typeof options.matchLabelExpressions !== "undefined") {
        resource.matchLabelExpressions = options.matchLabelExpressions;
    }
    return resource;
}
