import { AdmissionregistrationV1beta1WebhookClientConfig, AdmissionregistrationV1beta1ServiceReference } from "@kubernetes/client-node";
export declare interface AdmissionregistrationV1beta1WebhookClientConfigOptions {
    caBundle?: string;
    service?: AdmissionregistrationV1beta1ServiceReference;
    url?: string;
}
export function createAdmissionregistrationV1beta1WebhookClientConfig(options: AdmissionregistrationV1beta1WebhookClientConfigOptions = {}): AdmissionregistrationV1beta1WebhookClientConfig {
    const resource = new AdmissionregistrationV1beta1WebhookClientConfig();
    if (typeof options.caBundle !== "undefined") {
        resource.caBundle = options.caBundle;
    }
    if (typeof options.service !== "undefined") {
        resource.service = options.service;
    }
    if (typeof options.url !== "undefined") {
        resource.url = options.url;
    }
    return resource;
}
