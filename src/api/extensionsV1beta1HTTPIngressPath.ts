import { ExtensionsV1beta1HTTPIngressPath, ExtensionsV1beta1IngressBackend } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1HTTPIngressPathOptions {
    backend: ExtensionsV1beta1IngressBackend;
    path?: string;
}
export function createExtensionsV1beta1HTTPIngressPath(options: ExtensionsV1beta1HTTPIngressPathOptions): ExtensionsV1beta1HTTPIngressPath {
    const resource = new ExtensionsV1beta1HTTPIngressPath();
    if (typeof options.backend !== "undefined") {
        resource.backend = options.backend;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    return resource;
}
