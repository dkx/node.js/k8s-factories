import { ExtensionsV1beta1IngressTLS } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressTLSOptions {
    hosts?: string[];
    secretName?: string;
}
export function createExtensionsV1beta1IngressTLS(options: ExtensionsV1beta1IngressTLSOptions = {}): ExtensionsV1beta1IngressTLS {
    const resource = new ExtensionsV1beta1IngressTLS();
    if (typeof options.hosts !== "undefined") {
        resource.hosts = options.hosts;
    }
    if (typeof options.secretName !== "undefined") {
        resource.secretName = options.secretName;
    }
    return resource;
}
