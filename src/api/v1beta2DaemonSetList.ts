import { V1beta2DaemonSetList, V1beta2DaemonSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta2DaemonSetListOptions {
    items: V1beta2DaemonSet[];
    metadata?: V1ListMeta;
}
export function createV1beta2DaemonSetList(options: V1beta2DaemonSetListOptions): V1beta2DaemonSetList {
    const resource = new V1beta2DaemonSetList();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "DaemonSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
