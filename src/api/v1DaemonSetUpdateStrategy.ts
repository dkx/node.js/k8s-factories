import { V1DaemonSetUpdateStrategy, V1RollingUpdateDaemonSet } from "@kubernetes/client-node";
export declare interface V1DaemonSetUpdateStrategyOptions {
    rollingUpdate?: V1RollingUpdateDaemonSet;
    type?: string;
}
export function createV1DaemonSetUpdateStrategy(options: V1DaemonSetUpdateStrategyOptions = {}): V1DaemonSetUpdateStrategy {
    const resource = new V1DaemonSetUpdateStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
