import { V1alpha1AggregationRule, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1alpha1AggregationRuleOptions {
    clusterRoleSelectors?: V1LabelSelector[];
}
export function createV1alpha1AggregationRule(options: V1alpha1AggregationRuleOptions = {}): V1alpha1AggregationRule {
    const resource = new V1alpha1AggregationRule();
    if (typeof options.clusterRoleSelectors !== "undefined") {
        resource.clusterRoleSelectors = options.clusterRoleSelectors;
    }
    return resource;
}
