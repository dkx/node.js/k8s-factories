import { V1ObjectReference } from "@kubernetes/client-node";
export declare interface V1ObjectReferenceOptions {
    apiVersion?: string;
    fieldPath?: string;
    kind?: string;
    name?: string;
    namespace?: string;
    resourceVersion?: string;
    uid?: string;
}
export function createV1ObjectReference(options: V1ObjectReferenceOptions = {}): V1ObjectReference {
    const resource = new V1ObjectReference();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.fieldPath !== "undefined") {
        resource.fieldPath = options.fieldPath;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.resourceVersion !== "undefined") {
        resource.resourceVersion = options.resourceVersion;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    return resource;
}
