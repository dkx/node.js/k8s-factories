import { V1Lifecycle, V1Handler } from "@kubernetes/client-node";
export declare interface V1LifecycleOptions {
    postStart?: V1Handler;
    preStop?: V1Handler;
}
export function createV1Lifecycle(options: V1LifecycleOptions = {}): V1Lifecycle {
    const resource = new V1Lifecycle();
    if (typeof options.postStart !== "undefined") {
        resource.postStart = options.postStart;
    }
    if (typeof options.preStop !== "undefined") {
        resource.preStop = options.preStop;
    }
    return resource;
}
