import { V1PodStatus, V1PodCondition, V1ContainerStatus } from "@kubernetes/client-node";
export declare interface V1PodStatusOptions {
    conditions?: V1PodCondition[];
    containerStatuses?: V1ContainerStatus[];
    hostIP?: string;
    initContainerStatuses?: V1ContainerStatus[];
    message?: string;
    nominatedNodeName?: string;
    phase?: string;
    podIP?: string;
    qosClass?: string;
    reason?: string;
    startTime?: Date;
}
export function createV1PodStatus(options: V1PodStatusOptions = {}): V1PodStatus {
    const resource = new V1PodStatus();
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.containerStatuses !== "undefined") {
        resource.containerStatuses = options.containerStatuses;
    }
    if (typeof options.hostIP !== "undefined") {
        resource.hostIP = options.hostIP;
    }
    if (typeof options.initContainerStatuses !== "undefined") {
        resource.initContainerStatuses = options.initContainerStatuses;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.nominatedNodeName !== "undefined") {
        resource.nominatedNodeName = options.nominatedNodeName;
    }
    if (typeof options.phase !== "undefined") {
        resource.phase = options.phase;
    }
    if (typeof options.podIP !== "undefined") {
        resource.podIP = options.podIP;
    }
    if (typeof options.qosClass !== "undefined") {
        resource.qosClass = options.qosClass;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.startTime !== "undefined") {
        resource.startTime = options.startTime;
    }
    return resource;
}
