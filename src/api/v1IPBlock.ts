import { V1IPBlock } from "@kubernetes/client-node";
export declare interface V1IPBlockOptions {
    cidr: string;
    except?: string[];
}
export function createV1IPBlock(options: V1IPBlockOptions): V1IPBlock {
    const resource = new V1IPBlock();
    if (typeof options.cidr !== "undefined") {
        resource.cidr = options.cidr;
    }
    if (typeof options.except !== "undefined") {
        resource.except = options.except;
    }
    return resource;
}
