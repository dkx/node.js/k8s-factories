import { V1ResourceQuotaStatus } from "@kubernetes/client-node";
export declare interface V1ResourceQuotaStatusOptions {
    hard?: {
        [key: string]: string;
    };
    used?: {
        [key: string]: string;
    };
}
export function createV1ResourceQuotaStatus(options: V1ResourceQuotaStatusOptions = {}): V1ResourceQuotaStatus {
    const resource = new V1ResourceQuotaStatus();
    if (typeof options.hard !== "undefined") {
        resource.hard = options.hard;
    }
    if (typeof options.used !== "undefined") {
        resource.used = options.used;
    }
    return resource;
}
