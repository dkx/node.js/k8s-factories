import { V1JobSpec, V1LabelSelector, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface V1JobSpecOptions {
    activeDeadlineSeconds?: number;
    backoffLimit?: number;
    completions?: number;
    manualSelector?: boolean;
    parallelism?: number;
    selector?: V1LabelSelector;
    template: V1PodTemplateSpec;
    ttlSecondsAfterFinished?: number;
}
export function createV1JobSpec(options: V1JobSpecOptions): V1JobSpec {
    const resource = new V1JobSpec();
    if (typeof options.activeDeadlineSeconds !== "undefined") {
        resource.activeDeadlineSeconds = options.activeDeadlineSeconds;
    }
    if (typeof options.backoffLimit !== "undefined") {
        resource.backoffLimit = options.backoffLimit;
    }
    if (typeof options.completions !== "undefined") {
        resource.completions = options.completions;
    }
    if (typeof options.manualSelector !== "undefined") {
        resource.manualSelector = options.manualSelector;
    }
    if (typeof options.parallelism !== "undefined") {
        resource.parallelism = options.parallelism;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    if (typeof options.ttlSecondsAfterFinished !== "undefined") {
        resource.ttlSecondsAfterFinished = options.ttlSecondsAfterFinished;
    }
    return resource;
}
