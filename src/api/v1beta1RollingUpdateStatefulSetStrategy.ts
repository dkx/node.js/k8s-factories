import { V1beta1RollingUpdateStatefulSetStrategy } from "@kubernetes/client-node";
export declare interface V1beta1RollingUpdateStatefulSetStrategyOptions {
    partition?: number;
}
export function createV1beta1RollingUpdateStatefulSetStrategy(options: V1beta1RollingUpdateStatefulSetStrategyOptions = {}): V1beta1RollingUpdateStatefulSetStrategy {
    const resource = new V1beta1RollingUpdateStatefulSetStrategy();
    if (typeof options.partition !== "undefined") {
        resource.partition = options.partition;
    }
    return resource;
}
