import { V1EndpointsList, V1Endpoints, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1EndpointsListOptions {
    items: V1Endpoints[];
    metadata?: V1ListMeta;
}
export function createV1EndpointsList(options: V1EndpointsListOptions): V1EndpointsList {
    const resource = new V1EndpointsList();
    resource.apiVersion = "v1";
    resource.kind = "EndpointsList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
