import { PolicyV1beta1AllowedHostPath } from "@kubernetes/client-node";
export declare interface PolicyV1beta1AllowedHostPathOptions {
    pathPrefix?: string;
    readOnly?: boolean;
}
export function createPolicyV1beta1AllowedHostPath(options: PolicyV1beta1AllowedHostPathOptions = {}): PolicyV1beta1AllowedHostPath {
    const resource = new PolicyV1beta1AllowedHostPath();
    if (typeof options.pathPrefix !== "undefined") {
        resource.pathPrefix = options.pathPrefix;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
