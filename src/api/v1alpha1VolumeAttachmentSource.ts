import { V1alpha1VolumeAttachmentSource, V1PersistentVolumeSpec } from "@kubernetes/client-node";
export declare interface V1alpha1VolumeAttachmentSourceOptions {
    inlineVolumeSpec?: V1PersistentVolumeSpec;
    persistentVolumeName?: string;
}
export function createV1alpha1VolumeAttachmentSource(options: V1alpha1VolumeAttachmentSourceOptions = {}): V1alpha1VolumeAttachmentSource {
    const resource = new V1alpha1VolumeAttachmentSource();
    if (typeof options.inlineVolumeSpec !== "undefined") {
        resource.inlineVolumeSpec = options.inlineVolumeSpec;
    }
    if (typeof options.persistentVolumeName !== "undefined") {
        resource.persistentVolumeName = options.persistentVolumeName;
    }
    return resource;
}
