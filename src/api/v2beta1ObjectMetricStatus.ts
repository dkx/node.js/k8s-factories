import { V2beta1ObjectMetricStatus, V1LabelSelector, V2beta1CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V2beta1ObjectMetricStatusOptions {
    averageValue?: string;
    currentValue: string;
    metricName: string;
    selector?: V1LabelSelector;
    target: V2beta1CrossVersionObjectReference;
}
export function createV2beta1ObjectMetricStatus(options: V2beta1ObjectMetricStatusOptions): V2beta1ObjectMetricStatus {
    const resource = new V2beta1ObjectMetricStatus();
    if (typeof options.averageValue !== "undefined") {
        resource.averageValue = options.averageValue;
    }
    if (typeof options.currentValue !== "undefined") {
        resource.currentValue = options.currentValue;
    }
    if (typeof options.metricName !== "undefined") {
        resource.metricName = options.metricName;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    return resource;
}
