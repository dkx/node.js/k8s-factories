import { V1EnvFromSource, V1ConfigMapEnvSource, V1SecretEnvSource } from "@kubernetes/client-node";
export declare interface V1EnvFromSourceOptions {
    configMapRef?: V1ConfigMapEnvSource;
    prefix?: string;
    secretRef?: V1SecretEnvSource;
}
export function createV1EnvFromSource(options: V1EnvFromSourceOptions = {}): V1EnvFromSource {
    const resource = new V1EnvFromSource();
    if (typeof options.configMapRef !== "undefined") {
        resource.configMapRef = options.configMapRef;
    }
    if (typeof options.prefix !== "undefined") {
        resource.prefix = options.prefix;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    return resource;
}
