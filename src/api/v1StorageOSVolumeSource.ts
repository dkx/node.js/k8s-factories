import { V1StorageOSVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1StorageOSVolumeSourceOptions {
    fsType?: string;
    readOnly?: boolean;
    secretRef?: V1LocalObjectReference;
    volumeName?: string;
    volumeNamespace?: string;
}
export function createV1StorageOSVolumeSource(options: V1StorageOSVolumeSourceOptions = {}): V1StorageOSVolumeSource {
    const resource = new V1StorageOSVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.volumeName !== "undefined") {
        resource.volumeName = options.volumeName;
    }
    if (typeof options.volumeNamespace !== "undefined") {
        resource.volumeNamespace = options.volumeNamespace;
    }
    return resource;
}
