import { V1beta1AggregationRule, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1beta1AggregationRuleOptions {
    clusterRoleSelectors?: V1LabelSelector[];
}
export function createV1beta1AggregationRule(options: V1beta1AggregationRuleOptions = {}): V1beta1AggregationRule {
    const resource = new V1beta1AggregationRule();
    if (typeof options.clusterRoleSelectors !== "undefined") {
        resource.clusterRoleSelectors = options.clusterRoleSelectors;
    }
    return resource;
}
