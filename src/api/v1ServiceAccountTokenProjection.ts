import { V1ServiceAccountTokenProjection } from "@kubernetes/client-node";
export declare interface V1ServiceAccountTokenProjectionOptions {
    audience?: string;
    expirationSeconds?: number;
    path: string;
}
export function createV1ServiceAccountTokenProjection(options: V1ServiceAccountTokenProjectionOptions): V1ServiceAccountTokenProjection {
    const resource = new V1ServiceAccountTokenProjection();
    if (typeof options.audience !== "undefined") {
        resource.audience = options.audience;
    }
    if (typeof options.expirationSeconds !== "undefined") {
        resource.expirationSeconds = options.expirationSeconds;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    return resource;
}
