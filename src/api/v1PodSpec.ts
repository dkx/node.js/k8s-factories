import { V1PodSpec, V1Affinity, V1Container, V1PodDNSConfig, V1HostAlias, V1LocalObjectReference, V1PodReadinessGate, V1PodSecurityContext, V1Toleration, V1Volume } from "@kubernetes/client-node";
export declare interface V1PodSpecOptions {
    activeDeadlineSeconds?: number;
    affinity?: V1Affinity;
    automountServiceAccountToken?: boolean;
    containers: V1Container[];
    dnsConfig?: V1PodDNSConfig;
    dnsPolicy?: string;
    enableServiceLinks?: boolean;
    hostAliases?: V1HostAlias[];
    hostIPC?: boolean;
    hostNetwork?: boolean;
    hostPID?: boolean;
    hostname?: string;
    imagePullSecrets?: V1LocalObjectReference[];
    initContainers?: V1Container[];
    nodeName?: string;
    nodeSelector?: {
        [key: string]: string;
    };
    preemptionPolicy?: string;
    priority?: number;
    priorityClassName?: string;
    readinessGates?: V1PodReadinessGate[];
    restartPolicy?: string;
    runtimeClassName?: string;
    schedulerName?: string;
    securityContext?: V1PodSecurityContext;
    serviceAccount?: string;
    serviceAccountName?: string;
    shareProcessNamespace?: boolean;
    subdomain?: string;
    terminationGracePeriodSeconds?: number;
    tolerations?: V1Toleration[];
    volumes?: V1Volume[];
}
export function createV1PodSpec(options: V1PodSpecOptions): V1PodSpec {
    const resource = new V1PodSpec();
    if (typeof options.activeDeadlineSeconds !== "undefined") {
        resource.activeDeadlineSeconds = options.activeDeadlineSeconds;
    }
    if (typeof options.affinity !== "undefined") {
        resource.affinity = options.affinity;
    }
    if (typeof options.automountServiceAccountToken !== "undefined") {
        resource.automountServiceAccountToken = options.automountServiceAccountToken;
    }
    if (typeof options.containers !== "undefined") {
        resource.containers = options.containers;
    }
    if (typeof options.dnsConfig !== "undefined") {
        resource.dnsConfig = options.dnsConfig;
    }
    if (typeof options.dnsPolicy !== "undefined") {
        resource.dnsPolicy = options.dnsPolicy;
    }
    if (typeof options.enableServiceLinks !== "undefined") {
        resource.enableServiceLinks = options.enableServiceLinks;
    }
    if (typeof options.hostAliases !== "undefined") {
        resource.hostAliases = options.hostAliases;
    }
    if (typeof options.hostIPC !== "undefined") {
        resource.hostIPC = options.hostIPC;
    }
    if (typeof options.hostNetwork !== "undefined") {
        resource.hostNetwork = options.hostNetwork;
    }
    if (typeof options.hostPID !== "undefined") {
        resource.hostPID = options.hostPID;
    }
    if (typeof options.hostname !== "undefined") {
        resource.hostname = options.hostname;
    }
    if (typeof options.imagePullSecrets !== "undefined") {
        resource.imagePullSecrets = options.imagePullSecrets;
    }
    if (typeof options.initContainers !== "undefined") {
        resource.initContainers = options.initContainers;
    }
    if (typeof options.nodeName !== "undefined") {
        resource.nodeName = options.nodeName;
    }
    if (typeof options.nodeSelector !== "undefined") {
        resource.nodeSelector = options.nodeSelector;
    }
    if (typeof options.preemptionPolicy !== "undefined") {
        resource.preemptionPolicy = options.preemptionPolicy;
    }
    if (typeof options.priority !== "undefined") {
        resource.priority = options.priority;
    }
    if (typeof options.priorityClassName !== "undefined") {
        resource.priorityClassName = options.priorityClassName;
    }
    if (typeof options.readinessGates !== "undefined") {
        resource.readinessGates = options.readinessGates;
    }
    if (typeof options.restartPolicy !== "undefined") {
        resource.restartPolicy = options.restartPolicy;
    }
    if (typeof options.runtimeClassName !== "undefined") {
        resource.runtimeClassName = options.runtimeClassName;
    }
    if (typeof options.schedulerName !== "undefined") {
        resource.schedulerName = options.schedulerName;
    }
    if (typeof options.securityContext !== "undefined") {
        resource.securityContext = options.securityContext;
    }
    if (typeof options.serviceAccount !== "undefined") {
        resource.serviceAccount = options.serviceAccount;
    }
    if (typeof options.serviceAccountName !== "undefined") {
        resource.serviceAccountName = options.serviceAccountName;
    }
    if (typeof options.shareProcessNamespace !== "undefined") {
        resource.shareProcessNamespace = options.shareProcessNamespace;
    }
    if (typeof options.subdomain !== "undefined") {
        resource.subdomain = options.subdomain;
    }
    if (typeof options.terminationGracePeriodSeconds !== "undefined") {
        resource.terminationGracePeriodSeconds = options.terminationGracePeriodSeconds;
    }
    if (typeof options.tolerations !== "undefined") {
        resource.tolerations = options.tolerations;
    }
    if (typeof options.volumes !== "undefined") {
        resource.volumes = options.volumes;
    }
    return resource;
}
