import { V1beta1CustomResourceDefinitionSpec, V1beta1CustomResourceColumnDefinition, V1beta1CustomResourceConversion, V1beta1CustomResourceDefinitionNames, V1beta1CustomResourceSubresources, V1beta1CustomResourceValidation, V1beta1CustomResourceDefinitionVersion } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceDefinitionSpecOptions {
    additionalPrinterColumns?: V1beta1CustomResourceColumnDefinition[];
    conversion?: V1beta1CustomResourceConversion;
    group: string;
    names: V1beta1CustomResourceDefinitionNames;
    preserveUnknownFields?: boolean;
    scope: string;
    subresources?: V1beta1CustomResourceSubresources;
    validation?: V1beta1CustomResourceValidation;
    version?: string;
    versions?: V1beta1CustomResourceDefinitionVersion[];
}
export function createV1beta1CustomResourceDefinitionSpec(options: V1beta1CustomResourceDefinitionSpecOptions): V1beta1CustomResourceDefinitionSpec {
    const resource = new V1beta1CustomResourceDefinitionSpec();
    if (typeof options.additionalPrinterColumns !== "undefined") {
        resource.additionalPrinterColumns = options.additionalPrinterColumns;
    }
    if (typeof options.conversion !== "undefined") {
        resource.conversion = options.conversion;
    }
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.names !== "undefined") {
        resource.names = options.names;
    }
    if (typeof options.preserveUnknownFields !== "undefined") {
        resource.preserveUnknownFields = options.preserveUnknownFields;
    }
    if (typeof options.scope !== "undefined") {
        resource.scope = options.scope;
    }
    if (typeof options.subresources !== "undefined") {
        resource.subresources = options.subresources;
    }
    if (typeof options.validation !== "undefined") {
        resource.validation = options.validation;
    }
    if (typeof options.version !== "undefined") {
        resource.version = options.version;
    }
    if (typeof options.versions !== "undefined") {
        resource.versions = options.versions;
    }
    return resource;
}
