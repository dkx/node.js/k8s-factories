import { V2beta1HorizontalPodAutoscalerSpec, V2beta1MetricSpec, V2beta1CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V2beta1HorizontalPodAutoscalerSpecOptions {
    maxReplicas: number;
    metrics?: V2beta1MetricSpec[];
    minReplicas?: number;
    scaleTargetRef: V2beta1CrossVersionObjectReference;
}
export function createV2beta1HorizontalPodAutoscalerSpec(options: V2beta1HorizontalPodAutoscalerSpecOptions): V2beta1HorizontalPodAutoscalerSpec {
    const resource = new V2beta1HorizontalPodAutoscalerSpec();
    if (typeof options.maxReplicas !== "undefined") {
        resource.maxReplicas = options.maxReplicas;
    }
    if (typeof options.metrics !== "undefined") {
        resource.metrics = options.metrics;
    }
    if (typeof options.minReplicas !== "undefined") {
        resource.minReplicas = options.minReplicas;
    }
    if (typeof options.scaleTargetRef !== "undefined") {
        resource.scaleTargetRef = options.scaleTargetRef;
    }
    return resource;
}
