import { PolicyV1beta1HostPortRange } from "@kubernetes/client-node";
export declare interface PolicyV1beta1HostPortRangeOptions {
    max: number;
    min: number;
}
export function createPolicyV1beta1HostPortRange(options: PolicyV1beta1HostPortRangeOptions): PolicyV1beta1HostPortRange {
    const resource = new PolicyV1beta1HostPortRange();
    if (typeof options.max !== "undefined") {
        resource.max = options.max;
    }
    if (typeof options.min !== "undefined") {
        resource.min = options.min;
    }
    return resource;
}
