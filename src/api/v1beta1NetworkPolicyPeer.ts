import { V1beta1NetworkPolicyPeer, V1beta1IPBlock, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicyPeerOptions {
    ipBlock?: V1beta1IPBlock;
    namespaceSelector?: V1LabelSelector;
    podSelector?: V1LabelSelector;
}
export function createV1beta1NetworkPolicyPeer(options: V1beta1NetworkPolicyPeerOptions = {}): V1beta1NetworkPolicyPeer {
    const resource = new V1beta1NetworkPolicyPeer();
    if (typeof options.ipBlock !== "undefined") {
        resource.ipBlock = options.ipBlock;
    }
    if (typeof options.namespaceSelector !== "undefined") {
        resource.namespaceSelector = options.namespaceSelector;
    }
    if (typeof options.podSelector !== "undefined") {
        resource.podSelector = options.podSelector;
    }
    return resource;
}
