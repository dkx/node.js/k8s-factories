import { V1beta2StatefulSetStatus, V1beta2StatefulSetCondition } from "@kubernetes/client-node";
export declare interface V1beta2StatefulSetStatusOptions {
    collisionCount?: number;
    conditions?: V1beta2StatefulSetCondition[];
    currentReplicas?: number;
    currentRevision?: string;
    observedGeneration?: number;
    readyReplicas?: number;
    replicas: number;
    updateRevision?: string;
    updatedReplicas?: number;
}
export function createV1beta2StatefulSetStatus(options: V1beta2StatefulSetStatusOptions): V1beta2StatefulSetStatus {
    const resource = new V1beta2StatefulSetStatus();
    if (typeof options.collisionCount !== "undefined") {
        resource.collisionCount = options.collisionCount;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.currentReplicas !== "undefined") {
        resource.currentReplicas = options.currentReplicas;
    }
    if (typeof options.currentRevision !== "undefined") {
        resource.currentRevision = options.currentRevision;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.readyReplicas !== "undefined") {
        resource.readyReplicas = options.readyReplicas;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.updateRevision !== "undefined") {
        resource.updateRevision = options.updateRevision;
    }
    if (typeof options.updatedReplicas !== "undefined") {
        resource.updatedReplicas = options.updatedReplicas;
    }
    return resource;
}
