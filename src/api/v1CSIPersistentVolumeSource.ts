import { V1CSIPersistentVolumeSource, V1SecretReference } from "@kubernetes/client-node";
export declare interface V1CSIPersistentVolumeSourceOptions {
    controllerExpandSecretRef?: V1SecretReference;
    controllerPublishSecretRef?: V1SecretReference;
    driver: string;
    fsType?: string;
    nodePublishSecretRef?: V1SecretReference;
    nodeStageSecretRef?: V1SecretReference;
    readOnly?: boolean;
    volumeAttributes?: {
        [key: string]: string;
    };
    volumeHandle: string;
}
export function createV1CSIPersistentVolumeSource(options: V1CSIPersistentVolumeSourceOptions): V1CSIPersistentVolumeSource {
    const resource = new V1CSIPersistentVolumeSource();
    if (typeof options.controllerExpandSecretRef !== "undefined") {
        resource.controllerExpandSecretRef = options.controllerExpandSecretRef;
    }
    if (typeof options.controllerPublishSecretRef !== "undefined") {
        resource.controllerPublishSecretRef = options.controllerPublishSecretRef;
    }
    if (typeof options.driver !== "undefined") {
        resource.driver = options.driver;
    }
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.nodePublishSecretRef !== "undefined") {
        resource.nodePublishSecretRef = options.nodePublishSecretRef;
    }
    if (typeof options.nodeStageSecretRef !== "undefined") {
        resource.nodeStageSecretRef = options.nodeStageSecretRef;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.volumeAttributes !== "undefined") {
        resource.volumeAttributes = options.volumeAttributes;
    }
    if (typeof options.volumeHandle !== "undefined") {
        resource.volumeHandle = options.volumeHandle;
    }
    return resource;
}
