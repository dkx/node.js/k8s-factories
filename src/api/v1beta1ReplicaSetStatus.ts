import { V1beta1ReplicaSetStatus, V1beta1ReplicaSetCondition } from "@kubernetes/client-node";
export declare interface V1beta1ReplicaSetStatusOptions {
    availableReplicas?: number;
    conditions?: V1beta1ReplicaSetCondition[];
    fullyLabeledReplicas?: number;
    observedGeneration?: number;
    readyReplicas?: number;
    replicas: number;
}
export function createV1beta1ReplicaSetStatus(options: V1beta1ReplicaSetStatusOptions): V1beta1ReplicaSetStatus {
    const resource = new V1beta1ReplicaSetStatus();
    if (typeof options.availableReplicas !== "undefined") {
        resource.availableReplicas = options.availableReplicas;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.fullyLabeledReplicas !== "undefined") {
        resource.fullyLabeledReplicas = options.fullyLabeledReplicas;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.readyReplicas !== "undefined") {
        resource.readyReplicas = options.readyReplicas;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    return resource;
}
