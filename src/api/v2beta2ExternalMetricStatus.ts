import { V2beta2ExternalMetricStatus, V2beta2MetricValueStatus, V2beta2MetricIdentifier } from "@kubernetes/client-node";
export declare interface V2beta2ExternalMetricStatusOptions {
    current: V2beta2MetricValueStatus;
    metric: V2beta2MetricIdentifier;
}
export function createV2beta2ExternalMetricStatus(options: V2beta2ExternalMetricStatusOptions): V2beta2ExternalMetricStatus {
    const resource = new V2beta2ExternalMetricStatus();
    if (typeof options.current !== "undefined") {
        resource.current = options.current;
    }
    if (typeof options.metric !== "undefined") {
        resource.metric = options.metric;
    }
    return resource;
}
