import { V1ScopeSelector, V1ScopedResourceSelectorRequirement } from "@kubernetes/client-node";
export declare interface V1ScopeSelectorOptions {
    matchExpressions?: V1ScopedResourceSelectorRequirement[];
}
export function createV1ScopeSelector(options: V1ScopeSelectorOptions = {}): V1ScopeSelector {
    const resource = new V1ScopeSelector();
    if (typeof options.matchExpressions !== "undefined") {
        resource.matchExpressions = options.matchExpressions;
    }
    return resource;
}
