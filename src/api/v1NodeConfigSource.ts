import { V1NodeConfigSource, V1ConfigMapNodeConfigSource } from "@kubernetes/client-node";
export declare interface V1NodeConfigSourceOptions {
    configMap?: V1ConfigMapNodeConfigSource;
}
export function createV1NodeConfigSource(options: V1NodeConfigSourceOptions = {}): V1NodeConfigSource {
    const resource = new V1NodeConfigSource();
    if (typeof options.configMap !== "undefined") {
        resource.configMap = options.configMap;
    }
    return resource;
}
