import { V1HorizontalPodAutoscaler, V1ObjectMeta, V1HorizontalPodAutoscalerSpec, V1HorizontalPodAutoscalerStatus } from "@kubernetes/client-node";
export declare interface V1HorizontalPodAutoscalerOptions {
    metadata?: V1ObjectMeta;
    spec?: V1HorizontalPodAutoscalerSpec;
    status?: V1HorizontalPodAutoscalerStatus;
}
export function createV1HorizontalPodAutoscaler(options: V1HorizontalPodAutoscalerOptions = {}): V1HorizontalPodAutoscaler {
    const resource = new V1HorizontalPodAutoscaler();
    resource.apiVersion = "autoscaling/v1";
    resource.kind = "HorizontalPodAutoscaler";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
