import { V1VolumeDevice } from "@kubernetes/client-node";
export declare interface V1VolumeDeviceOptions {
    devicePath: string;
    name: string;
}
export function createV1VolumeDevice(options: V1VolumeDeviceOptions): V1VolumeDevice {
    const resource = new V1VolumeDevice();
    if (typeof options.devicePath !== "undefined") {
        resource.devicePath = options.devicePath;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
