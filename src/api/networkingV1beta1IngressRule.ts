import { NetworkingV1beta1IngressRule, NetworkingV1beta1HTTPIngressRuleValue } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressRuleOptions {
    host?: string;
    http?: NetworkingV1beta1HTTPIngressRuleValue;
}
export function createNetworkingV1beta1IngressRule(options: NetworkingV1beta1IngressRuleOptions = {}): NetworkingV1beta1IngressRule {
    const resource = new NetworkingV1beta1IngressRule();
    if (typeof options.host !== "undefined") {
        resource.host = options.host;
    }
    if (typeof options.http !== "undefined") {
        resource.http = options.http;
    }
    return resource;
}
