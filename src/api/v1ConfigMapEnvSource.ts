import { V1ConfigMapEnvSource } from "@kubernetes/client-node";
export declare interface V1ConfigMapEnvSourceOptions {
    name?: string;
    optional?: boolean;
}
export function createV1ConfigMapEnvSource(options: V1ConfigMapEnvSourceOptions = {}): V1ConfigMapEnvSource {
    const resource = new V1ConfigMapEnvSource();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    return resource;
}
