import { V1DaemonEndpoint } from "@kubernetes/client-node";
export declare interface V1DaemonEndpointOptions {
    Port: number;
}
export function createV1DaemonEndpoint(options: V1DaemonEndpointOptions): V1DaemonEndpoint {
    const resource = new V1DaemonEndpoint();
    if (typeof options.Port !== "undefined") {
        resource.Port = options.Port;
    }
    return resource;
}
