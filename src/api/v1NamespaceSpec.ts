import { V1NamespaceSpec } from "@kubernetes/client-node";
export declare interface V1NamespaceSpecOptions {
    finalizers?: string[];
}
export function createV1NamespaceSpec(options: V1NamespaceSpecOptions = {}): V1NamespaceSpec {
    const resource = new V1NamespaceSpec();
    if (typeof options.finalizers !== "undefined") {
        resource.finalizers = options.finalizers;
    }
    return resource;
}
