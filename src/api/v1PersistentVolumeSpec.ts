import { V1PersistentVolumeSpec, V1AWSElasticBlockStoreVolumeSource, V1AzureDiskVolumeSource, V1AzureFilePersistentVolumeSource, V1CephFSPersistentVolumeSource, V1CinderPersistentVolumeSource, V1ObjectReference, V1CSIPersistentVolumeSource, V1FCVolumeSource, V1FlexPersistentVolumeSource, V1FlockerVolumeSource, V1GCEPersistentDiskVolumeSource, V1GlusterfsPersistentVolumeSource, V1HostPathVolumeSource, V1ISCSIPersistentVolumeSource, V1LocalVolumeSource, V1NFSVolumeSource, V1VolumeNodeAffinity, V1PhotonPersistentDiskVolumeSource, V1PortworxVolumeSource, V1QuobyteVolumeSource, V1RBDPersistentVolumeSource, V1ScaleIOPersistentVolumeSource, V1StorageOSPersistentVolumeSource, V1VsphereVirtualDiskVolumeSource } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeSpecOptions {
    accessModes?: string[];
    awsElasticBlockStore?: V1AWSElasticBlockStoreVolumeSource;
    azureDisk?: V1AzureDiskVolumeSource;
    azureFile?: V1AzureFilePersistentVolumeSource;
    capacity?: {
        [key: string]: string;
    };
    cephfs?: V1CephFSPersistentVolumeSource;
    cinder?: V1CinderPersistentVolumeSource;
    claimRef?: V1ObjectReference;
    csi?: V1CSIPersistentVolumeSource;
    fc?: V1FCVolumeSource;
    flexVolume?: V1FlexPersistentVolumeSource;
    flocker?: V1FlockerVolumeSource;
    gcePersistentDisk?: V1GCEPersistentDiskVolumeSource;
    glusterfs?: V1GlusterfsPersistentVolumeSource;
    hostPath?: V1HostPathVolumeSource;
    iscsi?: V1ISCSIPersistentVolumeSource;
    local?: V1LocalVolumeSource;
    mountOptions?: string[];
    nfs?: V1NFSVolumeSource;
    nodeAffinity?: V1VolumeNodeAffinity;
    persistentVolumeReclaimPolicy?: string;
    photonPersistentDisk?: V1PhotonPersistentDiskVolumeSource;
    portworxVolume?: V1PortworxVolumeSource;
    quobyte?: V1QuobyteVolumeSource;
    rbd?: V1RBDPersistentVolumeSource;
    scaleIO?: V1ScaleIOPersistentVolumeSource;
    storageClassName?: string;
    storageos?: V1StorageOSPersistentVolumeSource;
    volumeMode?: string;
    vsphereVolume?: V1VsphereVirtualDiskVolumeSource;
}
export function createV1PersistentVolumeSpec(options: V1PersistentVolumeSpecOptions = {}): V1PersistentVolumeSpec {
    const resource = new V1PersistentVolumeSpec();
    if (typeof options.accessModes !== "undefined") {
        resource.accessModes = options.accessModes;
    }
    if (typeof options.awsElasticBlockStore !== "undefined") {
        resource.awsElasticBlockStore = options.awsElasticBlockStore;
    }
    if (typeof options.azureDisk !== "undefined") {
        resource.azureDisk = options.azureDisk;
    }
    if (typeof options.azureFile !== "undefined") {
        resource.azureFile = options.azureFile;
    }
    if (typeof options.capacity !== "undefined") {
        resource.capacity = options.capacity;
    }
    if (typeof options.cephfs !== "undefined") {
        resource.cephfs = options.cephfs;
    }
    if (typeof options.cinder !== "undefined") {
        resource.cinder = options.cinder;
    }
    if (typeof options.claimRef !== "undefined") {
        resource.claimRef = options.claimRef;
    }
    if (typeof options.csi !== "undefined") {
        resource.csi = options.csi;
    }
    if (typeof options.fc !== "undefined") {
        resource.fc = options.fc;
    }
    if (typeof options.flexVolume !== "undefined") {
        resource.flexVolume = options.flexVolume;
    }
    if (typeof options.flocker !== "undefined") {
        resource.flocker = options.flocker;
    }
    if (typeof options.gcePersistentDisk !== "undefined") {
        resource.gcePersistentDisk = options.gcePersistentDisk;
    }
    if (typeof options.glusterfs !== "undefined") {
        resource.glusterfs = options.glusterfs;
    }
    if (typeof options.hostPath !== "undefined") {
        resource.hostPath = options.hostPath;
    }
    if (typeof options.iscsi !== "undefined") {
        resource.iscsi = options.iscsi;
    }
    if (typeof options.local !== "undefined") {
        resource.local = options.local;
    }
    if (typeof options.mountOptions !== "undefined") {
        resource.mountOptions = options.mountOptions;
    }
    if (typeof options.nfs !== "undefined") {
        resource.nfs = options.nfs;
    }
    if (typeof options.nodeAffinity !== "undefined") {
        resource.nodeAffinity = options.nodeAffinity;
    }
    if (typeof options.persistentVolumeReclaimPolicy !== "undefined") {
        resource.persistentVolumeReclaimPolicy = options.persistentVolumeReclaimPolicy;
    }
    if (typeof options.photonPersistentDisk !== "undefined") {
        resource.photonPersistentDisk = options.photonPersistentDisk;
    }
    if (typeof options.portworxVolume !== "undefined") {
        resource.portworxVolume = options.portworxVolume;
    }
    if (typeof options.quobyte !== "undefined") {
        resource.quobyte = options.quobyte;
    }
    if (typeof options.rbd !== "undefined") {
        resource.rbd = options.rbd;
    }
    if (typeof options.scaleIO !== "undefined") {
        resource.scaleIO = options.scaleIO;
    }
    if (typeof options.storageClassName !== "undefined") {
        resource.storageClassName = options.storageClassName;
    }
    if (typeof options.storageos !== "undefined") {
        resource.storageos = options.storageos;
    }
    if (typeof options.volumeMode !== "undefined") {
        resource.volumeMode = options.volumeMode;
    }
    if (typeof options.vsphereVolume !== "undefined") {
        resource.vsphereVolume = options.vsphereVolume;
    }
    return resource;
}
