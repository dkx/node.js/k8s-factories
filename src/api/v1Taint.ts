import { V1Taint } from "@kubernetes/client-node";
export declare interface V1TaintOptions {
    effect: string;
    key: string;
    timeAdded?: Date;
    value?: string;
}
export function createV1Taint(options: V1TaintOptions): V1Taint {
    const resource = new V1Taint();
    if (typeof options.effect !== "undefined") {
        resource.effect = options.effect;
    }
    if (typeof options.key !== "undefined") {
        resource.key = options.key;
    }
    if (typeof options.timeAdded !== "undefined") {
        resource.timeAdded = options.timeAdded;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
