import { V1beta1CSIDriverSpec } from "@kubernetes/client-node";
export declare interface V1beta1CSIDriverSpecOptions {
    attachRequired?: boolean;
    podInfoOnMount?: boolean;
}
export function createV1beta1CSIDriverSpec(options: V1beta1CSIDriverSpecOptions = {}): V1beta1CSIDriverSpec {
    const resource = new V1beta1CSIDriverSpec();
    if (typeof options.attachRequired !== "undefined") {
        resource.attachRequired = options.attachRequired;
    }
    if (typeof options.podInfoOnMount !== "undefined") {
        resource.podInfoOnMount = options.podInfoOnMount;
    }
    return resource;
}
