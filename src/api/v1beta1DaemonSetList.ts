import { V1beta1DaemonSetList, V1beta1DaemonSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1DaemonSetListOptions {
    items: V1beta1DaemonSet[];
    metadata?: V1ListMeta;
}
export function createV1beta1DaemonSetList(options: V1beta1DaemonSetListOptions): V1beta1DaemonSetList {
    const resource = new V1beta1DaemonSetList();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "DaemonSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
