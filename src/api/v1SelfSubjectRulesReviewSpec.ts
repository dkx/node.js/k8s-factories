import { V1SelfSubjectRulesReviewSpec } from "@kubernetes/client-node";
export declare interface V1SelfSubjectRulesReviewSpecOptions {
    namespace?: string;
}
export function createV1SelfSubjectRulesReviewSpec(options: V1SelfSubjectRulesReviewSpecOptions = {}): V1SelfSubjectRulesReviewSpec {
    const resource = new V1SelfSubjectRulesReviewSpec();
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    return resource;
}
