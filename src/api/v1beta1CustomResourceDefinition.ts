import { V1beta1CustomResourceDefinition, V1ObjectMeta, V1beta1CustomResourceDefinitionSpec, V1beta1CustomResourceDefinitionStatus } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceDefinitionOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1CustomResourceDefinitionSpec;
    status?: V1beta1CustomResourceDefinitionStatus;
}
export function createV1beta1CustomResourceDefinition(options: V1beta1CustomResourceDefinitionOptions): V1beta1CustomResourceDefinition {
    const resource = new V1beta1CustomResourceDefinition();
    resource.apiVersion = "apiextensions.k8s.io/v1beta1";
    resource.kind = "CustomResourceDefinition";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
