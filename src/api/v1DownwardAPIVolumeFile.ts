import { V1DownwardAPIVolumeFile, V1ObjectFieldSelector, V1ResourceFieldSelector } from "@kubernetes/client-node";
export declare interface V1DownwardAPIVolumeFileOptions {
    fieldRef?: V1ObjectFieldSelector;
    mode?: number;
    path: string;
    resourceFieldRef?: V1ResourceFieldSelector;
}
export function createV1DownwardAPIVolumeFile(options: V1DownwardAPIVolumeFileOptions): V1DownwardAPIVolumeFile {
    const resource = new V1DownwardAPIVolumeFile();
    if (typeof options.fieldRef !== "undefined") {
        resource.fieldRef = options.fieldRef;
    }
    if (typeof options.mode !== "undefined") {
        resource.mode = options.mode;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.resourceFieldRef !== "undefined") {
        resource.resourceFieldRef = options.resourceFieldRef;
    }
    return resource;
}
