import { V1beta1APIServiceSpec, ApiregistrationV1beta1ServiceReference } from "@kubernetes/client-node";
export declare interface V1beta1APIServiceSpecOptions {
    caBundle?: string;
    group?: string;
    groupPriorityMinimum: number;
    insecureSkipTLSVerify?: boolean;
    service: ApiregistrationV1beta1ServiceReference;
    version?: string;
    versionPriority: number;
}
export function createV1beta1APIServiceSpec(options: V1beta1APIServiceSpecOptions): V1beta1APIServiceSpec {
    const resource = new V1beta1APIServiceSpec();
    if (typeof options.caBundle !== "undefined") {
        resource.caBundle = options.caBundle;
    }
    if (typeof options.group !== "undefined") {
        resource.group = options.group;
    }
    if (typeof options.groupPriorityMinimum !== "undefined") {
        resource.groupPriorityMinimum = options.groupPriorityMinimum;
    }
    if (typeof options.insecureSkipTLSVerify !== "undefined") {
        resource.insecureSkipTLSVerify = options.insecureSkipTLSVerify;
    }
    if (typeof options.service !== "undefined") {
        resource.service = options.service;
    }
    if (typeof options.version !== "undefined") {
        resource.version = options.version;
    }
    if (typeof options.versionPriority !== "undefined") {
        resource.versionPriority = options.versionPriority;
    }
    return resource;
}
