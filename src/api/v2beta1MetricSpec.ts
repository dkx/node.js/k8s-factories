import { V2beta1MetricSpec, V2beta1ExternalMetricSource, V2beta1ObjectMetricSource, V2beta1PodsMetricSource, V2beta1ResourceMetricSource } from "@kubernetes/client-node";
export declare interface V2beta1MetricSpecOptions {
    external?: V2beta1ExternalMetricSource;
    object?: V2beta1ObjectMetricSource;
    pods?: V2beta1PodsMetricSource;
    resource?: V2beta1ResourceMetricSource;
    type: string;
}
export function createV2beta1MetricSpec(options: V2beta1MetricSpecOptions): V2beta1MetricSpec {
    const resource = new V2beta1MetricSpec();
    if (typeof options.external !== "undefined") {
        resource.external = options.external;
    }
    if (typeof options.object !== "undefined") {
        resource.object = options.object;
    }
    if (typeof options.pods !== "undefined") {
        resource.pods = options.pods;
    }
    if (typeof options.resource !== "undefined") {
        resource.resource = options.resource;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
