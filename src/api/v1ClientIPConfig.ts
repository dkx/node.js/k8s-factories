import { V1ClientIPConfig } from "@kubernetes/client-node";
export declare interface V1ClientIPConfigOptions {
    timeoutSeconds?: number;
}
export function createV1ClientIPConfig(options: V1ClientIPConfigOptions = {}): V1ClientIPConfig {
    const resource = new V1ClientIPConfig();
    if (typeof options.timeoutSeconds !== "undefined") {
        resource.timeoutSeconds = options.timeoutSeconds;
    }
    return resource;
}
