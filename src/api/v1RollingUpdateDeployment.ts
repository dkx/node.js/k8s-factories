import { V1RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface V1RollingUpdateDeploymentOptions {
    maxSurge?: object;
    maxUnavailable?: object;
}
export function createV1RollingUpdateDeployment(options: V1RollingUpdateDeploymentOptions = {}): V1RollingUpdateDeployment {
    const resource = new V1RollingUpdateDeployment();
    if (typeof options.maxSurge !== "undefined") {
        resource.maxSurge = options.maxSurge;
    }
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
