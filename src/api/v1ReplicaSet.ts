import { V1ReplicaSet, V1ObjectMeta, V1ReplicaSetSpec, V1ReplicaSetStatus } from "@kubernetes/client-node";
export declare interface V1ReplicaSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1ReplicaSetSpec;
    status?: V1ReplicaSetStatus;
}
export function createV1ReplicaSet(options: V1ReplicaSetOptions = {}): V1ReplicaSet {
    const resource = new V1ReplicaSet();
    resource.apiVersion = "apps/v1";
    resource.kind = "ReplicaSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
