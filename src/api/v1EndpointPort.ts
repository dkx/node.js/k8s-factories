import { V1EndpointPort } from "@kubernetes/client-node";
export declare interface V1EndpointPortOptions {
    name?: string;
    port: number;
    protocol?: string;
}
export function createV1EndpointPort(options: V1EndpointPortOptions): V1EndpointPort {
    const resource = new V1EndpointPort();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    if (typeof options.protocol !== "undefined") {
        resource.protocol = options.protocol;
    }
    return resource;
}
