import { V1alpha1Subject } from "@kubernetes/client-node";
export declare interface V1alpha1SubjectOptions {
    apiVersion?: string;
    kind: string;
    name: string;
    namespace?: string;
}
export function createV1alpha1Subject(options: V1alpha1SubjectOptions): V1alpha1Subject {
    const resource = new V1alpha1Subject();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    return resource;
}
