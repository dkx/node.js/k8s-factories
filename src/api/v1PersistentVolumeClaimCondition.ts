import { V1PersistentVolumeClaimCondition } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeClaimConditionOptions {
    lastProbeTime?: Date;
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1PersistentVolumeClaimCondition(options: V1PersistentVolumeClaimConditionOptions): V1PersistentVolumeClaimCondition {
    const resource = new V1PersistentVolumeClaimCondition();
    if (typeof options.lastProbeTime !== "undefined") {
        resource.lastProbeTime = options.lastProbeTime;
    }
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
