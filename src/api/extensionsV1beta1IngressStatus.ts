import { ExtensionsV1beta1IngressStatus, V1LoadBalancerStatus } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressStatusOptions {
    loadBalancer?: V1LoadBalancerStatus;
}
export function createExtensionsV1beta1IngressStatus(options: ExtensionsV1beta1IngressStatusOptions = {}): ExtensionsV1beta1IngressStatus {
    const resource = new ExtensionsV1beta1IngressStatus();
    if (typeof options.loadBalancer !== "undefined") {
        resource.loadBalancer = options.loadBalancer;
    }
    return resource;
}
