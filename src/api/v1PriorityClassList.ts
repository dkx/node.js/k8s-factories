import { V1PriorityClassList, V1PriorityClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1PriorityClassListOptions {
    items: V1PriorityClass[];
    metadata?: V1ListMeta;
}
export function createV1PriorityClassList(options: V1PriorityClassListOptions): V1PriorityClassList {
    const resource = new V1PriorityClassList();
    resource.apiVersion = "scheduling.k8s.io/v1";
    resource.kind = "PriorityClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
