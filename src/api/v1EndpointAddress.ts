import { V1EndpointAddress, V1ObjectReference } from "@kubernetes/client-node";
export declare interface V1EndpointAddressOptions {
    hostname?: string;
    ip: string;
    nodeName?: string;
    targetRef?: V1ObjectReference;
}
export function createV1EndpointAddress(options: V1EndpointAddressOptions): V1EndpointAddress {
    const resource = new V1EndpointAddress();
    if (typeof options.hostname !== "undefined") {
        resource.hostname = options.hostname;
    }
    if (typeof options.ip !== "undefined") {
        resource.ip = options.ip;
    }
    if (typeof options.nodeName !== "undefined") {
        resource.nodeName = options.nodeName;
    }
    if (typeof options.targetRef !== "undefined") {
        resource.targetRef = options.targetRef;
    }
    return resource;
}
