import { V1Lease, V1ObjectMeta, V1LeaseSpec } from "@kubernetes/client-node";
export declare interface V1LeaseOptions {
    metadata?: V1ObjectMeta;
    spec?: V1LeaseSpec;
}
export function createV1Lease(options: V1LeaseOptions = {}): V1Lease {
    const resource = new V1Lease();
    resource.apiVersion = "coordination.k8s.io/v1";
    resource.kind = "Lease";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
