import { V1CinderPersistentVolumeSource, V1SecretReference } from "@kubernetes/client-node";
export declare interface V1CinderPersistentVolumeSourceOptions {
    fsType?: string;
    readOnly?: boolean;
    secretRef?: V1SecretReference;
    volumeID: string;
}
export function createV1CinderPersistentVolumeSource(options: V1CinderPersistentVolumeSourceOptions): V1CinderPersistentVolumeSource {
    const resource = new V1CinderPersistentVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.volumeID !== "undefined") {
        resource.volumeID = options.volumeID;
    }
    return resource;
}
