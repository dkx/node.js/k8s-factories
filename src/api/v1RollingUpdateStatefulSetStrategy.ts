import { V1RollingUpdateStatefulSetStrategy } from "@kubernetes/client-node";
export declare interface V1RollingUpdateStatefulSetStrategyOptions {
    partition?: number;
}
export function createV1RollingUpdateStatefulSetStrategy(options: V1RollingUpdateStatefulSetStrategyOptions = {}): V1RollingUpdateStatefulSetStrategy {
    const resource = new V1RollingUpdateStatefulSetStrategy();
    if (typeof options.partition !== "undefined") {
        resource.partition = options.partition;
    }
    return resource;
}
