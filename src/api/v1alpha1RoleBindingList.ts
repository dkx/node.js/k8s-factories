import { V1alpha1RoleBindingList, V1alpha1RoleBinding, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1RoleBindingListOptions {
    items: V1alpha1RoleBinding[];
    metadata?: V1ListMeta;
}
export function createV1alpha1RoleBindingList(options: V1alpha1RoleBindingListOptions): V1alpha1RoleBindingList {
    const resource = new V1alpha1RoleBindingList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1alpha1";
    resource.kind = "RoleBindingList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
