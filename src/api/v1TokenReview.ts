import { V1TokenReview, V1ObjectMeta, V1TokenReviewSpec, V1TokenReviewStatus } from "@kubernetes/client-node";
export declare interface V1TokenReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1TokenReviewSpec;
    status?: V1TokenReviewStatus;
}
export function createV1TokenReview(options: V1TokenReviewOptions): V1TokenReview {
    const resource = new V1TokenReview();
    resource.apiVersion = "authentication.k8s.io/v1";
    resource.kind = "TokenReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
