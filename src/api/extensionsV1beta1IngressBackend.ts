import { ExtensionsV1beta1IngressBackend } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1IngressBackendOptions {
    serviceName: string;
    servicePort: object;
}
export function createExtensionsV1beta1IngressBackend(options: ExtensionsV1beta1IngressBackendOptions): ExtensionsV1beta1IngressBackend {
    const resource = new ExtensionsV1beta1IngressBackend();
    if (typeof options.serviceName !== "undefined") {
        resource.serviceName = options.serviceName;
    }
    if (typeof options.servicePort !== "undefined") {
        resource.servicePort = options.servicePort;
    }
    return resource;
}
