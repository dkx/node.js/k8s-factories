import { V1ObjectFieldSelector } from "@kubernetes/client-node";
export declare interface V1ObjectFieldSelectorOptions {
    apiVersion?: string;
    fieldPath: string;
}
export function createV1ObjectFieldSelector(options: V1ObjectFieldSelectorOptions): V1ObjectFieldSelector {
    const resource = new V1ObjectFieldSelector();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.fieldPath !== "undefined") {
        resource.fieldPath = options.fieldPath;
    }
    return resource;
}
