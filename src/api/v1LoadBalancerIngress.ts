import { V1LoadBalancerIngress } from "@kubernetes/client-node";
export declare interface V1LoadBalancerIngressOptions {
    hostname?: string;
    ip?: string;
}
export function createV1LoadBalancerIngress(options: V1LoadBalancerIngressOptions = {}): V1LoadBalancerIngress {
    const resource = new V1LoadBalancerIngress();
    if (typeof options.hostname !== "undefined") {
        resource.hostname = options.hostname;
    }
    if (typeof options.ip !== "undefined") {
        resource.ip = options.ip;
    }
    return resource;
}
