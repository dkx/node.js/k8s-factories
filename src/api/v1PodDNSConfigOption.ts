import { V1PodDNSConfigOption } from "@kubernetes/client-node";
export declare interface V1PodDNSConfigOptionOptions {
    name?: string;
    value?: string;
}
export function createV1PodDNSConfigOption(options: V1PodDNSConfigOptionOptions = {}): V1PodDNSConfigOption {
    const resource = new V1PodDNSConfigOption();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
