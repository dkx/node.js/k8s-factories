import { V1alpha1PriorityClassList, V1alpha1PriorityClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1alpha1PriorityClassListOptions {
    items: V1alpha1PriorityClass[];
    metadata?: V1ListMeta;
}
export function createV1alpha1PriorityClassList(options: V1alpha1PriorityClassListOptions): V1alpha1PriorityClassList {
    const resource = new V1alpha1PriorityClassList();
    resource.apiVersion = "scheduling.k8s.io/v1alpha1";
    resource.kind = "PriorityClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
