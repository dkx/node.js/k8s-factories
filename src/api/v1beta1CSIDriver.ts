import { V1beta1CSIDriver, V1ObjectMeta, V1beta1CSIDriverSpec } from "@kubernetes/client-node";
export declare interface V1beta1CSIDriverOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1CSIDriverSpec;
}
export function createV1beta1CSIDriver(options: V1beta1CSIDriverOptions): V1beta1CSIDriver {
    const resource = new V1beta1CSIDriver();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "CSIDriver";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
