import { V1StatefulSetUpdateStrategy, V1RollingUpdateStatefulSetStrategy } from "@kubernetes/client-node";
export declare interface V1StatefulSetUpdateStrategyOptions {
    rollingUpdate?: V1RollingUpdateStatefulSetStrategy;
    type?: string;
}
export function createV1StatefulSetUpdateStrategy(options: V1StatefulSetUpdateStrategyOptions = {}): V1StatefulSetUpdateStrategy {
    const resource = new V1StatefulSetUpdateStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
