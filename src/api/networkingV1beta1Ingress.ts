import { NetworkingV1beta1Ingress, V1ObjectMeta, NetworkingV1beta1IngressSpec, NetworkingV1beta1IngressStatus } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressOptions {
    metadata?: V1ObjectMeta;
    spec?: NetworkingV1beta1IngressSpec;
    status?: NetworkingV1beta1IngressStatus;
}
export function createNetworkingV1beta1Ingress(options: NetworkingV1beta1IngressOptions = {}): NetworkingV1beta1Ingress {
    const resource = new NetworkingV1beta1Ingress();
    resource.apiVersion = "networking.k8s.io/v1beta1";
    resource.kind = "Ingress";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
