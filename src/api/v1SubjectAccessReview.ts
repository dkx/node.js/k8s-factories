import { V1SubjectAccessReview, V1ObjectMeta, V1SubjectAccessReviewSpec, V1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1SubjectAccessReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1SubjectAccessReviewSpec;
    status?: V1SubjectAccessReviewStatus;
}
export function createV1SubjectAccessReview(options: V1SubjectAccessReviewOptions): V1SubjectAccessReview {
    const resource = new V1SubjectAccessReview();
    resource.apiVersion = "authorization.k8s.io/v1";
    resource.kind = "SubjectAccessReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
