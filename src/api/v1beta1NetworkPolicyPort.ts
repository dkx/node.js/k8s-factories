import { V1beta1NetworkPolicyPort } from "@kubernetes/client-node";
export declare interface V1beta1NetworkPolicyPortOptions {
    port?: object;
    protocol?: string;
}
export function createV1beta1NetworkPolicyPort(options: V1beta1NetworkPolicyPortOptions = {}): V1beta1NetworkPolicyPort {
    const resource = new V1beta1NetworkPolicyPort();
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    if (typeof options.protocol !== "undefined") {
        resource.protocol = options.protocol;
    }
    return resource;
}
