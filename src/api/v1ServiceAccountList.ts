import { V1ServiceAccountList, V1ServiceAccount, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1ServiceAccountListOptions {
    items: V1ServiceAccount[];
    metadata?: V1ListMeta;
}
export function createV1ServiceAccountList(options: V1ServiceAccountListOptions): V1ServiceAccountList {
    const resource = new V1ServiceAccountList();
    resource.apiVersion = "v1";
    resource.kind = "ServiceAccountList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
