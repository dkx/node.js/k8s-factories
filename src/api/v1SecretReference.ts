import { V1SecretReference } from "@kubernetes/client-node";
export declare interface V1SecretReferenceOptions {
    name?: string;
    namespace?: string;
}
export function createV1SecretReference(options: V1SecretReferenceOptions = {}): V1SecretReference {
    const resource = new V1SecretReference();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    return resource;
}
