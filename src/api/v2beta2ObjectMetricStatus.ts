import { V2beta2ObjectMetricStatus, V2beta2MetricValueStatus, V2beta2CrossVersionObjectReference, V2beta2MetricIdentifier } from "@kubernetes/client-node";
export declare interface V2beta2ObjectMetricStatusOptions {
    current: V2beta2MetricValueStatus;
    describedObject: V2beta2CrossVersionObjectReference;
    metric: V2beta2MetricIdentifier;
}
export function createV2beta2ObjectMetricStatus(options: V2beta2ObjectMetricStatusOptions): V2beta2ObjectMetricStatus {
    const resource = new V2beta2ObjectMetricStatus();
    if (typeof options.current !== "undefined") {
        resource.current = options.current;
    }
    if (typeof options.describedObject !== "undefined") {
        resource.describedObject = options.describedObject;
    }
    if (typeof options.metric !== "undefined") {
        resource.metric = options.metric;
    }
    return resource;
}
