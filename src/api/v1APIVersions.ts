import { V1APIVersions, V1ServerAddressByClientCIDR } from "@kubernetes/client-node";
export declare interface V1APIVersionsOptions {
    serverAddressByClientCIDRs: V1ServerAddressByClientCIDR[];
    versions: string[];
}
export function createV1APIVersions(options: V1APIVersionsOptions): V1APIVersions {
    const resource = new V1APIVersions();
    resource.apiVersion = "v1";
    resource.kind = "APIVersions";
    if (typeof options.serverAddressByClientCIDRs !== "undefined") {
        resource.serverAddressByClientCIDRs = options.serverAddressByClientCIDRs;
    }
    if (typeof options.versions !== "undefined") {
        resource.versions = options.versions;
    }
    return resource;
}
