import { V1Pod, V1ObjectMeta, V1PodSpec, V1PodStatus } from "@kubernetes/client-node";
export declare interface V1PodOptions {
    metadata?: V1ObjectMeta;
    spec?: V1PodSpec;
    status?: V1PodStatus;
}
export function createV1Pod(options: V1PodOptions = {}): V1Pod {
    const resource = new V1Pod();
    resource.apiVersion = "v1";
    resource.kind = "Pod";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
