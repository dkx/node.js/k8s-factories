import { V1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1SubjectAccessReviewStatusOptions {
    allowed: boolean;
    denied?: boolean;
    evaluationError?: string;
    reason?: string;
}
export function createV1SubjectAccessReviewStatus(options: V1SubjectAccessReviewStatusOptions): V1SubjectAccessReviewStatus {
    const resource = new V1SubjectAccessReviewStatus();
    if (typeof options.allowed !== "undefined") {
        resource.allowed = options.allowed;
    }
    if (typeof options.denied !== "undefined") {
        resource.denied = options.denied;
    }
    if (typeof options.evaluationError !== "undefined") {
        resource.evaluationError = options.evaluationError;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    return resource;
}
