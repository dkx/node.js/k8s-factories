import { V1PersistentVolumeClaimList, V1PersistentVolumeClaim, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeClaimListOptions {
    items: V1PersistentVolumeClaim[];
    metadata?: V1ListMeta;
}
export function createV1PersistentVolumeClaimList(options: V1PersistentVolumeClaimListOptions): V1PersistentVolumeClaimList {
    const resource = new V1PersistentVolumeClaimList();
    resource.apiVersion = "v1";
    resource.kind = "PersistentVolumeClaimList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
