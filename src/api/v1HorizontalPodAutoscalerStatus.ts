import { V1HorizontalPodAutoscalerStatus } from "@kubernetes/client-node";
export declare interface V1HorizontalPodAutoscalerStatusOptions {
    currentCPUUtilizationPercentage?: number;
    currentReplicas: number;
    desiredReplicas: number;
    lastScaleTime?: Date;
    observedGeneration?: number;
}
export function createV1HorizontalPodAutoscalerStatus(options: V1HorizontalPodAutoscalerStatusOptions): V1HorizontalPodAutoscalerStatus {
    const resource = new V1HorizontalPodAutoscalerStatus();
    if (typeof options.currentCPUUtilizationPercentage !== "undefined") {
        resource.currentCPUUtilizationPercentage = options.currentCPUUtilizationPercentage;
    }
    if (typeof options.currentReplicas !== "undefined") {
        resource.currentReplicas = options.currentReplicas;
    }
    if (typeof options.desiredReplicas !== "undefined") {
        resource.desiredReplicas = options.desiredReplicas;
    }
    if (typeof options.lastScaleTime !== "undefined") {
        resource.lastScaleTime = options.lastScaleTime;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    return resource;
}
