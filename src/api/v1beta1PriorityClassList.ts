import { V1beta1PriorityClassList, V1beta1PriorityClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1PriorityClassListOptions {
    items: V1beta1PriorityClass[];
    metadata?: V1ListMeta;
}
export function createV1beta1PriorityClassList(options: V1beta1PriorityClassListOptions): V1beta1PriorityClassList {
    const resource = new V1beta1PriorityClassList();
    resource.apiVersion = "scheduling.k8s.io/v1beta1";
    resource.kind = "PriorityClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
