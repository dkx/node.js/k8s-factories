import { V1CSIVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1CSIVolumeSourceOptions {
    driver: string;
    fsType?: string;
    nodePublishSecretRef?: V1LocalObjectReference;
    readOnly?: boolean;
    volumeAttributes?: {
        [key: string]: string;
    };
}
export function createV1CSIVolumeSource(options: V1CSIVolumeSourceOptions): V1CSIVolumeSource {
    const resource = new V1CSIVolumeSource();
    if (typeof options.driver !== "undefined") {
        resource.driver = options.driver;
    }
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.nodePublishSecretRef !== "undefined") {
        resource.nodePublishSecretRef = options.nodePublishSecretRef;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.volumeAttributes !== "undefined") {
        resource.volumeAttributes = options.volumeAttributes;
    }
    return resource;
}
