import { V1beta1ClusterRoleBinding, V1ObjectMeta, V1beta1RoleRef, V1beta1Subject } from "@kubernetes/client-node";
export declare interface V1beta1ClusterRoleBindingOptions {
    metadata?: V1ObjectMeta;
    roleRef: V1beta1RoleRef;
    subjects?: V1beta1Subject[];
}
export function createV1beta1ClusterRoleBinding(options: V1beta1ClusterRoleBindingOptions): V1beta1ClusterRoleBinding {
    const resource = new V1beta1ClusterRoleBinding();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "ClusterRoleBinding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.roleRef !== "undefined") {
        resource.roleRef = options.roleRef;
    }
    if (typeof options.subjects !== "undefined") {
        resource.subjects = options.subjects;
    }
    return resource;
}
