import { V1beta1NonResourceRule } from "@kubernetes/client-node";
export declare interface V1beta1NonResourceRuleOptions {
    nonResourceURLs?: string[];
    verbs: string[];
}
export function createV1beta1NonResourceRule(options: V1beta1NonResourceRuleOptions): V1beta1NonResourceRule {
    const resource = new V1beta1NonResourceRule();
    if (typeof options.nonResourceURLs !== "undefined") {
        resource.nonResourceURLs = options.nonResourceURLs;
    }
    if (typeof options.verbs !== "undefined") {
        resource.verbs = options.verbs;
    }
    return resource;
}
