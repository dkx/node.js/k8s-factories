import { V1ContainerStatus, V1ContainerState } from "@kubernetes/client-node";
export declare interface V1ContainerStatusOptions {
    containerID?: string;
    image: string;
    imageID: string;
    lastState?: V1ContainerState;
    name: string;
    ready: boolean;
    restartCount: number;
    state?: V1ContainerState;
}
export function createV1ContainerStatus(options: V1ContainerStatusOptions): V1ContainerStatus {
    const resource = new V1ContainerStatus();
    if (typeof options.containerID !== "undefined") {
        resource.containerID = options.containerID;
    }
    if (typeof options.image !== "undefined") {
        resource.image = options.image;
    }
    if (typeof options.imageID !== "undefined") {
        resource.imageID = options.imageID;
    }
    if (typeof options.lastState !== "undefined") {
        resource.lastState = options.lastState;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.ready !== "undefined") {
        resource.ready = options.ready;
    }
    if (typeof options.restartCount !== "undefined") {
        resource.restartCount = options.restartCount;
    }
    if (typeof options.state !== "undefined") {
        resource.state = options.state;
    }
    return resource;
}
