import { V1DeploymentStatus, V1DeploymentCondition } from "@kubernetes/client-node";
export declare interface V1DeploymentStatusOptions {
    availableReplicas?: number;
    collisionCount?: number;
    conditions?: V1DeploymentCondition[];
    observedGeneration?: number;
    readyReplicas?: number;
    replicas?: number;
    unavailableReplicas?: number;
    updatedReplicas?: number;
}
export function createV1DeploymentStatus(options: V1DeploymentStatusOptions = {}): V1DeploymentStatus {
    const resource = new V1DeploymentStatus();
    if (typeof options.availableReplicas !== "undefined") {
        resource.availableReplicas = options.availableReplicas;
    }
    if (typeof options.collisionCount !== "undefined") {
        resource.collisionCount = options.collisionCount;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    if (typeof options.readyReplicas !== "undefined") {
        resource.readyReplicas = options.readyReplicas;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.unavailableReplicas !== "undefined") {
        resource.unavailableReplicas = options.unavailableReplicas;
    }
    if (typeof options.updatedReplicas !== "undefined") {
        resource.updatedReplicas = options.updatedReplicas;
    }
    return resource;
}
