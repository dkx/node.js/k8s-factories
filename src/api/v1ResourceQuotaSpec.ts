import { V1ResourceQuotaSpec, V1ScopeSelector } from "@kubernetes/client-node";
export declare interface V1ResourceQuotaSpecOptions {
    hard?: {
        [key: string]: string;
    };
    scopeSelector?: V1ScopeSelector;
    scopes?: string[];
}
export function createV1ResourceQuotaSpec(options: V1ResourceQuotaSpecOptions = {}): V1ResourceQuotaSpec {
    const resource = new V1ResourceQuotaSpec();
    if (typeof options.hard !== "undefined") {
        resource.hard = options.hard;
    }
    if (typeof options.scopeSelector !== "undefined") {
        resource.scopeSelector = options.scopeSelector;
    }
    if (typeof options.scopes !== "undefined") {
        resource.scopes = options.scopes;
    }
    return resource;
}
