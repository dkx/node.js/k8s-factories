import { V1beta1StorageClassList, V1beta1StorageClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1StorageClassListOptions {
    items: V1beta1StorageClass[];
    metadata?: V1ListMeta;
}
export function createV1beta1StorageClassList(options: V1beta1StorageClassListOptions): V1beta1StorageClassList {
    const resource = new V1beta1StorageClassList();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "StorageClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
