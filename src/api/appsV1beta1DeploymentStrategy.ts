import { AppsV1beta1DeploymentStrategy, AppsV1beta1RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface AppsV1beta1DeploymentStrategyOptions {
    rollingUpdate?: AppsV1beta1RollingUpdateDeployment;
    type?: string;
}
export function createAppsV1beta1DeploymentStrategy(options: AppsV1beta1DeploymentStrategyOptions = {}): AppsV1beta1DeploymentStrategy {
    const resource = new AppsV1beta1DeploymentStrategy();
    if (typeof options.rollingUpdate !== "undefined") {
        resource.rollingUpdate = options.rollingUpdate;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
