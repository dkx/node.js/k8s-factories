import { PolicyV1beta1SupplementalGroupsStrategyOptions, PolicyV1beta1IDRange } from "@kubernetes/client-node";
export declare interface PolicyV1beta1SupplementalGroupsStrategyOptionsOptions {
    ranges?: PolicyV1beta1IDRange[];
    rule?: string;
}
export function createPolicyV1beta1SupplementalGroupsStrategyOptions(options: PolicyV1beta1SupplementalGroupsStrategyOptionsOptions = {}): PolicyV1beta1SupplementalGroupsStrategyOptions {
    const resource = new PolicyV1beta1SupplementalGroupsStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
