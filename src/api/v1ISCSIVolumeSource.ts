import { V1ISCSIVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1ISCSIVolumeSourceOptions {
    chapAuthDiscovery?: boolean;
    chapAuthSession?: boolean;
    fsType?: string;
    initiatorName?: string;
    iqn: string;
    iscsiInterface?: string;
    lun: number;
    portals?: string[];
    readOnly?: boolean;
    secretRef?: V1LocalObjectReference;
    targetPortal: string;
}
export function createV1ISCSIVolumeSource(options: V1ISCSIVolumeSourceOptions): V1ISCSIVolumeSource {
    const resource = new V1ISCSIVolumeSource();
    if (typeof options.chapAuthDiscovery !== "undefined") {
        resource.chapAuthDiscovery = options.chapAuthDiscovery;
    }
    if (typeof options.chapAuthSession !== "undefined") {
        resource.chapAuthSession = options.chapAuthSession;
    }
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.initiatorName !== "undefined") {
        resource.initiatorName = options.initiatorName;
    }
    if (typeof options.iqn !== "undefined") {
        resource.iqn = options.iqn;
    }
    if (typeof options.iscsiInterface !== "undefined") {
        resource.iscsiInterface = options.iscsiInterface;
    }
    if (typeof options.lun !== "undefined") {
        resource.lun = options.lun;
    }
    if (typeof options.portals !== "undefined") {
        resource.portals = options.portals;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.targetPortal !== "undefined") {
        resource.targetPortal = options.targetPortal;
    }
    return resource;
}
