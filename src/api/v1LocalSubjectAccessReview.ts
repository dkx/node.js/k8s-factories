import { V1LocalSubjectAccessReview, V1ObjectMeta, V1SubjectAccessReviewSpec, V1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1LocalSubjectAccessReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1SubjectAccessReviewSpec;
    status?: V1SubjectAccessReviewStatus;
}
export function createV1LocalSubjectAccessReview(options: V1LocalSubjectAccessReviewOptions): V1LocalSubjectAccessReview {
    const resource = new V1LocalSubjectAccessReview();
    resource.apiVersion = "authorization.k8s.io/v1";
    resource.kind = "LocalSubjectAccessReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
