import { V1FCVolumeSource } from "@kubernetes/client-node";
export declare interface V1FCVolumeSourceOptions {
    fsType?: string;
    lun?: number;
    readOnly?: boolean;
    targetWWNs?: string[];
    wwids?: string[];
}
export function createV1FCVolumeSource(options: V1FCVolumeSourceOptions = {}): V1FCVolumeSource {
    const resource = new V1FCVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.lun !== "undefined") {
        resource.lun = options.lun;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.targetWWNs !== "undefined") {
        resource.targetWWNs = options.targetWWNs;
    }
    if (typeof options.wwids !== "undefined") {
        resource.wwids = options.wwids;
    }
    return resource;
}
