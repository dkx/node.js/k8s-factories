import { V2beta2MetricStatus, V2beta2ExternalMetricStatus, V2beta2ObjectMetricStatus, V2beta2PodsMetricStatus, V2beta2ResourceMetricStatus } from "@kubernetes/client-node";
export declare interface V2beta2MetricStatusOptions {
    external?: V2beta2ExternalMetricStatus;
    object?: V2beta2ObjectMetricStatus;
    pods?: V2beta2PodsMetricStatus;
    resource?: V2beta2ResourceMetricStatus;
    type: string;
}
export function createV2beta2MetricStatus(options: V2beta2MetricStatusOptions): V2beta2MetricStatus {
    const resource = new V2beta2MetricStatus();
    if (typeof options.external !== "undefined") {
        resource.external = options.external;
    }
    if (typeof options.object !== "undefined") {
        resource.object = options.object;
    }
    if (typeof options.pods !== "undefined") {
        resource.pods = options.pods;
    }
    if (typeof options.resource !== "undefined") {
        resource.resource = options.resource;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
