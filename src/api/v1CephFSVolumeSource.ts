import { V1CephFSVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1CephFSVolumeSourceOptions {
    monitors: string[];
    path?: string;
    readOnly?: boolean;
    secretFile?: string;
    secretRef?: V1LocalObjectReference;
    user?: string;
}
export function createV1CephFSVolumeSource(options: V1CephFSVolumeSourceOptions): V1CephFSVolumeSource {
    const resource = new V1CephFSVolumeSource();
    if (typeof options.monitors !== "undefined") {
        resource.monitors = options.monitors;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretFile !== "undefined") {
        resource.secretFile = options.secretFile;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
