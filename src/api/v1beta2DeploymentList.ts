import { V1beta2DeploymentList, V1beta2Deployment, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta2DeploymentListOptions {
    items: V1beta2Deployment[];
    metadata?: V1ListMeta;
}
export function createV1beta2DeploymentList(options: V1beta2DeploymentListOptions): V1beta2DeploymentList {
    const resource = new V1beta2DeploymentList();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "DeploymentList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
