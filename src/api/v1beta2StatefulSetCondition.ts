import { V1beta2StatefulSetCondition } from "@kubernetes/client-node";
export declare interface V1beta2StatefulSetConditionOptions {
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1beta2StatefulSetCondition(options: V1beta2StatefulSetConditionOptions): V1beta2StatefulSetCondition {
    const resource = new V1beta2StatefulSetCondition();
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
