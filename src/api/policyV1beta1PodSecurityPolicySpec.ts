import { PolicyV1beta1PodSecurityPolicySpec, PolicyV1beta1AllowedCSIDriver, PolicyV1beta1AllowedFlexVolume, PolicyV1beta1AllowedHostPath, PolicyV1beta1FSGroupStrategyOptions, PolicyV1beta1HostPortRange, PolicyV1beta1RunAsGroupStrategyOptions, PolicyV1beta1RunAsUserStrategyOptions, PolicyV1beta1RuntimeClassStrategyOptions, PolicyV1beta1SELinuxStrategyOptions, PolicyV1beta1SupplementalGroupsStrategyOptions } from "@kubernetes/client-node";
export declare interface PolicyV1beta1PodSecurityPolicySpecOptions {
    allowPrivilegeEscalation?: boolean;
    allowedCSIDrivers?: PolicyV1beta1AllowedCSIDriver[];
    allowedCapabilities?: string[];
    allowedFlexVolumes?: PolicyV1beta1AllowedFlexVolume[];
    allowedHostPaths?: PolicyV1beta1AllowedHostPath[];
    allowedProcMountTypes?: string[];
    allowedUnsafeSysctls?: string[];
    defaultAddCapabilities?: string[];
    defaultAllowPrivilegeEscalation?: boolean;
    forbiddenSysctls?: string[];
    fsGroup: PolicyV1beta1FSGroupStrategyOptions;
    hostIPC?: boolean;
    hostNetwork?: boolean;
    hostPID?: boolean;
    hostPorts?: PolicyV1beta1HostPortRange[];
    privileged?: boolean;
    readOnlyRootFilesystem?: boolean;
    requiredDropCapabilities?: string[];
    runAsGroup?: PolicyV1beta1RunAsGroupStrategyOptions;
    runAsUser: PolicyV1beta1RunAsUserStrategyOptions;
    runtimeClass?: PolicyV1beta1RuntimeClassStrategyOptions;
    seLinux: PolicyV1beta1SELinuxStrategyOptions;
    supplementalGroups: PolicyV1beta1SupplementalGroupsStrategyOptions;
    volumes?: string[];
}
export function createPolicyV1beta1PodSecurityPolicySpec(options: PolicyV1beta1PodSecurityPolicySpecOptions): PolicyV1beta1PodSecurityPolicySpec {
    const resource = new PolicyV1beta1PodSecurityPolicySpec();
    if (typeof options.allowPrivilegeEscalation !== "undefined") {
        resource.allowPrivilegeEscalation = options.allowPrivilegeEscalation;
    }
    if (typeof options.allowedCSIDrivers !== "undefined") {
        resource.allowedCSIDrivers = options.allowedCSIDrivers;
    }
    if (typeof options.allowedCapabilities !== "undefined") {
        resource.allowedCapabilities = options.allowedCapabilities;
    }
    if (typeof options.allowedFlexVolumes !== "undefined") {
        resource.allowedFlexVolumes = options.allowedFlexVolumes;
    }
    if (typeof options.allowedHostPaths !== "undefined") {
        resource.allowedHostPaths = options.allowedHostPaths;
    }
    if (typeof options.allowedProcMountTypes !== "undefined") {
        resource.allowedProcMountTypes = options.allowedProcMountTypes;
    }
    if (typeof options.allowedUnsafeSysctls !== "undefined") {
        resource.allowedUnsafeSysctls = options.allowedUnsafeSysctls;
    }
    if (typeof options.defaultAddCapabilities !== "undefined") {
        resource.defaultAddCapabilities = options.defaultAddCapabilities;
    }
    if (typeof options.defaultAllowPrivilegeEscalation !== "undefined") {
        resource.defaultAllowPrivilegeEscalation = options.defaultAllowPrivilegeEscalation;
    }
    if (typeof options.forbiddenSysctls !== "undefined") {
        resource.forbiddenSysctls = options.forbiddenSysctls;
    }
    if (typeof options.fsGroup !== "undefined") {
        resource.fsGroup = options.fsGroup;
    }
    if (typeof options.hostIPC !== "undefined") {
        resource.hostIPC = options.hostIPC;
    }
    if (typeof options.hostNetwork !== "undefined") {
        resource.hostNetwork = options.hostNetwork;
    }
    if (typeof options.hostPID !== "undefined") {
        resource.hostPID = options.hostPID;
    }
    if (typeof options.hostPorts !== "undefined") {
        resource.hostPorts = options.hostPorts;
    }
    if (typeof options.privileged !== "undefined") {
        resource.privileged = options.privileged;
    }
    if (typeof options.readOnlyRootFilesystem !== "undefined") {
        resource.readOnlyRootFilesystem = options.readOnlyRootFilesystem;
    }
    if (typeof options.requiredDropCapabilities !== "undefined") {
        resource.requiredDropCapabilities = options.requiredDropCapabilities;
    }
    if (typeof options.runAsGroup !== "undefined") {
        resource.runAsGroup = options.runAsGroup;
    }
    if (typeof options.runAsUser !== "undefined") {
        resource.runAsUser = options.runAsUser;
    }
    if (typeof options.runtimeClass !== "undefined") {
        resource.runtimeClass = options.runtimeClass;
    }
    if (typeof options.seLinux !== "undefined") {
        resource.seLinux = options.seLinux;
    }
    if (typeof options.supplementalGroups !== "undefined") {
        resource.supplementalGroups = options.supplementalGroups;
    }
    if (typeof options.volumes !== "undefined") {
        resource.volumes = options.volumes;
    }
    return resource;
}
