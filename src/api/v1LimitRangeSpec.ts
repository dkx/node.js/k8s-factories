import { V1LimitRangeSpec, V1LimitRangeItem } from "@kubernetes/client-node";
export declare interface V1LimitRangeSpecOptions {
    limits: V1LimitRangeItem[];
}
export function createV1LimitRangeSpec(options: V1LimitRangeSpecOptions): V1LimitRangeSpec {
    const resource = new V1LimitRangeSpec();
    if (typeof options.limits !== "undefined") {
        resource.limits = options.limits;
    }
    return resource;
}
