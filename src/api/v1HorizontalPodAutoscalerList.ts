import { V1HorizontalPodAutoscalerList, V1HorizontalPodAutoscaler, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1HorizontalPodAutoscalerListOptions {
    items: V1HorizontalPodAutoscaler[];
    metadata?: V1ListMeta;
}
export function createV1HorizontalPodAutoscalerList(options: V1HorizontalPodAutoscalerListOptions): V1HorizontalPodAutoscalerList {
    const resource = new V1HorizontalPodAutoscalerList();
    resource.apiVersion = "autoscaling/v1";
    resource.kind = "HorizontalPodAutoscalerList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
