import { V1Initializers, V1Initializer, V1Status } from "@kubernetes/client-node";
export declare interface V1InitializersOptions {
    pending: V1Initializer[];
    result?: V1Status;
}
export function createV1Initializers(options: V1InitializersOptions): V1Initializers {
    const resource = new V1Initializers();
    if (typeof options.pending !== "undefined") {
        resource.pending = options.pending;
    }
    if (typeof options.result !== "undefined") {
        resource.result = options.result;
    }
    return resource;
}
