import { V1beta1MutatingWebhookConfiguration, V1ObjectMeta, V1beta1MutatingWebhook } from "@kubernetes/client-node";
export declare interface V1beta1MutatingWebhookConfigurationOptions {
    metadata?: V1ObjectMeta;
    webhooks?: V1beta1MutatingWebhook[];
}
export function createV1beta1MutatingWebhookConfiguration(options: V1beta1MutatingWebhookConfigurationOptions = {}): V1beta1MutatingWebhookConfiguration {
    const resource = new V1beta1MutatingWebhookConfiguration();
    resource.apiVersion = "admissionregistration.k8s.io/v1beta1";
    resource.kind = "MutatingWebhookConfiguration";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.webhooks !== "undefined") {
        resource.webhooks = options.webhooks;
    }
    return resource;
}
