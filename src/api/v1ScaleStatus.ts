import { V1ScaleStatus } from "@kubernetes/client-node";
export declare interface V1ScaleStatusOptions {
    replicas: number;
    selector?: string;
}
export function createV1ScaleStatus(options: V1ScaleStatusOptions): V1ScaleStatus {
    const resource = new V1ScaleStatus();
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    return resource;
}
