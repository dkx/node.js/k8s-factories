import { V1beta1CustomResourceDefinitionStatus, V1beta1CustomResourceDefinitionNames, V1beta1CustomResourceDefinitionCondition } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceDefinitionStatusOptions {
    acceptedNames: V1beta1CustomResourceDefinitionNames;
    conditions: V1beta1CustomResourceDefinitionCondition[];
    storedVersions: string[];
}
export function createV1beta1CustomResourceDefinitionStatus(options: V1beta1CustomResourceDefinitionStatusOptions): V1beta1CustomResourceDefinitionStatus {
    const resource = new V1beta1CustomResourceDefinitionStatus();
    if (typeof options.acceptedNames !== "undefined") {
        resource.acceptedNames = options.acceptedNames;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.storedVersions !== "undefined") {
        resource.storedVersions = options.storedVersions;
    }
    return resource;
}
