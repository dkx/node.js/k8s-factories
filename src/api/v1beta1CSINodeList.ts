import { V1beta1CSINodeList, V1beta1CSINode, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1CSINodeListOptions {
    items: V1beta1CSINode[];
    metadata?: V1ListMeta;
}
export function createV1beta1CSINodeList(options: V1beta1CSINodeListOptions): V1beta1CSINodeList {
    const resource = new V1beta1CSINodeList();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "CSINodeList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
