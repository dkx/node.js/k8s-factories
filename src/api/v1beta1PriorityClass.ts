import { V1beta1PriorityClass, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1beta1PriorityClassOptions {
    description?: string;
    globalDefault?: boolean;
    metadata?: V1ObjectMeta;
    preemptionPolicy?: string;
    value: number;
}
export function createV1beta1PriorityClass(options: V1beta1PriorityClassOptions): V1beta1PriorityClass {
    const resource = new V1beta1PriorityClass();
    resource.apiVersion = "scheduling.k8s.io/v1beta1";
    resource.kind = "PriorityClass";
    if (typeof options.description !== "undefined") {
        resource.description = options.description;
    }
    if (typeof options.globalDefault !== "undefined") {
        resource.globalDefault = options.globalDefault;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.preemptionPolicy !== "undefined") {
        resource.preemptionPolicy = options.preemptionPolicy;
    }
    if (typeof options.value !== "undefined") {
        resource.value = options.value;
    }
    return resource;
}
