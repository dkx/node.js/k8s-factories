import { V1beta1CustomResourceSubresourceScale } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceSubresourceScaleOptions {
    labelSelectorPath?: string;
    specReplicasPath: string;
    statusReplicasPath: string;
}
export function createV1beta1CustomResourceSubresourceScale(options: V1beta1CustomResourceSubresourceScaleOptions): V1beta1CustomResourceSubresourceScale {
    const resource = new V1beta1CustomResourceSubresourceScale();
    if (typeof options.labelSelectorPath !== "undefined") {
        resource.labelSelectorPath = options.labelSelectorPath;
    }
    if (typeof options.specReplicasPath !== "undefined") {
        resource.specReplicasPath = options.specReplicasPath;
    }
    if (typeof options.statusReplicasPath !== "undefined") {
        resource.statusReplicasPath = options.statusReplicasPath;
    }
    return resource;
}
