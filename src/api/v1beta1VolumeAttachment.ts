import { V1beta1VolumeAttachment, V1ObjectMeta, V1beta1VolumeAttachmentSpec, V1beta1VolumeAttachmentStatus } from "@kubernetes/client-node";
export declare interface V1beta1VolumeAttachmentOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1VolumeAttachmentSpec;
    status?: V1beta1VolumeAttachmentStatus;
}
export function createV1beta1VolumeAttachment(options: V1beta1VolumeAttachmentOptions): V1beta1VolumeAttachment {
    const resource = new V1beta1VolumeAttachment();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "VolumeAttachment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
