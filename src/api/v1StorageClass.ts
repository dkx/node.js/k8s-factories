import { V1StorageClass, V1TopologySelectorTerm, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1StorageClassOptions {
    allowVolumeExpansion?: boolean;
    allowedTopologies?: V1TopologySelectorTerm[];
    metadata?: V1ObjectMeta;
    mountOptions?: string[];
    parameters?: {
        [key: string]: string;
    };
    provisioner: string;
    reclaimPolicy?: string;
    volumeBindingMode?: string;
}
export function createV1StorageClass(options: V1StorageClassOptions): V1StorageClass {
    const resource = new V1StorageClass();
    resource.apiVersion = "storage.k8s.io/v1";
    resource.kind = "StorageClass";
    if (typeof options.allowVolumeExpansion !== "undefined") {
        resource.allowVolumeExpansion = options.allowVolumeExpansion;
    }
    if (typeof options.allowedTopologies !== "undefined") {
        resource.allowedTopologies = options.allowedTopologies;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.mountOptions !== "undefined") {
        resource.mountOptions = options.mountOptions;
    }
    if (typeof options.parameters !== "undefined") {
        resource.parameters = options.parameters;
    }
    if (typeof options.provisioner !== "undefined") {
        resource.provisioner = options.provisioner;
    }
    if (typeof options.reclaimPolicy !== "undefined") {
        resource.reclaimPolicy = options.reclaimPolicy;
    }
    if (typeof options.volumeBindingMode !== "undefined") {
        resource.volumeBindingMode = options.volumeBindingMode;
    }
    return resource;
}
