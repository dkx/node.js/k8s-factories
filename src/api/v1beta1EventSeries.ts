import { V1beta1EventSeries } from "@kubernetes/client-node";
export declare interface V1beta1EventSeriesOptions {
    count: number;
    lastObservedTime: Date;
    state: string;
}
export function createV1beta1EventSeries(options: V1beta1EventSeriesOptions): V1beta1EventSeries {
    const resource = new V1beta1EventSeries();
    if (typeof options.count !== "undefined") {
        resource.count = options.count;
    }
    if (typeof options.lastObservedTime !== "undefined") {
        resource.lastObservedTime = options.lastObservedTime;
    }
    if (typeof options.state !== "undefined") {
        resource.state = options.state;
    }
    return resource;
}
