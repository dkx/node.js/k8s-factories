import { AppsV1beta1DeploymentSpec, AppsV1beta1RollbackConfig, V1LabelSelector, AppsV1beta1DeploymentStrategy, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface AppsV1beta1DeploymentSpecOptions {
    minReadySeconds?: number;
    paused?: boolean;
    progressDeadlineSeconds?: number;
    replicas?: number;
    revisionHistoryLimit?: number;
    rollbackTo?: AppsV1beta1RollbackConfig;
    selector?: V1LabelSelector;
    strategy?: AppsV1beta1DeploymentStrategy;
    template: V1PodTemplateSpec;
}
export function createAppsV1beta1DeploymentSpec(options: AppsV1beta1DeploymentSpecOptions): AppsV1beta1DeploymentSpec {
    const resource = new AppsV1beta1DeploymentSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.paused !== "undefined") {
        resource.paused = options.paused;
    }
    if (typeof options.progressDeadlineSeconds !== "undefined") {
        resource.progressDeadlineSeconds = options.progressDeadlineSeconds;
    }
    if (typeof options.replicas !== "undefined") {
        resource.replicas = options.replicas;
    }
    if (typeof options.revisionHistoryLimit !== "undefined") {
        resource.revisionHistoryLimit = options.revisionHistoryLimit;
    }
    if (typeof options.rollbackTo !== "undefined") {
        resource.rollbackTo = options.rollbackTo;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.strategy !== "undefined") {
        resource.strategy = options.strategy;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    return resource;
}
