import { V1beta1DaemonSet, V1ObjectMeta, V1beta1DaemonSetSpec, V1beta1DaemonSetStatus } from "@kubernetes/client-node";
export declare interface V1beta1DaemonSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta1DaemonSetSpec;
    status?: V1beta1DaemonSetStatus;
}
export function createV1beta1DaemonSet(options: V1beta1DaemonSetOptions = {}): V1beta1DaemonSet {
    const resource = new V1beta1DaemonSet();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "DaemonSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
