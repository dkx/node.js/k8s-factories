import { V1alpha1AuditSinkSpec, V1alpha1Policy, V1alpha1Webhook } from "@kubernetes/client-node";
export declare interface V1alpha1AuditSinkSpecOptions {
    policy: V1alpha1Policy;
    webhook: V1alpha1Webhook;
}
export function createV1alpha1AuditSinkSpec(options: V1alpha1AuditSinkSpecOptions): V1alpha1AuditSinkSpec {
    const resource = new V1alpha1AuditSinkSpec();
    if (typeof options.policy !== "undefined") {
        resource.policy = options.policy;
    }
    if (typeof options.webhook !== "undefined") {
        resource.webhook = options.webhook;
    }
    return resource;
}
