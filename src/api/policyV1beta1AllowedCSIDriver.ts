import { PolicyV1beta1AllowedCSIDriver } from "@kubernetes/client-node";
export declare interface PolicyV1beta1AllowedCSIDriverOptions {
    name: string;
}
export function createPolicyV1beta1AllowedCSIDriver(options: PolicyV1beta1AllowedCSIDriverOptions): PolicyV1beta1AllowedCSIDriver {
    const resource = new PolicyV1beta1AllowedCSIDriver();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
