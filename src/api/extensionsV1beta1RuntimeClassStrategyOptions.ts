import { ExtensionsV1beta1RuntimeClassStrategyOptions } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1RuntimeClassStrategyOptionsOptions {
    allowedRuntimeClassNames: string[];
    defaultRuntimeClassName?: string;
}
export function createExtensionsV1beta1RuntimeClassStrategyOptions(options: ExtensionsV1beta1RuntimeClassStrategyOptionsOptions): ExtensionsV1beta1RuntimeClassStrategyOptions {
    const resource = new ExtensionsV1beta1RuntimeClassStrategyOptions();
    if (typeof options.allowedRuntimeClassNames !== "undefined") {
        resource.allowedRuntimeClassNames = options.allowedRuntimeClassNames;
    }
    if (typeof options.defaultRuntimeClassName !== "undefined") {
        resource.defaultRuntimeClassName = options.defaultRuntimeClassName;
    }
    return resource;
}
