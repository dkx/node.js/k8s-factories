import { V1beta2RollingUpdateStatefulSetStrategy } from "@kubernetes/client-node";
export declare interface V1beta2RollingUpdateStatefulSetStrategyOptions {
    partition?: number;
}
export function createV1beta2RollingUpdateStatefulSetStrategy(options: V1beta2RollingUpdateStatefulSetStrategyOptions = {}): V1beta2RollingUpdateStatefulSetStrategy {
    const resource = new V1beta2RollingUpdateStatefulSetStrategy();
    if (typeof options.partition !== "undefined") {
        resource.partition = options.partition;
    }
    return resource;
}
