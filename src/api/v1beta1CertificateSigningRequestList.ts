import { V1beta1CertificateSigningRequestList, V1beta1CertificateSigningRequest, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1CertificateSigningRequestListOptions {
    items: V1beta1CertificateSigningRequest[];
    metadata?: V1ListMeta;
}
export function createV1beta1CertificateSigningRequestList(options: V1beta1CertificateSigningRequestListOptions): V1beta1CertificateSigningRequestList {
    const resource = new V1beta1CertificateSigningRequestList();
    resource.apiVersion = "certificates.k8s.io/v1beta1";
    resource.kind = "CertificateSigningRequestList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
