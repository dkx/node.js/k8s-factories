import { NetworkingV1beta1HTTPIngressPath, NetworkingV1beta1IngressBackend } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1HTTPIngressPathOptions {
    backend: NetworkingV1beta1IngressBackend;
    path?: string;
}
export function createNetworkingV1beta1HTTPIngressPath(options: NetworkingV1beta1HTTPIngressPathOptions): NetworkingV1beta1HTTPIngressPath {
    const resource = new NetworkingV1beta1HTTPIngressPath();
    if (typeof options.backend !== "undefined") {
        resource.backend = options.backend;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    return resource;
}
