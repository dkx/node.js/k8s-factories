import { V1beta1ControllerRevision, RuntimeRawExtension, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1beta1ControllerRevisionOptions {
    data?: RuntimeRawExtension;
    metadata?: V1ObjectMeta;
    revision: number;
}
export function createV1beta1ControllerRevision(options: V1beta1ControllerRevisionOptions): V1beta1ControllerRevision {
    const resource = new V1beta1ControllerRevision();
    resource.apiVersion = "apps/v1beta1";
    resource.kind = "ControllerRevision";
    if (typeof options.data !== "undefined") {
        resource.data = options.data;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.revision !== "undefined") {
        resource.revision = options.revision;
    }
    return resource;
}
