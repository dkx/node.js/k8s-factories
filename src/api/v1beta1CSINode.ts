import { V1beta1CSINode, V1ObjectMeta, V1beta1CSINodeSpec } from "@kubernetes/client-node";
export declare interface V1beta1CSINodeOptions {
    metadata?: V1ObjectMeta;
    spec: V1beta1CSINodeSpec;
}
export function createV1beta1CSINode(options: V1beta1CSINodeOptions): V1beta1CSINode {
    const resource = new V1beta1CSINode();
    resource.apiVersion = "storage.k8s.io/v1beta1";
    resource.kind = "CSINode";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
