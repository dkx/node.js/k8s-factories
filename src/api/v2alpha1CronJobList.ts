import { V2alpha1CronJobList, V2alpha1CronJob, V1ListMeta } from "@kubernetes/client-node";
export declare interface V2alpha1CronJobListOptions {
    items: V2alpha1CronJob[];
    metadata?: V1ListMeta;
}
export function createV2alpha1CronJobList(options: V2alpha1CronJobListOptions): V2alpha1CronJobList {
    const resource = new V2alpha1CronJobList();
    resource.apiVersion = "batch/v2alpha1";
    resource.kind = "CronJobList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
