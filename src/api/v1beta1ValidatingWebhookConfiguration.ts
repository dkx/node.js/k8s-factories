import { V1beta1ValidatingWebhookConfiguration, V1ObjectMeta, V1beta1ValidatingWebhook } from "@kubernetes/client-node";
export declare interface V1beta1ValidatingWebhookConfigurationOptions {
    metadata?: V1ObjectMeta;
    webhooks?: V1beta1ValidatingWebhook[];
}
export function createV1beta1ValidatingWebhookConfiguration(options: V1beta1ValidatingWebhookConfigurationOptions = {}): V1beta1ValidatingWebhookConfiguration {
    const resource = new V1beta1ValidatingWebhookConfiguration();
    resource.apiVersion = "admissionregistration.k8s.io/v1beta1";
    resource.kind = "ValidatingWebhookConfiguration";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.webhooks !== "undefined") {
        resource.webhooks = options.webhooks;
    }
    return resource;
}
