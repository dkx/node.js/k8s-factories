import { V1beta1CustomResourceDefinitionNames } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceDefinitionNamesOptions {
    categories?: string[];
    kind: string;
    listKind?: string;
    plural: string;
    shortNames?: string[];
    singular?: string;
}
export function createV1beta1CustomResourceDefinitionNames(options: V1beta1CustomResourceDefinitionNamesOptions): V1beta1CustomResourceDefinitionNames {
    const resource = new V1beta1CustomResourceDefinitionNames();
    if (typeof options.categories !== "undefined") {
        resource.categories = options.categories;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.listKind !== "undefined") {
        resource.listKind = options.listKind;
    }
    if (typeof options.plural !== "undefined") {
        resource.plural = options.plural;
    }
    if (typeof options.shortNames !== "undefined") {
        resource.shortNames = options.shortNames;
    }
    if (typeof options.singular !== "undefined") {
        resource.singular = options.singular;
    }
    return resource;
}
