import { V1beta1CertificateSigningRequestCondition } from "@kubernetes/client-node";
export declare interface V1beta1CertificateSigningRequestConditionOptions {
    lastUpdateTime?: Date;
    message?: string;
    reason?: string;
    type: string;
}
export function createV1beta1CertificateSigningRequestCondition(options: V1beta1CertificateSigningRequestConditionOptions): V1beta1CertificateSigningRequestCondition {
    const resource = new V1beta1CertificateSigningRequestCondition();
    if (typeof options.lastUpdateTime !== "undefined") {
        resource.lastUpdateTime = options.lastUpdateTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
