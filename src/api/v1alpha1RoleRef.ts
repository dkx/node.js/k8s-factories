import { V1alpha1RoleRef } from "@kubernetes/client-node";
export declare interface V1alpha1RoleRefOptions {
    apiGroup: string;
    kind: string;
    name: string;
}
export function createV1alpha1RoleRef(options: V1alpha1RoleRefOptions): V1alpha1RoleRef {
    const resource = new V1alpha1RoleRef();
    if (typeof options.apiGroup !== "undefined") {
        resource.apiGroup = options.apiGroup;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
