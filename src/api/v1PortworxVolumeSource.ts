import { V1PortworxVolumeSource } from "@kubernetes/client-node";
export declare interface V1PortworxVolumeSourceOptions {
    fsType?: string;
    readOnly?: boolean;
    volumeID: string;
}
export function createV1PortworxVolumeSource(options: V1PortworxVolumeSourceOptions): V1PortworxVolumeSource {
    const resource = new V1PortworxVolumeSource();
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.volumeID !== "undefined") {
        resource.volumeID = options.volumeID;
    }
    return resource;
}
