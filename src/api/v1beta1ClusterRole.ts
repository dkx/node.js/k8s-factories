import { V1beta1ClusterRole, V1beta1AggregationRule, V1ObjectMeta, V1beta1PolicyRule } from "@kubernetes/client-node";
export declare interface V1beta1ClusterRoleOptions {
    aggregationRule?: V1beta1AggregationRule;
    metadata?: V1ObjectMeta;
    rules?: V1beta1PolicyRule[];
}
export function createV1beta1ClusterRole(options: V1beta1ClusterRoleOptions = {}): V1beta1ClusterRole {
    const resource = new V1beta1ClusterRole();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "ClusterRole";
    if (typeof options.aggregationRule !== "undefined") {
        resource.aggregationRule = options.aggregationRule;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    return resource;
}
