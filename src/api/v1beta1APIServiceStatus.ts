import { V1beta1APIServiceStatus, V1beta1APIServiceCondition } from "@kubernetes/client-node";
export declare interface V1beta1APIServiceStatusOptions {
    conditions?: V1beta1APIServiceCondition[];
}
export function createV1beta1APIServiceStatus(options: V1beta1APIServiceStatusOptions = {}): V1beta1APIServiceStatus {
    const resource = new V1beta1APIServiceStatus();
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    return resource;
}
