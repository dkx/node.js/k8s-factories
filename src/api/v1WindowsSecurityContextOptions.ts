import { V1WindowsSecurityContextOptions } from "@kubernetes/client-node";
export declare interface V1WindowsSecurityContextOptionsOptions {
    gmsaCredentialSpec?: string;
    gmsaCredentialSpecName?: string;
}
export function createV1WindowsSecurityContextOptions(options: V1WindowsSecurityContextOptionsOptions = {}): V1WindowsSecurityContextOptions {
    const resource = new V1WindowsSecurityContextOptions();
    if (typeof options.gmsaCredentialSpec !== "undefined") {
        resource.gmsaCredentialSpec = options.gmsaCredentialSpec;
    }
    if (typeof options.gmsaCredentialSpecName !== "undefined") {
        resource.gmsaCredentialSpecName = options.gmsaCredentialSpecName;
    }
    return resource;
}
