import { V1ContainerStateRunning } from "@kubernetes/client-node";
export declare interface V1ContainerStateRunningOptions {
    startedAt?: Date;
}
export function createV1ContainerStateRunning(options: V1ContainerStateRunningOptions = {}): V1ContainerStateRunning {
    const resource = new V1ContainerStateRunning();
    if (typeof options.startedAt !== "undefined") {
        resource.startedAt = options.startedAt;
    }
    return resource;
}
