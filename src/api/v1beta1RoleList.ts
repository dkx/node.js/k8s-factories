import { V1beta1RoleList, V1beta1Role, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1RoleListOptions {
    items: V1beta1Role[];
    metadata?: V1ListMeta;
}
export function createV1beta1RoleList(options: V1beta1RoleListOptions): V1beta1RoleList {
    const resource = new V1beta1RoleList();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "RoleList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
