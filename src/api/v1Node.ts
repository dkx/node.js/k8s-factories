import { V1Node, V1ObjectMeta, V1NodeSpec, V1NodeStatus } from "@kubernetes/client-node";
export declare interface V1NodeOptions {
    metadata?: V1ObjectMeta;
    spec?: V1NodeSpec;
    status?: V1NodeStatus;
}
export function createV1Node(options: V1NodeOptions = {}): V1Node {
    const resource = new V1Node();
    resource.apiVersion = "v1";
    resource.kind = "Node";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
