import { V1beta1DaemonSetCondition } from "@kubernetes/client-node";
export declare interface V1beta1DaemonSetConditionOptions {
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV1beta1DaemonSetCondition(options: V1beta1DaemonSetConditionOptions): V1beta1DaemonSetCondition {
    const resource = new V1beta1DaemonSetCondition();
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
