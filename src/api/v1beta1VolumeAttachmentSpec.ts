import { V1beta1VolumeAttachmentSpec, V1beta1VolumeAttachmentSource } from "@kubernetes/client-node";
export declare interface V1beta1VolumeAttachmentSpecOptions {
    attacher: string;
    nodeName: string;
    source: V1beta1VolumeAttachmentSource;
}
export function createV1beta1VolumeAttachmentSpec(options: V1beta1VolumeAttachmentSpecOptions): V1beta1VolumeAttachmentSpec {
    const resource = new V1beta1VolumeAttachmentSpec();
    if (typeof options.attacher !== "undefined") {
        resource.attacher = options.attacher;
    }
    if (typeof options.nodeName !== "undefined") {
        resource.nodeName = options.nodeName;
    }
    if (typeof options.source !== "undefined") {
        resource.source = options.source;
    }
    return resource;
}
