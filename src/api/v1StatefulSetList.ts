import { V1StatefulSetList, V1StatefulSet, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1StatefulSetListOptions {
    items: V1StatefulSet[];
    metadata?: V1ListMeta;
}
export function createV1StatefulSetList(options: V1StatefulSetListOptions): V1StatefulSetList {
    const resource = new V1StatefulSetList();
    resource.apiVersion = "apps/v1";
    resource.kind = "StatefulSetList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
