import { V1beta1RoleBinding, V1ObjectMeta, V1beta1RoleRef, V1beta1Subject } from "@kubernetes/client-node";
export declare interface V1beta1RoleBindingOptions {
    metadata?: V1ObjectMeta;
    roleRef: V1beta1RoleRef;
    subjects?: V1beta1Subject[];
}
export function createV1beta1RoleBinding(options: V1beta1RoleBindingOptions): V1beta1RoleBinding {
    const resource = new V1beta1RoleBinding();
    resource.apiVersion = "rbac.authorization.k8s.io/v1beta1";
    resource.kind = "RoleBinding";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.roleRef !== "undefined") {
        resource.roleRef = options.roleRef;
    }
    if (typeof options.subjects !== "undefined") {
        resource.subjects = options.subjects;
    }
    return resource;
}
