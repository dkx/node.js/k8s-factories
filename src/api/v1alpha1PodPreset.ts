import { V1alpha1PodPreset, V1ObjectMeta, V1alpha1PodPresetSpec } from "@kubernetes/client-node";
export declare interface V1alpha1PodPresetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1alpha1PodPresetSpec;
}
export function createV1alpha1PodPreset(options: V1alpha1PodPresetOptions = {}): V1alpha1PodPreset {
    const resource = new V1alpha1PodPreset();
    resource.apiVersion = "settings.k8s.io/v1alpha1";
    resource.kind = "PodPreset";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
