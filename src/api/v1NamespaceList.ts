import { V1NamespaceList, V1Namespace, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1NamespaceListOptions {
    items: V1Namespace[];
    metadata?: V1ListMeta;
}
export function createV1NamespaceList(options: V1NamespaceListOptions): V1NamespaceList {
    const resource = new V1NamespaceList();
    resource.apiVersion = "v1";
    resource.kind = "NamespaceList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
