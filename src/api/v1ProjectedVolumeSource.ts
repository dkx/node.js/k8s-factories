import { V1ProjectedVolumeSource, V1VolumeProjection } from "@kubernetes/client-node";
export declare interface V1ProjectedVolumeSourceOptions {
    defaultMode?: number;
    sources: V1VolumeProjection[];
}
export function createV1ProjectedVolumeSource(options: V1ProjectedVolumeSourceOptions): V1ProjectedVolumeSource {
    const resource = new V1ProjectedVolumeSource();
    if (typeof options.defaultMode !== "undefined") {
        resource.defaultMode = options.defaultMode;
    }
    if (typeof options.sources !== "undefined") {
        resource.sources = options.sources;
    }
    return resource;
}
