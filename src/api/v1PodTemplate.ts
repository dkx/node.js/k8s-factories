import { V1PodTemplate, V1ObjectMeta, V1PodTemplateSpec } from "@kubernetes/client-node";
export declare interface V1PodTemplateOptions {
    metadata?: V1ObjectMeta;
    template?: V1PodTemplateSpec;
}
export function createV1PodTemplate(options: V1PodTemplateOptions = {}): V1PodTemplate {
    const resource = new V1PodTemplate();
    resource.apiVersion = "v1";
    resource.kind = "PodTemplate";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    return resource;
}
