import { ExtensionsV1beta1Deployment, V1ObjectMeta, ExtensionsV1beta1DeploymentSpec, ExtensionsV1beta1DeploymentStatus } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1DeploymentOptions {
    metadata?: V1ObjectMeta;
    spec?: ExtensionsV1beta1DeploymentSpec;
    status?: ExtensionsV1beta1DeploymentStatus;
}
export function createExtensionsV1beta1Deployment(options: ExtensionsV1beta1DeploymentOptions = {}): ExtensionsV1beta1Deployment {
    const resource = new ExtensionsV1beta1Deployment();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "Deployment";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
