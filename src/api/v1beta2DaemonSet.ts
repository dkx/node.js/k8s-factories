import { V1beta2DaemonSet, V1ObjectMeta, V1beta2DaemonSetSpec, V1beta2DaemonSetStatus } from "@kubernetes/client-node";
export declare interface V1beta2DaemonSetOptions {
    metadata?: V1ObjectMeta;
    spec?: V1beta2DaemonSetSpec;
    status?: V1beta2DaemonSetStatus;
}
export function createV1beta2DaemonSet(options: V1beta2DaemonSetOptions = {}): V1beta2DaemonSet {
    const resource = new V1beta2DaemonSet();
    resource.apiVersion = "apps/v1beta2";
    resource.kind = "DaemonSet";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
