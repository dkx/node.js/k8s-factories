import { V1ServicePort } from "@kubernetes/client-node";
export declare interface V1ServicePortOptions {
    name?: string;
    nodePort?: number;
    port: number;
    protocol?: string;
    targetPort?: object;
}
export function createV1ServicePort(options: V1ServicePortOptions): V1ServicePort {
    const resource = new V1ServicePort();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.nodePort !== "undefined") {
        resource.nodePort = options.nodePort;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    if (typeof options.protocol !== "undefined") {
        resource.protocol = options.protocol;
    }
    if (typeof options.targetPort !== "undefined") {
        resource.targetPort = options.targetPort;
    }
    return resource;
}
