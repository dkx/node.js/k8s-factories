import { V1beta1LeaseList, V1beta1Lease, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1LeaseListOptions {
    items: V1beta1Lease[];
    metadata?: V1ListMeta;
}
export function createV1beta1LeaseList(options: V1beta1LeaseListOptions): V1beta1LeaseList {
    const resource = new V1beta1LeaseList();
    resource.apiVersion = "coordination.k8s.io/v1beta1";
    resource.kind = "LeaseList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
