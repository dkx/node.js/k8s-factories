import { V1APIGroup, V1GroupVersionForDiscovery, V1ServerAddressByClientCIDR } from "@kubernetes/client-node";
export declare interface V1APIGroupOptions {
    name: string;
    preferredVersion?: V1GroupVersionForDiscovery;
    serverAddressByClientCIDRs?: V1ServerAddressByClientCIDR[];
    versions: V1GroupVersionForDiscovery[];
}
export function createV1APIGroup(options: V1APIGroupOptions): V1APIGroup {
    const resource = new V1APIGroup();
    resource.apiVersion = "v1";
    resource.kind = "APIGroup";
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.preferredVersion !== "undefined") {
        resource.preferredVersion = options.preferredVersion;
    }
    if (typeof options.serverAddressByClientCIDRs !== "undefined") {
        resource.serverAddressByClientCIDRs = options.serverAddressByClientCIDRs;
    }
    if (typeof options.versions !== "undefined") {
        resource.versions = options.versions;
    }
    return resource;
}
