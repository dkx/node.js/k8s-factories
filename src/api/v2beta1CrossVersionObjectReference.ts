import { V2beta1CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V2beta1CrossVersionObjectReferenceOptions {
    apiVersion?: string;
    kind: string;
    name: string;
}
export function createV2beta1CrossVersionObjectReference(options: V2beta1CrossVersionObjectReferenceOptions): V2beta1CrossVersionObjectReference {
    const resource = new V2beta1CrossVersionObjectReference();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
