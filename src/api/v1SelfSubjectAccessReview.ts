import { V1SelfSubjectAccessReview, V1ObjectMeta, V1SelfSubjectAccessReviewSpec, V1SubjectAccessReviewStatus } from "@kubernetes/client-node";
export declare interface V1SelfSubjectAccessReviewOptions {
    metadata?: V1ObjectMeta;
    spec: V1SelfSubjectAccessReviewSpec;
    status?: V1SubjectAccessReviewStatus;
}
export function createV1SelfSubjectAccessReview(options: V1SelfSubjectAccessReviewOptions): V1SelfSubjectAccessReview {
    const resource = new V1SelfSubjectAccessReview();
    resource.apiVersion = "authorization.k8s.io/v1";
    resource.kind = "SelfSubjectAccessReview";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
