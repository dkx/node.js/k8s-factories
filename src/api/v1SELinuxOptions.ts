import { V1SELinuxOptions } from "@kubernetes/client-node";
export declare interface V1SELinuxOptionsOptions {
    level?: string;
    role?: string;
    type?: string;
    user?: string;
}
export function createV1SELinuxOptions(options: V1SELinuxOptionsOptions = {}): V1SELinuxOptions {
    const resource = new V1SELinuxOptions();
    if (typeof options.level !== "undefined") {
        resource.level = options.level;
    }
    if (typeof options.role !== "undefined") {
        resource.role = options.role;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    if (typeof options.user !== "undefined") {
        resource.user = options.user;
    }
    return resource;
}
