import { AdmissionregistrationV1beta1ServiceReference } from "@kubernetes/client-node";
export declare interface AdmissionregistrationV1beta1ServiceReferenceOptions {
    name: string;
    namespace: string;
    path?: string;
    port?: number;
}
export function createAdmissionregistrationV1beta1ServiceReference(options: AdmissionregistrationV1beta1ServiceReferenceOptions): AdmissionregistrationV1beta1ServiceReference {
    const resource = new AdmissionregistrationV1beta1ServiceReference();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    return resource;
}
