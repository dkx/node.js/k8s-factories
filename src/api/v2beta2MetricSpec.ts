import { V2beta2MetricSpec, V2beta2ExternalMetricSource, V2beta2ObjectMetricSource, V2beta2PodsMetricSource, V2beta2ResourceMetricSource } from "@kubernetes/client-node";
export declare interface V2beta2MetricSpecOptions {
    external?: V2beta2ExternalMetricSource;
    object?: V2beta2ObjectMetricSource;
    pods?: V2beta2PodsMetricSource;
    resource?: V2beta2ResourceMetricSource;
    type: string;
}
export function createV2beta2MetricSpec(options: V2beta2MetricSpecOptions): V2beta2MetricSpec {
    const resource = new V2beta2MetricSpec();
    if (typeof options.external !== "undefined") {
        resource.external = options.external;
    }
    if (typeof options.object !== "undefined") {
        resource.object = options.object;
    }
    if (typeof options.pods !== "undefined") {
        resource.pods = options.pods;
    }
    if (typeof options.resource !== "undefined") {
        resource.resource = options.resource;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
