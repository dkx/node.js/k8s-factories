import { V1beta2DaemonSetSpec, V1LabelSelector, V1PodTemplateSpec, V1beta2DaemonSetUpdateStrategy } from "@kubernetes/client-node";
export declare interface V1beta2DaemonSetSpecOptions {
    minReadySeconds?: number;
    revisionHistoryLimit?: number;
    selector: V1LabelSelector;
    template: V1PodTemplateSpec;
    updateStrategy?: V1beta2DaemonSetUpdateStrategy;
}
export function createV1beta2DaemonSetSpec(options: V1beta2DaemonSetSpecOptions): V1beta2DaemonSetSpec {
    const resource = new V1beta2DaemonSetSpec();
    if (typeof options.minReadySeconds !== "undefined") {
        resource.minReadySeconds = options.minReadySeconds;
    }
    if (typeof options.revisionHistoryLimit !== "undefined") {
        resource.revisionHistoryLimit = options.revisionHistoryLimit;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    if (typeof options.template !== "undefined") {
        resource.template = options.template;
    }
    if (typeof options.updateStrategy !== "undefined") {
        resource.updateStrategy = options.updateStrategy;
    }
    return resource;
}
