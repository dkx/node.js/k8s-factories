import { ExtensionsV1beta1DeploymentRollback, ExtensionsV1beta1RollbackConfig } from "@kubernetes/client-node";
export declare interface ExtensionsV1beta1DeploymentRollbackOptions {
    name: string;
    rollbackTo: ExtensionsV1beta1RollbackConfig;
    updatedAnnotations?: {
        [key: string]: string;
    };
}
export function createExtensionsV1beta1DeploymentRollback(options: ExtensionsV1beta1DeploymentRollbackOptions): ExtensionsV1beta1DeploymentRollback {
    const resource = new ExtensionsV1beta1DeploymentRollback();
    resource.apiVersion = "extensions/v1beta1";
    resource.kind = "DeploymentRollback";
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.rollbackTo !== "undefined") {
        resource.rollbackTo = options.rollbackTo;
    }
    if (typeof options.updatedAnnotations !== "undefined") {
        resource.updatedAnnotations = options.updatedAnnotations;
    }
    return resource;
}
