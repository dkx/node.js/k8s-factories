import { V2beta2CrossVersionObjectReference } from "@kubernetes/client-node";
export declare interface V2beta2CrossVersionObjectReferenceOptions {
    apiVersion?: string;
    kind: string;
    name: string;
}
export function createV2beta2CrossVersionObjectReference(options: V2beta2CrossVersionObjectReferenceOptions): V2beta2CrossVersionObjectReference {
    const resource = new V2beta2CrossVersionObjectReference();
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    return resource;
}
