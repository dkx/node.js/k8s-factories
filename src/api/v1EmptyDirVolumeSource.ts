import { V1EmptyDirVolumeSource } from "@kubernetes/client-node";
export declare interface V1EmptyDirVolumeSourceOptions {
    medium?: string;
    sizeLimit?: string;
}
export function createV1EmptyDirVolumeSource(options: V1EmptyDirVolumeSourceOptions = {}): V1EmptyDirVolumeSource {
    const resource = new V1EmptyDirVolumeSource();
    if (typeof options.medium !== "undefined") {
        resource.medium = options.medium;
    }
    if (typeof options.sizeLimit !== "undefined") {
        resource.sizeLimit = options.sizeLimit;
    }
    return resource;
}
