import { V2beta1HorizontalPodAutoscalerList, V2beta1HorizontalPodAutoscaler, V1ListMeta } from "@kubernetes/client-node";
export declare interface V2beta1HorizontalPodAutoscalerListOptions {
    items: V2beta1HorizontalPodAutoscaler[];
    metadata?: V1ListMeta;
}
export function createV2beta1HorizontalPodAutoscalerList(options: V2beta1HorizontalPodAutoscalerListOptions): V2beta1HorizontalPodAutoscalerList {
    const resource = new V2beta1HorizontalPodAutoscalerList();
    resource.apiVersion = "autoscaling/v2beta1";
    resource.kind = "HorizontalPodAutoscalerList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
