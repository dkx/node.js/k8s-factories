import { V1NetworkPolicySpec, V1NetworkPolicyEgressRule, V1NetworkPolicyIngressRule, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1NetworkPolicySpecOptions {
    egress?: V1NetworkPolicyEgressRule[];
    ingress?: V1NetworkPolicyIngressRule[];
    podSelector: V1LabelSelector;
    policyTypes?: string[];
}
export function createV1NetworkPolicySpec(options: V1NetworkPolicySpecOptions): V1NetworkPolicySpec {
    const resource = new V1NetworkPolicySpec();
    if (typeof options.egress !== "undefined") {
        resource.egress = options.egress;
    }
    if (typeof options.ingress !== "undefined") {
        resource.ingress = options.ingress;
    }
    if (typeof options.podSelector !== "undefined") {
        resource.podSelector = options.podSelector;
    }
    if (typeof options.policyTypes !== "undefined") {
        resource.policyTypes = options.policyTypes;
    }
    return resource;
}
