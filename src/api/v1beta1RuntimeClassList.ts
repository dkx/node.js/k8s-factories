import { V1beta1RuntimeClassList, V1beta1RuntimeClass, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1RuntimeClassListOptions {
    items: V1beta1RuntimeClass[];
    metadata?: V1ListMeta;
}
export function createV1beta1RuntimeClassList(options: V1beta1RuntimeClassListOptions): V1beta1RuntimeClassList {
    const resource = new V1beta1RuntimeClassList();
    resource.apiVersion = "node.k8s.io/v1beta1";
    resource.kind = "RuntimeClassList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
