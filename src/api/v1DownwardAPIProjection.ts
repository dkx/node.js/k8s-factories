import { V1DownwardAPIProjection, V1DownwardAPIVolumeFile } from "@kubernetes/client-node";
export declare interface V1DownwardAPIProjectionOptions {
    items?: V1DownwardAPIVolumeFile[];
}
export function createV1DownwardAPIProjection(options: V1DownwardAPIProjectionOptions = {}): V1DownwardAPIProjection {
    const resource = new V1DownwardAPIProjection();
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    return resource;
}
