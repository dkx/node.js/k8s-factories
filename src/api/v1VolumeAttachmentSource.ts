import { V1VolumeAttachmentSource, V1PersistentVolumeSpec } from "@kubernetes/client-node";
export declare interface V1VolumeAttachmentSourceOptions {
    inlineVolumeSpec?: V1PersistentVolumeSpec;
    persistentVolumeName?: string;
}
export function createV1VolumeAttachmentSource(options: V1VolumeAttachmentSourceOptions = {}): V1VolumeAttachmentSource {
    const resource = new V1VolumeAttachmentSource();
    if (typeof options.inlineVolumeSpec !== "undefined") {
        resource.inlineVolumeSpec = options.inlineVolumeSpec;
    }
    if (typeof options.persistentVolumeName !== "undefined") {
        resource.persistentVolumeName = options.persistentVolumeName;
    }
    return resource;
}
