import { V1JobStatus, V1JobCondition } from "@kubernetes/client-node";
export declare interface V1JobStatusOptions {
    active?: number;
    completionTime?: Date;
    conditions?: V1JobCondition[];
    failed?: number;
    startTime?: Date;
    succeeded?: number;
}
export function createV1JobStatus(options: V1JobStatusOptions = {}): V1JobStatus {
    const resource = new V1JobStatus();
    if (typeof options.active !== "undefined") {
        resource.active = options.active;
    }
    if (typeof options.completionTime !== "undefined") {
        resource.completionTime = options.completionTime;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.failed !== "undefined") {
        resource.failed = options.failed;
    }
    if (typeof options.startTime !== "undefined") {
        resource.startTime = options.startTime;
    }
    if (typeof options.succeeded !== "undefined") {
        resource.succeeded = options.succeeded;
    }
    return resource;
}
