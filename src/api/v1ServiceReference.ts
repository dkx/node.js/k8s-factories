import { V1ServiceReference } from "@kubernetes/client-node";
export declare interface V1ServiceReferenceOptions {
    name?: string;
    namespace?: string;
    port?: number;
}
export function createV1ServiceReference(options: V1ServiceReferenceOptions = {}): V1ServiceReference {
    const resource = new V1ServiceReference();
    if (typeof options.name !== "undefined") {
        resource.name = options.name;
    }
    if (typeof options.namespace !== "undefined") {
        resource.namespace = options.namespace;
    }
    if (typeof options.port !== "undefined") {
        resource.port = options.port;
    }
    return resource;
}
