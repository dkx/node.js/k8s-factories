import { V1APIGroupList, V1APIGroup } from "@kubernetes/client-node";
export declare interface V1APIGroupListOptions {
    groups: V1APIGroup[];
}
export function createV1APIGroupList(options: V1APIGroupListOptions): V1APIGroupList {
    const resource = new V1APIGroupList();
    resource.apiVersion = "v1";
    resource.kind = "APIGroupList";
    if (typeof options.groups !== "undefined") {
        resource.groups = options.groups;
    }
    return resource;
}
