import { V1beta1CustomResourceDefinitionList, V1beta1CustomResourceDefinition, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1CustomResourceDefinitionListOptions {
    items: V1beta1CustomResourceDefinition[];
    metadata?: V1ListMeta;
}
export function createV1beta1CustomResourceDefinitionList(options: V1beta1CustomResourceDefinitionListOptions): V1beta1CustomResourceDefinitionList {
    const resource = new V1beta1CustomResourceDefinitionList();
    resource.apiVersion = "apiextensions.k8s.io/v1beta1";
    resource.kind = "CustomResourceDefinitionList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
