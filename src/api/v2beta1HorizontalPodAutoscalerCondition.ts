import { V2beta1HorizontalPodAutoscalerCondition } from "@kubernetes/client-node";
export declare interface V2beta1HorizontalPodAutoscalerConditionOptions {
    lastTransitionTime?: Date;
    message?: string;
    reason?: string;
    status: string;
    type: string;
}
export function createV2beta1HorizontalPodAutoscalerCondition(options: V2beta1HorizontalPodAutoscalerConditionOptions): V2beta1HorizontalPodAutoscalerCondition {
    const resource = new V2beta1HorizontalPodAutoscalerCondition();
    if (typeof options.lastTransitionTime !== "undefined") {
        resource.lastTransitionTime = options.lastTransitionTime;
    }
    if (typeof options.message !== "undefined") {
        resource.message = options.message;
    }
    if (typeof options.reason !== "undefined") {
        resource.reason = options.reason;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
