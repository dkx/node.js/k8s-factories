import { V1beta1PodDisruptionBudgetStatus } from "@kubernetes/client-node";
export declare interface V1beta1PodDisruptionBudgetStatusOptions {
    currentHealthy: number;
    desiredHealthy: number;
    disruptedPods?: {
        [key: string]: Date;
    };
    disruptionsAllowed: number;
    expectedPods: number;
    observedGeneration?: number;
}
export function createV1beta1PodDisruptionBudgetStatus(options: V1beta1PodDisruptionBudgetStatusOptions): V1beta1PodDisruptionBudgetStatus {
    const resource = new V1beta1PodDisruptionBudgetStatus();
    if (typeof options.currentHealthy !== "undefined") {
        resource.currentHealthy = options.currentHealthy;
    }
    if (typeof options.desiredHealthy !== "undefined") {
        resource.desiredHealthy = options.desiredHealthy;
    }
    if (typeof options.disruptedPods !== "undefined") {
        resource.disruptedPods = options.disruptedPods;
    }
    if (typeof options.disruptionsAllowed !== "undefined") {
        resource.disruptionsAllowed = options.disruptionsAllowed;
    }
    if (typeof options.expectedPods !== "undefined") {
        resource.expectedPods = options.expectedPods;
    }
    if (typeof options.observedGeneration !== "undefined") {
        resource.observedGeneration = options.observedGeneration;
    }
    return resource;
}
