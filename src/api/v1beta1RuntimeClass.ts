import { V1beta1RuntimeClass, V1ObjectMeta } from "@kubernetes/client-node";
export declare interface V1beta1RuntimeClassOptions {
    handler: string;
    metadata?: V1ObjectMeta;
}
export function createV1beta1RuntimeClass(options: V1beta1RuntimeClassOptions): V1beta1RuntimeClass {
    const resource = new V1beta1RuntimeClass();
    resource.apiVersion = "node.k8s.io/v1beta1";
    resource.kind = "RuntimeClass";
    if (typeof options.handler !== "undefined") {
        resource.handler = options.handler;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
