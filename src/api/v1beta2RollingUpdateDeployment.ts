import { V1beta2RollingUpdateDeployment } from "@kubernetes/client-node";
export declare interface V1beta2RollingUpdateDeploymentOptions {
    maxSurge?: object;
    maxUnavailable?: object;
}
export function createV1beta2RollingUpdateDeployment(options: V1beta2RollingUpdateDeploymentOptions = {}): V1beta2RollingUpdateDeployment {
    const resource = new V1beta2RollingUpdateDeployment();
    if (typeof options.maxSurge !== "undefined") {
        resource.maxSurge = options.maxSurge;
    }
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    return resource;
}
