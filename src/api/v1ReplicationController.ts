import { V1ReplicationController, V1ObjectMeta, V1ReplicationControllerSpec, V1ReplicationControllerStatus } from "@kubernetes/client-node";
export declare interface V1ReplicationControllerOptions {
    metadata?: V1ObjectMeta;
    spec?: V1ReplicationControllerSpec;
    status?: V1ReplicationControllerStatus;
}
export function createV1ReplicationController(options: V1ReplicationControllerOptions = {}): V1ReplicationController {
    const resource = new V1ReplicationController();
    resource.apiVersion = "v1";
    resource.kind = "ReplicationController";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
