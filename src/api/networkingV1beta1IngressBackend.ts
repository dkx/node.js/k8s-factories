import { NetworkingV1beta1IngressBackend } from "@kubernetes/client-node";
export declare interface NetworkingV1beta1IngressBackendOptions {
    serviceName: string;
    servicePort: object;
}
export function createNetworkingV1beta1IngressBackend(options: NetworkingV1beta1IngressBackendOptions): NetworkingV1beta1IngressBackend {
    const resource = new NetworkingV1beta1IngressBackend();
    if (typeof options.serviceName !== "undefined") {
        resource.serviceName = options.serviceName;
    }
    if (typeof options.servicePort !== "undefined") {
        resource.servicePort = options.servicePort;
    }
    return resource;
}
