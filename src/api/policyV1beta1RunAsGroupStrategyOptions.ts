import { PolicyV1beta1RunAsGroupStrategyOptions, PolicyV1beta1IDRange } from "@kubernetes/client-node";
export declare interface PolicyV1beta1RunAsGroupStrategyOptionsOptions {
    ranges?: PolicyV1beta1IDRange[];
    rule: string;
}
export function createPolicyV1beta1RunAsGroupStrategyOptions(options: PolicyV1beta1RunAsGroupStrategyOptionsOptions): PolicyV1beta1RunAsGroupStrategyOptions {
    const resource = new PolicyV1beta1RunAsGroupStrategyOptions();
    if (typeof options.ranges !== "undefined") {
        resource.ranges = options.ranges;
    }
    if (typeof options.rule !== "undefined") {
        resource.rule = options.rule;
    }
    return resource;
}
