import { V1AzureFilePersistentVolumeSource } from "@kubernetes/client-node";
export declare interface V1AzureFilePersistentVolumeSourceOptions {
    readOnly?: boolean;
    secretName: string;
    secretNamespace?: string;
    shareName: string;
}
export function createV1AzureFilePersistentVolumeSource(options: V1AzureFilePersistentVolumeSourceOptions): V1AzureFilePersistentVolumeSource {
    const resource = new V1AzureFilePersistentVolumeSource();
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretName !== "undefined") {
        resource.secretName = options.secretName;
    }
    if (typeof options.secretNamespace !== "undefined") {
        resource.secretNamespace = options.secretNamespace;
    }
    if (typeof options.shareName !== "undefined") {
        resource.shareName = options.shareName;
    }
    return resource;
}
