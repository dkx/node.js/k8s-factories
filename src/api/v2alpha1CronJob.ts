import { V2alpha1CronJob, V1ObjectMeta, V2alpha1CronJobSpec, V2alpha1CronJobStatus } from "@kubernetes/client-node";
export declare interface V2alpha1CronJobOptions {
    metadata?: V1ObjectMeta;
    spec?: V2alpha1CronJobSpec;
    status?: V2alpha1CronJobStatus;
}
export function createV2alpha1CronJob(options: V2alpha1CronJobOptions = {}): V2alpha1CronJob {
    const resource = new V2alpha1CronJob();
    resource.apiVersion = "batch/v2alpha1";
    resource.kind = "CronJob";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
