import { V1FlexVolumeSource, V1LocalObjectReference } from "@kubernetes/client-node";
export declare interface V1FlexVolumeSourceOptions {
    driver: string;
    fsType?: string;
    options?: {
        [key: string]: string;
    };
    readOnly?: boolean;
    secretRef?: V1LocalObjectReference;
}
export function createV1FlexVolumeSource(options: V1FlexVolumeSourceOptions): V1FlexVolumeSource {
    const resource = new V1FlexVolumeSource();
    if (typeof options.driver !== "undefined") {
        resource.driver = options.driver;
    }
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.options !== "undefined") {
        resource.options = options.options;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    if (typeof options.secretRef !== "undefined") {
        resource.secretRef = options.secretRef;
    }
    return resource;
}
