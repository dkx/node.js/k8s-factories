import { V1ResourceQuota, V1ObjectMeta, V1ResourceQuotaSpec, V1ResourceQuotaStatus } from "@kubernetes/client-node";
export declare interface V1ResourceQuotaOptions {
    metadata?: V1ObjectMeta;
    spec?: V1ResourceQuotaSpec;
    status?: V1ResourceQuotaStatus;
}
export function createV1ResourceQuota(options: V1ResourceQuotaOptions = {}): V1ResourceQuota {
    const resource = new V1ResourceQuota();
    resource.apiVersion = "v1";
    resource.kind = "ResourceQuota";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    if (typeof options.status !== "undefined") {
        resource.status = options.status;
    }
    return resource;
}
