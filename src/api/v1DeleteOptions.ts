import { V1DeleteOptions, V1Preconditions } from "@kubernetes/client-node";
export declare interface V1DeleteOptionsOptions {
    apiVersion?: "v1" | "admission.k8s.io/v1beta1" | "admissionregistration.k8s.io/v1beta1" | "apiextensions.k8s.io/v1beta1" | "apiregistration.k8s.io/v1" | "apiregistration.k8s.io/v1beta1" | "apps/v1" | "apps/v1beta1" | "apps/v1beta2" | "auditregistration.k8s.io/v1alpha1" | "authentication.k8s.io/v1" | "authentication.k8s.io/v1beta1" | "authorization.k8s.io/v1" | "authorization.k8s.io/v1beta1" | "autoscaling/v1" | "autoscaling/v2beta1" | "autoscaling/v2beta2" | "batch/v1" | "batch/v1beta1" | "batch/v2alpha1" | "certificates.k8s.io/v1beta1" | "coordination.k8s.io/v1" | "coordination.k8s.io/v1beta1" | "events.k8s.io/v1beta1" | "extensions/v1beta1" | "imagepolicy.k8s.io/v1alpha1" | "networking.k8s.io/v1" | "networking.k8s.io/v1beta1" | "node.k8s.io/v1alpha1" | "node.k8s.io/v1beta1" | "policy/v1beta1" | "rbac.authorization.k8s.io/v1" | "rbac.authorization.k8s.io/v1alpha1" | "rbac.authorization.k8s.io/v1beta1" | "scheduling.k8s.io/v1" | "scheduling.k8s.io/v1alpha1" | "scheduling.k8s.io/v1beta1" | "settings.k8s.io/v1alpha1" | "storage.k8s.io/v1" | "storage.k8s.io/v1alpha1" | "storage.k8s.io/v1beta1";
    dryRun?: string[];
    gracePeriodSeconds?: number;
    orphanDependents?: boolean;
    preconditions?: V1Preconditions;
    propagationPolicy?: string;
}
export function createV1DeleteOptions(options: V1DeleteOptionsOptions = {}): V1DeleteOptions {
    const resource = new V1DeleteOptions();
    resource.kind = "DeleteOptions";
    if (typeof options.apiVersion !== "undefined") {
        resource.apiVersion = options.apiVersion;
    }
    if (typeof options.dryRun !== "undefined") {
        resource.dryRun = options.dryRun;
    }
    if (typeof options.gracePeriodSeconds !== "undefined") {
        resource.gracePeriodSeconds = options.gracePeriodSeconds;
    }
    if (typeof options.orphanDependents !== "undefined") {
        resource.orphanDependents = options.orphanDependents;
    }
    if (typeof options.preconditions !== "undefined") {
        resource.preconditions = options.preconditions;
    }
    if (typeof options.propagationPolicy !== "undefined") {
        resource.propagationPolicy = options.propagationPolicy;
    }
    return resource;
}
