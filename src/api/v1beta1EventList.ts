import { V1beta1EventList, V1beta1Event, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1EventListOptions {
    items: V1beta1Event[];
    metadata?: V1ListMeta;
}
export function createV1beta1EventList(options: V1beta1EventListOptions): V1beta1EventList {
    const resource = new V1beta1EventList();
    resource.apiVersion = "events.k8s.io/v1beta1";
    resource.kind = "EventList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
