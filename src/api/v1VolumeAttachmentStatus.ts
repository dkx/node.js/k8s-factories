import { V1VolumeAttachmentStatus, V1VolumeError } from "@kubernetes/client-node";
export declare interface V1VolumeAttachmentStatusOptions {
    attachError?: V1VolumeError;
    attached: boolean;
    attachmentMetadata?: {
        [key: string]: string;
    };
    detachError?: V1VolumeError;
}
export function createV1VolumeAttachmentStatus(options: V1VolumeAttachmentStatusOptions): V1VolumeAttachmentStatus {
    const resource = new V1VolumeAttachmentStatus();
    if (typeof options.attachError !== "undefined") {
        resource.attachError = options.attachError;
    }
    if (typeof options.attached !== "undefined") {
        resource.attached = options.attached;
    }
    if (typeof options.attachmentMetadata !== "undefined") {
        resource.attachmentMetadata = options.attachmentMetadata;
    }
    if (typeof options.detachError !== "undefined") {
        resource.detachError = options.detachError;
    }
    return resource;
}
