import { V1WeightedPodAffinityTerm, V1PodAffinityTerm } from "@kubernetes/client-node";
export declare interface V1WeightedPodAffinityTermOptions {
    podAffinityTerm: V1PodAffinityTerm;
    weight: number;
}
export function createV1WeightedPodAffinityTerm(options: V1WeightedPodAffinityTermOptions): V1WeightedPodAffinityTerm {
    const resource = new V1WeightedPodAffinityTerm();
    if (typeof options.podAffinityTerm !== "undefined") {
        resource.podAffinityTerm = options.podAffinityTerm;
    }
    if (typeof options.weight !== "undefined") {
        resource.weight = options.weight;
    }
    return resource;
}
