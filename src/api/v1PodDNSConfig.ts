import { V1PodDNSConfig, V1PodDNSConfigOption } from "@kubernetes/client-node";
export declare interface V1PodDNSConfigOptions {
    nameservers?: string[];
    options?: V1PodDNSConfigOption[];
    searches?: string[];
}
export function createV1PodDNSConfig(options: V1PodDNSConfigOptions = {}): V1PodDNSConfig {
    const resource = new V1PodDNSConfig();
    if (typeof options.nameservers !== "undefined") {
        resource.nameservers = options.nameservers;
    }
    if (typeof options.options !== "undefined") {
        resource.options = options.options;
    }
    if (typeof options.searches !== "undefined") {
        resource.searches = options.searches;
    }
    return resource;
}
