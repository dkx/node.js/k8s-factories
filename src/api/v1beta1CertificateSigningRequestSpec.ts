import { V1beta1CertificateSigningRequestSpec } from "@kubernetes/client-node";
export declare interface V1beta1CertificateSigningRequestSpecOptions {
    extra?: {
        [key: string]: string[];
    };
    groups?: string[];
    request: string;
    uid?: string;
    usages?: string[];
    username?: string;
}
export function createV1beta1CertificateSigningRequestSpec(options: V1beta1CertificateSigningRequestSpecOptions): V1beta1CertificateSigningRequestSpec {
    const resource = new V1beta1CertificateSigningRequestSpec();
    if (typeof options.extra !== "undefined") {
        resource.extra = options.extra;
    }
    if (typeof options.groups !== "undefined") {
        resource.groups = options.groups;
    }
    if (typeof options.request !== "undefined") {
        resource.request = options.request;
    }
    if (typeof options.uid !== "undefined") {
        resource.uid = options.uid;
    }
    if (typeof options.usages !== "undefined") {
        resource.usages = options.usages;
    }
    if (typeof options.username !== "undefined") {
        resource.username = options.username;
    }
    return resource;
}
