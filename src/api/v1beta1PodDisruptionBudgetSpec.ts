import { V1beta1PodDisruptionBudgetSpec, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1beta1PodDisruptionBudgetSpecOptions {
    maxUnavailable?: object;
    minAvailable?: object;
    selector?: V1LabelSelector;
}
export function createV1beta1PodDisruptionBudgetSpec(options: V1beta1PodDisruptionBudgetSpecOptions = {}): V1beta1PodDisruptionBudgetSpec {
    const resource = new V1beta1PodDisruptionBudgetSpec();
    if (typeof options.maxUnavailable !== "undefined") {
        resource.maxUnavailable = options.maxUnavailable;
    }
    if (typeof options.minAvailable !== "undefined") {
        resource.minAvailable = options.minAvailable;
    }
    if (typeof options.selector !== "undefined") {
        resource.selector = options.selector;
    }
    return resource;
}
