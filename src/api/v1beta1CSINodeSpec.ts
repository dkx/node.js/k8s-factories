import { V1beta1CSINodeSpec, V1beta1CSINodeDriver } from "@kubernetes/client-node";
export declare interface V1beta1CSINodeSpecOptions {
    drivers: V1beta1CSINodeDriver[];
}
export function createV1beta1CSINodeSpec(options: V1beta1CSINodeSpecOptions): V1beta1CSINodeSpec {
    const resource = new V1beta1CSINodeSpec();
    if (typeof options.drivers !== "undefined") {
        resource.drivers = options.drivers;
    }
    return resource;
}
