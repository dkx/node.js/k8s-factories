import { V1SelfSubjectAccessReviewSpec, V1NonResourceAttributes, V1ResourceAttributes } from "@kubernetes/client-node";
export declare interface V1SelfSubjectAccessReviewSpecOptions {
    nonResourceAttributes?: V1NonResourceAttributes;
    resourceAttributes?: V1ResourceAttributes;
}
export function createV1SelfSubjectAccessReviewSpec(options: V1SelfSubjectAccessReviewSpecOptions = {}): V1SelfSubjectAccessReviewSpec {
    const resource = new V1SelfSubjectAccessReviewSpec();
    if (typeof options.nonResourceAttributes !== "undefined") {
        resource.nonResourceAttributes = options.nonResourceAttributes;
    }
    if (typeof options.resourceAttributes !== "undefined") {
        resource.resourceAttributes = options.resourceAttributes;
    }
    return resource;
}
