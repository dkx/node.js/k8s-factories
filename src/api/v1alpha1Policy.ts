import { V1alpha1Policy } from "@kubernetes/client-node";
export declare interface V1alpha1PolicyOptions {
    level: string;
    stages?: string[];
}
export function createV1alpha1Policy(options: V1alpha1PolicyOptions): V1alpha1Policy {
    const resource = new V1alpha1Policy();
    if (typeof options.level !== "undefined") {
        resource.level = options.level;
    }
    if (typeof options.stages !== "undefined") {
        resource.stages = options.stages;
    }
    return resource;
}
