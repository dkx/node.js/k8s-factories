import { V1PersistentVolumeClaimStatus, V1PersistentVolumeClaimCondition } from "@kubernetes/client-node";
export declare interface V1PersistentVolumeClaimStatusOptions {
    accessModes?: string[];
    capacity?: {
        [key: string]: string;
    };
    conditions?: V1PersistentVolumeClaimCondition[];
    phase?: string;
}
export function createV1PersistentVolumeClaimStatus(options: V1PersistentVolumeClaimStatusOptions = {}): V1PersistentVolumeClaimStatus {
    const resource = new V1PersistentVolumeClaimStatus();
    if (typeof options.accessModes !== "undefined") {
        resource.accessModes = options.accessModes;
    }
    if (typeof options.capacity !== "undefined") {
        resource.capacity = options.capacity;
    }
    if (typeof options.conditions !== "undefined") {
        resource.conditions = options.conditions;
    }
    if (typeof options.phase !== "undefined") {
        resource.phase = options.phase;
    }
    return resource;
}
