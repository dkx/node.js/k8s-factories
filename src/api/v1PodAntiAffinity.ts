import { V1PodAntiAffinity, V1WeightedPodAffinityTerm, V1PodAffinityTerm } from "@kubernetes/client-node";
export declare interface V1PodAntiAffinityOptions {
    preferredDuringSchedulingIgnoredDuringExecution?: V1WeightedPodAffinityTerm[];
    requiredDuringSchedulingIgnoredDuringExecution?: V1PodAffinityTerm[];
}
export function createV1PodAntiAffinity(options: V1PodAntiAffinityOptions = {}): V1PodAntiAffinity {
    const resource = new V1PodAntiAffinity();
    if (typeof options.preferredDuringSchedulingIgnoredDuringExecution !== "undefined") {
        resource.preferredDuringSchedulingIgnoredDuringExecution = options.preferredDuringSchedulingIgnoredDuringExecution;
    }
    if (typeof options.requiredDuringSchedulingIgnoredDuringExecution !== "undefined") {
        resource.requiredDuringSchedulingIgnoredDuringExecution = options.requiredDuringSchedulingIgnoredDuringExecution;
    }
    return resource;
}
