import { V1Role, V1ObjectMeta, V1PolicyRule } from "@kubernetes/client-node";
export declare interface V1RoleOptions {
    metadata?: V1ObjectMeta;
    rules?: V1PolicyRule[];
}
export function createV1Role(options: V1RoleOptions = {}): V1Role {
    const resource = new V1Role();
    resource.apiVersion = "rbac.authorization.k8s.io/v1";
    resource.kind = "Role";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.rules !== "undefined") {
        resource.rules = options.rules;
    }
    return resource;
}
