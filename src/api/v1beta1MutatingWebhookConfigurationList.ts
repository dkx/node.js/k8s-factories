import { V1beta1MutatingWebhookConfigurationList, V1beta1MutatingWebhookConfiguration, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1MutatingWebhookConfigurationListOptions {
    items: V1beta1MutatingWebhookConfiguration[];
    metadata?: V1ListMeta;
}
export function createV1beta1MutatingWebhookConfigurationList(options: V1beta1MutatingWebhookConfigurationListOptions): V1beta1MutatingWebhookConfigurationList {
    const resource = new V1beta1MutatingWebhookConfigurationList();
    resource.apiVersion = "admissionregistration.k8s.io/v1beta1";
    resource.kind = "MutatingWebhookConfigurationList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
