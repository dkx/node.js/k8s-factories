import { V1NetworkPolicyList, V1NetworkPolicy, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1NetworkPolicyListOptions {
    items: V1NetworkPolicy[];
    metadata?: V1ListMeta;
}
export function createV1NetworkPolicyList(options: V1NetworkPolicyListOptions): V1NetworkPolicyList {
    const resource = new V1NetworkPolicyList();
    resource.apiVersion = "networking.k8s.io/v1";
    resource.kind = "NetworkPolicyList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
