import { V1GlusterfsVolumeSource } from "@kubernetes/client-node";
export declare interface V1GlusterfsVolumeSourceOptions {
    endpoints: string;
    path: string;
    readOnly?: boolean;
}
export function createV1GlusterfsVolumeSource(options: V1GlusterfsVolumeSourceOptions): V1GlusterfsVolumeSource {
    const resource = new V1GlusterfsVolumeSource();
    if (typeof options.endpoints !== "undefined") {
        resource.endpoints = options.endpoints;
    }
    if (typeof options.path !== "undefined") {
        resource.path = options.path;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
