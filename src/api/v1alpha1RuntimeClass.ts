import { V1alpha1RuntimeClass, V1ObjectMeta, V1alpha1RuntimeClassSpec } from "@kubernetes/client-node";
export declare interface V1alpha1RuntimeClassOptions {
    metadata?: V1ObjectMeta;
    spec: V1alpha1RuntimeClassSpec;
}
export function createV1alpha1RuntimeClass(options: V1alpha1RuntimeClassOptions): V1alpha1RuntimeClass {
    const resource = new V1alpha1RuntimeClass();
    resource.apiVersion = "node.k8s.io/v1alpha1";
    resource.kind = "RuntimeClass";
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    if (typeof options.spec !== "undefined") {
        resource.spec = options.spec;
    }
    return resource;
}
