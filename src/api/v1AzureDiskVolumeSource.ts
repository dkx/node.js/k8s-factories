import { V1AzureDiskVolumeSource } from "@kubernetes/client-node";
export declare interface V1AzureDiskVolumeSourceOptions {
    cachingMode?: string;
    diskName: string;
    diskURI: string;
    fsType?: string;
    kind?: string;
    readOnly?: boolean;
}
export function createV1AzureDiskVolumeSource(options: V1AzureDiskVolumeSourceOptions): V1AzureDiskVolumeSource {
    const resource = new V1AzureDiskVolumeSource();
    if (typeof options.cachingMode !== "undefined") {
        resource.cachingMode = options.cachingMode;
    }
    if (typeof options.diskName !== "undefined") {
        resource.diskName = options.diskName;
    }
    if (typeof options.diskURI !== "undefined") {
        resource.diskURI = options.diskURI;
    }
    if (typeof options.fsType !== "undefined") {
        resource.fsType = options.fsType;
    }
    if (typeof options.kind !== "undefined") {
        resource.kind = options.kind;
    }
    if (typeof options.readOnly !== "undefined") {
        resource.readOnly = options.readOnly;
    }
    return resource;
}
