import { V1beta1CronJobList, V1beta1CronJob, V1ListMeta } from "@kubernetes/client-node";
export declare interface V1beta1CronJobListOptions {
    items: V1beta1CronJob[];
    metadata?: V1ListMeta;
}
export function createV1beta1CronJobList(options: V1beta1CronJobListOptions): V1beta1CronJobList {
    const resource = new V1beta1CronJobList();
    resource.apiVersion = "batch/v1beta1";
    resource.kind = "CronJobList";
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.metadata !== "undefined") {
        resource.metadata = options.metadata;
    }
    return resource;
}
