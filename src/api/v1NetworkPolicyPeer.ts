import { V1NetworkPolicyPeer, V1IPBlock, V1LabelSelector } from "@kubernetes/client-node";
export declare interface V1NetworkPolicyPeerOptions {
    ipBlock?: V1IPBlock;
    namespaceSelector?: V1LabelSelector;
    podSelector?: V1LabelSelector;
}
export function createV1NetworkPolicyPeer(options: V1NetworkPolicyPeerOptions = {}): V1NetworkPolicyPeer {
    const resource = new V1NetworkPolicyPeer();
    if (typeof options.ipBlock !== "undefined") {
        resource.ipBlock = options.ipBlock;
    }
    if (typeof options.namespaceSelector !== "undefined") {
        resource.namespaceSelector = options.namespaceSelector;
    }
    if (typeof options.podSelector !== "undefined") {
        resource.podSelector = options.podSelector;
    }
    return resource;
}
