import { V1SecretVolumeSource, V1KeyToPath } from "@kubernetes/client-node";
export declare interface V1SecretVolumeSourceOptions {
    defaultMode?: number;
    items?: V1KeyToPath[];
    optional?: boolean;
    secretName?: string;
}
export function createV1SecretVolumeSource(options: V1SecretVolumeSourceOptions = {}): V1SecretVolumeSource {
    const resource = new V1SecretVolumeSource();
    if (typeof options.defaultMode !== "undefined") {
        resource.defaultMode = options.defaultMode;
    }
    if (typeof options.items !== "undefined") {
        resource.items = options.items;
    }
    if (typeof options.optional !== "undefined") {
        resource.optional = options.optional;
    }
    if (typeof options.secretName !== "undefined") {
        resource.secretName = options.secretName;
    }
    return resource;
}
