import { V2beta1MetricStatus, V2beta1ExternalMetricStatus, V2beta1ObjectMetricStatus, V2beta1PodsMetricStatus, V2beta1ResourceMetricStatus } from "@kubernetes/client-node";
export declare interface V2beta1MetricStatusOptions {
    external?: V2beta1ExternalMetricStatus;
    object?: V2beta1ObjectMetricStatus;
    pods?: V2beta1PodsMetricStatus;
    resource?: V2beta1ResourceMetricStatus;
    type: string;
}
export function createV2beta1MetricStatus(options: V2beta1MetricStatusOptions): V2beta1MetricStatus {
    const resource = new V2beta1MetricStatus();
    if (typeof options.external !== "undefined") {
        resource.external = options.external;
    }
    if (typeof options.object !== "undefined") {
        resource.object = options.object;
    }
    if (typeof options.pods !== "undefined") {
        resource.pods = options.pods;
    }
    if (typeof options.resource !== "undefined") {
        resource.resource = options.resource;
    }
    if (typeof options.type !== "undefined") {
        resource.type = options.type;
    }
    return resource;
}
