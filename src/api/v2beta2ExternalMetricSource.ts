import { V2beta2ExternalMetricSource, V2beta2MetricIdentifier, V2beta2MetricTarget } from "@kubernetes/client-node";
export declare interface V2beta2ExternalMetricSourceOptions {
    metric: V2beta2MetricIdentifier;
    target: V2beta2MetricTarget;
}
export function createV2beta2ExternalMetricSource(options: V2beta2ExternalMetricSourceOptions): V2beta2ExternalMetricSource {
    const resource = new V2beta2ExternalMetricSource();
    if (typeof options.metric !== "undefined") {
        resource.metric = options.metric;
    }
    if (typeof options.target !== "undefined") {
        resource.target = options.target;
    }
    return resource;
}
