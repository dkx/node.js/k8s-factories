# DKX/K8S Factories

Factories for kubernetes resources

## Installation

```bash
$ npm install --save @dkx/k8s-factories
```

## Usage

```typescript
import {AppsV1Api, KubeConfig} from '@kubernetes/client-node';
import {createV1Deployment, createV1ObjectMeta, createV1DeploymentSpec} from '@dkx/k8s-factories';

async function run(): Promise<void>
{
    const config = new KubeConfig();
    config.loadFromDefault();

    const client = config.makeApiClient(AppsV1Api);

    await client.createNamespacedDeployment('default', createV1Deployment({
        metadata: createV1ObjectMeta({
            name: 'my-app',
        }),
        spec: createV1DeploymentSpec({
            // todo
        }),
    }));
}
```
